<?php

namespace App;

use Illuminate\Support\Facades\Lang;

	class Language extends BaseModel
	{
	           
        
        /**
    	 * The database table used by the Ardent.
    	 *
    	 * @var string
    	 */
    	protected $table = 'languages';


        // rules
        public static $rules = [
        ];


    	/**
    	 * The attributes that are mass assignable.
    	 *
    	 * @var array
    	 */
    	protected $fillable = [
    		'name',
		    'iso',
		    'translate',
		    'app'
	    ];


        //Filter by this fields
        public $filters = [
            'limit'         => -1,
            'page'          => 0,
            'order'         => 'asc',
            'orderby'       => 'name',
            'groupby'       => '',
            'where'         => []
        ];

        public static function getJson($status){
            return json_encode(Lang::get($status));
        }
        

	}
