<?php

namespace App;

class Tax extends BaseModel
{


	protected $table = 'taxes';


    public static $rules = [
        'name'                  => 'required',
//	    'type'                  => 'required'
    ];



	protected $fillable = [
		'user_id',
		'name',
		'type',
		'account_number',
		'value',
		'default',
		'status',
	];


}
