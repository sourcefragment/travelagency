<?php

namespace App;


class ZoneStop extends BaseModel
{
    //
	protected $table = 'zone_stops';

	protected $fillable = [
		'zone_id',
		'stop_id',
		'order'
	];

	public static $rules = [
		'zone_id'           => 'required',
		'stop_id'           => 'required'
	];

	public static $belongs = ['stop'];

	function stop(){
		return $this->belongsTo('App\Stop','stop_id','id');
	}

	function zone(){
		return $this->belongsTo('App\Zone','zone_id','id');
	}

}
