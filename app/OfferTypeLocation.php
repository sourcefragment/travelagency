<?php

namespace App;


class OfferTypeLocation extends BaseModel
{
    //
	protected $table = 'offer_type_locations';
	protected $fillable = [
		'offer_type_id',
		'location_id'
	];

	public static $rules = [
		'offer_type_id'     => 'required',
		'location_id'       => 'required'
	];
}
