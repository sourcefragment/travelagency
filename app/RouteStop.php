<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RouteStop extends BaseModel
{
    //
	protected $table = 'route_stops';
	protected $fillable = [
		'order',
		'route_id',
		'status',
		'stop_id',
		'zone_id',
	];

	public static $rules = [
		'route_id'  => 'required',
		'stop_id'   => 'required'
	];

	public $filters = [
		'limit'         => 10,
		'page'          => 0,
		'order'         => 'asc',
		'orderby'       => 'order',
		'groupby'       => '',
		'where'         => [],
		'style'         => 'details'
	];

	public static $belongs = ['stop'];

	public function stop(){
		return $this->belongsTo('App\Stop','stop_id','id');
	}

	/**Triggered before creation
	*/
	public function beforeCreate(){

		if( !$this->order ){
			$this->order = RouteStop::where('route_id',$this->route_id)
					->where('zone_id',$this->zone_id)
				->count()+1;
		}
	}
}
