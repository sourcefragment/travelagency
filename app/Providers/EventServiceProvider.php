<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],
	    'App\Events\BookingPending' => [
	    	'App\Listeners\BookingPendingListener'
	    ],
	    'App\Events\BookingConfirmed' => [
			'App\Listeners\BookingConfirmedListener'
	    ],
	    'App\Events\BookingRejected'    => [
	    	'App\Listeners\BookingRejectedListener'
	    ],
	    'App\Events\BookingCancelled' => [
	    	'App\Listeners\BookingCancelledListener'
	    ],
	    'App\Events\BookingUpdated' => [
	    	'App\Listeners\BookingUpdatedListener'
	    ],
	    'App\Events\ClientRegistered'   => [
	    	'App\Listeners\ClientRegisteredListener'
	    ],
	    'App\Events\PaymentConfirmed'   => [
	    	'App\Listeners\PaymentConfirmedListener'
	    ],
	    'App\Events\PaymentCancelled'   => [
	    	'App\Listeners\PaymentCancelledListener'
	    ],
	    'App\Events\PaymentRejected'    => [
	    	'App\Listeners\PaymentRejectedListener'
	    ],
	    'App\Events\NotificationCreated'    => [
	    	'App\Listeners\NotificationCreatedListener'
	    ],
	    'App\Events\InvoiceCancelled'   => [
	    	'App\Listeners\InvoiceCancelledListener'
	    ],
	    'App\Events\InvoiceClosed'  => [
	    	'App\Listeners\InvoiceClosedListener'
	    ],

    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
