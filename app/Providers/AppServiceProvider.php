<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Register custom validators


	    //Validate NIF
	    Validator::extend('nif',function ($attribute,$value,$parameters,$validator){

	    });

	    //Validate CIF
	    Validator::extend('cif',function ($attribute,$value,$parameters,$validator){


	    });

	    //Validate NIE
	    Validator::extend('nie',function ($attribute,$value,$parameters,$validator){


	    });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
