<?php

namespace App\Providers;

use App\Agent;
use App\Attachment;
use App\Booking;
use App\BookingExtra;
use App\BookingItem;
use App\BookingMeta;
use App\Category;
use App\City;
use App\Client;
use App\Company;
use App\Country;
use App\Currency;
use App\Discount;
use App\Event;
use App\Invoice;
use App\InvoiceItem;
use App\Item;
use App\Language;
use App\Location;
use App\Notification;
use App\Offer;
use App\OfferAvailable;
use App\OfferContent;
use App\OfferExtra;
use App\OfferExtraAvailable;
use App\OfferExtraContent;
use App\OfferLanguage;
use App\OfferMeta;
use App\Permission;
use App\Policies\BookingPolicy;
use App\Policies\RolePolicy;
use App\Role;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
//        'App\Model' => 'App\Policies\ModelPolicy',
	    Role::class => RolePolicy::class,
	    Permission::class => Permission::class
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        //
    }
}
