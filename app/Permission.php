<?php

namespace App;

class Permission extends BaseModel
{


    protected $table = 'permissions';


    public static $rules = [
	    'role_id'       => 'required',
        'section'       => 'required'
    ];



    protected $fillable = [
        'role_id',
        'section',
        'c',
        'r',
        'u',
        'd',

    ];

	public $filters = [
		'limit'         => 10,
		'page'          => 0,
		'order'         => 'desc',
		'orderby'       => 'id',
		'groupby'       => '',
		'where'         => [],
		'style'         => 'details'
	];

	public static $belongs = ['sections'];

	/**Protect permissions before update
	*/
	public function beforeUpdate(){


	}


	public function sections(){
		return $this->belongsTo('App\Section','section','slug');
	}

}
