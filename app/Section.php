<?php
/**
 * Created by PhpStorm.
 * User: mario
 * Date: 9/22/16
 * Time: 11:06 AM
 */

namespace App;


use Illuminate\Database\Eloquent\ModelNotFoundException;

class Section extends BaseModel
{

	protected $table = 'sections';

	protected $fillable = [
		'name',
		'slug',
		'menu_order',
		'parent_id',
		'menu_visible',
		'autoload'
	];

	public static $rules = [];

	public static $belongs = [];

	public $filters = [
		'limit'         => 100,
		'page'          => 0,
		'order'         => 'asc',
		'orderby'       => 'parent_id,menu_order',
		'groupby'       => '',
		'where'         => []
	];


	public function children(){
		return $this->hasMany('App\Section','parent_id');
	}

	public function beforeCreate(){

		$this->slug = $this->getSlug($this->name);
	}

	/**Set section parent by id
	 * @param  $slug string, parent slug
	*/
	public function setParentBySlug( $slug ){

		if(empty($slug))
			return false;

		try{
			$parent = Section::where('slug',$slug)
				->firstOrFail();

			$this->parent_id = $parent->id;
			$this->save();

		}catch (\PDOException $e){

		}
		return true;

	}



}