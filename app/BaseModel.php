<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use LaravelArdent\Ardent\Ardent;

class BaseModel extends Ardent
{

    protected $table = 'base_model';
    //Enable soft deletes
    use SoftDeletes;

     protected $dates =
	    [
	        'created_at',
	        'updated_at',
	        'deleted_at',
	    ];

    public $autoHydrateEntityFromInput = true;
    public $forceEntityHydrationFromInput = true;
    public $autoPurgeRedundantAttributes = true;

	public static $rules = [];

    public static $throwOnFind = true;
    protected $softDelete = true;

	//Filter by this fields
	public $filters = [
		'limit'         => 10,
		'page'          => 0,
		'order'         => 'desc',
		'orderby'       => 'id',
		'groupby'       => '',
		'where'         => [],
		'style'         => 'details'
	];

    //comma separated fields to return
    public $fields = '';


    //Pivot tables
    public static $belongs = [];

    public function getTableName()
    {
        return $this->table;
    }

    /**Get active model's items scope
    */
    public function scopeActive( $query )
    {
        return $query;
    }

	/**Get entities that belong to the user
	 */

	public function scopeOwn( $query ){
		return $query;
	}


	/**Create slug
	 */
	protected function getSlug( $text ){

		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);

		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);

		// trim
		$text = trim($text, '-');

		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);

		// lowercase
		$text = strtolower($text);

		if (empty($text)) {
			return 'n-a';
		}

		return $text;

	}

	/**Get relations for multi table search
	*/
	public function getRelations(){
		return static::$belongs;
	}

}