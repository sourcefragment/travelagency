<?php

namespace App;


class Location extends BaseModel
{
    //
	protected $table = 'locations';

	protected $fillable = [
		'user_id',
		'parent_id',
		'city_id',
		'name',
		'slug',
		'order',
		'featured',
		'status',
	];

	public $filters = [
		'limit'         => 10,
		'page'          => 0,
		'order'         => 'asc',
		'orderby'       => 'name',
		'groupby'       => '',
		'where'         => [],
		'style'         => 'details'
	];


	public static $rules = [
		'name'         => 'required|min:3'
	];

	public static $belongs = ['city'];


	/**Return city relationship
	*/
	public function city(){
		return $this->belongsTo('App\City','city_id','id');
	}


	/**
	*/
	public function beforeCreate(){

		$this->slug = $this->getSlug($this->name);

		if( !$this->user_id ){
			$this->user_id = auth()->user()->id;
		}

	}
}
