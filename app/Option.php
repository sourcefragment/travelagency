<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends BaseModel
{

	//Allowed option types
	private $types = [
		'booking-status',
		'client-type',
		'contract-type',
		'group-type',
		'invoice-status',
		'invoice-type',
		'revision-type',
		'treatment',
		'value-type',
		'vehicle-status-type',
		'insurance-type',
		'expense-type',
		'maintenance-type',
	];


	protected $table = 'options';

	protected $fillable = [
		'user_id',
		'name',
		'key',
		'value',
		'type',
		'prefix',
		'color',
		'system',
		'status'
	];

	public static $rules = [
		'name'              => 'required|min:3',
		'type'              => 'required'
	];


	/**Protect option before delete
	 */
	public function beforeDelete(){

		if( $this->system ){
			return false;
		}
		return true;
	}

	//Filter by this fields
	public $filters = [
		'limit'         => 10,
		'page'          => 0,
		'order'         => 'asc',
		'orderby'       => 'name',
	];

	/**On before create
	 */
	public function beforeCreate(){

		if( !$this->key ){
			$this->key = strtolower( str_replace(' ','-',$this->name));
		}
	}

	/**
	 */
	public function beforeSave(){

		$this->prefix = strtoupper($this->prefix);
	}


	public function scopeActive($query)
	{
		return $query->where('status',1);
	}


    /**Get option by name
     * @param string $key, the option slug
     * @param string $type, the option type
     */
    public static function getByKey( $key,$type ){

        $option = Option::where('type',$type)
            ->where('key',$key)
            ->first();

        if( $option ){
            return $option->id;
        }
        return false;
    }

}
