<?php

    namespace App;

	class Country extends BaseModel
	{
    	protected $table = 'countries';



        // rules
        public static $rules =
            [
				'sortname'          => 'required',
	            'name'              => 'required'
            ];



        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable =
            [
                'sortname',
                'name',
            ];

        /**
         * The attributes excluded from the Ardent's JSON form.
         *
         * @var array
         */
        protected $hidden =
            [
            ];

        /**
         * The attributes are casting
         * @var array
         */
        protected $casts =
            [
            ];


        //Filter by this fields
        public $filters = [
            'limit'         => -1,
            'page'          => 0,
            'order'         => 'asc',
            'orderby'       => 'name',
            'groupby'       => '',
            'where'         => []
        ];


        public function cities()
        {
            return $this->hasManyThrough(
                'App\City',
                'App\State',
                'country_id',
                'state_id',
                'id')->orderby('name','asc');
        }



	}
