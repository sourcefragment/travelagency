<?php

namespace App;


class Template extends BaseModel
{
	protected $table = 'templates';


	protected $fillable = [
		'content',
		'margin_bottom',
		'margin_footer',
		'margin_header',
		'margin_left',
		'margin_right',
		'margin_top',
		'module',
		'name',
		'slug',
		'status',
		'user_id',
	];

	public static $rules = [
		'name'          => 'required|min:3',
		'module'        => 'required'
	];

	public static $types = [
		''
	];


	public function beforeCreate(){


		if( !$this->user_id && auth()->user()->id )
			$this->user_id = auth()->user()->id;

		$this->slug = $this->getSlug($this->name);

	}




}
