<?php

namespace App;

use Mockery\Exception;

class Item extends BaseModel
{
    //
    protected $table = 'items';
    protected $fillable = [
        'user_id',
        'name',
        'description',
        'slug',
        'single',
        'billable',
        'status',
        'system'
    ];

    public static $rules = [
        'name'      => 'required'
    ];


    public function beforeCreate(){

        if( !$this->user_id ){
            $this->user_id = auth()->user()->id;
        }

        $this->slug = $this->getSlug($this->name);
    }

    public function beforeDelete(){

    	if( $this->system ){
    		throw new Exception('Not available');
	    }


    }

    public function beforeUpdate(){

    }

}
