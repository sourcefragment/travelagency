<?php

    namespace App;

	class BookingMeta extends BaseModel
	{
	           
        
        /**
    	 * The database table used by the Ardent.
    	 *
    	 * @var string
    	 */
    	protected $table = 'booking_meta';


        
        // rules
        public static $rules = 
        [
            "name"    => "required|min:3|max:30",
            "surname" => "required|min:3|max:30",
            "email"   => "required|unique:users|Between:3,64|Email",
            "phone"   => "Required|numeric|min:9",
            "password"=> "Required",
            "address" => "Required|Between:3,64",
        ];



    	/**
    	 * The attributes that are mass assignable.
    	 *
    	 * @var array
    	 */
    	protected $fillable = [
            ];

    	/**
    	 * The attributes excluded from the Ardent's JSON form.
    	 *
    	 * @var array
    	 */
    	protected $hidden = [
            ];
            
        /**
         * The attributes are casting
         * @var array
         */
        protected $casts = [
            ];


        public function bookings()
        {
            return $this->belongsTo('Booking');
        }    
        

	}
