<?php

namespace App;


class InvoiceItem extends BaseModel
{
	protected $table = 'invoice_items';

	protected $fillable = [
		'invoice_id',
		'currency_id',
		'tax_id',
		'name',
		'description',
		'number',
		'quantity',
		'cost_price',
		'unit_price',
		'total_price',
		'tax_price',
		'list_price',
		'discount',
		'discount_type'
	];

	public static $rules = [

	];



}
