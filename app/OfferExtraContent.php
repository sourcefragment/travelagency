<?php

    namespace App;

	class OfferExtraContent extends BaseModel
	{
	           
        
        /**
    	 * The database table used by the Ardent.
    	 *
    	 * @var string
    	 */
    	protected $table = 'offer_extra_content';


        
        // rules
        public static $rules = [

            ];



    	/**
    	 * The attributes that are mass assignable.
    	 *
    	 * @var array
    	 */
    	protected $fillable = [
            ];

    	/**
    	 * The attributes excluded from the Ardent's JSON form.
    	 *
    	 * @var array
    	 */
    	protected $hidden = [
            ];
            
        /**
         * The attributes are casting
         * @var array
         */
        protected $casts = [
        
        ];
        

	}
