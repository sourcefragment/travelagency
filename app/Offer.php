<?php

namespace App;

use App\Http\Controllers\Core;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Offer extends BaseModel
{

    protected $table = 'offers';
	protected $appends = [
		'total_bookings',
		'adult_price_value',
		'adult_net_price_value',
		'children_price_value',
		'children_net_price_value',
		'senior_price_value',
		'senior_net_price_value',
	];

	protected $fillable = [
		'book_in_advance_days',
		'book_in_advance_hours',
		'confirmation_required',
		'contact_name',
		'contact_phone',
		'featured',
		'lunch_included',
		'max_baby_age',
		'max_children_age',
		'max_duration_time',
		'min_children_age',
		'min_duration_time',
		'name',
		'offer_type_id',
		'require_passenger_details',
		'senior_age',
		'slug',
		'status',
		'tour_operator_id',
		'user_id',
		'visa_required',
	];


    public static $rules = [
        'tour_operator_id'          => 'required',
        'offer_type_id'             => 'required',
        'name'                      => 'required|min:3',
    ];

	public static $belongs = ['type','extras','tour_operator','prices'];


	public function users(){
        return $this->belongsTo('App\User');
    }

    public function extras(){
        return $this->hasMany('App\OfferExtra','offer_id','id');
    }

    public function routes(){
        return $this->hasMany('App\Route','offer_id','id');
    }

    public function prices(){
		return $this->hasMany('App\OfferPrice','offer_id','id');
    }

    public function type(){
        return $this->belongsTo('App\OfferType','offer_type_id');
    }

    public function getTotalBookingsAttribute(){
        return Booking::where('offer_id',$this->id)->count();
    }

    /**Get tour operator relationship
    */
    public function tour_operator(){
        return $this->belongsTo('App\TourOperator','tour_operator_id','id');
    }


    /**
    */
    public function getAdultPriceValueAttribute(){
		return $this->getPrice('adult',false,true);
    }

    public function getChildrenPriceValueAttribute(){
		return $this->getPrice('children',false,true);
    }

    public function getSeniorPriceValueAttribute(){
	   return $this->getPrice('senior',false,true);
    }

    public function getAdultNetPriceValueAttribute(){
	    return $this->getPrice('adult',true,true);
    }

    public function getChildrenNetPriceValueAttribute(){
	    return $this->getPrice('children',true,true);
    }

    public function getSeniorNetPriceValueAttribute(){
	    return $this->getPrice('senior',true,true);
    }

    /**Get Offer price by type
    */
    private function getPrice( $type,$net=true,$value=false ){

    	foreach ( $this->prices as $price ){

    		if( $price->type == $type ){
			    $p = $net ? $price->net_price : $price->pvp_price;
				return $value ? Core::formatPrice($p) : $p;
		    }
	    }
	    return 0;
    }




}
