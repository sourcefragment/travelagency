<?php

namespace App;

class State extends BaseModel
{


    protected $table = 'states';

    public static $rules =[
        'name'                  => 'required',
	    'country_id'            => 'required'
    ];


    protected $fillable = [
        'name',
        'country_id',
    ];




}
