<?php

namespace App;

use App\Http\Controllers\Core;

class OfferType extends BaseModel
{


    protected $table = 'offer_types';
    protected $appends = [
    	'offers',
	    'price_value'
    ];

	protected $fillable = [
		'city_id',
		'code',
		'commission',
		'description',
		'duration',
		'featured',
		'name',
		'order',
		'price',
		'rating',
		'slug',
		'status',
		'user_id',
	];

    public static $rules = [
        'name'                     => 'required|min:3',
    ];


	public static $belongs = ['attachments'];

	public function attachments(){
		return $this->hasMany('App\Attachment','item_id','id');
	}


	/**Before model creation
	*/
	public function beforeCreate(){

		$this->slug = $this->getSlug($this->name);
		$this->code = strtoupper(uniqid());

		//Assign creator
		if( !$this->user_id ){
			$this->user_id = auth()->user()->id;
		}
	}

	/**Count total offers
	*/
	public function getOffersAttribute(){

		$offers = Offer::where('offer_type_id',$this->id)
			->count();
		return $offers;
	}

	public function getPriceValueAttribute(){
		return Core::formatPrice($this->price);
	}



}
