<?php

namespace App;

class Route extends BaseModel
{

    protected $appends = ['zones'];
    protected $table = 'routes';
    public static $rules = [
    	'offer_id'          => 'required',
        'name'              => 'required|min:3'
    ];

    protected $fillable = [
	    'city_id',
	    'departure_time',
	    'description',
	    'from',
	    'location_id',
	    'name',
	    'status',
	    'to',
	    'user_id',
        'offer_id',
    ];


    public static $belongs = ['offer','location'];


    /**Return related offer
    */
    public function offer(){
        return $this->belongsTo('App\Offer','offer_id','id');
    }

    /**Return related location
    */
    public function location(){
        return $this->belongsTo('App\Location','location_id','id');
    }

	/**Auto assign user on creation
	*/
	public function beforeCreate(){

		$this->slug = $this->getSlug($this->name);

		if( !$this->user_id ){
			$this->user_id = auth()->user()->id;
		}

		if( !$this->departure_time ){
			$this->departure_time = date('H:i:s',time());
		}

	}

	/**Get custom stops attribute
	*/
	public function getZonesAttribute(){
	    $zones = RouteZone::where('route_id',$this->id)
            ->get();
	    return $zones->count();
    }


}
