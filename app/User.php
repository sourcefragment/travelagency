<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class User extends BaseModel implements AuthenticatableContract, CanResetPasswordContract
{

    use Authenticatable, CanResetPassword;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';


    //hash password
    public static $passwordAttributes  = ['password'];
    public $autoHashPasswordAttributes = true;

    // rules
    public static $rules =
    [
        'email'                         => 'required|unique:users|email',
        'password'                      => 'required|confirmed|min:8',
        'password_confirmation'         => 'required|min:8',
        'role_id'                       => 'required'
    ];

    public static $dateFormats = [
		'y/m/d',
	    'y-m-d',
	    'Y/m/d',
	    'Y-m-d',
	    'd F Y',
	    'Y d F',
	    'D d F Y'
    ];

    public static $timeFormats = [
        'h:i a',
	    'H:i:s'
    ];



    public $routes = [];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'billing_name',
		'language_id',
	    'currency',
	    'date_format',
	    'location',
	    'start_of_week',
	    'time_format',
	    'timezone',
	    'zipcode',
        'address',
        'api_key',
        'billing_address',
        'billing_cif',
        'billing_city_id',
        'billing_country_id',
        'billing_tel',
        'city_id',
        'company_id',
        'dni',
        'email',
        'last_login',
        'lastname',
        'mobile',
        'name',
        'password',
        'password_confirmation',
        'phone',
        'role_id',
        'status',
        'user_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
            'password',
            'remember_token'
        ];



    /**Define relationship
     */
    public static $belongs = ['role','company','city','language'];


    /**Get user role
    */
    public function role(){
        return $this->belongsTo('App\Role','role_id','id');
    }


    /**Get user company
    */
    public function company(){
        return $this->belongsTo('App\Company','company_id','id');
    }


   /**Get city
   */
   public function city(){
        return $this->belongsTo('App\City','city_id','id');
   }


   /**Get language ref
   */
   public function language(){
   	    return $this->belongsTo('App\Language','language_id','id');
   }

    /**Return is the user is active, status = 1
        */
    public function scopeIsActive( $query ){
        return $query->whereStatus(1);
    }


    /**Check if a user has a specific role
     * @param string $role, the role name to check
     * @return boolean
    */
    public function hasRole( $role ){

    }

    /**Check if a user can perform certain actions
     * @param
    */
    public function can( $action ){

    }

    /**Check if user can be deleted before delete
    */
    public function beforeDelete(){

        //Protect self delete
        if( $this->id == Auth::User()->id )
            return false;


        //Protect master user delete
        if( $this->id == 1 )
            return false;

    }


    /**Before update
    */
    public function beforeUpdate(){

        //Protect empty password
        if( empty( $this->password ) ){
	        unset( $this->password );
        }


        //Protect role update
        if( $this->id == auth()->user()->id ){
			$this->role_id = auth()->user()->role_id;
        }

		return true;
    }


    /**Get language locale
     */
    public function getLocale(){

	    $userLang = $this->language_id;

	    if( empty($userLang) ){
	    	return env('APP_LOCALE');
	    }

	    $lang = Language::find($userLang);

	    if( $lang->exists ){

		    //Check if translation folder exist
		    if( File::exists(resource_path('lang/'.$lang->iso )) ){
			    return $lang->iso;
		    }else{
			    return env('APP_LOCALE');
		    }
	    }else{
		    return env('APP_LOCALE');
	    }
    }


    /**Permissions
    */
    public function permissions(){

    	$permissions = Permission::where('role_id',$this->role_id);
    	return $permissions;

    }

    /**Check if user has permissions for a specific module
     * @param $module string, module name lowercase
     * @param $action string, action name
     * @return boolean
    */
    public function hasPermission($module,$action){

    	$permission = Permission::where('role_id',$this->role_id)
		    ->where('section',$module)
		    ->first();


    	if( !$permission ){
    		return false;
	    }

	    switch ($action ){

		    case 'create':
		    	return $permission->c === 1;
		        break;

		    case 'view':
		    	return $permission->r === 1;
		    	break;

		    case 'update':
		    	return $permission->u === 1;
		    	break;

		    case 'delete':
		    	return $permission->d === 1;
		    	break;

		    default: return false;

	    }
    }


    /**Get timezones
    */
    public static function getTimezones(){
		return \DateTimeZone::listIdentifiers();
    }


    /**Check if user is super admin
    */
    public function isSuperAdmin(){

    	return false;
    	return $this->id === 1;

    }

}
