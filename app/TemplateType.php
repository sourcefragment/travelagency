<?php

namespace App;


class TemplateType extends BaseModel
{
    //
	protected $table = 'template_types';
	protected $fillable = [
		'name',
		'slug'
	];


	/**Before creating template
	*/
	public function beforeCreate(){

		$this->slug = $this->getSlug($this->name);
	}
}
