<?php

namespace App;


use App\Http\Controllers\Core;

class OfferPrice extends BaseModel
{
    protected $table = 'offer_prices';

    protected $appends = [
        'net_price_value',
	    'pvp_price_value',
	    'discount_price_value',
    ];
	public static $rules = [
		'offer_id'              => 'required',
		'type'                  => 'required',
		'net_price'             => 'required',
		'pvp_price'             => 'required'
	];

	public $fillable = [
		'discount',
		'discount_price',
		'net_price',
		'offer_id',
		'pvp_price',
		'type',
	];


	public function getNetPriceValueAttribute(){
		return Core::formatPrice($this->net_price);
	}

	public function getPvpPriceValueAttribute(){
		return Core::formatPrice($this->pvp_price);
	}

	public function getDiscountPriceValueAttribute(){
		return Core::formatPrice($this->discount_price);
	}




}
