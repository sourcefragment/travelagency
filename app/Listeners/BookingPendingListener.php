<?php

namespace App\Listeners;

use App\Events\BookingPending;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookingPendingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookingPending  $event
     * @return void
     */
    public function handle(BookingPending $event)
    {
        //
    }
}
