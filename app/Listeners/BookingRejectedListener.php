<?php

namespace App\Listeners;

use App\Events\BookingRejected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookingRejectedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookingRejected  $event
     * @return void
     */
    public function handle(BookingRejected $event)
    {
        //
    }
}
