<?php

namespace App\Listeners;

use App\Events\InvoiceClosed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvoiceClosedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  InvoiceClosed  $event
     * @return void
     */
    public function handle(InvoiceClosed $event)
    {
        //
    }
}
