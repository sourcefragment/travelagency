<?php

namespace App\Listeners;

use App\Events\BookingUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookingUpdatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookingUpdated  $event
     * @return void
     */
    public function handle(BookingUpdated $event)
    {
        //
    }
}
