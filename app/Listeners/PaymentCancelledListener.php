<?php

namespace App\Listeners;

use App\Events\PaymentCancelled;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaymentCancelledListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PaymentCancelled  $event
     * @return void
     */
    public function handle(PaymentCancelled $event)
    {
        //
    }
}
