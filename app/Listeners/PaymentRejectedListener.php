<?php

namespace App\Listeners;

use App\Events\PaymentRejected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaymentRejectedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PaymentRejected  $event
     * @return void
     */
    public function handle(PaymentRejected $event)
    {
        //
    }
}
