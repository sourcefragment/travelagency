<?php

namespace App\Listeners;

use App\Events\InvoiceCancelled;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvoiceCancelledListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  InvoiceCancelled  $event
     * @return void
     */
    public function handle(InvoiceCancelled $event)
    {
        //
    }
}
