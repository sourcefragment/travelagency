<?php

namespace App;

class Event extends BaseModel
{
    //
    protected $table = 'events';
    protected $appends = [
    	'start_date_value',
	    'end_date_value'
    ];

    protected $fillable = [
	    'name',
	    'repeat_mode',
        'end_date',
        'endless',
        'friday',
        'item_id',
        'module',
        'monday',
        'repeat_each',
        'saturday',
        'start_date',
        'status',
        'sunday',
        'thursday',
        'tuesday',
        'wednesday',
    ];

    public static $rules = [
        'item_id'       => 'required',
        'module'        => 'required',
	    'name'          => 'required',
	    'start_date'    => 'required|date',
	    'repeat_mode'   => 'required',
    ];


    public function beforeSave(){

    }


    /**======================
     * Field Accessors
     * ======================
    */
    public function getStartDateValueAttribute( $value ){
    	return date('d-m-Y',strtotime($this->start_date));
    }

    public function getEndDateValueAttribute( $value ){
    	return date('d-m-Y',strtotime($this->end_date));
    }




}
