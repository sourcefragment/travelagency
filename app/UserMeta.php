<?php

namespace App;
use Illuminate\Support\Facades\Auth;

class UserMeta extends BaseModel
{


    /**
     * The database table used by the Ardent.
     *
     * @var string
     */
    protected $table = 'user_meta';

    // rules
    public static $rules =
        [
            'meta_name'   => 'required',
            'meta_value' => 'required',
        ];



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'meta_key',
        'meta_value',
        'meta_name'
        ];

    /**
     * The attributes excluded from the Ardent's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        ];

    /**
     * The attributes are casting
     * @var array
     */
    protected $casts = [

    ];

	/**
	*/
	public function beforeCreate(){

		//Auto assign user
		if( !$this->user_id ){
			$this->user_id = auth()->user()->id;
		}

		//Sanitize slug
		$this->meta_key = strtolower(str_replace(' ','-',$this->meta_name));
		$this->meta_key = str_replace('_','-',$this->meta_key);

	}




}
