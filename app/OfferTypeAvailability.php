<?php

namespace App;


class OfferTypeAvailability extends BaseModel
{
    //
    protected $table = 'offer_type_availability';
    protected $fillable = [
        'offer_type_id',
        'date'
    ];

    public static $rules = [
        'offer_type_id'     => 'required',
        'date'              => 'required|date'
    ];

}
