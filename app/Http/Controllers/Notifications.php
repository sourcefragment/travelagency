<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class Notifications extends BaseController
{
    protected  $model = 'Notification';

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {

        $model = new $this->model;
        $request->request->add(['sender_id' => auth()->user()->id ]);
        $model->save();


        if( $model->save() ){

            return response()->json( $request->all(),201 );

        }else{

            $response = array(
                'errors'        => $model->errors(),
                'fields'        => $request->all(),
                'code'          => 500,
            );

            return response()->json( $response,500 );

        }
    }

}
