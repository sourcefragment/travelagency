<?php

namespace App\Http\Controllers;

use App\Currency;
use App\Language;
use App\Section;
use App\Permission;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class Index extends BaseController
{
    /**
     * @return view
     */
    public function index(  )
    {


	    //Only logged in users with sessions can access the app
		if( !auth()->user() ){
			return;
		}

		$user = auth()->user();


		try{
			$language = Language::find($user->language_id);
			App::setLocale($language->iso);
		}catch (ModelNotFoundException $e ){
			App::setLocale( env('APP_LOCALE'));

		}


	    $allowedSections = [];
	    $permissions = Permission::select(DB::raw('*'))
	        ->where('role_id',$user->role_id)
			->where('r',1)
		    ->get();

		foreach ($permissions as $permission ){
			$allowedSections[] = $permission->section;
		}

	    $sections = Section::with([
	    	'children' => function($query){
	    	    $query->orderBy('menu_order','asc');
	    }])
		    ->where('parent_id',0)
		    ->whereIn('slug',$allowedSections)
		    ->orderBy('menu_order','asc')
		    ->get();



	    $modules = $this->getModules();
		$angularModules = array_keys($modules);
		array_walk($angularModules,function(&$item){
			$item = 'app.'.$item;
		});

	    $data = [
            'user'              => $user,
            'lang'              => Language::getJson('global'),
		    'sections'          => $sections,
		    'modules'           => $modules,
		    'const'             => json_encode( Config::get('constants')),
		    'angularModules'    => json_encode($angularModules),
		    'settings'          => json_encode($this->getSettings())
        ];

        return view('index', $data);
    }

    /**Module loader
    */
    private function checkModule(){

    }

    /**Get angular modules
    */
    private function getModules(){

    	$files = File::directories(public_path('app/js/modules'));
	    $modules = [];

	    //For each directory in modules directory
	    foreach ( $files as $file ){

	    	$name = basename($file);


		    //Check if module loader exists
	    	if( File::exists( $file.DIRECTORY_SEPARATOR.$name.'.module.js') ){

			    //Add module loader
			    $jsFiles = [];
			    $jsFiles[] = $name.'.module.js';

			    //Get directories
			    $directories = File::directories($file);

			    //Do for each directory
			    foreach ($directories as $directory ){

				    $moduleFiles = File::allFiles($directory);
					//For each file in dir
				    foreach ( $moduleFiles as $f ){

					    $extension = File::extension($f);
					    if( $extension == 'js'){
						    $fname = $f->getFilename();
						    $jsFiles[] = basename($directory).DIRECTORY_SEPARATOR.$fname;
					    }
				    }
			    }


			    $modules[$name] = $jsFiles;


		    }
	    }
	    return $modules;
    }


    public function getSettings(){

    	$data = [
    		'timezones'     => \DateTimeZone::listIdentifiers(),
		    'currencies'    => Currency::get(),
		    'dateFormats'   => User::$dateFormats,
		    'timeFormats'   => User::$timeFormats
	    ];
    	return $data;
    }


    /**Return available user modules
    */
    public function getAvailableModules(){

    	$modules = Section::orderBy('name','asc')
		    ->get();

    	return response()->json($modules);

    }


    /**Get module fields
    */
    public function getModuleFields( $module ){

    	try{
    		$model = new $module;
    		return response()->json($model);
	    }catch (\Exception $e ){
    		return response()->json([
    			'error'     => $e->getCode(),
			    'msg'       => $e->getMessage()
		    ]);
	    }
    }


}
