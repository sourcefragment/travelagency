<?php

namespace App\Http\Controllers;



class PaymentMethods extends BaseController
{
    protected  $model = 'PaymentMethod';


	/**Check that the payment method is not protected before deleting
	*/
	public function beforeDelete(){

		if( $this->private )
			return false;

	}

}
