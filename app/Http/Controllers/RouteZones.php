<?php

namespace App\Http\Controllers;


use App\RouteStop;
use App\RouteZone;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;

class RouteZones extends BaseController
{
    //
    protected $model = 'RouteZone';



    /**Delete attached zone to route and its stops
     * This method will delete all stops atached to the route as well
     * @param $id, zone id reference
     * @return Response
    */
    public function destroy($id, $force = true){


	    try{

		    $routeZone = RouteZone::find($id);

		    //Re order zones
		    $routeZones = RouteZone::where('order','>',$routeZone->order)
			    ->where('route_id',$routeZone->route_id)
		        ->get();

		    foreach ( $routeZones as $rz ){
		    	$rz->order = --$rz->order;
		    	$rz->save();
		    }

		    //Delete stops from Route that belong to zone
		    $stops = RouteStop::where('zone_id',$routeZone->zone_id)
			    ->forceDelete();

		    $routeZone->forceDelete();


		    return response()->json([
		    	'route_zone'    => $routeZone,
			    'stops'         => $stops,
			    'zone_id'       => $id
		    ], 200 );


	    }catch( ModelNotFoundException $e ){

	    	$response = array(
			    'error'     => $e->getMessage(),
			    'model'     => $e->getModel(),
			    'code'      => $e->getCode(),
		    );
		    return response()->json( $response, 404 );

	    }catch( \PDOException $e ){

	    	return response()->json([
			    'error'             => $e->getMessage(),
			    'code'              => $e->getCode()
		    ],500);
	    }


    }


    /**Toggle route zone status
    */
    public function toggle( $id ){

    	$routeZone = RouteZone::find($id);
    	$routeZone->status = !$routeZone->status;
    	$routeZone->save();

    	return response()->json($routeZone);
    }
}
