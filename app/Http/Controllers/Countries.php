<?php

namespace App\Http\Controllers;


use App\Country;

class Countries extends BaseController
{
    protected  $model = 'Country';


    public function getCities( $id ){
        $country = Country::find( $id );
        $cities = $country->cities;
        return response()->json($cities);

    }
}
