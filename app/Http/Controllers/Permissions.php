<?php

namespace App\Http\Controllers;


class Permissions extends BaseController
{
    protected  $model = 'Permission';


	/**
	 * Display a listing of the resource.
	 * @return \Illuminate\Http\Response
	 */
	public function getAll( $id )
	{

		$model = new $this->model;
		$permissions = $model::with('sections')
			->where('role_id',$id )
			->get();

		if( $permissions ){

			return response()->json( $permissions ,200 );

		}else{
			return response()->json( [
				'error' => 'not_found'
			] ,404 );
		}


	}


}
