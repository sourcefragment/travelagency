<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;;
use App\Role;
use App\Permission;
use App\Section;

use Illuminate\Support\Facades\Input;

class Roles extends BaseController
{
    protected  $model = 'Role';

    public function getPermissions($id){
        $role = Role::find( $id );
        $permissions = $role->permissions;
        return response()->json($permissions);
    }




    /**Delete roles and permissions
     *
    */
    public function destroy($id, $force = true)
    {

        $role = Role::find( $id );

        if( !$role->exists ){
            return response()->json([
                'error'         => 'not_found',
                'role_id'       => $id
            ],404);
        }

        if( $role->private ){
            return response()->json([
                'error'     => 'private_role_delete_error'
            ],403);
        }

        //Get all permissions under role
        Permission::where('role_id',$id)
            ->forceDelete();

        return parent::destroy($id,$force);

    }



}
