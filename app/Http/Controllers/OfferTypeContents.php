<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class OfferTypeContents extends BaseController
{
    protected  $model = 'OfferTypeContent';



	/**
	 * Display a listing of the resource.
	 * @return \Illuminate\Http\Response
	 */
	public function getAll( $id )
	{

		$model = new $this->model;
		$belongs = $model::$belongs;
		$params = parent::getParams();

		try{

			if( $params['limit'] == -1 ){
				$results = parent::search( $params )
					->with( $belongs )
					->where('offer_type_id',$id)
					->get();

			}else{
				$results = parent::search( $params )
					->with( $belongs )
					->where('offer_type_id',$id)
					->paginate( $params['limit'] );
			}

		}catch( \Exception $e ){

			if( env( 'APP_DEBUG',false) ){
				return response()->json( [
					'msg'       => $e->getMessage(),
					'line'      => $e->getLine(),
					'file'      => $e->getFile(),
//	                    'trace'     => $e->getTraceAsString()
				],500);
			}else{
				return response()->json(array(
					'server error' => 'Query error'
				),500);
			}
		}

		return response()->json( $results ,200 );

	}


}
