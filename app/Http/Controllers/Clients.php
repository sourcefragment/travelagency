<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Http\Response;

class Clients extends BaseController
{
    //
	protected $model = 'Client';


	/**Return client registerd addresses
	 * @param $id int
	 * @return Response
	*/
	function getAddresses( $id ){

		$addresses = Address::where('client_id',$id)
			->get();

		return response()->json($addresses);
	}

}
