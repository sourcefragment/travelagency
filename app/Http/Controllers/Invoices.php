<?php

namespace App\Http\Controllers;



use App\Invoice;
use App\InvoiceItem;
use Illuminate\Http\Request;

class Invoices extends BaseController
{
    protected  $model = 'Invoice';

	/**Get next valid invoice number before creating invoice
	 * @param int $type, invoice type id reference
	 */
	public function validateInvoice( $type ){


		$number = Invoice::getNumber($type);

		if( $number ){
			return response()->json([
				'number'    => $number
			],200);
		}

		return response()->json([
			'errors' => [
				'type'      => ['not_found']
			]
		],400);


	}


	/**Create invoice
	*/
	public function store(Request $request)
	{
		try{

			$model = new $this->model;
			$model->save();

			if( count($model->errors()) ){

				return response()->json( [
					'errors'    => $model->errors()
				],500 );
			}

			//Add items if present
			$items = $request->get('items');
			foreach ($items as $item ){

				$new = InvoiceItem::create([
					'invoice_id'            => $model->id,
					'item_id'               => $item['id'],
					'tax_id'                => $model->tax_id,
					'quantity'              => $item['quantity'],
					'price'                 => $item['price'],
					'price_type'            => $item['price_type'],
					'discount'              => $item['discount'],
					'discount_value'        => $item['discount_value']
				]);
			}

			return response()->json( $model,201 );



		}catch ( \PDOException $e ){

			$msg = $e->getMessage();
			$code = $e->getCode();
			$errors = new \stdClass();

			if( $code == 23000 ){
				$errors->record = 'duplicated_field';
			}

			$response = array(
				'msg'           => $msg,
				'errors'        => $errors,
				'code'          => $code,
			);

			return response()->json( $response,500 );

		}catch( \Exception $e ){

			$msg = $e->getMessage();
			$code = $e->getCode();
			$errors = new \stdClass();

			if( $code == 23000 ){
				$errors->record = 'duplicated_field';
			}

			$response = array(
				'msg'           => $msg,
				'errors'        => $errors,
				'code'          => $code,
			);

			return response()->json( $response,500 );

		}
	}

}
