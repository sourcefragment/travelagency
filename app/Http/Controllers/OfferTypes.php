<?php

namespace App\Http\Controllers;


use App\Category;
use App\Location;
use function foo\func;
use Illuminate\Http\Request;
use App\Attachment;
use App\Event;
use App\Offer;
use App\OfferTypeAvailability;
use App\OfferTypeCategory;
use App\OfferTypeContent;
use App\OfferTypeLocation;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class OfferTypes extends BaseController
{
    protected  $model = 'OfferType';




	/**Get offer type media attachments
	*/
	public function getMedia( $id ){

		$files = Attachment::where('module','offer-type')
			->where('item_id',$id)
			->get();


		if( $files ){
			return response()->json($files,200);
		}else{
			return response()->json([
				'error'         => 'not_found',
				'msg'           => 'media_not_found',
				'code'          => 404
			],404);
		}


	}


	/**Get content
	*/
	public function getContent( $id ){

		$content = OfferTypeContent::with('language')
			->where('offer_type_id',$id)
			->get();
//			->toSql();

		return response()->json($content);
	}


	/**Get unavailable dates for offer type
	*/
	public function getDates( $id ){

        $dates = Event::select('*')
            ->where('item_id',$id)
            ->get();

        return response()->json($dates);
    }


    /**Get offers
     * @param int $id, offer type id
     * @return response
    */
    public function getOffers( $id ){

        $offers = Offer::where('offer_type_id',$id)
            ->get();

        return response()->json($offers);
    }


    /**Add category to location
     * @param $id int, offer type id
     * @param Request
    */
    public function addCategory( $id,Request $request ){

		$categoryId = $request->get('category_id');

		try{
			$query = OfferTypeCategory::create([
				'offer_type_id'     => $id,
				'category_id'       => $categoryId
			]);
			return response()->json($query);

		}catch(\PDOException $e ){
			return response()->json($e,500);
		}


    }

    /**Remove category
    */
    public function deleteCategory( $id,Request $request ){

    	try{

    		$categoryId = $request->get('category_id');
    		$query = OfferTypeCategory::where('offer_type_id',$id )
			    ->where('category_id',$categoryId)
			    ->firstOrFail()
			    ->forceDelete();
    		return response()->json($query);

	    }catch( \PDOException $e ){
    		return response()->json($e,500);
	    }

    }



    /**Add location
     * @param $id int, offer type id reference
     * @param $request Request
     * @return Response
    */
    public function addLocation( $id,Request $request ){

    	$locationId = $request->get('location_id');

    	try{
    		$query = OfferTypeLocation::create([
    			'offer_type_id'     => $id,
			    'location_id'       => $locationId
		    ]);
    		return response()->json($query);
	    }catch( \PDOException $e ){
    		return response()->json($e,500);
	    }

    }


    /**Delete location
     * @param  $id int, offer type location id reference
    */
    public function deleteLocation( $id,Request $request ){

    	try{

    		$locationId = $request->get('location_id');

    		$query = OfferTypeLocation::where('offer_type_id',$id)
			    ->where('location_id',$locationId)
			    ->firstOrFail()
			    ->forceDelete();

    		return response()->json($query);
	    }catch( \PDOException $e ){
    		return response()->json($e,500);
	    }
    }


    /**Get locations
    */
    public function getLocations( $id ){

    	$locations = OfferTypeLocation::where('offer_type_id',$id)
		    ->get();
    	/*$locations = Location::select(DB::raw('locations.*,offer_type_locations.id as lid'))
	        ->leftJoin('offer_type_locations',function ($join){
    		$join->on('locations.id','=','offer_type_locations.location_id');
	    })
		    ->where(function ($where)use($id){
		    	$where->whereNull('offer_type_locations.location_id');
		    	$where->orWhere('offer_type_locations.offer_type_id',$id);
		    })
		    ->orderBy('locations.name','ASC')
		    ->get();*/

    	return response()->json($locations);
    }


    /**Get categories
    */
    public function getCategories( $id ){

    	$categories = OfferTypeCategory::where('offer_type_id',$id)
		    ->get();
    	/*$categories = Category::select(DB::raw('categories.*,offer_type_categories.id as cid'))
	        ->leftJoin('offer_type_categories',function ($join)use($id){
    		$join->on('categories.id','=','offer_type_categories.category_id');

	    })
		    ->where(function ($where)use($id){
			    $where->whereNull('offer_type_categories.offer_type_id');
			    $where->orWhere('offer_type_categories.offer_type_id',$id);
		    })
		    ->orderby('categories.name','ASC')
		    ->get();*/
    	return response()->json($categories);
    }
}
