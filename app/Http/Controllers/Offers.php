<?php

namespace App\Http\Controllers;

use App\Event;
use App\Offer;
use App\OfferExtra;
use App\OfferLanguage;
use App\OfferPrice;
use App\OfferVisa;
use App\Route;
use App\RouteAvailability;
use App\Stop;
use GuzzleHttp\Psr7\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Offers extends BaseController
{
    protected  $model = 'Offer';

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {

        $model = new $this->model;
        $request->request->add(['user_id' => auth()->user()->id ]);
        $model->save();


        if( $model->save() ){

            return response()->json( $request->all(),201 );

        }else{

            $response = array(
                'errors'        => $model->errors(),
                'fields'        => $request->all(),
                'code'          => 500,
            );

            return response()->json( $response,500 );

        }
    }


    /**Load offer routes
     * @param int $id, route id
    */
    public function getRoutes( $id ){
        $routes = Route::with('location')
            ->where('offer_id',$id)
            ->get();
        return response()->json($routes);
    }

    /**Get offer available extras
    */
    public function getExtras( $id ){
    	$extras = OfferExtra::select('*')
		    ->where('offer_id',$id)
		    ->get();
	    return response()->json($extras);
    }


    /**Get offer required visas
    */
    public function getVisas( $id ){
    	$visas = OfferVisa::select('*')
		    ->where('offer_id',$id)
		    ->get();

	    return response()->json($visas);
    }


    /**GEt offer available languages
    */
    public function getLanguages( $id ){
    	$langs = OfferLanguage::select('*')
		    ->where('offer_id',$id)
		    ->get();
	    return response()->json($langs);
    }


    /**Add array of country ids to visas
     * delete all that are not in array
    */
    public function addCountries( Request $request ){

        $countries = $request->get('countries');
        $offerId = $request->get('offer_id');
        $required = $request->get('required');

        //Delete all countries not in array
        OfferVisa::where('offer_id',$offerId)
            ->where('required',$required)
            ->forceDelete();


        foreach ($countries as $country ) {

            OfferVisa::create([
                'country_id'    => $country,
                'offer_id'      => $offerId,
                'required'      => $required
            ]);

        }
        return response()->json([
           'countries'  => $countries,
            'offer_id'  => $offerId,
            'required'  => $required
        ]);
    }


    /**Delete country from visa offer
    */
    public function deleteCountry( $offer,$country ){

        OfferVisa::where('country_id',$country)
            ->where('offer_id',$offer)
            ->forceDelete();

        return response()->json([
            'country_id'    => $country,
            'offer_id'      => $offer
        ]);
    }

    /**Delete language attached to offer
    */
    public function deleteLanguage($offer,$language){
        OfferLanguage::where('offer_id',$offer)
            ->where('language_id',$language)
            ->forceDelete();

        return response()->json([
            'offer_id'      => $offer,
            'language_id'   => $language
        ]);
    }



    /**Get offer available stops at date
     * @param int $id
    */
    public function getAvailableStops( $id, Request $request ){

        $date = $request->get('date');

        $routes = Route::where('offer_id',$id)
            ->get();

        return response()->json([
        	'data'  => $routes
        ]);

    }


    /**Get available dates for an offer
     * @param $id int, offer id reference
     * @param $request Request
    */
    public function getAvailableDates( $id,Request $request){

		$routes = Route::where('offer_id',$id)
			->lists('id')
			->toArray();

		$dates = Event::select(DB::raw('events.*,routes.name as route_name'))
			->leftJoin('routes',function ($join){
			$join->on('events.item_id','=','routes.id');
			})
			->where('module','routes')
			->whereIn('item_id',$routes)
			->get();

		return response()->json($dates);

    }


    /**Get required visas for the offer
     * @param $id int, offer id reference
    */
    public function getRequiredVisas( $id ){
		$visas = OfferVisa::with('country')
			->where('offer_id',$id)
			->where('required',1)
			->get();

		return response()->json($visas);

    }


    /**Get not required visas
    */
    public function getNotRequiredVisas( $id ){

	    $visas = OfferVisa::with('country')
	        ->where('offer_id',$id)
		    ->where('required',0)
		    ->get();

	    return response()->json($visas);

    }

    /**Get offer prices
     * @param $id int, offer id reference
     * @return Response
    */
    public function getPrices( $id ){

    	$prices = OfferPrice::where('offer_id',$id)
		    ->get();

    	return response()->json($prices);
    }


    /**Add offer language
     * @param $offer int, offer id reference
     * @param $language int, language id regerence
    */
    public function addLanguage( $offer,$language ){

    	try{
		    $language = OfferLanguage::create([
			    'offer_id'      => $offer,
			    'language_id'   => $language
		    ]);
		    return response()->json($language);

	    }catch (\PDOException $e ){

		    $msg = $e->getMessage();
		    $code = $e->getCode();
		    $errors = new \stdClass();

		    if( $code == 23000 ){
			    $errors->record = 'duplicated_field';
		    }

		    $response = array(
			    'msg'           => $msg,
			    'errors'        => $errors,
			    'code'          => $code,
		    );

		    return response()->json( $response,500 );
	    }


    }


    /**Delete offer langauge
    */
    public function removeLanguage( $offer,$language ){

    	$language = OfferLanguage::where('offer_id',$offer)
		    ->where('language_id',$language)
		    ->forceDelete();

    	return response()->json($language);

    }


    /**Toggle offer as featured
    */
    public function setFeatured( $id ){


    	try{
		    $offer = Offer::findOrFail($id);
		    $offer->featured = !$offer->featured;
		    $offer->save();

		    return response()->json($offer);
	    }catch(\PDOException $e ){

    		return response()->json([
    			'error'     => $e->getCode(),
			    'msg'       => $e->getMessage()
		    ],500);
	    }catch (ModelNotFoundException $e ){
		    return response()->json([
			    'error'     => $e->getCode(),
			    'msg'       => $e->getMessage()
		    ],500);
	    }


    }


}
