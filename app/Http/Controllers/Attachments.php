<?php

namespace App\Http\Controllers;

use App\Attachment;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Image;
use Mockery\CountValidator\Exception;

//Imagine
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Orchestra\Imagine\Facade as Imagine;
use Psy\Exception\RuntimeException;

class Attachments extends BaseController
{

	protected $model = 'Attachment';
	private $allowedExtensions = [
		'jpg',
		'png',
		'pdf',
		'gif'
	];

	private $allowedFileSize = [
		'width'     => 1900,
		'height'    => 1200
	];

	/**Create new attachment and allow file upload
	 */
	public function store( Request $request ){

		try{

			$file = $request->file('file');
			$slug = uniqid();
			$extension = $file->getClientOriginalExtension();
			$size = $file->getSize();
			$module = $request->get('module');

			//File absolute path
			$currentDate = Carbon::now();
			$year = date('Y',time());
			$month = date('m',time());
			$absolutePath = env('UPLOAD_DIR').$module.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month;
			$basePath = public_path($absolutePath);
			$url = $absolutePath.DIRECTORY_SEPARATOR;
			$name = $slug.'.'.$extension;


			//Create attachment
			$model = new $this->model;
			$model->item_id = $request->get('item_id');
			$model->module = $module;
			$model->name = $file->getClientOriginalName();
			$model->url = $url;
			$model->extension = $extension;
			$model->size = $size;
			$model->filename = $name;
			$model->save();

			if( count($model->errors()) ){

				return response()->json( [
					'errors'    => $model->errors()
				],500 );

			}else{

				//Upload file
				if( $file->move( $basePath,$name ) ) {

					//Create thumbnail
					$width = Attachment::THUMBNAIL_WIDTH;
					$height = Attachment::THUMBNAIL_HEIGHT;

					$mode = ImageInterface::THUMBNAIL_OUTBOUND;
					$size = new Box($width,$height);
					$thumbPrefix = Attachment::THUMBNAIL_PREFIX;
					$thumbFile = $basePath.DIRECTORY_SEPARATOR.$thumbPrefix.$name;
					$thumbnail = Imagine::open($basePath.DIRECTORY_SEPARATOR.$name)
						->thumbnail($size,$mode);

					$thumbnail->save($thumbFile);


				}
			}

			return response()->json( $model,201 );


		}catch (Exception $e){

			return response()->json([
				'error'         => $e->getMessage()
			],500);

		}catch( \Imagine\Exception\RuntimeException $e ){
			return response()->json([
				'error'         => $e->getMessage()
			],500);
		}




	}




	/**
	 * Remove the specified resource from storage.
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id,$force=true )
	{

		try{

			$model = $this->model;
			$attachment = $model::find( $id );

			if( $attachment ){

				$thumbPrefix = Attachment::THUMBNAIL_PREFIX;
				$image = public_path($attachment->url.$attachment->filename);
				$thumbnail = public_path($attachment->url.$thumbPrefix.$attachment->filename);
				$delete = Storage::delete( [$image,$thumbnail] );
				$attachment->forceDelete();

				return response()->json([
					'msg'           => $delete,
					'image'         => $image,
					'thumbnail'     => $thumbnail
				], 200 );
			}

		}catch( ModelNotFoundException $e ){
			$response = array(
				'error'     => $e->getMessage(),
				'model'     => $e->getModel(),
				'code'      => $e->getCode(),
			);
			return response()->json( $response, 404 );

		}catch( \PDOException $e ){
			return response()->json([
				'error'             => $e->getMessage(),
				'code'              => $e->getCode()
			],500);
		}

	}



	/**Get all attachments of an object
	 */
	public function getAll( $module,$id ){


		$model = new $this->model;
		$attachments = $model->where('module',$module)
			->where('item_id',$id)
			->get();

		return response()->json($attachments,200);

	}


	/**Download attachment file
	 * @param int $id, attachment id ref
	 * @return file
	 */
	public function download( $id ){

		$model = new $this->model;
		$attachment = $model->find($id);


		if( $attachment->exists){

			$filename = $attachment->url.$attachment->filename;
			$file = public_path($filename);

			if( File::exists($file) ){
				return response()->download($file);
			}
		}
		return response()->json([
			'error'         => 'not_found',
			'file'          => $id
		],404);

	}

}
