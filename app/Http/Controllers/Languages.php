<?php

namespace App\Http\Controllers;



use App\Language;

class Languages extends BaseController
{
    protected  $model = 'Language';


    /**Get default marked languages
    */
    public function getDefault(){

    	$languages = Language::where('default',1)
		    ->orderBy('name','ASC')
		    ->get();

    	return response()->json($languages);

    }
}
