<?php

namespace App\Http\Controllers;


use App\Template;

class Templates extends BaseController
{
    //
	protected $model = 'Template';


	/**Get template types
	*/
	public function getTypes(){

		$types = Template::$types;

		return response()->json($types);
	}
}
