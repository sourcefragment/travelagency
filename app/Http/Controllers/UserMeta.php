<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserMeta extends BaseController
{
    protected  $model = 'UserMeta';


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request )
    {
        $model = new $this->model;
        $model->save();

        if( $model->save() ){

            return response()->json( $request->all(),201 );

        }else{

            $response = array(
                'errors'        => $model->errors(),
                'fields'        => $request->all(),
                'code'          => 500,
            );

            return response()->json( $response,500 );

        }
    }


	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index( $userId = null )
	{

		$model = new $this->model;
		$belongs = $model::$belongs;
		$params = $this->getParams($userId);

		try{

			if( $params['limit'] == -1 ){
				if($userId!=null)
					$results = $this->search( $params )
						->with( $belongs )->where('user_id', $userId)
						->get();
				else
					$results = $this->search( $params )
						->with( $belongs )
						->get();

			}else{
				if($userId!=null)
					$results = $this->search( $params )
						->with( $belongs )->where('user_id', $userId)
						->paginate( $params['limit'] );
				else
					$results = $this->search( $params )
						->with( $belongs )
						->paginate( $params['limit'] );
			}

		}catch( \Exception $e ){

			if( env( 'APP_DEBUG',false) ){
				return response()->json( $e,500);
			}else{
				return response()->json(array(
					'server error' => 'Query error'
				),300);
			}
		}

		return response()->json( $results ,200 );


	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id, $meta = null )
	{
		$model = $this->model;
		$obj = $model::find( $meta );


		if( $obj->save() ){

			return response()->json( $obj,200 );

		}else{

			$response = array(
				'errors'        => $obj->errors(),
				'fields'        => $request->all(),
				'code'          => 500,
			);

			return response()->json( $response,500 );

		}
	}


	/**Get all user metadata
	 * @param  int $id
	 * @return http response
	*/
	public function getAll( $id ){

		$model = new $this->model;
		$belongs = $model::$belongs;
		$params = parent::getParams();

		try{

			if( $params['limit'] == -1 ){
				$results = parent::search( $params )
					->with( $belongs )
					->where('user_id',$id)
					->get();

			}else{
				$results = parent::search( $params )
					->with( $belongs )
					->where('user_id',$id)
					->paginate( $params['limit'] );
			}

		}catch( \Exception $e ){

			if( env( 'APP_DEBUG',false) ){
				return response()->json( [
					'msg'       => $e->getMessage(),
					'line'      => $e->getLine(),
					'file'      => $e->getFile(),
				],500);
			}else{
				return response()->json(array(
					'server error' => 'Query error'
				),500);
			}
		}

		return response()->json( $results ,200 );

	}
}
