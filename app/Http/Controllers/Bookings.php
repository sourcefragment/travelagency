<?php

namespace App\Http\Controllers;


use App\Http\Requests\Request;
use App\OfferType;

class Bookings extends BaseController
{
    protected  $model = 'Booking';



    /**Return available offers for a specific date range
    */
    public function availableOffers( Request $request ){

        $from = $request->get('date_from');
        $to = $request->get('date_to');
        $categories = $request->get('category');
        $locations = $request->get('location');
        $language = $request->get('language');


        $offers = OfferType::select('*')
            ->leftJoin('offers',function ($join){
                $join->on('offers.offer_type_id','=','offer_types.id');
            })
            ->leftJoin('routes',function ($join){
                $join->on('offers.id','=','routes.offer_id');
            })
            ->leftJoin('route_availability',function ($join){
                $join->on('routes.id','=','route_availability.route_id');
            })
            ->leftJoin('route_exception',function ($join){
                $join->on('routes.id','=','route_exception.route_id');
            })
            ->where(function($query)use($from,$to){
                $query->whereDate('route_availability');
            });

    }

}
