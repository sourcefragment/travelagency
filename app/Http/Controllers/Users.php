<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

use App\User;

class Users extends BaseController
{
    protected  $model = 'User';


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = $this->model;
        $user = $model::find( $id );

	    //Check if password is being sent
		if( !Input::has('password') ){

			$user::$rules['password'] = '';
			$user::$rules['password_confirmation'] = '';
			$user->autoHashPasswordAttributes = false;

		}

        if( $user->updateUniques() ){

            return response()->json( $user,200 );

        }else{

            $response = array(
                'errors'        => $user->errors(),
                'fields'        => $request->all(),
                'code'          => 500,
            );

            return response()->json( $response,500 );

        }
    }


    /**Delete user
     * @overrides parent
     * @param int $id, the user id ref
    */
    public function destroy($id,$force=false)
    {
	    $model = $this->model;
	    $user = $model::find( $id );

	    if( $user->id == 1 ){
	    	return response()->json( [
	    		'error'     => 'admin_user_cant_be_deleted',
		    ],403);
	    }

    	return parent::destroy($id,$force); // TODO: Change the autogenerated stub
    }


    /**Get tour operators
    */
    public function getTourOperators(){

    	$belongs = User::$belongs;
    	$tourOperators = User::select(DB::raw('*'))
//		    ->with($belongs)
	        ->own()
		    ->where('role_id',3)
		    ->get();

	    return response()->json($tourOperators,200);
    }


}
