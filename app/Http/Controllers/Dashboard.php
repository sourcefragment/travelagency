<?php

namespace App\Http\Controllers;


use App\Agent;
use App\Booking;
use App\Client;
use App\Invoice;
use App\OfferType;
use App\Option;
use App\Payment;
use App\TourOperator;
use Illuminate\Support\Facades\DB;

class Dashboard extends BaseController
{


    /**Return bookings pending of confirmation
     *
    */
    public function bookingsPendingOfConfirmation(){


    	$bookings = [];
    	$total = 0;

    	$status = Option::where('key','pending')
		    ->where('type','booking-status')
		    ->first();

    	if( $status ){

		    $bookings = Booking::with(['client','status'])
		        ->where('status_id',$status->id)
			    ->get();

		    $total = Booking::where('status_id',$status->id)
			    ->count();


	    }

	    return response()->json([
		    'total'     => $total,
		    'bookings'  => $bookings
	    ]);

    }


    /**Return bookings pending of payment
    */
    public function bookingsPendingOfPayment(){


	    $bookings = [];
		$total = 0;

	    $status = Option::where('key','confirmed')
		    ->where('type','booking-status')
		    ->first();

	    if( $status ){

		    $bookings = Booking::with(['client','status'])
			    ->where('status_id',$status->id)
			    ->get();

		    $total = Booking::where('status_id',$status->id)
			    ->count();


	    }

	    return response()->json([
		    'total'     => $total,
		    'bookings'  => $bookings
	    ]);

    }

    /**Return bookings rejected by TO
    */
    public function bookingsRejectedByTo(){

	    $bookings = [];
	    $total = 0;

	    $status = Option::where('key','rejected')
		    ->where('type','booking-status')
		    ->first();



	    if( $status ){

		    $bookings = Booking::with(['client','status'])
			    ->where('status_id',$status->id)
			    ->get();

		    $total = Booking::where('status_id',$status->id)
			    ->count();


	    }

	    return response()->json([
		    'total'     => $total,
		    'bookings'  => $bookings
	    ]);

    }


    /**Return agents with negative balance
    */
    public function agentsWithNegativeBalance(){


    	$agents = Agent::where('current_credit','<=',0)
		    ->get();

    	$total = Agent::where('current_credit','<=',0)
		    ->count();

    	return response()->json([
    		'total'         => $total,
		    'agents'        => $agents
	    ]);


    }


    /**Get dashborad stats
    */
    public function getStats(){

    	$totalOffers = OfferType::where('status',1)
	        ->count();

    	$totalClients = Client::where('status',1)
	        ->count();

    	$totalAgents = Agent::where('status',1)
	        ->count();

    	$totalTourOperators = TourOperator::where('status',1)
	        ->count();

    	return response()->json([
    		'total_offers'              => $totalOffers,
		    'total_clients'             => $totalClients,
		    'total_agents'              => $totalAgents,
		    'total_tour_operators'      => $totalTourOperators
	    ]);
    }

    /**Load latest bookings
    */
    public function getLatestBookings(){

    	$bookings = Booking::with( Booking::$belongs )
		    ->get();

    	return response()->json($bookings);

    }


}
