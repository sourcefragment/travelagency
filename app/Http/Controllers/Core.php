<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class Core extends Controller
{
    //

	/**Format price according to user's settings
	 * @param $price float, price value
	 * @return string, formatted price
	*/
	public static function formatPrice( $price ){
		$currency = auth()->user()->currency;
		$currency = !empty($currency) ? $currency:'€';
		return $currency.' '.number_format($price,2);
	}


	/**Format date according to user's settings
	 * @param $date string, date to format
	 * @return string, formatted date
	*/
	public static function formatDate( $date ){
		$format = auth()->user()->date_format;
		$format = !empty($format) ? $format:'d-m-Y';
		return date($format,strtotime($date));
	}
}
