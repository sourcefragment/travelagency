<?php

namespace App\Http\Controllers;

use App\Language;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Exception\NotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Facades\JWTAuth;

class Login extends BaseController
{

	/**Login view
	 * @return string html view
	*/
	public function get()
    {

        $data = [
            'lang'                      => Language::getJson('login'),
	        'app_name'                  => env('APP_NAME'),
	        'app_description'           => '',
	        'app_logo'                  => '',

        ];

        return view('login',$data);
    }


    /**Login user
    */
    public function login(Request $request)
    {


    	$credentials = $request->only('email', 'password');

	    try {

		    $token = JWTAuth::attempt($credentials);

		    //Login user with session
		    Auth::attempt( $credentials );

		    // verify the credentials and create a token for the user
		    if (! $token ) {

			    return response()->json([
				    'error'             => 'login_error',
				    'credentials'       => $credentials
			    ], 401);

		    }


	    } catch ( TokenExpiredException $e) {

		    return response()->json(['token_expired'], $e->getStatusCode());

	    } catch ( JWTException $e) {

		    return response()->json(['token_absent'], $e->getStatusCode());

	    }catch( NotFoundHttpException $e ){
	    	return response()->json(['error'    => $e->getMessage() ],401);
	    }

	    // if no errors are encountered we can return a JWT
	    return response()->json(compact('token'));

    }

    /**Logout user
    */
    public function logout(Request $request)
    {

    	Session::flush();
        Auth::logout();

        if( $request->ajax() ){
            return response()->json(['msg' => 'success'],200);
        }else{
            return redirect()->guest('login');
        }
    }

    /**Show register view
    */
    public function register(){


    }

    /**Restore users account by email
    */
    public function restore( Request $request ){


    	try{
		    $email = $request->input('email');
		    $user = User::where('email',$email)
		        ->first();

		    Password::sendResetLink([
		    	'email' => $user->email
		    ],function ($message){
		    	$message->subject('Test');
		    });


		    $mail = Mail::send( 'templates.users.recover-password',['user'=>$user],function ($m) use ($user){
				$m->from('mario.cristobal@sebcreativos.es','Easy Tour');
		    	$m->to($user->email,$user->name)->subject('Test');
		    });

		    return response()->json([
		    	'user'      => $user,
			    'mail'      => $mail
		    ]);

	    }catch(\PDOException $e){
    		return response()->json([
    			'error'     => $e->getCode(),
			    'msg'       => $e->getMessage()
		    ],404);
	    }


    }
}
