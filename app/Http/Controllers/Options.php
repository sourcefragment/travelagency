<?php

namespace App\Http\Controllers;

class Options extends BaseController
{
	//
	protected $model = 'Option';


	/**Get options by type
	 */
	public function getOptions( $type ){
		$model = new $this->model;
		$options = $model::where('type',$type)
			->get();

		return response()->json($options);
	}

}