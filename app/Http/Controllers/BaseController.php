<?php

    namespace App\Http\Controllers;

    use Illuminate\Contracts\Validation\ValidationException;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Database\QueryException;
    use Illuminate\Http\Request;
    use App\AlertFilter;
    use Illuminate\Http\Response;
    use Illuminate\Support\Facades\Input;
    use Illuminate\Support\Facades\Log;
    use Illuminate\Support\Facades\Validator;
    use DB;
    use League\Flysystem\Exception;


    class BaseController extends Controller
    {
        protected  $model = 'BaseModel';

        //Response format
        protected $format = 'json';


        public function __construct( )
        {

            $this->model = "\\App\\".$this->model;
        }


        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {


	        $model = new $this->model;
	        $belongs = $model::$belongs;
	        $params = $this->getParams();

	        /**Only show related models if style is present*/
	        if( !isset($params['style']) ){
		        $belongs = [];
	        }else{

		        if( $params['style'] != 'details'){
			        $belongs = [];
		        }else{

			        //Strip out related models
			        if( isset($params['without']) ){
				        $without = explode(',',$params['without']);
				        $belongs = array_diff($belongs, $without);
			        }
		        }

	        }


	        try{

		        if( $params['limit'] == -1 ){
			        $results = $this->search( $params )
				        ->own()
				        ->with( $belongs )
				        ->get();

		        }else{
			        $results = $this->search( $params )
				        ->own()
				        ->with( $belongs )
				        ->paginate( $params['limit'] );
		        }

	        }catch( \Exception $e ){

		        if( env( 'APP_DEBUG',false) ){
			        return response()->json( [
				        'msg'       => $e->getMessage(),
				        'line'      => $e->getLine(),
				        'file'      => $e->getFile(),
//	                    'trace'     => $e->getTraceAsString()
			        ],500);
		        }else{
			        return response()->json(array(
				        'server error' => 'Query error'
			        ),500);
		        }
	        }


	        return response()->json( $results ,200 );

        }

        /**Create custom search from query params
         * @param array, assoc array of parameters
         */
        public function search( $params )
        {

	        $model = new $this->model;
	        $query = $model;
	        $query->active();


	        //The where parameter is an array of comma separated key values
	        if( !empty($params['where']) ) {

		        if( is_array($params['where']) ){
			        foreach( $params['where'] as $value ){
				        $items = explode(',',$value);
				        $query .= $query->where( $items[0] , 'LIKE', '%'.$items[1].'%' );
			        }
		        }else{
			        $items = explode(',',$params['where']);
			        $query = $query->where( $items[0] , 'LIKE', '%'.$items[1].'%' );
		        }

	        }

	        //Search by fields
	        if( !empty($params['filter']) ){

		        $fields = json_decode($params['filter']);
		        $conditions = [];
		        foreach( $fields as $key => $value ){

			        //Search on relationship
			        if( strpos($key,'.') !== false ){

				        $relations = $model->getRelations();
				        $c = explode('.',$key);
				        $table = $c[0];
				        $col = $c[1];
				        if( in_array($table,$relations) ){
					        $query = $query->whereHas($table,function($query) use($col,$value){
						        $query->where($col,'LIKE','%'.$value.'%');
					        });
				        }

			        }else{
				        $conditions[] = [$key,'LIKE',"%$value%"];
			        }

		        }
		        $query = $query->where($conditions);

	        }

	        //Sorting
	        if( !empty($params['sort']) ){

		        $fields = json_decode($params['sort']);
		        foreach ( $fields as $key => $value ){
			        $query = $query->orderBy( $key,$value );
		        }

	        }

	        //Order by
	        if( !empty( $params['orderby']) ){

		        $multi = explode(',',$params['orderby']);


		        if( count( $multi ) > 1 ){
			        foreach ( $multi as $m ){
				        $query = $query->orderBy( $m ,$params['order']);
			        }
		        }else{
			        $query = $query->orderBy( $params['orderby'],$params['order']);
		        }

	        }

	        //Group by
	        if( !empty($params['groupby']) )
		        $query = $query->groupBy( $params['groupby']);

	        //Get fields
	        if( !empty($params['fields']) ){
		        $fields = explode(",",$params['fields']);
		        $query = $query->select($fields);
	        }

	        return $query;

        }

        /**Get query valid model params
         * @return array, assoc array of query params valid in the model
        */
        public function getParams()
        {

            $model = new $this->model;
            $inputs = Input::all();
            return array_merge( $model->filters,$inputs );

        }


        /**Show the model table view
        */
        public function view()
        {

        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {

        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store( Request $request )
        {



	        try{

		        $model = new $this->model;
		        $model->save();

		        if( count($model->errors()) ){

			        return response()->json( [
			        	'errors'    => $model->errors()
			        ],500 );
		        }
		        return response()->json( $model,201 );



	        }catch ( \PDOException $e ){

		        $msg = $e->getMessage();
		        $code = $e->getCode();
		        $errors = new \stdClass();

		        if( $code == 23000 ){
			        $errors->record = 'duplicated_field';
		        }

		        $response = array(
			        'msg'           => $msg,
			        'errors'        => $errors,
			        'code'          => $code,
                );

		        return response()->json( $response,500 );

	        }catch( \Exception $e ){

		        $msg = $e->getMessage();
		        $code = $e->getCode();
		        $errors = new \stdClass();

		        if( $code == 23000 ){
			        $errors->record = 'duplicated_field';
		        }

		        $response = array(
			        'msg'           => $msg,
			        'errors'        => $errors,
			        'code'          => $code,
		        );

		        return response()->json( $response,500 );
		        
	        }


        }

        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show( $id )
        {



	        try{

		        $model = $this->model;
		        $belongs = $model::$belongs;
		        $params = $this->getParams();

		        /**Only show related models if style is present*/
		        if( !isset($params['style']) ){
			        $belongs = [];
		        }else{

			        if( $params['style'] != 'details'){
				        $belongs = [];
			        }else{

				        //Strip out related models
				        if( isset($params['without']) ){
					        $without = explode(',',$params['without']);
					        $belongs = array_diff($belongs, $without);
				        }
			        }

		        }

		        $obj = $model::with($belongs)->find($id);

		        return response()->json( $obj );

	        }catch ( ModelNotFoundException $e ){
		        return response()->json([
			        'error'         => $e->getMessage(),
			        'code'          => $e->getCode()
		        ],404);
	        }

        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {


        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {



	        try{

		        $model = $this->model;
		        $obj = $model::find( $id );

//		        $this->authorize('update',$obj);

		        $obj->save();




		        if( count( $obj->errors()) ){
			        return response()->json( $obj->errors(),501 );
		        }
		        return response()->json($obj);

	        }catch( \PDOException $e ){


		        $msg = '';
		        $code = $e->getCode();

		        if( $code == 23000 ){
			        $msg = 'duplicated_field';
		        }

		        $response = array(
			        'msg'           => $msg,
			        'error'         => $e->getCode(),
			        'fields'        => $request->all(),
			        'code'          => 501,
		        );

		        return response()->json( $response,501 );

	        }catch (ModelNotFoundException $m ){
		        return response()->json([ 'id' => $id ],404);
	        }

        }

        /**
         * Remove the specified resource from storage.
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroy( $id,$force=false )
        {

            try{

                $model = $this->model;
                $model = $model::find( $id );

                if( $force ){
                    $model->forceDelete();
                }else{
                    $model->delete();
                }

                return response()->json( $id, 200 );

            }catch( ModelNotFoundException $e ){
                $response = array(
                    'error'     => $e->getMessage(),
                    'model'     => $e->getModel(),
                    'code'      => $e->getCode(),
                );
                return response()->json( $response, 404 );

            }catch( \PDOException $e ){
	            return response()->json([
		            'error'             => $e->getMessage(),
		            'code'              => $e->getCode()
	            ],500);
            }

        }



        /**Return the response according to the request format
         * @param array $data
         */
        public static function response( $data )
        {

            if( count($data) ){
                return response()->json( $data,200 );
            }else{
                return response()->json( $data,400 );
            }

        }

        /**Get model fields
        */
        public function getFields(){
        	$model = $this->model;
        	return $model::$fillable;
        }


        /**Toggle model status
         * @param $id int
        */
        public function toggle( $id ){

	        $model = $this->model;
	        $model = $model::find( $id );

	        if( isset($model->status) ){
		        $model->status = !$model->status;
		        $model->save();
	        }


			return response()->json($model);

        }


        /**Toggle model featured
        */
        public function toggleFeatured( $id ){

	        $model = $this->model;
	        $model = $model::find( $id );

	        $model->featured = !$model->featured;
	        $model->save();
	        return response()->json($model);
        }
    }