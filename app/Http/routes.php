<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**Unprotected routes*/

Route::get('login', 'Login@get'); // return vista html para login
Route::post('login', 'Login@login');
Route::get('logout', 'Login@logout');

Route::post('register', 'Login@register');
Route::post('restore', 'Login@restore');
Route::post('change-password','Login@change-password');

Route::post('client/register','Clients@register');
Route::post('tour-operator/register','TourOperator@register');


//Bookings
Route::post('bookings/register-payment','Bookings@registerPayment');
Route::post('bookings/cancel-payment','Bookings@cancelPayment');
Route::post('booking','Bookings@addBooking');





//This route loads the app
Route::get('/', [
	'middleware' => 'auth',
	'uses' => 'Index@index']);


Route::group([
    'prefix' => 'api/v1',
    'middleware' => ['jwt.auth','permissions'],
], function () {


	Route::resource('addresses','Addresses');
	Route::resource('agents','Agents');
	Route::resource('attachments','Attachments');
	Route::resource('availability','RouteAvailabilities');
	Route::resource('bookings', 'Bookings');
	Route::resource('categories', 'Categories');
	Route::resource('cities', 'Cities');
	Route::resource('clients', 'Clients');
	Route::resource('companies', 'Companies');
	Route::resource('countries', 'Countries');
	Route::resource('discounts', 'Discounts');
	Route::resource('invoices','Invoices');
	Route::resource('invoice-items','InvoiceItems');
	Route::resource('languages', 'Languages');
	Route::resource('locations','Locations');
	Route::resource('notifications', 'Notifications');
	Route::resource('offer-content', 'OfferContents');
	Route::resource('offer-prices', 'OfferPrices');
	Route::resource('offer-type-availability', 'OfferTypeAvailabilities');
	Route::resource('offer-type-content', 'OfferTypeContents');
	Route::resource('offer-types', 'OfferTypes');
	Route::resource('offer-visas', 'OfferVisas');
	Route::resource('offer-languages', 'OfferLanguages');
	Route::resource('offers', 'Offers');
	Route::resource('offer-extras', 'OfferExtras');
	Route::resource('payment-methods', 'PaymentMethods');
	Route::resource('payment-status', 'PaymentStatuses');
	Route::resource('payments', 'Payments');
	Route::resource('permissions','Permissions');
	Route::resource('roles', 'Roles');
	Route::resource('route-zones', 'RouteZones');
	Route::resource('route-stops', 'RouteStops');
	Route::resource('routes', 'Routes');
	Route::resource('states', 'States');
	Route::resource('stops', 'Stops');
	Route::resource('options','Options');
	Route::resource('taxes', 'Taxes');
	Route::resource('templates','Templates');
	Route::resource('tour-operators','TourOperators');
	Route::resource('user-metas', 'UserMeta');
	Route::resource('users', 'Users');
	Route::resource('zones', 'Zones');
	Route::resource('zone-stops', 'ZoneStops');
	Route::resource('items','Items');
	Route::resource('contents','Contents');
	Route::resource('events','Events');


	Route::get('country/cities/{id}','Countries@getCities');
	Route::get('offer-type/{id}/content', 'OfferTypes@getContent');
	Route::get('offer-type/{id}/attachments','OfferTypes@getMedia');
	Route::get('role/{id}/permissions','Permissions@getAll');
	Route::get('sections','Sections@index');
	Route::get('user/{id}/metas', 'UserMeta@getAll');

	Route::get('zone/{id}/stops', 'Zones@getStops');

	//Get options according to type
	Route::get('option/{type}','Options@getOptions');
	Route::get('invoice/{id}/items','InvoiceItems@getItems');



	//Attachaments
	Route::get('download/attachment/{id}','Attachments@download');


	//Dashboard
	Route::get('dashboard/bookings/pending-of-confirmation','Dashboard@bookingsPendingOfConfirmation');
	Route::get('dashboard/bookings/pending-of-payment','Dashboard@bookingsPendingOfPayment');
	Route::get('dashboard/bookings/rejected-by-to','Dashboard@bookingsRejectedByTo');
	Route::get('dashboard/agents/negative-balance','Dashboard@agentsWithNegativeBalance');
	Route::get('dashboard/bookings/latest','Dashboard@getLatestBookings');
	Route::get('dashboard/stats','Dashboard@getStats');


	//Invoices
	Route::get('invoice/validate/{type}','Invoices@validateInvoice');


	//Offers
	Route::get('offer/{id}/extras','Offers@getExtras');
	Route::get('offer/{id}/routes','Offers@getRoutes');
	Route::get('offer/{id}/languages','Offers@getLanguages');
	Route::get('offer/{id}/visas','Offers@getVisas');
	Route::get('offer/{id}/required-visas','Offers@getRequiredVisas');
	Route::get('offer/{id}/not-required-visas','Offers@getNotRequiredVisas');
    Route::post('offer/{id}/visas/countries','Offers@addCountries');
    Route::delete('offer/{offer}/visa/country/{country}','Offers@deleteCountry');
    Route::delete('offer/{offer}/language/{language}','Offers@deleteLanguage');
    Route::get('offer/{id}/available-stops','Offers@getAvailableStops');
    Route::get('offer/{id}/available-dates','Offers@getAvailableDates');
    Route::get('offer/{id}/prices','Offers@getPrices');
    Route::get('offer/{offer}/add-language/{language}','Offers@addLanguage');
    Route::get('offer/{offer}/remove-language/{language}','Offers@removeLanguage');
    Route::get('offer/{id}/set-featured','Offers@setFeatured');

	//Offer types
	Route::get('offer-type/{id}/locations','OfferTypes@getLocations');
	Route::get('offer-type/{id}/categories','OfferTypes@getCategories');
    Route::get('offer-type/{id}/dates','OfferTypes@getDates');
    Route::get('offer-type/{id}/offers','OfferTypes@getOffers');

    Route::post('offer-type/{id}/location','OfferTypes@addLocation');
    Route::get('offer-type/{id}/remove-location','OfferTypes@deleteLocation');

    Route::post('offer-type/{id}/category','OfferTypes@addCategory');
    Route::get('offer-type/{id}/remove-category','OfferTypes@deleteCategory');


    //Content
    Route::get('stop/{id}/content','Stops@getContent');

    //Routes
    Route::get('route/{id}/clone','Routes@cloneRoute');
    Route::get('route/{id}/zones','Routes@getZones');
    Route::get('route/{id}/available-zones','Routes@getAvailableZones');
    Route::post('route/{id}/zone','Routes@addZone');
    Route::get('route/{id}/available-stops','Routes@getAvailableStops');
    Route::get('route/{id}/stops','Routes@getStops');
    Route::get('route/{id}/available-dates','Routes@availableDates');
    Route::post('route/{id}/stop','Routes@addStop');
    Route::delete('route/stop/{id}','Routes@deleteStop');
	Route::get('route/zone/{id}/reorder','Routes@orderZone');
	Route::get('route/stop/{id}/reorder','Routes@orderStop');
	Route::get('route/{id}/zone-stops','Routes@getStops');
	Route::get('route/{id}/pickup-points','Routes@getPickupPoints');

	Route::get('route-zone/{id}/toggle','RouteZones@toggle');

    //Stops
	Route::post('stop/clone/{id}','Stops@cloneStop');
	Route::post('zone/clone/{id}','Zones@cloneZone');


	Route::get('default-languages','Languages@getDefault');

	Route::get('modules','Index@getAvailableModules');
	Route::get('template/types','Templates@getTypes');
	Route::get('module/{id}/fields','Index@getModuleFields');


	//Toggle routes
	Route::get('categories/{id}/toggle-featured','Categories@toggleFeatured');
	Route::get('offer-types/{id}/toggle-featured','OfferTypes@toggleFeatured');
	Route::get('locations/{id}/toggle-featured','Locations@toggleFeatured');

	Route::get('client/{id}/addresses','Clients@getAddresses');





});
