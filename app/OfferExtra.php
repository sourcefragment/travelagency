<?php

namespace App;

class OfferExtra extends BaseModel
{

    protected $table = 'offer_extra';
	protected $fillable = [
		'offer_id',
		'item_id',
		'tax_id',
		'confirmation_required',
		'pvp_price',
		'net_price',
		'discount',
		'price_type',
		'included',

	];

	public static $rules = [
        'offer_id'          => 'required',
		'item_id'           => 'required',
    ];



}
