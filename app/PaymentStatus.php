<?php

namespace App;


class PaymentStatus extends BaseModel
{
	//
	protected $table = 'payment_status';

	protected $fillable = [
		'user_id',
		'name',
		'slug',
		'system'
	];

	public static $rules = [
		'name'                  => 'required'
	];


	public $filters = [
		'limit'         => 10,
		'page'          => 0,
		'order'         => 'asc',
		'orderby'       => 'name',
		'groupby'       => '',
		'where'         => [],
		'style'         => 'details'
	];

	/**Before create
	 */
	public function beforeCreate(){

		if( !$this->user_id ){
			$this->user_id = auth()->user()->id;
		}
	}


	/**Before save
	 */
	public function beforeSave(){

		if( !$this->slug ){
			$this->slug = $this->getSlug($this->name);
		}
	}

}