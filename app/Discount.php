<?php

namespace App;


class Discount extends BaseModel
{
    protected $table = 'discounts';

    //////////////////////////ARDENT//////////////////////

    // rules
    public static $rules =
        [
            'name'               => 'required'
        ];

    public static $customMessages = [
        'required'              => 'The :attribute field is required.',
    ];

    //relationship
    public static $relationsData = [

    ];

    //////////////////////////END ARDENT//////////////////////

	protected $fillable = [
		'user_id',
		'name',
		'value',
		'status'
	];
}
