<?php

namespace App;


class Setting extends BaseModel
{

	protected $table = 'setting';

	protected $fillable = [
		'user_id',
		'app_name',
		'logo',
		'primary_color',
		'secondary_color',
		'print_logo',
		'print_header_bg_color',
		'print_body_bg_color',
	];
}
