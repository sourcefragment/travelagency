<?php

namespace App;

class OfferVisa extends BaseModel
{

    protected $table = 'offer_visas';
    protected $appends = ['name'];
    protected $fillable = [
        'offer_id',
        'country_id',
        'required'
    ];

    public static $rules = [
		'offer_id'              => 'required',
	    'country_id'            => 'required'
    ];


	public static $belongs = ['country'];

    public function country(){
        return $this->belongsTo('App\Country','country_id','id');
    }


    public function getNameAttribute(){
    	return $this->country->name;
    }
}
