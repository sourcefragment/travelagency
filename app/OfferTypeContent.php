<?php

namespace App;

class OfferTypeContent extends BaseModel
{


    protected $table = 'offer_type_contents';
	protected $fillable = [
		'user_id',
		'offer_type_id',
		'language_id',
		'title',
		'short_description',
		'long_description',
		'ticket_description'
	];

    public static $rules = [
            'offer_type_id'     => 'required',
            'language_id'       => 'required',
            'title'             => 'required|min:3'
    ];



	public static $belongs = ['language'];

	public function language(){
		return $this->belongsTo('App\Language','language_id','id');
	}


	public function beforeCreate(){

		if( !$this->user_id ){
			$this->user_id = auth()->user()->id;
		}
	}




}
