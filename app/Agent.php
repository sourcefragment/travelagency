<?php

namespace App;

class Agent extends BaseModel
{
    //
	protected $table = 'agents';
	protected $appends = [
		'credit_limit_value',
		'current_credit_value',
		'used_credit_value'
	];
	protected $fillable = [
		'address',
		'credit_limit',
		'billing_address',
		'billing_city_id',
		'billing_location',
		'billing_name',
		'billing_nif',
		'billing_phone',
		'billing_second_address',
		'billing_zip',
		'city_id',
		'company_id',
		'current_credit',
		'different_billing',
		'dob',
		'email',
		'fax',
		'language_id',
		'last_login',
		'last_name',
		'location',
		'mobile',
		'name',
		'nif',
		'overview',
		'password',
		'phone',
		'second_address',
		'status',
		'treatment',
		'used_credit',
		'user_id',
		'zipcode',
	];


	public static $belongs = ['city','billingCity','user','language'];

	public function city(){
		return $this->belongsTo('App\City','city_id','id');
	}

	public function billingCity(){
		return $this->belongsTo('App\City','city_id','id');
	}

	public function user(){
		return $this->belongsTo('App\User','user_id','id');
	}

	public function language(){
		return $this->belongsTo('App\Language','language_id','id');
	}


	public static $rules = [
		'name'          => 'required|min:3'
	];

	public function beforeCreate(){

		if( !$this->user_id ){
			$this->user_id = auth()->user()->id;
		}
	}


	/**=======================
	 *Field Accessors
	 *
	 ======================*/
	public function getCurrentCreditValueAttribute(){
		$price = number_format($this->current_credit,2);
		return $this->toCurrency($price);
	}

	public function getCreditLimitValueAttribute(){
		$price = number_format($this->credit_limit,2);
		return $this->toCurrency($price);
	}

	public function getUsedCreditValueAttribute(){
		$price = number_format($this->used_credit,2);
		return $this->toCurrency($price);
	}


	public function toCurrency($value){
		$currency = auth()->user()->currency;
		return "$currency $value";
	}
}
