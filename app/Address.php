<?php

namespace App;


class Address extends BaseModel
{
    //
	protected $table = 'addresses';
	protected $fillable = [
		'client_id',
		'city_id',
		'type',
		'address',
		'zipcode',
		'location',
		'phone',
	];

	public static $rules = [
		'client_id'         => 'required',
		'type'              => 'required',
		'address'           => 'required'
	];
}
