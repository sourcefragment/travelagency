<?php

namespace App;


class Category extends BaseModel
{
    //
	protected $table = 'categories';

	protected $fillable = [
		'user_id',
		'name',
		'slug',
		'description',
		'order',
		'featured',
		'status',
	];


	public static $rules = [
		'name'          => 'required|min:3'
	];

	/**
	*/
	public function beforeCreate(){

		$this->slug = $this->getSlug($this->name);

		//Assig category creator
		if( !$this->user_id ){
			$this->user_id = auth()->user()->id;
		}

	}
}
