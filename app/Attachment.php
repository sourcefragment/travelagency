<?php

namespace App;

class Attachment extends BaseModel
{
	const THUMBNAIL_WIDTH = 320;
	const THUMBNAIL_HEIGHT = 320;
	const THUMBNAIL_PREFIX = 'thumb-';

	protected $table = 'attachments';
	protected $fillable = [
		'item_id',
		'user_id',
		'module',
		'name',
		'slug',
		'url',
		'filename',
		'size',
		'order',
		'downloadable',
		'extension',
	];

	public static $rules = [
		'item_id'                   => 'required',
		'module'                    => 'required',
		'file'                      => 'mimes:jpeg,png,jpg|dimensions:max_width=1900,max_height=1200'
	];


	/**Before create
	 */
	public function beforeCreate(){


		//Allow user overwrite
		if( !$this->user_id ){
			$this->user_id = auth()->user()->id;
		}

		$this->slug = $this->getSlug($this->name);

	}


}
