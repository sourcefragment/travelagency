<?php

namespace App;

class TourOperator extends BaseModel
{
    //
	protected $table = 'tour_operators';
	protected $fillable = [
		'address',
		'billing_address',
		'billing_second_address',
		'billing_city_id',
		'billing_location',
		'billing_name',
		'billing_nif',
		'billing_phone',
		'billing_zip',
		'city_id',
		'company_id',
		'dob',
		'different_billing',
		'email',
		'fax',
		'language_id',
		'last_login',
		'last_name',
		'location',
		'mobile',
		'name',
		'nif',
		'overview',
		'password',
		'phone',
		'second_address',
		'status',
		'treatment',
		'user_id',
		'zipcode',
	];

	public static $rules = [
		'name'          => 'required|min:3',
		'email'         => 'sometimes|email'
	];


	public static $belongs = ['user','city','billingCity','language'];


	public function user(){
		return $this->belongsTo('App\User','user_id','id');
	}

	public function city(){
		return $this->belongsTo('App\City','city_id','id');
	}

	public function billingCity(){
		return $this->belongsTo('App\City','billing_city_id','id');
	}

	public function language(){
		return $this->belongsTo('App\Language','language_id','id');
	}

	public function beforeCreate(){

		if( !$this->user_id ){
			$this->user_id = auth()->user()->id;
		}

	}
}
