<?php

namespace App;

class Notification extends BaseModel
{


    /**
     * The database table used by the Ardent.
     *
     * @var string
     */
    protected $table = 'notifications';


    //////////////////////////ARDENT//////////////////////

    // rules
    public static $rules =
        [
            "receiver_id" => "required"

        ];

    public static $customMessages = [
        'required' => 'The :attribute field is required.',
    ];

    //relationship
    public static $relationsData = [

    ];

    //////////////////////////END ARDENT//////////////////////

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sender_id',
        'receiver_id',
        'title',
        'message',
        'status',
        'read_at',
        'type'
    ];

    /**
     * The attributes excluded from the Ardent's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * The attributes are casting
     * @var array
     */
    protected $casts = [
    ];

    public static $belongs = [
    ];

}
