<?php

namespace App;

class City extends BaseModel
{

    protected $table = 'cities';

    public static $rules = [
		'name'              => 'required|min:3',
        'state_id'          => 'required'
    ];


    protected $fillable = [
            'name',
            'state_id'
        ];


	public static $belongs = ['state'];

    public function state(){
        return $this->belongsTo('App\State','state_id','id');
    }

}
