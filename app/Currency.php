<?php

namespace App;

class Currency extends BaseModel
{
    protected $table = 'currencies';

	protected $fillable = [
		'iso',
		'symbol',
		'unicode',
		'position',
		'status'
	];
}
