<?php

    namespace App;

class OfferContent extends BaseModel
{

    protected $table = 'offer_content';

    public static $rules = [
    	'language_id'           => 'required',
    ];

    protected $fillable = [
        'name',
        'offer_id',
        'language_id',
        'short_description',
        'description',
        'ticket_description'
        ];


}
