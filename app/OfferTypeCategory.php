<?php

namespace App;


class OfferTypeCategory extends BaseModel
{
    //
	protected $table = 'offer_type_categories';
	protected $fillable = [
		'offer_type_id',
		'category_id'
	];

	public static $rules = [
		'offer_type_id'     => 'required',
		'category_id'       => 'required'
	];
}
