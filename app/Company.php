<?php

namespace App;

use Illuminate\Support\Facades\Auth;

class Company extends BaseModel
{
    /**
     * The database table used by the Ardent.
     *
     * @var string
     */
    protected $table = 'companies';


    public static $rules = [
        'name'          => 'required|min:3',
        'email'         => 'required|email'
    ];



    protected $fillable = [
	    'billing_address',
	    'billing_city_id',
	    'billing_location',
	    'billing_name',
	    'billing_phone',
	    'billing_second_address',
	    'billing_zip',
        'cif',
        'city_id',
        'country_id',
        'email',
        'fax',
        'invoice_address',
        'name',
        'phone',
        'user_id',
        'webpage',
        'zipcode',

    ];



	public static $belongs = [ 'city' ];


	/**Get related city
	*/
	public function city(){
		return $this->belongsTo('App\City','city_id','id');
	}


	/**Asing user id before create
	*/
	public function beforeCreate(){

		$this->user_id = Auth::User()->id;

	}


}
