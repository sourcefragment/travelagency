<?php

namespace App;

class Branding extends BaseModel
{


    /**
     * The database table used by the Ardent.
     *
     * @var string
     */
    protected $table = 'branding';


    //////////////////////////ARDENT//////////////////////

    // rules
    public static $rules =
        [
            "app_language_id" => "required",
        ];

    public static $customMessages = [
        'required' => 'The :attribute field is required.',
    ];

    //relationship
    public static $relationsData = [

    ];

    //////////////////////////END ARDENT//////////////////////

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'app_language_id'
    ];

    /**
     * The attributes excluded from the Ardent's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * The attributes are casting
     * @var array
     */
    protected $casts = [
    ];

    public static $belongs = [
    ];

}
