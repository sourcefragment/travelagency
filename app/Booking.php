<?php

namespace App;

use App\Events\BookingCancelled;
use App\Events\BookingConfirmed;
use App\Events\BookingRejected;
use App\Http\Controllers\Core;

class Booking extends BaseModel
{

    protected $table = 'bookings';
    protected $appends = [
    	'total_value',
	    'date_value'
    ];
	public static $rules = [
//		'date'                  => 'required',
		'client_id'             => 'required',
        'offer_type_id'         => 'required',
	];

	public static $belongs = ['user','agent','offerType','offer','language','client','status','stop'];


	protected $fillable = [
		'agent_id',
		'client_comments',
		'client_id',
		'code',
		'date',
		'discount',
		'discount_value',
		'internal_comments',
		'language_id',
		'language_id',
		'offer_id',
		'offer_type_id',
		'status_id',
		'stop_id',
		'subtotal',
		'tax_id',
		'tax_price',
		'total',
		'total_adults',
		'total_babies',
		'total_children',
		'total_lunch_adults',
		'total_lunch_children',
		'total_seniors',
		'tour_operator_comments',
		'user_id',
		'zone_id',
	];

	public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }

    public function agent(){
        return $this->belongsTo('App\Agent', 'agent_id','id');
    }

    public function offerType(){
        return $this->belongsTo('App\OfferType','offer_type_id','id');
    }

    public function offer(){
    	return $this->belongsTo('App\Offer','offer_id','id');
    }

    public function language(){
    	return $this->belongsTo('App\Language','language_id','id');
    }

    public function client(){
    	return $this->belongsTo('App\Client','client_id','id');
    }


    function status(){
        return $this->belongsTo('App\Option','status_id','id');
    }

    function stop(){
    	return $this->belongsTo('App\Stop','stop_id','id');
    }

    public function beforeCreate(){

    	if( !$this->user_id ){
    		$this->user_id = auth()->user()->id;
	    }

	    $this->code = strtoupper(uniqid());

    	if( !$this->status_id ){
    	    $this->status_id = Option::getByKey('pending','booking-status');
        }

    }



    /**
    */
    public function afterUpdate(){

    	$status = Option::find($this->status_id );

    	switch ( $status->slug ){

		    case 'cancelled': event(new BookingCancelled($this));
		    	break;

		    case 'confirmed': event( new BookingConfirmed($this));
		    	break;

		    case 'paid':
		    	break;

		    case 'pending':
		    	break;

		    case 'rejected': event( new BookingRejected($this));
		    	break;


	    }
    }

    /**====================
     * Field accessors
     *
     ======================*/
    public function getDateValueAttribute(){
    	return Core::formatDate($this->date);
    }

    public function getTotalValueAttribute(){
    	return Core::formatPrice($this->total);
    }
}
