<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends BaseModel
{
    //
	protected $table = 'clients';

	protected $fillable = [
		'address',
		'billing_address',
		'billing_city_id',
		'billing_location',
		'billing_name',
		'billing_nif',
		'billing_phone',
		'billing_zip',
		'city_id',
		'company_id',
		'country_id',
		'different_billing',
		'dob',
		'email',
		'fax',
		'is_passenger',
		'language_id',
		'last_login',
		'last_name',
		'location',
		'mobile',
		'name',
		'nif',
		'overview',
		'passport',
		'passport_date',
		'password',
		'phone',
		'second_address',
		'status',
		'treatment',
		'user_id',
		'zipcode',
	];



	public static $rules = [
		'email'                     => 'required|email',
		'name'                      => 'required|min:3',
		'phone'                     => 'required|min:9'
	];

	public static $belongs = ['city','billingCity','language','user','company'];


	/**City relationship
	*/
	public function city(){
		return $this->belongsTo('App\City','city_id','id');
	}

	/**Billing city relationship
	*/
	public function billingCity(){
		return $this->belongsTo('App\City','city_id','id');
	}


	/**Language relationship
	*/
	public function language(){
		return $this->belongsTo('App\Language','language_id','id');
	}

	/**User relationship
	*/
	public function user(){
		return $this->belongsTo('App\User','user_id','id');
	}

	/**Company relationship
	*/
	public function company(){
		return $this->belongsTo('App\Company','company_id','id');
	}


}
