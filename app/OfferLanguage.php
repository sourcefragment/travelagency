<?php

namespace App;

class OfferLanguage extends BaseModel
{

	protected $table = 'offer_languages';
	protected $fillable = [
		'offer_id',
		'language_id'
	];
    public static $rules = [
        'offer_id'              => 'required',
	    'language_id'           => 'required'
    ];

    
}
