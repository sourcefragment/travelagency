<?php

namespace App;


use Carbon\Carbon;

class RouteZone extends BaseModel
{
    //
	protected $table = 'route_zones';
    protected $appends = ['departure_time'];

	protected $fillable = [
		'route_id',
		'zone_id',
		'supplement',
		'time_offset',
		'order'
	];

	public static $rules = [
		'route_id'          => 'required',
		'zone_id'           => 'required'
	];

	public $filters = [
		'limit'         => 10,
		'page'          => 0,
		'order'         => 'asc',
		'orderby'       => 'order',
		'groupby'       => '',
		'where'         => [],
		'style'         => 'details'
	];

	public static $belongs = ['zone'];

	public function zone(){
	    return $this->belongsTo('App\Zone','zone_id','id');
    }

    public function route(){
		return $this->belongsTo('App\Route','route_id','id');
    }


    /**Calc recursive departure time zone
     * */
    public function getDepartureTimeAttribute(){


    	//Do for first zone
		if( $this->order === 1 ){
			$fromTime = Carbon::parse($this->route->departure_time);
		}else{
			$prevZone = RouteZone::where('route_id',$this->route_id)
				->where('order','<',$this->order)
				->orderBy('order','DESC')
				->first();
			$fromTime = Carbon::parse($prevZone->getDepartureTimeAttribute());
		}

		$toTime = Carbon::parse($this->time_offset);
	    return $fromTime->addHours($toTime->hour)
		    ->addMinutes($toTime->minute)
		    ->toTimeString();

    }

    public function getTimeOffsetAttribute( $value ){
    	return date('H:i',strtotime($value));
    }

    public function beforeCreate(){

    	if( !$this->order ){
    		$this->order = RouteZone::where('route_id',$this->route_id)
			    ->count() + 1;
	    }
    }
}
