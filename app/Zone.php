<?php
    
namespace App;

class Zone extends BaseModel
{

    protected $table = 'zones';
    protected $appends = ['stops'];

    public static $rules = [
        'name'    => 'required|min:3',
    ];


    protected $fillable = [
        'user_id',
	    'city_id',
	    'name',
	    'description',
	    'lat',
	    'lng',
		'radius',
	    'status'
    ];


    public static $belongs = ['city'];

	/**Get related city object
	*/
    public function city(){
        return $this->belongsTo('App\City','city_id','id');
    }


    /**Auto assign user on create
    */
    public function beforeCreate(){

    	if( !$this->user_id )
            $this->user_id = auth()->user()->id;

    }

    public function getStopsAttribute(){
        $stops = Stop::where('zone_id',$this->id)
            ->count();
        return $stops;
    }


    /**Delete related tables after delete
    */
    public function afterDelete(){

    	RouteZone::where('zone_id',$this->id)
		    ->forceDelete();

    }
}