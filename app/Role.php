<?php

    namespace App;

    use App\Section;
    use App\Permission;

	class Role extends BaseModel
	{
	           

    	protected $table = 'roles';

		protected $appends = ['total_users'];

        public static $rules =[
            	'name'          => 'required|min:3',
        ];


		public static $belongs = [ 'permissions' ];
    	/**
    	 * The attributes that are mass assignable.
    	 *
    	 * @var array
    	 */
    	protected $fillable = [
    	    'name',
		    'status',
		    'slug',
		    'private'
	    ];

    	/**
    	 * The attributes excluded from the Ardent's JSON form.
    	 *
    	 * @var array
    	 */
    	protected $hidden = [
            ];


		public $filters = [
			'limit'         => 10,
			'page'          => 0,
			'order'         => 'asc',
			'orderby'       => 'name',
			'groupby'       => '',
			'where'         => []
		];



        public function permissions()
        {
            return $this->hasMany(
                'App\Permission',
                'role_id'
                )->orderby('section','asc');
        }


        /**Convert slug before create
        */
        public function beforeCreate(){

        	$this->slug = $this->getSlug($this->name);

        }



        /**Convert slug before update
        */
        public function beforeUpdate(){

//        	if( $this->isPrivate() ){
//		        return false;
//	        }
//			return true;

        }

        /**Protect before delete
        */
        public function beforeDelete(){

        	if( $this->isPrivate() ){
        		return false;
	        }
	        return true;
        }


        /**Check if the role is a private role
        */
        private function isPrivate(){
        	return $this->private;
        }



        /**Fill up the permissions after role creation
        */
        public function afterCreate(){

        	$sections = Section::all();

	        foreach ( $sections as $section ){
	        	Permission::create([
			        'role_id'       => $this->id,
			        'section'       => $section->slug,
			        'c'             => 0,
			        'r'             => 0,
			        'u'             => 0,
			        'd'             => 0
		        ]);
	        }

        }


        /**Add total users attribute
        */
        public function getTotalUsersAttribute(){
        	return User::where('role_id',$this->id)
		        ->count();
        }

        

	}
