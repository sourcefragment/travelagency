<?php

namespace App;


class Invoice extends BaseModel
{

	protected $table = 'invoices';
	protected $appends = [
		'total_value',
		'subtotal_value'
	];
	protected $fillable = [
		'billing_address',
		'billing_city_id',
		'billing_contact',
		'billing_zipcode',
		'booking_id',
		'client_id',
		'currency_id',
		'date',
		'description',
		'discount',
		'due_date',
		'number',
		'payment_method_id',
		'payment_status_id',
		'prefix',
		'status_id',
		'subtotal',
		'tax',
		'tax_id',
		'title',
		'total',
		'type_id',
		'user_id',
	];

	public static $belongs = ['type','user','client','paymentMethod','status','taxType','billingCity','status'];

	public function type(){
		return $this->belongsTo('App\Option','type_id','id');
	}

	public function user(){
		return $this->belongsTo('App\User','user_id','id');
	}

	public function client(){
		return $this->belongsTo('App\Client','client_id','id');
	}

	public function paymentMethod(){
		return $this->belongsTo('App\PaymentMethod','payment_method_id','id');
	}

	public function paymentStatus(){
		return $this->belongsTo('App\PaymentStatus','payment_status_id','id');
	}

	public function taxType(){
		return $this->belongsTo('App\Tax','tax_id','id');
	}

	public function billingCity(){
		return $this->belongsTo('App\City','city_id','id');
	}

	public function status(){
		return $this->belongsTo('App\Option','status_id','id');
	}

	public function booking(){
		return $this->belongsTo('App\Bookings','booking_id','id');
	}

	public function items(){
		return $this->hasMany('App\InvoiceItem');
	}

	public static $rules = [
		'title'                         => 'required|min:3',
		'type_id'                       => 'required',
	];



	/**Return valid invoice number according to format
	 */
	public static function getNumber( $typeId ){

		try{

			$type = Option::find($typeId);

			$prefix = !empty($type->prefix) ? $type->prefix.'-':'';
			$date = date('Y',time());


			$total = Invoice::where('type_id',$typeId)
				->orderBy('id','desc')
				->limit(1)
				->count();

			$index = $total + 1;

		}catch (\Exception $e){
			throw  new \Exception($typeId);
		}

		return sprintf("%s%d-%'.04d",$prefix,$date,$index);

	}

	/**Before invoice creation
	 */
	public function beforeCreate(){


		//Auto assign user
		if( !$this->user_id ){
			$this->user_id = auth()->user()->id;
		}

		if( !$this->date ){
			$this->date = date('Y-m-d',time());
			$this->due_date = date('Y-m-d',strtotime('now +15 days'));
		}

		//Assign number on creation
		$this->number = Invoice::getNumber($this->type_id);


		//Set invoice has pending by default
		$this->status_id = Option::getByKey('pending','invoice-status');

	}


	/**=====================
	 *Field Accessors
	 *
	 ======================*/

	public function getTotalValueAttribute( $value ){
		$price = number_format($value,2);
		return $this->toCurrency($price);
	}

	public function getSubtotalValueAttribute( $value ){
		$price = number_format($value,2);
		return $this->toCurrency($price);
	}

	public function getDateAttribute( $value ){
		return date('d-m-Y',strtotime($value));
	}

	public function getDueDateAttribute( $value ){
		return date('d-m-Y',strtotime($value));
	}

	public function toCurrency($value){
		$currency = auth()->user()->currency;
		return "$currency $value";
	}

}
