<?php

namespace App;


class PaymentMethod extends BaseModel
{

	protected $table = 'payment_methods';
	protected $appends = [
		'commission_value'
	];
	protected $fillable = [
		'commission',
		'commission_percent',
		'name',
		'status',
		'sub_account',
		'type',
		'user_id',

	];

	public static $rules = [
		'name'              => 'required|min:3',
	];



	public function getCommissionValueAttribute(){
		$price = number_format($this->commission,2);
		return $this->toCurrency($price);
	}

	public function toCurrency($value){
		$currency = auth()->user()->currency;
		return "$currency $value";
	}

}
