<?php

namespace App;


class Content extends BaseModel
{
    //
    protected $table = 'content';
    protected $fillable = [
        'object_id',
        'user_id',
        'language_id',
        'module',
        'title',
        'slug',
        'short_description',
        'long_description',
        'ticket_description'
    ];


    public static $rules = [
        'object_id'         => 'required',
        'language_id'       => 'required',
        'module'            => 'required'
    ];


    public static $belongs = ['language'];


    public function language(){
        return $this->belongsTo('App\Language','language_id','id');
    }


    public function beforeCreate(){

        //Auto assign user
        if( !$this->user_id ){
            $this->user_id = auth()->user()->id;
        }

    }

}
