<?php

namespace App;

class Stop extends BaseModel
{


    protected $table = 'stops';

    public static $rules = [
        'name'              => 'required',
        'lat'               => 'required',
        'lng'               => 'required',
	    'zone_id'           => 'required'
    ];

    protected $fillable = [
	    'description',
	    'status',
	    'zone_id',
        'heading',
        'lat',
        'lng',
        'name',
        'pitch',
        'user_id',
        'zoom',
    ];


	public static $belongs = ['zone'];


	/**Return related zone
	*/
	public function zone(){
		return $this->belongsTo('App\Zone','zone_id','id');
	}
	/**Assign to user on creation
	*/
	public function beforeCreate(){

		if( !$this->user_id ){
			$this->user_id = auth()->user()->id;
		}
	}



}
