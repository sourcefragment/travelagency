<?php

    namespace App;

	class OfferMeta extends BaseModel
	{
	           
        
        /**
    	 * The database table used by the Ardent.
    	 *
    	 * @var string
    	 */
    	protected $table = 'offer_meta';


        
        // rules
        public static $rules = 
            [
               
            ];



    	/**
    	 * The attributes that are mass assignable.
    	 *
    	 * @var array
    	 */
    	protected $fillable = [
    	    'offer_id',
    	    'meta_key',
            'meta_value'
            ];

    	/**
    	 * The attributes excluded from the Ardent's JSON form.
    	 *
    	 * @var array
    	 */
    	protected $hidden = [
            ];
            
        /**
         * The attributes are casting
         * @var array
         */
        protected $casts = [
        
        ];
        

	}
