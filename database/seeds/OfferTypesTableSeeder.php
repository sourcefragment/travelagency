<?php

use Illuminate\Database\Seeder;

class OfferTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
	    DB::table('offer_types')->truncate();

	    $items = [
		    ['Advanced open water Dive course',295],
		    ['Alfarnatejo',41],
		    ['Ceuta',59],
		    ['Ceuta Más Cerca 2 días',99],
		    ['Ciudades Imperiales 2 días',0],
		    ['Ciudades Imperiales 7 días',549],
		    ['Cordoba Visita',48],
		    ['Costa Vasca Tour',65],
		    ['Countryside Marbella y Mijas',23],
		    ['Escapadas Cordoba y Sevilla',75],
		    ['Escapadas Gibraltar y Ronda',75],
		    ['Escapadas Granada y Antequera',75],
		    ['Fantasia Marroqui',85],
		    ['Flamenco',33],
		    ['Gibraltar Compras',18],
		    ['Gibraltar Compras Express',20],
		    ['Gibraltar Túneles',42],
		    ['Gibraltar Visita',40],
		    ['Gibraltar Delfines',40],
		    ['Gibraltar Visita Express',42],
		    ['Granada y Alhambra',63],
		    ['Jerez y Cádiz',45],
		    ['Marbella & Puerto Banús',23],
		    ['Nerja (con cuevas) y Frigiliana',31],
		    ['Nerja y Frigiliana',25],
		    ['Open water dive course',395],
		    ['Panorámica Madrid y Santiago Bernabeu Tour',38],
		    ['Panorámica Madrid',22],
		    ['Palma Compras',24],
		    ['Panorámica de Madrid',22],
		    ['Panorámica de Madrid nocturna',17],
		    ['Panorámica de Madrid y Palacio Real',36],
		    ['Panorámica y Feria San Isidro',40],
		    ['Ronda en tren',36],
		    ['Ronda visita',34],
		    ['San Sebastián y Biarritz',75],
		    ['Santuario de Loyola, Getaria, Zarauz, etc',75],
		    ['Scuba recue diver',300],
		    ['Sevilla Visita',48],
		    ['Tanger ( Marruecos )',64],
		    ['Tanger 2 Días ( Marruecos )',125],
		    ['Tanger 3 Días ( Marruecos )',161],
		    ['Tour Santiago Bernabeu',16],
		    ['Visita de ASturias y Palacio Real',47],
		    ['Vitoria y Rioja Alvaesa',75],
	    ];

	    foreach ($items as $value){

		    $new = \App\OfferType::create([
			    'name'          => $value[0],
			    'user_id'       => 1,
			    'price'         => $value[1]
		    ]);


	    }

	    DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
