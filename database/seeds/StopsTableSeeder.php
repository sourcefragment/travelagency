<?php

use Illuminate\Database\Seeder;

class StopsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
	    DB::table('stops')->truncate();

	    $handler = fopen(database_path('mysql/stops.csv'),'r');

	    while( $row = fgetcsv($handler,1000,',')){

			\App\Stop::create([
				'user_id'       => 1,
				'zone_id'       => $row[1],
				'lat'           => $row[2],
				'lng'           => $row[3],
				'heading'       => $row[4],
				'pitch'         => $row[5],
				'zoom'          => $row[6],
				'name'          => $row[7],
				'description'   => $row[8]

			]);
	    }


	    DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
