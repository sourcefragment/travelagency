<?php

    use Illuminate\Database\Seeder;
    use App\User;

    class UserTableSeeder extends Seeder {
        public function run() {

            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            DB::table('users')->truncate();

            User::create([
                'name'                      => 'Admin',
                'lastname'                  => 'Admin',
                'password'                  => env('ADMIN_PASS'),
                'password_confirmation'     => env('ADMIN_PASS'),
                'email'                     => env('ADMIN_EMAIL'),
                'status'                    => true,
                'role_id'                   => 1
            ]);

            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }
