<?php

 use Illuminate\Database\Seeder;
 use App\PaymentMethod;

 class PaymentMethodsTableSeeder extends Seeder {

 	public function run() {

         DB::statement('SET FOREIGN_KEY_CHECKS=0;');
         DB::table('payment_methods')->truncate();


	     $methods = [
	     	[
		        'name'          => 'Cash',
		        'slug'          => 'cash',
		        'type'          => 'local',
		        'commission'    => 0.0,
		        'status'        => true,
		        'private'       => true,
	        ],
		     [
			     'name'         => 'Card',
			     'slug'         => 'card',
			     'type'         => 'local',
			     'commission'   => 0.0,
			     'status'       => true,
			     'private'      => true,
		     ],
		     [
			     'name'         => 'Bank transfer',
			     'slug'         => 'bank-transfer',
			     'type'         => 'local',
			     'commission'   => 0.0,
			     'status'       => true,
			     'private'       => true,
		     ],
		     [
			     'name'          => 'Paypal',
			     'slug'          => 'paypal',
			     'type'          => 'local',
			     'commission'         => 0.0,
			     'status'          => true,
			     'private'          => true,
		     ],


	     ];

	     foreach ( $methods as $method ){
		     PaymentMethod::create( $method );
	     }



         DB::statement('SET FOREIGN_KEY_CHECKS=1;');
     }


 }
