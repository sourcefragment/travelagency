<?php

use Illuminate\Database\Seeder;

use App\PaymentStatus;

class PaymentStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

	    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
	    DB::table('payment_status')->truncate();

	    $status = [

		    [
		    	'user_id'       => 1,
		    	'name'          => 'Pending',
			    'slug'          => 'pending',
			    'system'        => true
		    ],
		    [
			    'user_id'       => 1,
		    	'name'          => 'Reserved',
			    'slug'          => 'reserved',
			    'system'        => true
		    ],
		    [
			    'user_id'       => 1,
		    	'name'          => 'Deposit',
			    'slug'          => 'deposit',
			    'system'        => true
		    ],
		    [
			    'user_id'       => 1,
		    	'name'          => 'Paid',
			    'slug'          => 'paid',
			    'system'        => true
		    ],
		    [
			    'user_id'       => 1,
		    	'name'          => 'Waiting',
			    'slug'          => 'waiting',
			    'system'        => true
		    ],
		    [
			    'user_id'       => 1,
		    	'name'          => 'Cancelled',
			    'slug'          => 'cancelled',
			    'system'        => true
		    ],
		    [
			    'user_id'       => 1,
		    	'name'          => 'Void',
			    'slug'          => 'void',
			    'system'        => true
		    ],
	    ];

	    foreach ( $status as $s ){

		    PaymentStatus::create( $s );

	    }


	    DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
