<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
	    DB::table('categories')->truncate();

	    $items = [
			'Excursión 1 día',
		    'Excursión 2 días',
		    'Entradas',
		    'Toros',
		    'Submarinismo',
		    'Circuitos',
		    'Actividades',
		    'Reservas de Golf',
		    'Parques'
	    ];

	    foreach ($items as $value){

		    $item = \App\Category::create([
			    'name'  => $value,
			    'user_id'   => 1
		    ]);


	    }

	    DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
