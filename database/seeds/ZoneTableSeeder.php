<?php

use Illuminate\Database\Seeder;

use App\Zone;

class ZoneTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

	    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
	    DB::table('zones')->truncate();

	    $sql = file_get_contents(database_path('mysql/zones.sql'));
	    DB::unprepared($sql);
	    DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
