<?php

use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
	    DB::table('locations')->truncate();

	    $items = [
		    'Alhaurín de la Torre',
		    'Alhaurín el Grande',
		    'Antequera',
		    'Benahavís',
		    'Benalmadena',
		    'Cabopino',
		    'Casarabonela',
		    'Casares',
		    'Coín',
		    'Cádiz',
		    'Cártama',
		    'El Faro',
		    'Frigiliana',
		    'Fuengirola',
		    'Gaucín',
		    'Gibraltar',
		    'Istán',
		    'La Cala de Mijas',
		    'La Duquesa',
		    'Malaga',
		    'Manilva',
		    'Marbella',
		    'Mijas Costa',
		    'Mijas',
		    'Monda',
		    'Nerja',
		    'Nueva Andalucía',
		    'Ojén',
		    'Puerto Banús',
		    'Riviera del Sol',
		    'Ronda',
		    'San Pedro',
		    'San Roque',
		    'Sotogrande',
		    'Torremolinos',
		    'Álora',
	    ];

	    foreach ($items as $value){

		    $new = \App\Location::create([
			    'name'  => $value,
			    'user_id'   => 1
		    ]);


	    }

	    DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
