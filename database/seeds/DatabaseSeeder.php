<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountryTableSeeder::class);
        $this->command->comment('countries table seeded');

        $this->call(CityTableSeeder::class);
        $this->command->comment('cities table seeded');

        $this->call(StateTableSeeder::class);
        $this->command->comment('states table seeded');

        $this->call(LanguageTableSeeder::class);
        $this->command->comment('languages table seeded');

        $this->call(RoleTableSeeder::class);
        $this->command->comment('Role table seeded');

        $this->call(UserTableSeeder::class);
        $this->command->comment('Users table seeded');

	    $this->call(SectionsTableSeeder::class);
	    $this->command->comment('sections table seeded');

	    $this->call(PermissionTableSeeder::class);
        $this->command->comment('permission table seeded');

	    $this->call(CurrencyTableSeeder::class);
	    $this->command->comment('currencies table seeded');

	    $this->call(TaxesTableSeeder::class);
	    $this->command->comment('taxes table seeded');

        $this->call(PaymentMethodsTableSeeder::class);
        $this->command->comment('payment methods table seeded');

	    $this->call(PaymentStatusSeeder::class);
	    $this->command->comment('payment status table seeded');

        $this->call(OptionsTableSeeder::class);
        $this->command->comment('options table seeded');

	    $this->call(ZoneTableSeeder::class);
	    $this->command->comment('zones table seeded');

	    $this->call(StopsTableSeeder::class);
	    $this->command->comment('stops table seeded');

	    $this->call(CategoriesTableSeeder::class);
	    $this->command->comment('categories table seeded');

	    $this->call(LocationsTableSeeder::class);
	    $this->command->comment('locations table seeded');


	    $this->call(OfferTypesTableSeeder::class);
	    $this->command->comment('offer types table seeded');

    }

}
