<?php

    use Illuminate\Database\Seeder;
    use App\State;

    class StateTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {

            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            DB::table('states')->truncate();

            $sql = file_get_contents(database_path('mysql/states.sql'));
            DB::unprepared($sql);

            DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        }
    }
