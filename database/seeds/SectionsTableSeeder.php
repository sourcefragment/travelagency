<?php

use Illuminate\Database\Seeder;

class SectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
	    DB::table('sections')->truncate();

	    $sections = [
		    ['Dashboard','dashboard',1,'','fa-dashboard'],
			['Bookings','bookings',3,'','fa-calendar'],
			['Tours','',4,0,'fa-bus'],
			['Offers','offers',2,'tours','fa-newspaper-o'],
			['Categories','categories',6,'tours','fa-slack'],
			['Zones','zones',4,'tours','fa-bullseye'],
			['Routes','routes',3,'tours','fa-road'],
			['Stops','stops',5,'tours','fa-map-marker'],
			['Users','users',11,'','fa-users'],
			['Contacts','contacts',6,'','fa-users'],
			['Accounting','',7,'','fa-calculator'],
			['Invoices','invoices',1,'accounting','fa-file-text'],
			['Payment Methods','payment-methods',2,'accounting','fa-money'],
			['Taxes','taxes',4,'accounting','fa-credit-card'],
			['Settings','settings',9,'','fa-cogs'],
			['Notifications','notifications',8,'','fa-bell'],
			['Roles','roles',10,'settings','fa-sitemap'],
			['Companies','companies',10,'settings','fa-university'],
			['Templates','templates',10,'settings','fa-file-code-o'],
			['Locations','locations',7,'tours','fa-globe'],
			['Payment Status','payment-status',3,'accounting','fa-money'],
			['Offer Types','offer-types',1,'tours','fa-ticket'],
			['Options','options',0,'settings','fa-list-ul'],
			['Clients','clients',0,'contacts','fa-user'],
			['Tour Operators','tour-operators',0,'contacts','fa-rocket'],
			['Agents','agents',0,'contacts','fa-briefcase'],
			['Items','items',9,'tours','fa-puzzle-piece'],
			['General','general',10,'settings','fa-wrench'],
	    ];


	    foreach ($sections as $section){
	    	$s = \App\Section::create([
	    		'name'              => $section[0],
			    'module'            => $section[1],
			    'menu_order'        => $section[2],
			    'icon'              => $section[4],
			    'menu_visible'           => true
		    ]);

	    	$s->setParentBySlug($section[3]);
	    }

	    //$sql = file_get_contents(database_path('mysql/sections.sql'));
	    //DB::unprepared($sql);

	    DB::statement('SET FOREIGN_KEY_CHECKS=1;');


    }
}
