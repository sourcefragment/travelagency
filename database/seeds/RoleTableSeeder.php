<?php

    use Illuminate\Database\Seeder;
    use App\Role;

    class RoleTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {

            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            DB::table('roles')->truncate();

	        $roles = [

	        	[
	        		'name'          => 'Administrator',
			        'slug'          => 'administrator',
			        'private'       => true,
			        'status'        => true
		        ],
		        [
			        'name'          => 'Manager',
			        'slug'          => 'manager',
			        'private'       => true,
			        'status'        => true
		        ],
		        [
			        'name'          => 'Tour Operator',
			        'slug'          => 'tour_operator',
			        'private'       => true,
			        'status'        => true
		        ],
		        [
			        'name'          => 'Mediator',
			        'slug'          => 'mediator',
			        'private'       => true,
			        'status'        => true
		        ],
		        [
			        'name'          => 'User',
			        'slug'          => 'user',
			        'private'       => true,
			        'status'        => true
		        ],
		        [
			        'name'          => 'Customer',
			        'slug'          => 'customer',
			        'private'       => true,
			        'status'        => true
		        ],


	        ];

	        foreach ( $roles as $role ){
	            Role::create( $role );
	        }


            DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        }
    }
