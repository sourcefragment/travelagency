<?php

use Illuminate\Database\Seeder;

class CurrencyTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		DB::table('currencies')->truncate();

		$sql = file_get_contents(database_path('mysql/currencies.sql'));
		DB::unprepared($sql);

		DB::statement('SET FOREIGN_KEY_CHECKS=1;');

	}
}
