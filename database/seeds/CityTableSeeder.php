<?php

use Illuminate\Database\Seeder;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     */
   
    
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('cities')->truncate();

        $sql = file_get_contents(database_path('mysql/cities.sql'));
        DB::unprepared($sql);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
