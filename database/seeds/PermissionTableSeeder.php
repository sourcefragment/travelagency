<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\Section;
use App\Role;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('permissions')->truncate();

	    $sections = Section::all();
		$roles = Role::all();

	    foreach ( $roles as $role ){

		    foreach ( $sections as $section ){

			    Permission::create([
				    'role_id' => $role->id,
				    'section' => $section->slug,
				    'c' => 1,
				    'r' => 1,
				    'u' => 1,
				    'd' => 1,
			    ]);

		    }

	    }



        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
