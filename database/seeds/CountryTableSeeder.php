<?php

use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('countries')->truncate();

        $sql = file_get_contents(database_path('mysql/countries.sql'));
        DB::unprepared($sql);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
