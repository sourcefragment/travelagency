<?php

use Illuminate\Database\Seeder;
use App\Tax;

class TaxesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

	    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
	    DB::table('taxes')->truncate();

	    Tax::create([
		    'user_id'       => 1,
		    'name'          => 'Iva',
		    'value'         => 21,
		    'status'        => true,
	    ]);

	    DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
