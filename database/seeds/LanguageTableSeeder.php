<?php

use Illuminate\Database\Seeder;
use App\Language;

class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

	    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
	    DB::table('languages')->truncate();

	    $sql = file_get_contents(database_path('mysql/languages.sql'));
	    DB::unprepared($sql);

	    DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
