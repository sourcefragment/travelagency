<?php

use Illuminate\Database\Seeder;

class TemplateTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

	    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
	    DB::table('template_types')->truncate();


	    $items = [
	    	[ 'name'    => '' ],
	    	[ 'name'    => '' ],
	    	[ 'name'    => '' ],
	    	[ 'name'    => '' ],
	    	[ 'name'    => '' ],
	    	[ 'name'    => '' ],
	    	[ 'name'    => '' ],
	    	[ 'name'    => '' ],
	    	[ 'name'    => '' ],
	    	[ 'name'    => '' ],
	    	[ 'name'    => '' ],
	    	[ 'name'    => '' ],
	    ];

	    DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
