<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfferTypeContents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_type_contents', function (Blueprint $table) {

        	$table->engine = 'InnoDB';

            $table->increments('id');
	        $table->unsignedBigInteger('user_id');
	        $table->unsignedBigInteger('offer_type_id');
	        $table->unsignedBigInteger('language_id');


	        $table->string('title');
	        $table->text('short_description');
	        $table->text('long_description');
	        $table->text('ticket_description');


            $table->timestamps();
	        $table->softDeletes();

	        $table->unique(['offer_type_id','language_id'],'unique_offer_type_language');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('offer_type_contents');
    }
}
