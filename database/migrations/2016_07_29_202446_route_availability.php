<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RouteAvailability extends Migration
{
        private $dbName = 'route_availability';

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up() 
        {
            if(!Schema::hasTable($this->dbName)):
                Schema::create($this->dbName, function(Blueprint $table) 
                {
                    $table->engine = 'InnoDB';
                    $table->bigIncrements('id');
                    $table->unsignedBigInteger('route_id');


                    $table->timestamps();
                    $table->softDeletes();

	                $table->index(['route_id']);

                });
            endif;
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down() 
        {
                Schema::dropIfExists($this->dbName);
        }
}
