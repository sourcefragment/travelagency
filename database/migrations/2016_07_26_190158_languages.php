<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Languages extends Migration
{
    private $dbName = 'languages';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable($this->dbName)):
            Schema::create($this->dbName, function(Blueprint $table) 
            {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');

                $table->string('name',60);
                $table->string('iso',2);
                $table->string('translate',60);
                $table->boolean('app');
                $table->boolean('default')->default(0);


                $table->timestamps();
                $table->softDeletes();

	            $table->index(['name']);
	            $table->unique(['name']);

            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->dbName);
    }
}
