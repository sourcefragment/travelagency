<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfferExtraContent extends Migration
{
    private $dbName = 'offer_extra_content';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
            if(!Schema::hasTable($this->dbName)):
                Schema::create($this->dbName, function(Blueprint $table)
                {
                    $table->engine = 'InnoDB';
                    $table->bigIncrements('id');
                    $table->unsignedBigInteger('offer_extra_id');
                    $table->unsignedBigInteger('language_id');

                    $table->string('name', 50);
                    $table->text('description')->nullable();
                    $table->text('short_description')->nulable();

                    $table->timestamps();
                    $table->softDeletes();

                    $table->index(['offer_extra_id']);
	                $table->unique(['offer_extra_id','language_id']);


                });
            endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::dropIfExists($this->dbName);
    }
}