<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Stops extends Migration
{
        private $dbName = 'stops';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            if(!Schema::hasTable($this->dbName)):

                Schema::create($this->dbName, function(Blueprint $table)
                {
                    $table->engine = 'InnoDB';
                    $table->bigIncrements('id');
					$table->unsignedBigInteger('user_id');
	                $table->unsignedBigInteger('zone_id')->nullable();

                    $table->string('name',40);
                    $table->string('slug',40);
                    $table->string('description')->nullable();
                    $table->decimal('lat',12,8)->nullable();
                    $table->decimal('lng',12,8)->nullable();
                    $table->smallInteger('heading')->nullable();
                    $table->smallInteger('pitch')->nullable();
                    $table->tinyInteger('zoom')->nullable();
                    $table->boolean('status')->default(1);

                    $table->timestamps();
                    $table->softDeletes();

	                $table->unique('name','zone_id');


                });
            endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::dropIfExists($this->dbName);
    }
}