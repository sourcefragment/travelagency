<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Items extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->unsignedBigInteger('user_id')->nullable();

            $table->string('name');
            $table->string('slug')->nullable();
            $table->string('description')->nullable();
            $table->boolean('single')->default(0);
            $table->boolean('billable')->default(0);
            $table->boolean('status')->default(1);
            $table->boolean('system')->default(0);

            $table->decimal('price',10,2)->nullable();
            $table->tinyInteger('min')->nullable();
            $table->tinyInteger('max')->nullable();
            $table->string('default_value')->nullable();
            $table->boolean('required')->default(false);
            $table->boolean('auto_include')->default(false);


            $table->timestamps();
            $table->softDeletes();

            $table->unique(['name','slug'],'unique_item_name_slug');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items');
    }
}
