<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfferMeta extends Migration
{
    private $dbName = 'offer_meta';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if(!Schema::hasTable($this->dbName)):
            Schema::create($this->dbName, function(Blueprint $table) 
            {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->unsignedBigInteger('offer_id');

	            $table->smallInteger('meta_type');
	            $table->string('meta_name', 100);
	            $table->string('meta_key',100);
	            $table->text('meta_value');

	            $table->timestamps();
	            $table->softDeletes();

                $table->index(['offer_id']);
	            $table->unique(['offer_id','meta_key']);


            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->dbName);
    }
}
