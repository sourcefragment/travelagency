<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfferLanguages extends Migration
{
    private $dbName = 'offer_languages';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable($this->dbName)):
            Schema::create($this->dbName, function(Blueprint $table) 
            {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->unsignedBigInteger('offer_id');
                $table->unsignedBigInteger('language_id');

	            $table->timestamps();
	            $table->softDeletes();

	            $table->index(['offer_id','language_id']);
	            $table->unique(['offer_id','language_id']);


            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->dbName);
    }
}
