<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Attachments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachments', function (Blueprint $table) {

        	$table->engine = 'InnoDB';

        	$table->increments('id');
	        $table->unsignedBigInteger('item_id');
	        $table->unsignedBigInteger('user_id');

	        $table->string('module',20);
	        $table->string('name');
	        $table->string('slug');
	        $table->string('url');
	        $table->string('filename');
	        $table->integer('size');
	        $table->smallInteger('order')->nullable();
	        $table->boolean('downloadable')->default(1);
	        $table->string('extension',3)->nullable();


	        $table->timestamps();
	        $table->softDeletes();

	        $table->index(['item_id','user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attachments');
    }
}
