<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RoutesZones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route_zones', function (Blueprint $table) {

        	$table->engine = 'InnoDB';
        	$table->increments('id');
	        $table->unsignedBigInteger('route_id');
	        $table->unsignedBigInteger('zone_id');

	        $table->decimal('supplement',8,2)->default(0);
	        $table->time('time_offset')->nullable();
	        $table->tinyInteger('order')->nullable();
	        $table->boolean('status')->default(1);

            $table->timestamps();
			$table->softDeletes();

	        $table->unique(['route_id','zone_id'],'unique_route_zone');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('route_zones');
    }
}
