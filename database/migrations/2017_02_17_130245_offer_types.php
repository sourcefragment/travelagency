<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfferTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_types', function (Blueprint $table) {

        	$table->engine = 'InnoDB';
        	$table->increments('id');
	        $table->unsignedBigInteger('city_id');
	        $table->unsignedBigInteger('user_id');

	        $table->string('code');
	        $table->string('name');
	        $table->string('slug');

			$table->decimal('price',8,2)->default(0);
	        $table->tinyInteger('rating')->nullable();
	        $table->tinyInteger('commission')->nullable();
	        $table->tinyInteger('order')->nullable();
	        $table->string('description')->nullable();
	        $table->time('duration')->nullable();
	        $table->boolean('featured')->default(0);
	        $table->boolean('status')->default(1);

            $table->timestamps();
	        $table->softDeletes();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('offer_types');
    }
}
