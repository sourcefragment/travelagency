<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration
{
    private $dbName = 'users';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            if(!Schema::hasTable($this->dbName)):
                Schema::create($this->dbName, function(Blueprint $table)
                {
                    $table->engine = 'InnoDB';
                    $table->bigIncrements('id');
                    $table->unsignedBigInteger('role_id');
                    $table->unsignedBigInteger('user_id');
                    $table->unsignedBigInteger('language_id')->nullable();
                    $table->unsignedBigInteger('city_id')->nullable();
                    $table->unsignedBigInteger('company_id')->nullable();


                    $table->string('email',100);
                    $table->string('password',60);
                    $table->string('dni',20)->nullable();
                    $table->string('name',40)->nullable();
                    $table->string('lastname',70)->nullable();
                    $table->string('api_key',60)->nullable();
                    $table->string('phone',30)->nullable();
                    $table->string('mobile',30)->nullable();
                    $table->string('address')->nullable();
                    $table->string('zipcode',10)->nullable();
                    $table->string('location')->nullable();

                    $table->smallInteger('timezone')->nullable();
                    $table->string('currency',3)->nullable();
                    $table->string('date_format',20)->nullable();
                    $table->string('time_format',10)->nullable();
                    $table->tinyInteger('start_of_week')->nullable();


                    $table->timestamp('last_login')->nullable();
                    $table->boolean('status')->default(1);


                    $table->rememberToken();
                    $table->timestamps();
                    $table->softDeletes();

                    $table->unique(['email']);
                    $table->index(['email','name','lastname']);
                });

            endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
            Schema::dropIfExists($this->dbName);
    }
}