<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Roles extends Migration
{
    private $dbName = 'roles';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if(!Schema::hasTable($this->dbName)):
            Schema::create($this->dbName, function(Blueprint $table) 
            {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
	            $table->unsignedBigInteger('user_id');

                $table->string('name',40)->unique();
                $table->string('slug',40);
                $table->boolean('private')->default(0);
                $table->boolean('status')->default(1);

                $table->timestamps();
                $table->softDeletes();

	            $table->index(['user_id']);
	            $table->unique(['slug']);

            });

        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->dbName);
    }
}
