<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Content extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('object_id');
            $table->unsignedBigInteger('language_id');

            $table->string('module',20);
            $table->string('title');
            $table->string('slug');
            $table->string('short_description');
            $table->string('ticket_description');
            $table->text('long_description');


            $table->timestamps();
            $table->softDeletes();

            $table->unique(['object_id','module','language_id'],'unique_object_module_content');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('content');
    }
}
