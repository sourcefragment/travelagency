<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Zones extends Migration
{
	private $dbName = 'zones';


	public function up()
	{
		if(!Schema::hasTable($this->dbName)):
			Schema::create($this->dbName, function(Blueprint $table)
			{
				$table->engine = 'InnoDB';
				$table->bigIncrements('id');
				$table->unsignedBigInteger('user_id');
				$table->unsignedBigInteger('city_id');

				$table->string('name');
				$table->string('slug');

				$table->string('description')->nullable();
				$table->decimal('lat',12,8)->nullable();
				$table->decimal('lng',12,8)->nullable();
				$table->smallInteger('radius')->nullable();
				$table->boolean('status')->default(1);

				$table->timestamps();
				$table->softDeletes();

				$table->index(['user_id','city_id']);
				$table->unique(['user_id','name','city_id']);


			});
		endif;
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists($this->dbName);
	}
}
