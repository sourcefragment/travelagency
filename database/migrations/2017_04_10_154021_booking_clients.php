<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BookingClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_clients', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('language_id')->nullable();
            $table->unsignedBigInteger('city_id')->nullable();
            $table->unsignedBigInteger('billing_city_id')->nullable();


            $table->string('treatment',6)->nullable();//mr,mrs,dr,ms,sr,prof
            $table->string('name',40);
            $table->string('last_name',40)->nullable();
            $table->string('dni',20)->nullable();
            $table->string('passport',20)->nullable();
            $table->string('email')->nullable();
            $table->string('password',60)->nullable();
            $table->string('phone',20)->nullable();
            $table->string('mobile',20)->nullable();
            $table->string('address')->nullable();
            $table->string('second_address')->nullable();
            $table->string('location')->nullable();
            $table->string('zipcode',20)->nullable();
            $table->date('dob')->nullable();
            $table->text('overview')->nullable();


            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booking_clients');
    }
}
