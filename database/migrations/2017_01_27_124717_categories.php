<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Categories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {

        	$table->engine = 'InnoDB';
            $table->increments('id');
	        $table->unsignedBigInteger('user_id');
	        $table->unsignedBigInteger('parent_id')->nullable();

	        $table->string('name');
	        $table->string('slug');
	        $table->string('description')->nullable();
	        $table->tinyInteger('order')->nullable();
	        $table->boolean('featured')->default(0);
	        $table->boolean('status')->default(1);

            $table->timestamps();
	        $table->softDeletes();

	        $table->unique(['slug']);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
    }
}
