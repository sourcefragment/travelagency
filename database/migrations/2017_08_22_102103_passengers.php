<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Passengers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('passengers', function (Blueprint $table) {
            $table->increments('id');

	        $table->unsignedBigInteger('country_id')->nullable();
	        $table->unsignedBigInteger('client_id')->nullable();

	        $table->string('name',40);
	        $table->string('last_name',40)->nullable();
	        $table->string('email')->nullable();
	        $table->string('nif',20)->nullable();
	        $table->string('phone',20)->nullable();
	        $table->string('address')->nullable();
	        $table->string('second_address')->nullable();
	        $table->string('location')->nullable();
	        $table->string('passport',40)->nullable();
	        $table->date('passport_date')->nullable();
	        $table->string('zipcode',20)->nullable();
	        $table->date('dob')->nullable();
	        $table->boolean('status')->defalult(true);


            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('passengers');
    }
}
