<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Sections extends Migration
{
	private $dbName = 'sections';


	public function up()
	{
		if(!Schema::hasTable($this->dbName)):
			Schema::create($this->dbName, function(Blueprint $table)
			{
				$table->engine = 'InnoDB';
				$table->bigIncrements('id');
				$table->unsignedBigInteger('parent_id');

				$table->string('name',30);
				$table->string('slug',30);
				$table->string('module',30);
				$table->string('icon',30)->nullable();
				$table->smallInteger('menu_order')->nullable();
				$table->boolean('menu_visible')->default(1);
				$table->boolean('autoload')->default(1);

				$table->timestamps();
				$table->softDeletes();

				$table->index(['parent_id']);
				$table->unique(['slug']);

			});
		endif;
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists($this->dbName);
	}

}
