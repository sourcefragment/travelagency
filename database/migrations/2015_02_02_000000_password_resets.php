<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PasswordResets extends Migration
{

    private $dbName = 'password_resets';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable($this->dbName)):
            Schema::create( $this->dbName, function( Blueprint $table )
            {
                $table->engine = 'InnoDB';
                $table->string('email')->index();
                $table->string('token')->index();

                $table->timestamp('created_at');
                $table->softDeletes();

            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( $this->dbName );
    }
}
