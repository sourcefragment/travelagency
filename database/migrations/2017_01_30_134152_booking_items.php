<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BookingItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_items', function (Blueprint $table) {

        	$table->engine = 'InnoDB';
            $table->increments('id');
	        $table->unsignedBigInteger('booking_id');
	        $table->unsignedBigInteger('item_id');
	        $table->unsignedBigInteger('tax_id')->nullable();

	        $table->smallInteger('quantity')->nullable();
	        $table->decimal('price',8,2)->nullable();
	        $table->string('price_type',20)->nullable();
	        $table->tinyInteger('discount')->nullable();
	        $table->decimal('discount_value',8,2)->nullable();
	        $table->boolean('discount_is_value')->nullable();


            $table->timestamps();

	        $table->unique(['booking_id','item_id'],'unique_booking_item');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booking_items');
    }
}
