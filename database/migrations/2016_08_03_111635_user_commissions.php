<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserCommissions extends Migration
{
    private $dbName = 'user_commissions';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() 
    {
        if(!Schema::hasTable($this->dbName)):
            Schema::create($this->dbName, function(Blueprint $table) 
            {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->unsignedBigInteger('user_id');
                $table->unsignedBigInteger('offer_id');

	            $table->tinyInteger('type');
	            $table->smallInteger('commission');

                $table->timestamps();
                $table->softDeletes();

	            $table->index(['user_id','offer_id']);
	            $table->unique(['user_id','offer_id']);


            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() 
    {
            Schema::dropIfExists($this->dbName);
    }
}
