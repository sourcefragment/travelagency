<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TemplateTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_types', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name',40);
            $table->string('slug',40);

            $table->timestamps();
            $table->softDeletes();

            $table->unique(['name','slug'],'unique_template_name_slug');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('template_types');
    }
}
