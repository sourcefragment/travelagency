<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Discounts extends Migration
{
	private $dbName = 'discounts';


	public function up()
	{
		if(!Schema::hasTable($this->dbName)):
			Schema::create($this->dbName, function(Blueprint $table)
			{
				$table->engine = 'InnoDB';
				$table->bigIncrements('id');
				$table->unsignedBigInteger('user_id');
				$table->unsignedBigInteger('offer_id');

				$table->string('name',100);
				$table->decimal('value',8,2);
				$table->boolean('status')->default(0);

				$table->timestamps();
				$table->softDeletes();

				$table->index(['user_id']);

			});
		endif;
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists($this->dbName);
	}
}
