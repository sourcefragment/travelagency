<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Invoices extends Migration
{
    private $dbName = 'invoices';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable($this->dbName)):
            Schema::create($this->dbName, function(Blueprint $table) 
            {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');

	            $table->unsignedBigInteger('user_id')->nullable();
	            $table->unsignedBigInteger('type_id');
	            $table->unsignedBigInteger('status_id')->nullable();
	            $table->unsignedBigInteger('booking_id')->nullable();
	            $table->unsignedBigInteger('client_id')->nullable();
	            $table->unsignedBigInteger('payment_status_id')->nullable();
	            $table->unsignedBigInteger('payment_method_id')->nullable();
	            $table->unsignedBigInteger('currency_id')->nullable();
	            $table->unsignedBigInteger('tax_id')->nullable();
	            $table->unsignedBigInteger('billing_city_id')->nullable();


	            $table->string('title')->nullable();
	            $table->string('number');
	            $table->string('prefix')->nullable();
	            $table->string('description')->nullable();

	            //Billing
	            $table->string('billing_name')->nullable();
	            $table->string('billing_email')->nullable();
	            $table->string('billing_phone')->nullable();
	            $table->string('billing_cif')->nullable();
	            $table->string('billing_address')->nullable();
	            $table->string('billing_zipcode')->nullable();


	            //Values
	            $table->decimal('total',10,2)->nullable();
	            $table->decimal('subtotal',10,2)->nullable();
	            $table->tinyInteger('discount')->nullable();
	            $table->decimal('discount_value',5,2)->nullable();
	            $table->date('due_date')->nullable();
	            $table->date('date')->nullable();

                $table->timestamps();
                $table->softDeletes();



            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->dbName);
    }
}
