<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaymentMethods extends Migration
{
	private $dbName = 'payment_methods';


	public function up()
	{
		if(!Schema::hasTable($this->dbName)):
			Schema::create($this->dbName, function(Blueprint $table)
			{
				$table->engine = 'InnoDB';
				$table->bigIncrements('id');
				$table->unsignedBigInteger('user_id');

				$table->string('name',100);
				$table->string('slug',100);
				$table->string('sub_account',20);
				$table->tinyInteger('type')->nullable();
                $table->decimal('commission',5,2)->nullable();
                $table->tinyInteger('commission_percent')->nullable();
				$table->boolean('private')->default(0);
				$table->boolean('status')->default(1);

				$table->timestamps();
				$table->softDeletes();

				$table->index(['user_id','name']);
				$table->unique(['user_id','name'],'unique_payment_method_user_name');

			});
		endif;
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists($this->dbName);
	}
}
