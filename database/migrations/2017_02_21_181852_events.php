<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Events extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
	        $table->unsignedBigInteger('item_id');

	        $table->string('module',20);
	        $table->string('name');
	        $table->string('slug');
	        $table->date('start_date')->nullable();
	        $table->date('end_date')->nullable();
	        $table->boolean('endless')->nullable();
	        $table->string('repeat_mode');
	        $table->smallInteger('repeat_each')->nullable();

	        $table->boolean('monday')->nullable();
	        $table->boolean('tuesday')->nullable();
	        $table->boolean('wednesday')->nullable();
	        $table->boolean('thursday')->nullable();
	        $table->boolean('friday')->nullable();
	        $table->boolean('saturday')->nullable();
	        $table->boolean('sunday')->nullable();


	        $table->boolean('status')->default(1);

            $table->timestamps();
	        $table->softDeletes();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}
