<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfferUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_users', function (Blueprint $table) {

        	$table->engine = 'InnoDB';
            $table->increments('id');
	        $table->unsignedBigInteger('user_id');
	        $table->unsignedBigInteger('offer_id');

	        $table->string('name');
	        $table->string('slug');
	        $table->decimal('net_price',8,2);
	        $table->decimal('pvp_price',8,2);
	        $table->decimal('children_price',8,2);
	        $table->decimal('adult_price',8,2);
	        $table->decimal('student_price',8,2);
	        $table->decimal('senior_price',8,2);
	        $table->decimal('children_lunch_price',8,2);
	        $table->decimal('adult_lunch_price',8,2);
	        $table->decimal('student_lunch_price',8,2);
	        $table->decimal('senior_lunch_price',8,2);

	        $table->integer('book_in_advance')->nullable();
	        $table->boolean('confirmation_required')->default(0);
	        $table->boolean('lunch_included')->default(0);
	        $table->boolean('visa_required')->default(0);
	        $table->boolean('featured')->default(0);
	        $table->boolean('status')->default(0);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('offer_users');
    }
}
