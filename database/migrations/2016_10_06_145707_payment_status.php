<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaymentStatus extends Migration
{
	private $dbName = 'payment_status';


	public function up()
	{
		if(!Schema::hasTable($this->dbName)):
			Schema::create($this->dbName, function(Blueprint $table)
			{
				$table->engine = 'InnoDB';
				$table->bigIncrements('id');
				$table->unsignedBigInteger('user_id')->nullable();

				$table->string('name',20);
				$table->string('slug',20);
				$table->boolean('system')->default(0);
				$table->boolean('status')->default(1);

				$table->timestamps();
				$table->softDeletes();

				$table->unique(['slug']);

			});
		endif;
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists($this->dbName);
	}
}
