<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class States extends Migration
{
    private $dbName = 'states';


    public function up()
    {
        if(!Schema::hasTable($this->dbName)):
            Schema::create($this->dbName, function(Blueprint $table)
            {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
	            $table->unsignedBigInteger('country_id');

	            $table->string('name');

                $table->timestamps();
                $table->softDeletes();

	            $table->index(['country_id']);


            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->dbName);
    }
}
