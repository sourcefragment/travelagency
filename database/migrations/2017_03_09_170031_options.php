<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Options extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('options', function (Blueprint $table) {

	        $table->engine = 'InnoDB';
	        $table->increments('id');

	        $table->string('name');
	        $table->string('key');
	        $table->string('type');
	        $table->string('value')->nullable();
	        $table->string('prefix',4)->nullable();
	        $table->string('color',7)->nullable();
	        $table->boolean('system')->nullable();
	        $table->boolean('status')->default(1);

	        $table->timestamps();
	        $table->softDeletes();

	        $table->unique(['key','type']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('options');
    }
}
