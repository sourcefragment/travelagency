<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfferTypeLocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_type_locations', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedBigInteger('offer_type_id');
            $table->unsignedBigInteger('location_id');

            $table->timestamps();
            $table->softDeletes();

            $table->unique(['offer_type_id','location_id'],'unique_offer_location');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('offer_type_locations');
    }
}
