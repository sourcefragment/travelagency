<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Settings extends Migration
{
	private $dbName = 'settings';


	public function up()
	{
		if(!Schema::hasTable($this->dbName)):
			Schema::create($this->dbName, function(Blueprint $table)
			{
				$table->engine = 'InnoDB';
				$table->bigIncrements('id');
				$table->unsignedBigInteger('user_id');

				$table->string('app_name')->nullable();
				$table->string('logo')->nullable();
				$table->string('primary_color',6)->nullable();
				$table->string('secondary_color',6)->nullable();
				$table->string('print_logo')->nullable();
				$table->string('print_header_bg_color')->nullable();
				$table->string('print_body_bg_color')->nullable();

				$table->timestamps();
				$table->softDeletes();

				$table->index(['user_id']);
				$table->unique(['user_id']);


			});
		endif;
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists($this->dbName);
	}
}
