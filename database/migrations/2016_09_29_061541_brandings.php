<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Brandings extends Migration
{
    private $dbName = 'branding';


    public function up()
    {
        if(!Schema::hasTable($this->dbName)):
            Schema::create($this->dbName, function(Blueprint $table)
            {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->unsignedBigInteger('user_id');
                $table->unsignedBigInteger('app_language_id');

                $table->string('app_homepage',30)->nullable();
                $table->string('app_user_avatar')->nullable();
                $table->string('app_name',20)->nullable();
                $table->string('app_title')->nullable();
                $table->text('app_description')->nullable();
                $table->string('login_bg_color',6)->nullable();
                $table->string('login_box_bg_color',6)->nullable();
                $table->string('login_logo')->nullable();
                $table->text('login_terms_and_conditions')->nullable();
                $table->string('primary_color',6)->nullable();
                $table->string('secondary_color',6)->nullable();
                $table->string('main_logo')->nullable();

                $table->timestamps();
                $table->softDeletes();



            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->dbName);
    }
}
