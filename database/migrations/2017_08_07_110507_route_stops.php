<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RouteStops extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route_stops', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedBigInteger('route_id');
            $table->unsignedBigInteger('stop_id');
            $table->unsignedBigInteger('zone_id');
            $table->smallInteger('order');
            $table->boolean('status')->default(1);

            $table->timestamps();
            $table->softDeletes();
            $table->unique(['route_id','stop_id','zone_id'],'unique_zone_stop');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('route_stops');
    }
}
