<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Templates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void'Malaga',
		    'Marbella',
		    'Fuengirola',
		    'Nerja',
		    'Frigiliana',
		    'Alhaurín de la Torre',
		    'Benahavís',
		    'Casarabonela',
		    'La Cala de Mijas',
		    'Mijas',
		    'Ojén',
		    'San Roque',
		    'Alhaurín el Grande',
		    'Benalmadena',
		    'Casares',
		    'La Duquesa',
		    'Mijas Costa',
		    'Puerto Banús',
		    'Sotogrande',
		    'Álora',
		    'Cabopino',
		    'Coín',
		    'Gaucín',
		    'Monda',
		    'Riviera del Sol',
		    'Torremolinos',
		    'El Faro',
		    'Manilva',
		    'Ronda',
		    'Cádiz',
		    'Gibraltar',
		    'Antequera',
		    'Cártama',
		    'Istán',
		    'Nueva Andalucía',
		    'San Pedro'
     */
    public function up()
    {
        Schema::create('templates', function (Blueprint $table) {

	        $table->engine = 'InnoDB';
	        $table->increments('id');
	        $table->unsignedBigInteger('user_id');

	        $table->string('name');
	        $table->string('slug');
	        $table->string('module',20);
	        $table->string('type',20);
	        $table->text('content')->nullable();
	        $table->smallInteger('margin_left')->nullable();
	        $table->smallInteger('margin_right')->nullable();
	        $table->smallInteger('margin_top')->nullable();
	        $table->smallInteger('margin_bottom')->nullable();
	        $table->smallInteger('margin_header')->nullable();
	        $table->smallInteger('margin_footer')->nullable();

	        $table->boolean('status')->default(1);
	        $table->boolean('system')->nullable();


	        $table->timestamps();
	        $table->softDeletes();

	        $table->index(['user_id','name']);
	        $table->unique(['user_id','name'],'unique_template_name');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('templates');
    }
}
