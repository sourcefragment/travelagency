<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Offers extends Migration
{
    private $dbName = 'offers';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
            if(!Schema::hasTable($this->dbName)):
                Schema::create($this->dbName, function(Blueprint $table)
                {
                    $table->engine = 'InnoDB';
                    $table->bigIncrements('id');
                    $table->unsignedBigInteger('user_id');
                    $table->unsignedBigInteger('tour_operator_id');
                    $table->unsignedBigInteger('offer_type_id');

                    $table->string('name');
                    $table->string('slug');


                    $table->smallInteger('max_baby_age');
                    $table->smallInteger('min_children_age');
                    $table->smallInteger('max_children_age');
                    $table->smallInteger('senior_age');

                    $table->time('min_duration_time')->nullable();
                    $table->time('max_duration_time')->nullable();

                    $table->tinyInteger('book_in_advance_days')->nullable();
                    $table->time('book_in_advance_hours')->nullable();
                    $table->boolean('confirmation_required')->default(0);
                    $table->boolean('lunch_included')->default(0);
                    $table->boolean('visa_required')->default(0);
                    $table->boolean('featured')->default(0);
                    $table->boolean('status')->default(1);

	                $table->string('contact_name',40)->nullable();
	                $table->string('contact_phone',40)->nullable();
	                $table->boolean('require_passenger_details')->default(0);


                    $table->timestamps();
                    $table->softDeletes();

                    $table->index(['user_id','offer_type_id','name']);

	                $table->unique(['offer_type_id','tour_operator_id'],'unique_offer_tour_operator');


                });

            endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::dropIfExists($this->dbName);
    }
}