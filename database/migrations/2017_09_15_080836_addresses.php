<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Addresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('client_id');
			$table->unsignedBigInteger('city_id');

            $table->string('type',20);
            $table->string('address',200);
            $table->string('zipcode',10)->nullable();
            $table->string('location',20)->nullable();
            $table->string('phone',20)->nullable();


            $table->timestamps();
            $table->softDeletes();

            $table->unique(['client_id','type'],'unique_client_address');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('addresses', function (Blueprint $table) {
            //
        });
    }
}
