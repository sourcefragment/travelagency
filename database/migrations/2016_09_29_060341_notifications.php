<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Notifications extends Migration
{
    private $dbName = 'notifications';


    public function up()
    {
        if(!Schema::hasTable($this->dbName)):
            Schema::create($this->dbName, function(Blueprint $table)
            {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->unsignedBigInteger('sender_id');
                $table->unsignedBigInteger('receiver_id');

                $table->string('type',20);
                $table->string('event',20);
                $table->string('subject');
                $table->string('recipient');
                $table->string('bcc');
                $table->text('message');
                $table->time('deliver_time');
                $table->boolean('scheduled_deliver');
                $table->boolean('html_format');
                $table->boolean('text_format');
                $table->boolean('status')->default(1);
                $table->boolean('read')->default(0);
                $table->boolean('important')->default(0);

                $table->timestamps();
                $table->softDeletes();

	            $table->index(['sender_id','receiver_id']);

            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->dbName);
    }
}
