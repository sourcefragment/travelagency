<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfferTypeAvailability extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_type_availability', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedBigInteger('offer_type_id');

            $table->date('date');

            $table->timestamps();
            $table->softDeletes();
            $table->unique(['offer_type_id','date'],'unique_offer_type_date');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('offer_type_availability');
    }
}
