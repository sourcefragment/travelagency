<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Clients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {

	        $table->engine = 'InnoDB';
	        $table->increments('id');
	        $table->unsignedBigInteger('user_id');
	        $table->unsignedBigInteger('company_id');
	        $table->unsignedBigInteger('language_id')->nullable();
	        $table->unsignedBigInteger('city_id')->nullable();
	        $table->unsignedBigInteger('billing_city_id')->nullable();


	        $table->string('treatment',6)->nullable();//mr,mrs,dr,ms,sr,prof
	        $table->string('name',40);
	        $table->string('last_name',40)->nullable();
	        $table->string('nif',20)->nullable();
	        $table->string('email')->nullable();
	        $table->string('password',60)->nullable();
	        $table->string('phone',20)->nullable();
	        $table->string('mobile',20)->nullable();
	        $table->string('fax',20)->nullable();
	        $table->string('address')->nullable();
	        $table->string('second_address')->nullable();
	        $table->string('location')->nullable();
	        $table->string('passport',40)->nullable();
	        $table->date('passport_date')->nullable();
	        $table->string('zipcode',20)->nullable();
	        $table->date('dob')->nullable();
	        $table->text('overview')->nullable();


	        //Billing details
	        $table->string('billing_name',40)->nullable();
	        $table->string('billing_nif',20)->nullable();
	        $table->string('billing_address')->nullable();
	        $table->string('billing_second_address')->nullable();
	        $table->string('billing_location',40)->nullable();
	        $table->string('billing_zip',10)->nullable();
	        $table->string('billing_phone',20)->nullable();
	        $table->boolean('different_billing')->nullable();

	        $table->timestamp('last_login')->nullable();

	        $table->boolean('status')->nullable();

	        //Register data
	        $table->boolean('auto_register')->nullable();
	        $table->boolean('is_passenger')->nullable();
	        $table->string('register_url')->nullable();


	        $table->timestamps();
	        $table->softDeletes();

	        $table->unique(['email']);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clients');
    }
}
