<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Locations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {

        	$table->engine = 'InnoDB';
            $table->increments('id');
	        $table->unsignedBigInteger('user_id');
	        $table->unsignedBigInteger('parent_id');
	        $table->unsignedBigInteger('city_id');

	        $table->string('name');
	        $table->string('slug');
	        $table->integer('order')->nullable();
	        $table->boolean('featured')->default(0);
	        $table->boolean('status')->default(1);


            $table->timestamps();
	        $table->softDeletes();

	        $table->unique(['name','slug']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('locations');
    }
}
