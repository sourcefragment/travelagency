<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfferPrices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('offer_prices', function (Blueprint $table) {
		    $table->increments('id');
		    $table->unsignedBigInteger('offer_id');

		    $table->string('type',20);
		    $table->decimal('net_price',8,2)->default(0);
		    $table->decimal('pvp_price',8,2)->default(0);
		    $table->decimal('discount_price',8,2)->default(0);
		    $table->tinyInteger('discount')->default(0);


		    $table->timestamps();
		    $table->softDeletes();

		    $table->unique(['offer_id','type'],'unique_offer_price');

	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('offer_prices', function (Blueprint $table) {
            //
        });
    }
}
