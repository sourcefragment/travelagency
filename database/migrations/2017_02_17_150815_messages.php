<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Messages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {

        	$table->engine = 'InnoDB';
        	$table->increments('id');
	        $table->unsignedBigInteger('sender_id');
	        $table->unsignedBigInteger('receiver_id');

	        $table->string('subject');
	        $table->text('message')->nullable();
	        $table->boolean('is_html')->default(0);
	        $table->dateTime('read_at')->nullable();
	        $table->boolean('status')->default(0);


            $table->timestamps();
	        $table->softDeletes();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('messages');
    }
}
