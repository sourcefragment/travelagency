<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfferCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_categories', function (Blueprint $table) {
        	
        	$table->engine = 'InnoDB';
            $table->increments('id');
	        $table->unsignedBigInteger('offer_id');
	        $table->unsignedBigInteger('category_id');
	        
	        $table->integer('order')->default(0);

            $table->timestamps();
	        $table->softDeletes();
	        
	        $table->index(['offer_id','category_id']);
	        $table->unique(['offer_id','category_id']);
	        
	        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('offer_categories');
    }
}
