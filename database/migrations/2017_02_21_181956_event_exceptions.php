<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventExceptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_exceptions', function (Blueprint $table) {
            $table->increments('id');
	        $table->unsignedBigInteger('event_id');

	        $table->date('star_date')->nullable();
	        $table->date('end_date')->nullabel();
	        $table->boolean('is_rescheduled')->default(0);
	        $table->boolean('is_cancelled')->default(0);

            $table->timestamps();
	        $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_exceptions');
    }
}
