<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Routes extends Migration
{
    private $dbName = 'routes';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            if(!Schema::hasTable($this->dbName)):
                Schema::create($this->dbName, function(Blueprint $table)
                {
                    $table->engine = 'InnoDB';
                    $table->bigIncrements('id');
                    $table->unsignedBigInteger('offer_id');
                    $table->unsignedBigInteger('user_id');
                    $table->unsignedBigInteger('location_id')->nullable();
                    $table->unsignedBigInteger('city_id')->nullable();


                    $table->string('name');
                    $table->string('slug');
                    $table->text('description')->nullable();
                    $table->boolean('status')->default(1);
                    $table->time('departure_time')->nullable();

                    $table->timestamps();
                    $table->softDeletes();

                    $table->index(['offer_id','name']);

                });
            endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
            Schema::dropIfExists($this->dbName);
    }
}