<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Permissions extends Migration
{
    private $dbName = 'permissions';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if(!Schema::hasTable($this->dbName)):
            Schema::create($this->dbName, function(Blueprint $table) 
            {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->unsignedBigInteger('role_id');

                $table->string('section',30);
                $table->boolean('c')->default(0);
                $table->boolean('r')->default(0);
                $table->boolean('u')->default(0);
                $table->boolean('d')->default(0);

                $table->timestamps();
                $table->softDeletes();

	             $table->unique(['role_id','section']);
	            $table->index(['role_id','section']);

            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->dbName);
    }
}
