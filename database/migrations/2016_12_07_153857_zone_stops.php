<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ZoneStops extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zone_stops', function (Blueprint $table) {

        	$table->engine = 'InnoDB';
        	$table->increments('id');
	        $table->unsignedBigInteger('zone_id');
	        $table->unsignedBigInteger('stop_id');

	        $table->tinyInteger('order');

            $table->timestamps();
	        $table->softDeletes();

	        $table->unique(['zone_id','stop_id'],'unique_zone_stops');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('zone_stops');
    }
}
