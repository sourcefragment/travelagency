<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RouteDates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route_meta', function (Blueprint $table) {

        	$table->engine = 'InnoDB';
            $table->increments('id');
	        $table->bigInteger('route_id')->unsigned();

	        $table->timestamp('from');
	        $table->timestamp('to')->nullable();
	        $table->timestamp('interval')->nullable();
	        $table->smallInteger('year')->nullable();
	        $table->tinyInteger('month')->nullable();
	        $table->tinyInteger('day')->nullable();
	        $table->tinyInteger('week')->nullable();
	        $table->tinyInteger('weekday')->nullable();

            $table->timestamps();

	        $table->index(['route_id','from']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('route_meta');
    }
}
