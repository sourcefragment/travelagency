<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Bookings extends Migration
{
    private $dbName = 'bookings';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
              if(!Schema::hasTable($this->dbName)):
                  Schema::create($this->dbName, function(Blueprint $table)
                  {
                      $table->engine = 'InnoDB';
                      $table->bigIncrements('id');
                      $table->unsignedBigInteger('offer_type_id');
	                  $table->unsignedBigInteger('stop_id');
	                  $table->unsignedBigInteger('zone_id');
	                  $table->unsignedBigInteger('user_id');
	                  $table->unsignedBigInteger('client_id');
	                  $table->unsignedBigInteger('agent_id')->nullable();
	                  $table->unsignedBigInteger('offer_id')->nullable();
	                  $table->unsignedBigInteger('language_id')->nullable();
	                  $table->unsignedBigInteger('tax_id')->nullable();
	                  $table->unsignedBigInteger('status_id')->nullable();


	                  $table->string('code');
                      $table->dateTime('date');
                      $table->decimal('subtotal',10,2)->default(0);
                      $table->tinyInteger('discount')->default(0);
                      $table->decimal('discount_value',10,2)->default(0);
                      $table->decimal('tax_price',10,2)->default(0);
                      $table->decimal('total',10,2)->default(0);

                      //Comments
                      $table->text('tour_operator_comments')->nullable();
                      $table->text('client_comments')->nullable();
                      $table->text('internal_comments')->nullable();

                      //details
                      $table->tinyInteger('total_adults')->default(0);
                      $table->tinyInteger('total_children')->default(0);
                      $table->tinyInteger('total_babies')->default(0);
                      $table->tinyInteger('total_seniors')->default(0);
                      $table->tinyInteger('total_lunch_adults')->default(0);
                      $table->tinyInteger('total_lunch_children')->default(0);

                      //Prices
                      $table->decimal('adult_price',8,2)->default(0);
                      $table->decimal('children_price',8,2)->default(0);
                      $table->decimal('senior_price',8,2)->default(0);
                      $table->decimal('adult_lunch_price',8,2)->default(0);
                      $table->decimal('children_lunch_price',8,2)->default(0);



                      $table->timestamps();
                      $table->softDeletes();

                      $table->index(['offer_id','user_id','client_id']);

                  });
              endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->dbName);
    }
}