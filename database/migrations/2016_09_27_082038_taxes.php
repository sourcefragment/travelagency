<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Taxes extends Migration
{
	private $dbName = 'taxes';


	public function up()
	{
		if(!Schema::hasTable($this->dbName)):
			Schema::create($this->dbName, function(Blueprint $table)
			{
				$table->engine = 'InnoDB';
				$table->bigIncrements('id');
				$table->unsignedBigInteger('user_id');

				$table->string('name');
				$table->string('prefix',10)->nullable();
				$table->string('slug');

				$table->tinyInteger('type')->nullable();
				$table->string('account_number',40)->nullable();
				$table->decimal('value',5,2)->default(0);
				$table->boolean('default')->default(0);
				$table->boolean('status')->default(1);

				$table->timestamps();
				$table->softDeletes();

				$table->index(['name','type']);
				$table->unique(['slug']);


			});
		endif;
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists($this->dbName);
	}
}
