<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Payments extends Migration
{
    private $dbName = 'payments';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if(!Schema::hasTable($this->dbName)):
            Schema::create($this->dbName, function(Blueprint $table) 
            {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->unsignedBigInteger('item_id');
                $table->unsignedBigInteger('payment_method_id');
                $table->unsignedBigInteger('payment_status_id');
                $table->unsignedBigInteger('client_id');
                $table->unsignedBigInteger('user_id');

                $table->string('module',20);
                $table->string('token');
                $table->date('date');
                $table->decimal('total',10,2);
                $table->string('observations');

                $table->timestamps();
                $table->softDeletes();

	            $table->index(['item_id','module']);

            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->dbName);
    }
}
