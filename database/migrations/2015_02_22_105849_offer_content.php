<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfferContent extends Migration
{
    private $dbName = 'offer_content';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
            if(!Schema::hasTable($this->dbName)):
                Schema::create($this->dbName, function(Blueprint $table)
                {
                    $table->engine = 'InnoDB';
                    $table->bigIncrements('id');
                    $table->unsignedBigInteger('offer_id');
                    $table->unsignedBigInteger('language_id');

                    $table->string('title');
                    $table->text('short_description')->nullable();
                    $table->text('long_description')->nullable();
                    $table->text('ticket_description')->nullable();

                    $table->timestamps();
                    $table->softDeletes();

                    $table->index(['offer_id','language_id']);

                });
            endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
            Schema::dropIfExists($this->dbName);
    }
}