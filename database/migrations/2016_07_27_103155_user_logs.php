<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserLogs extends Migration
{
    private $dbName = 'user_logs';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable($this->dbName)):
            Schema::create($this->dbName, function(Blueprint $table) 
            {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->unsignedBigInteger('user_id');

                $table->string('method',30)->nullable();
                $table->string('action',50)->nullable();
                $table->string('ip',16)->nullable();
                $table->string('browser',40)->nullable();

                $table->timestamps();
                $table->softDeletes();

	            $table->index(['user_id']);

            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->dbName);
    }
}
