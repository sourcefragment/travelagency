<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfferExtra extends Migration
{
    private $dbName = 'offer_extra';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
            if(!Schema::hasTable($this->dbName)):
                Schema::create($this->dbName, function(Blueprint $table)
                {
                    $table->engine = 'InnoDB';
                    $table->bigIncrements('id');
                    $table->unsignedBigInteger('offer_id');
                    $table->unsignedBigInteger('tax_id')->nullable();
	                $table->unsignedBigInteger('item_id')->nullable();

                    $table->boolean('confirmation_required')->default(0);
                    $table->decimal('pvp_price',10,2)->nullable();
                    $table->decimal('net_price',10,2)->nullable();
                    $table->string('price_type',20)->nullable();
                    $table->tinyInteger('discount')->nullable();
	                $table->boolean('included')->nullable();

                    $table->timestamps();
                    $table->softDeletes();

                    $table->index(['offer_id']);
	                $table->unique(['offer_id','item_id'],'unique_offer_item');

                });
            endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::dropIfExists($this->dbName);
    }
}