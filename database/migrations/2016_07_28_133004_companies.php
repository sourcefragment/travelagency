<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Companies extends Migration
{
    private $dbName = 'companies';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if(!Schema::hasTable($this->dbName)):
            Schema::create($this->dbName, function(Blueprint $table) 
            {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->unsignedBigInteger('user_id'); //create de company
	            $table->unsignedBigInteger('city_id')->nullable();

	            $table->string('name');
	            $table->string('phone', 30)->nullable();
	            $table->string('fax', 30)->nullable();
	            $table->string('email')->nullable();
	            $table->string('cif', 20)->nullable();
	            $table->string('invoice_address')->nullable();
                $table->string('zipcode', 20)->nullable();
                $table->string('webpage')->nullable();

	            $table->string('billing_name',40)->nullable();
	            $table->string('billing_nif',20)->nullable();
	            $table->string('billing_address')->nullable();
	            $table->string('billing_second_address')->nullable();
	            $table->string('billing_location',40)->nullable();
	            $table->string('billing_zip',10)->nullable();
	            $table->string('billing_phone',20)->nullable();
	            $table->boolean('different_billing')->nullable();

                $table->timestamps();
                $table->softDeletes();

	            $table->index(['user_id','name']);
	            $table->unique(['user_id','name','email']);


            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->dbName);
    }
}
