<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfferTypeCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_type_categories', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedBigInteger('offer_type_id');
            $table->unsignedBigInteger('category_id');

            $table->timestamps();
            $table->softDeletes();

            $table->unique(['offer_type_id','category_id'],'unique_offer_category');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('offer_type_categories');
    }
}
