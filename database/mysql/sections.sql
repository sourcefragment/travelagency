

INSERT INTO `sections` (`id`,`name`, `slug`,`menu_order`,`parent_id`,`icon`) VALUES
(1,'Dashboard','dashboard',1,0,'fa-dashboard'),
#(2,'Planner','planner',2,0,'fa-tasks'),
(3,'Bookings','bookings',3,0,'fa-calendar'),
(4,'Tours','tours',4,0,'fa-bus'),
(5,'Offers','offers',2,4,'fa-newspaper-o'),
(6,'Categories','categories',6,4,'fa-slack'),
(7,'Zones','zones',4,4,'fa-bullseye'),
(8,'Routes','routes',3,4,'fa-road'),
(9,'Stops','stops',5,4,'fa-map-marker'),
(10,'Users','users',11,0,'fa-users'),
(11,'Contacts','contacts',6,0,'fa-users'),
(12,'Accounting','accounting',7,0,'fa-calculator'),
(13,'Invoices','invoices',1,12,'fa-file-text'),
(14,'Payment Methods','payment-methods',2,12,'fa-money'),
(15,'Taxes','taxes',4,12,'fa-credit-card'),
(16,'Settings','settings',9,0,'fa-cogs'),
(17,'Notifications','notifications',8,0,'fa-bell'),
(18,'Roles','roles',10,16,'fa-sitemap'),
(19,'Companies','companies',10,16,'fa-university'),
(20,'Templates','templates',10,16,'fa-file-code-o'),
(21,'Locations','locations',7,4,'fa-globe'),
(22,'Payment Status','payment-status',3,12,'fa-money'),
(23,'Types','offer-types',1,4,'fa-ticket'),
(26,'Options','options',0,16,'fa-list-ul'),
(27,'Clients','clients',0,11,'fa-user'),
(28,'Tour Operators','tour-operators',0,11,'fa-rocket'),
(29,'Agents','agents',0,11,'fa-briefcase'),
(30,'Items','items',9,4,'fa-puzzle-piece'),
(31,'General','general',10,16,'fa-wrench');


