INSERT INTO `options` (`name`, `key`,`value`,`type`,`system`,`status`,`color`) VALUES
('Pending','pending','','booking-status',1,1,'#f8ac59'),
('Confirmed','confirmed','','booking-status',1,1,'#1c84c6'),
('Cancelled','cancelled','','booking-status',1,1,'#ed5565'),
('Rejected','rejected','','booking-status',1,1,'#ed5565'),
('Paid','paid','','booking-status',1,1,'#23c6c8'),
('Paid','paid','','invoice-status',1,1,'#23c6c8'),
('Cancelled','cancelled','','invoice-status',1,1,'#ed5565'),
('Sent','sent','','invoice-status',1,1,'#23c6c8'),
('Pending','pending','','invoice-status',1,1,'#f8ac59'),
('Simple','simple','','invoice-type',1,1,''),
('Rectification','rectification','','invoice-type',1,1,'');