var gulp = require('gulp'),
    concat = require('gulp-concat'),
    minify = require('gulp-minify'),
    notify = require('gulp-notify'),
    cleanCSS = require('gulp-clean-css'),
    // watch = require('gulp-watch'),
    runSequence = require('run-sequence'),
    plumber = require('gulp-plumber'),
	sass = require('gulp-sass');
	// sourcemaps = require('gulp-sourcemaps');

gulp.task('library-css', function () {
    var css = [
        'bootstrap/bootstrap.css',
        'plugins/toastr/toaster.css',
        'plugins/sweetalert/sweetalert.css',
        'font-awesome/css/font-awesome.min.css',
        'plugins/ui-select/select.min.css',
        'plugins/switchery/switchery.css',
        'plugins/datapicker/angular-datapicker.css',
        'plugins/daterangepicker/daterangepicker-bs3.css',
        'plugins/clockpicker/clockpicker.css',
        'plugins/fullcalendar/dist/fullcalendar.min.css',
        'plugins/summernote/summernote.css',
        'plugins/summernote/summernote-bs3.css',
        'plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
        'plugins/colorpicker/colorpicker.css',
        'plugins/ngImgCrop/ng-img-crop.css',
        'theme/animate.css',
	    'plugins/iCheck/custom.css',
        'theme/style.css',
	    'plugins/dropzone/css/basic.css',
	    'plugins/ng-table/ng-table.min.css',
	    'plugins/touchspin/jquery.bootstrap-touchspin.min.css',
        'plugins/angular-datepicker/angular-datepicker.min.css'

    ];

    return gulp.src(css,{cwd:'public/app/lib/'})
        .pipe(concat('library.css'))
        .pipe(gulp.dest('public/app/dist'))
        .pipe(cleanCSS({
            compatibility: 'ie8',
            keepSpecialComments: 0
        }))
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }));

});
gulp.task('library-js', function () {

    var scripts = [
        'jquery/jquery.min.js',
        'plugins/jquery-ui/jquery-ui.min.js',
        'bootstrap/bootstrap.min.js',
        'plugins/metisMenu/jquery.metisMenu.js',
        'plugins/slimscroll/jquery.slimscroll.min.js',
        'plugins/pace/pace.min.js',
        'theme/inspinia.js',
        'angular/angular.js',
        'angular/angular-sanitize.js',
        'plugins/angular-animate/angular-animate.js',
        'plugins/oclazyload/dist/ocLazyLoad.min.js',
        'angular-translate/angular-translate.min.js',
        'ui-router/angular-ui-router.min.js',
        'bootstrap/ui-bootstrap-tpls-1.1.2.min.js',
        'plugins/toastr/toaster.js',
        'plugins/sweetalert/angular-sweetalert.min.js',
        'plugins/sweetalert/sweetalert.min.js',
        'plugins/moment/moment.min.js',
        'plugins/switchery/switchery.js',
        'plugins/switchery/ng-switchery.js',
        'plugins/ui-calendar/calendar.js',
        'plugins/fullcalendar/dist/fullcalendar.min.js',
        'plugins/fullcalendar/dist/gcal.js',
        'plugins/datapicker/angular-datepicker.js',
        'plugins/daterangepicker/daterangepicker.js',
        'plugins/daterangepicker/angular-daterangepicker.js',
        'plugins/clockpicker/clockpicker.js',
	    'plugins/uievents/event.js',
        'plugins/summernote/summernote.min.js',
        'plugins/summernote/angular-summernote.min.js',
        'plugins/angular-idle/angular-idle.js',
        'satellizer/satellizer.min.js',
	    'plugins/ui-select/select.min.js',
	    'plugins/touchspin/jquery.bootstrap-touchspin.min.js',
        'plugins/iCheck/icheck.min.js',
        'plugins/colorpicker/bootstrap-colorpicker-module.js',
        'plugins/ngImgCrop/ng-img-crop.js',
	    'ngmap/ng-map.min.js',
	    'plugins/ui-sortable/sortable.js',
	    'plugins/dropzone/dropzone.min.js',
	    'plugins/dropzone/angular-dropzone.js',
	    'plugins/blueimp/blueimp.min.js',
	    'plugins/ng-table/ng-table.min.js',
        'plugins/angular-datepicker/angular-datepicker.min.js'
    ];


    return gulp.src(scripts,{cwd:'public/app/lib/'})
        .pipe(concat('library.js', {newLine: ';'}))
        .pipe(gulp.dest('public/app/dist'));

});

gulp.task('general-css', function () {

    var css = [
        'public/app/dist/library.css',
        'public/app/lib/config/style.css'
    ];

    return gulp.src(css)
        .pipe(concat('general.css'))
        .pipe(cleanCSS({
            compatibility: 'ie8',
            keepSpecialComments: 0
        }))
        .pipe(gulp.dest('public/app/dist'))
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }));

});

gulp.task('custom-css',function(){

	var files = [
		'public/app/css/scss/styles.scss'
	];

	return gulp.src(files)
		.pipe(sass({outputStyle:'compressed'}).on('error',sass.logError))
		.pipe(gulp.dest('public/app/css'));

});



gulp.task('general-js', function () {

    var scripts = [
        'public/app/dist/library.js',
        'public/app/script.js'

    ];


    return gulp.src(scripts)
        .pipe(concat('general.js'))
        .pipe(minify({
            ext: {
                src: '-debug.js',
                min: '.js'
            }
        }))
        .pipe(gulp.dest('public/app/dist'))
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }));
});


//App modules
gulp.task('modules', function () {

	var modules = fs.readdirSync('.public/app/js');

	for( var i in modules ){
		var dir = modules[i];
	}

});


gulp.task('general', function (callback) {
    runSequence('general-css',
        'general-js',
        callback);
});

gulp.task('library', function (callback) {
    runSequence('library-css',
        'library-js',
        callback);
});


gulp.task('login-js',function (callback) {

	var files = [
		'jquery/jquery.min.js',
		'angular/angular.js',
		'plugins/angular-idle/angular-idle.js',
		'plugins/oclazyload/dist/ocLazyLoad.min.js',
		'angular-translate/angular-translate.min.js',
		'ui-router/angular-ui-router.min.js',
		'plugins/angular-notify/angular-notify.min.js',
		'plugins/angular-animate/angular-animate.js',
		'plugins/toastr/toaster.js',
		'satellizer/satellizer.min.js'
	];


	return gulp.src(files,{cwd:'public/app/lib/'})
		.pipe(concat('login.js', {newLine: ';'}))
		.pipe(gulp.dest('public/app/dist'));

});
gulp.task('default',['general']);
