<?php

return [
    /** BOTONES **/
    'index sin _'                                                                 => 'inicio',
    'create'                                                                => 'crear',
    'add'                                                                   => 'añadir',

    /** MENSAJES **/
    'create_success'                                                        => 'Zona creada con éxito',
    'create_fail'                                                           => 'Fallo al crear la zona',
    'update_success'                                                        => 'Zona actualizada con éxito',
    'update_fail'                                                           => 'Fallo al actualizar la zona',
    'delete_success'                                                        => 'Zona borrada con éxito',
    'delete_fail'                                                           => 'Fallo al borrar la zona',

    /** TÍTULOS **/
    'area_updating'                                                         => 'Actualizando zona',
    'area_creating'                                                         => 'Creando zona',
    'languages'                                                             => 'idiomas',

    'last_update'                                                           => 'última modificación',
    'show'                                                                  => 'mostrar',
    'areas_listed'                                                          => 'listado de zonas',
    'edit'                                                                  => 'editar',
    'update'                                                                => 'actualizar',
    'delete'                                                                => 'borrar',
    'delete_confirm_title'                                                  => 'Borrando zona...',
    'delete_confirm_text'                                                   => 'Vas a borrar la zona y todo lo relacionado a ella. ¿Estás seguro?',
    'delete_confirm_confirm_button'                                         => 'Sí, lo estoy.',
    'delete_confirm_cancel_button'                                          => 'No, cancelar acción.',
    'name'                                                                  => 'nombre',
    'placeholder_name'                                                      => 'nombre',
    'text_language'                                                         => 'idioma textos',
    'title_pdf'                                                             => 'Listado de zonas',
    'show_in_google_maps'                                                   => 'Ver en google maps',
    'search'                                                                => 'Buscar',
    'placeholder_search'                                                    => 'Buscar',
    'information'                                                           => 'información',
    'stops'                                                                 => 'paradas',
    'add_stop'                                                              => 'añadir parada',
    'route'                                                                 => 'Zonas',
    'create_area'                                                           => 'crear zona',
    'region'                                                                => 'región',
    'country'                                                               => 'país',
    'latitude'                                                              => 'latitud',
    'longitude'                                                             => 'longitud',
    'quit'                                                                  => 'quitar',

    /** ATRIBUTOS **/
    'added_at'                                                              => 'Añadida el',
    'language'                                                              => 'idioma',
    'updated_at'                                                            => 'modificada el',
];
