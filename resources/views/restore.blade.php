<!DOCTYPE html>
<html ng-app="app" lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>
        {{ $app_name }} | {{ trans('restore') }}
    </title>

    {{-- Estilos principales de la aplicación --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('app/dist/library.css') }}" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300|Titillium+Web:200,300,400' rel='stylesheet' type='text/css'>

    <link type="image/x-icon" href="{{ asset('backend/img/favicon.png') }}" rel="shortcut icon"/>

</head>
<body ng-controller="RestoreController as $ctrl" class="pace-done gray-bg" landing-scrollspy="" id="page-top">
    <div class="row">
        <div class="col-md-12">
            <div id="login-box">
                <div class="row">
                    <div class="col-md-12">
                        
                        <header id="login-header">
                            <div id="login-logo">
                                <img src="{{ asset('backend/img/logo.png') }}" alt="" />
                            </div>
                        </header>
                        
                        <div id="login-box-inner">
                            <form role="form" >
                                
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-envelope"></i>
                                    </span>
                                    <input class="form-control" type="email" name="email" value="{{ old("email") }}" placeholder="{{ trans('login.email') }}">
                                </div>
                                
                                <div class="row">
                                    <div class="col-xs-12">
                                        <button type="submit" class="btn btn-success col-xs-12">
                                            {{ trans('login.restore') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script src="{{ asset('app/dist/login.js') }}"></script>
    
    <script src="{{asset("app/js/login/app.js")}}"></script>
    <script src="{{asset("app/js/login/translations.js")}}"></script>
    <script src="{{asset("app/js/login/controllers.js")}}"></script>

</body>
</html>
