<!DOCTYPE html>
<html ng-app="app" lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title>
			{{ $app_name }} | {{ trans('login.register') }}
		</title>

		{{-- Estilos principales de la aplicación --}}
		<link rel="stylesheet" type="text/css" href="{{ asset("css/dist/library.css") }}" />
		<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300|Titillium+Web:200,300,400' rel='stylesheet' type='text/css'>

		<link type="image/x-icon" href="{{ asset("backend/img/favicon.png") }}" rel="shortcut icon"/>

	</head>
	
	<body ng-controller="MainController as main" class="{{$state.current.data.specialClass}}" landing-scrollspy id="page-top">
	
		<!-- Main view  -->
		<div ui-view></div>
		
		
		<script src="{{ asset('app/dist/login.js') }}"></script>
		
		<script src="{{asset("app/js/login/app.js")}}"></script>
		<script src="{{asset("app/js/login/translations.js")}}"></script>
		<script src="{{asset("app/js/login/controllers.js")}}"></script>
	</body>
</html>

