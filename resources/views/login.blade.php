 <!DOCTYPE html>
<html ng-app="login" lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>
            {{ $app_name }} | Login
        </title>

        {{-- Estilos principales de la aplicación --}}
        <link rel="stylesheet" type="text/css" href="{{ asset("app/dist/general.css") }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset("app/css/login.css") }}" />
        <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300|Titillium+Web:200,300,400' rel='stylesheet' type='text/css'>

        {{--<link type="image/x-icon" href="{{asset("app/img/favicon.png")}}" rel="shortcut icon"/>--}}
        <script type="text/javascript">
            {{-- translations --}}
            var Lang = {!! $lang !!};
	        var APP_NAME = '{{ $app_name }}';
            var APP_DESCRIPTION = '{{ $app_description }}';
            var APP_LOGO = '{{ $app_logo }}';

            var BASE_URL = '{{ url('') }}';
//	        console.log(Lang);
        </script>

    </head>
    <toaster-container toaster-options="
        {'time-out':{ 'toast-success': 5,'toast-warning': 10, 'toast-error': 0 } }">
    </toaster-container>
    

    <body ng-controller="MainCtrl as main" class="login-bg" landing-scrollspy id="page-top">

    <!-- Main view  -->
    <div ui-view></div>


    <script src="{{ asset('app/dist/login.js') }}"></script>

    <script src="{{asset("app/js/login/app.js")}}"></script>
    <script src="{{asset("app/js/login/translations.js")}}"></script>
    <script src="{{asset("app/js/login/controllers.js")}}"></script>
    </body>
 
</html>