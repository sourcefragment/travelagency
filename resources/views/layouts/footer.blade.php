{{--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQTpXj82d8UpCi97wzo_nKXL7nYrd4G70"></script>--}}
{{--<script src="http://maps.google.com/maps/api/js?key=AIzaSyDQTpXj82d8UpCi97wzo_nKXL7nYrd4G70"></script>--}}
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC75H_jt1PknXd9a-6qpk7t8RhpKtWzxJw">
</script>
<!--Library-->
<script src="{{ asset('app/dist/library.js?v=2')  }}"></script>

@if( env('APP_ENV') == 'development' )
	
	
	
	
	
    <!--Global-->
    <script src="{{ asset('app/js/app.js') }}"></script>
    <script src="{{ asset('app/js/common/config.js') }}"></script>
    <script src="{{ asset('app/js/common/translations.js') }}"></script>
    <script src="{{ asset('app/js/common/controllers.js') }}"></script>
    <script src="{{ asset('app/js/common/directives.js') }}"></script>
    <script src="{{ asset('app/js/common/directives/toasterDirective.js') }}"></script>
    <script src="{{ asset('app/js/common/filters.js') }}"></script>
    <script src="{{ asset('app/js/common/services/httpInterceptor.js')  }}"></script>
    <script src="{{ asset('app/js/modules/modules.js')  }}"></script>
    <script src="{{ asset('app/js/common/services/apiService.js')  }}"></script>


    <!--Load  available modules first-->
    @foreach ( $modules as $key => $module )
	    {!! "<!--$key-->" !!}
	    @foreach( $module as $file)
		    <script src="app/js/modules/{{ $key }}/{{ $file }}"></script>
	    @endforeach
    @endforeach
    

@else
    
	<script src="{{ asset('app/dist/general.js') }}"></script>

@endif
