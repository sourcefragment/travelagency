<div class="footer">
    <div>
        <strong>Powered by <a href="http://sebcreativos.es">SEBCreativos</a></strong> © 2016
    </div>
</div>

<!--table pagination template-->
<script type="text/ng-template" id="custom/pager">
	
	<div class="row">
		<div class="col-sm-6">
			<ul class="pagination pagination-sm" ng-if="params.pages.length">
				<li
						ng-switch="page.type"
						ng-class="{ 'disabled' : !page.active && !page.current }"
						ng-repeat="page in params.pages">
					<a class="page-link" ng-switch-when="prev" ng-click="params.page(page.number)">
						<i class="fa fa-chevron-left"></i>
					</a>
					<a class="page-link" ng-switch-when="next" ng-click="params.page(page.number)">
						<i class="fa fa-chevron-right"></i>
					</a>
				</li>
				<li class="pagination-details">[[ params.current ]] [[ 'of'|translate ]] [[ params.total() ]]</li>
			</ul>
			<span class="pagination-total">[[ 'total_found'|translate ]] : [[ params.total() ]]</span>
		</div>
		
		<div class="col-sm-6">
			<ul class="pagination pagination-sm pull-right">
				<li class="pagination-page"
				    ng-repeat="limit in params.settings().counts"
				    ng-class="{ 'active' : limit == params.count()}"
				>
					<a ng-click="params.count(limit)">[[ limit ]]</a>
				</li>
			</ul>
		</div>
	</div>
</script>

<!--table custom text-->
<script type="text/ng-template" id="custom/text">
	<input
		type="text" name="[[ name ]]"
		ng-disabled="$filterRow.disabled"
		ng-model="params.filter()[name]"
		class="input-filter form-control"
		placeholder="[[ getFilterPlaceholderValue(filter, name) ]]"/>
</script>

<!--table custom number-->
<script type="text/ng-template" id="custom/number">
	<input
		type="number"
		name="[[name]]"
		ng-disabled="$filterRow.disabled"
		ng-model="params.filter()[name]"
		class="input-filter form-control"
		placeholder="[[ getFilterPlaceholderValue(filter, name)]]"/>
</script>

<!--table custom select-->
<script type="text/ng-template" id="custom/select">
	<select
		ng-options="data.id as data.title for data in $selectData"
		ng-table-select-filter-ds="$column"
		ng-disabled="$filterRow.disabled"
		ng-model="params.filter()[name]"
		class="filter filter-select form-control" name="[[ name]] ">
			<option style="display:none" value=""></option>
	</select>
</script>

<!--table custom role selector-->
<script type="text/ng-template" id="custom/roles">
	<select
			ng-options="data.id as data.name for data in params.data"
			ng-table-select-filter-ds="$column"
			ng-disabled="$filterRow.disabled"
			ng-model="params.filter()[name]"
			class="filter filter-select form-control" name="[[ name ]]">
		<option style="" value=""></option>
	</select>
</script>

<!--table custom offer type select-->
<script type="text/ng-template" id="custom/offer-types">
	<select
			ng-options="data.id as data.name for data in offerTypes"
			ng-table-select-filter-ds="$column"
			ng-disabled="$filterRow.disabled"
			ng-model="params.filter()[name]"
			class="filter filter-select form-control" name="[[ name ]]">
		<option style="" value=""></option>
	</select>
</script>

<script id="custom/header" type="text/ng-template">
	<tr>
		<th title="[[ $column.headerTitle(this) ]]"
		    ng-repeat="$column in $columns"
		    ng-class="{
                    'sortable': $column.sortable(this),
                    'sort-asc': params.sorting()[$column.sortable(this)]=='asc',
                    'sort-desc': params.sorting()[$column.sortable(this)]=='desc'
                  }"
		    ng-click="sortBy($column, $event)"
		    ng-if="$column.show(this)"
		    ng-init="template = $column.headerTemplateURL(this)"
		    name="[[ $column .title ]]"
		    class="header [[ $column.class(this) ]]">
			<div ng-if="!template" class="ng-table-header" ng-class="{'sort-indicator': params.settings().sortingIndicator == 'div'}">
				<span ng-bind="$column.title(this)" ng-class="{'sort-indicator': params.settings().sortingIndicator == 'span'}"></span>
			</div>
			<div ng-if="template" ng-include="template"></div>
		</th>
	</tr>
</script>

<!--table checkbox filter-->
<script id="custom/checkbox" type="text/ng-template">
	<input type="checkbox" ickeck ng-model="params.allSelected" ng.click="params.selectAll()" class="f">
</script>