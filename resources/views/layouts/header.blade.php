<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>
        EasyDayTours
    </title>

    {{-- Estilos principales de la aplicación --}}
    <link rel="stylesheet" type="text/css" href="{{asset("app/dist/library.css")}}" />
    <link rel="stylesheet" type="text/css" href="{{asset("app/css/styles.css")}}" />

    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300|Titillium+Web:200,300,400' rel='stylesheet' type='text/css'>

    {{--<link type="image/x-icon" href="{{asset("app/img/favicon.png")}}" rel="shortcut icon"/>--}}

    <script type="text/javascript">
        {{-- translations --}}
        var Lang = {!! $lang !!};

        {{-- translations --}}
        var user = {!! auth()->user() !!};

        {{-- routes --}}
        var routes = [];
	    
	    var CONST = {!! $const !!};
	    
	    var MODULES = {!!  $angularModules !!};

	    var settings = {!! $settings !!};
	   
    </script>

    <style type="text/css">
        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak],
        .ng-cloak, .x-ng-cloak,
        .ng-hide:not(.ng-hide-animate) {
            display: none !important;
        }

        ng\:form {
            display: block;
        }

        .ng-animate-shim {
            visibility:hidden;
        }

        .ng-anchor {
            position:absolute;
        }

    </style>
</head>