<!DOCTYPE html>
<html ng-app="app" lang="{{app()->getLocale()}}">
    @include('layouts.header')

    <toaster-container toaster-options="
        {
        'time-out':{
            'toast-success': 5000,
            'toast-warning': 4000,
            'toast-error': 8000,
            },
        'body-output-type':'directive',
        }">
    </toaster-container>
    <body  landing-scrollspy class="[[$state.current.data.specialClass]]" id="page-top" ng-controller="MainController as main">

        {{-- Extensión para el contenido de la página --}}
        @yield('content-body')

        <!-- Footer -->
        @include('layouts.footer')
    </body>
</html>