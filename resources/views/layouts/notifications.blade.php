<script type="text/javascript">

    @foreach($errors->all() as $error)
        new NotificationFx({
            message : '<span class="icon fa fa-bullhorn fa-2x"></span><p>{{$error}}</p>',
            layout : 'bar',
            effect : 'slidetop',
            type : 'error', // notice, warning or error

        }).show();
    @endforeach
    
    @if(Session::has("notice"))
        new NotificationFx({
            message : '<span class="icon fa fa-bullhorn fa-2x"></span><p>{{Session::get("notice")}}</p>',
            layout : 'bar',
            effect : 'slidetop',
            type : 'notice', // notice, warning or error
        }).show();
    @endif
    
    @if(Session::has("warning"))
        new NotificationFx({
            message : '<span class="icon fa fa-bullhorn fa-2x"></span><p>{{Session::get("warning")}}</p>',
            layout : 'bar',
            effect : 'slidetop',
            type : 'warning', // notice, warning or error
        }).show();
    @endif
    
    @if(Session::has("error"))
        new NotificationFx({
            message : '<span class="icon fa fa-bullhorn fa-2x"></span><p>{{Session::get("error")}}</p>',
            layout : 'bar',
            effect : 'slidetop',
            type : 'error', // notice, warning or error
        }).show();
    @endif
    
</script>