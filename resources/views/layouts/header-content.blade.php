
<div class="row border-bottom m-b-sm">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        
	    
	    <!--search-->
	    <div class="navbar-header">
            <span minimaliza-sidebar></span>
            <form role="search" class="navbar-form-custom">
                <div class="form-group">
                    <input type="text" placeholder="[['search' | translate ]]" class="form-control" name="top-search" id="top-search">
                </div>
            </form>
        </div>
	    
	    
        <ul class="nav navbar-top-links navbar-right">
	

			<!--messages-->
            <li uib-dropdown>
                <a class="count-info" href uib-dropdown-toggle>
                    <i class="fa fa-envelope"></i>
                    <span class="label label-warning"></span>
                </a>
                <ul class="dropdown-messages animated fadeInDown" uib-dropdown-menu>
                    
                    <li class="divider"></li>
                    <li>
                        <div class="text-center link-block">
                            <a ui-sref="mailbox.inbox" class="btn btn-default">
                                <i class="fa fa-envelope"></i>
                                <strong>{{ trans('global.read_all_messages') }}</strong>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
	        
	        
	        <!--alerts-->
            <li uib-dropdown>
                <a class="count-info" href uib-dropdown-toggle>
                    <i class="fa fa-bell"></i> <span class="label label-primary"></span>
                </a>
                <ul class="dropdown-alerts animated fadeInDown" uib-dropdown-menu >
                    
                    <li class="divider"></li>
                    <li>
                        <div class="text-center link-block">
                            <a ui-sref="miscellaneous.notify" class="btn btn-default">
                                <i class="fa fa-bell"></i>
                                <strong>{{ trans('global.read_all_notifications') }}</strong>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
	        
	        
	        
	        <!--logout btn-->
            <li>
                <a ui-sref="logout">
                    <i class="fa fa-power-off"></i>{{ trans('global.logout') }}
                </a>
            </li>
            
        </ul>

    </nav>
</div>

{{--<div class="row">--}}
    {{--<span class="label label-primary visible-xs">XS</span>--}}
    {{--<span class="label label-primary visible-sm">SM</span>--}}
    {{--<span class="label label-primary visible-md">MD</span>--}}
    {{--<span class="label label-primary visible-lg">LG</span>--}}
{{--</div>--}}