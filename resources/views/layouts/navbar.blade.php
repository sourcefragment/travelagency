<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul side-navigation class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
	                
                    <img alt="{{ trans('global.profile_image') }}" class="image-circle" src="{{ asset('app/img/logo-small.png') }}" />
	                
	                <a data-toggle="dropdown" class="dropdown-toggle">
                            <span class="clear">
	                            <span class="block m-t-xs">
		                            <strong class="font-bold">
			                            [[ user.name ]] [[ user.lastname ]]
		                            </strong>
	                            </span>
		                        <span class="text-muted text-xs block">
			                        {{ $user->role->name }}
			                        <b class="caret"></b>
		                        </span>
                            </span>
	                </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a ui-sref="user-edit({ id: {{ $user->id }} })" href="#" >
		                        [[ 'profile' | translate   ]]
	                        </a>
                        </li>
                        <li class="divider"></li>
                        <li><a ui-sref="logout">
		                        {{ trans('global.logout') }}
	                        </a>
                        </li>
                    </ul>
	                
                </div>
                <div class="logo-element">
                    EDT
                </div>
            </li>
            @foreach ( $sections as $section )

				
				@if ( count($section->children)  )

					@if( $section->menu_visible )
				        <li ng-class="{ in: $state.includes( '{{ $section->slug }}') }">
					        <a href="">
						        <i class="fa {{ $section->icon }}"></i>
						        <span class="nav-label">
							        {{ trans( 'global.'.$section->slug ) }}
						        </span>
						        <span class="fa arrow"></span>
					        </a>
					        <ul class="nav nav-second-level collapse" ng-class="{ in: $state.includes( '{{ $section->slug }}') }">
							@foreach( $section->children as $child )
						        @if( $child->menu_visible )
							        <li ui-sref-active="">
								        <a ui-sref="{{ $child->slug }}">
									        <i class="fa {{ $child->icon }}"></i>
									        {{ trans( 'global.'.$child->slug ) }}
								        </a>
							        </li>
							    @endif
							@endforeach
						    </ul>

						</li>
					@endif
					
				@else
					
					@if( !$section->parent_id )
						@if( $section->menu_visible )
					        <li ui-sref-active="active" class="{ in: $state.includes( '{{ $section->slug }}') }">
						        <a ui-sref="{{ $section->slug }}" href="#/{{ $section->slug }}">
							        <i class="fa {{ $section->icon }}"></i>
							        <span class="nav-label">{{ trans( 'global.'.$section->slug) }}</span>
						        </a>
					        </li>
						@endif
					@endif
					
				@endif
					
	        @endforeach
	        
	        
        </ul>

    </div>
</nav>