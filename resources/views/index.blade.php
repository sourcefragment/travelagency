@extends('layouts.content')

@section('content-body')

        <div id="wrapper" ng-show="!appIsLoading" class="ng-hide">
        {{-- menu lateral --}}
        @include('layouts.navbar')

        <!-- ng-class with current state name give you the ability to extended customization your view -->
            <!-- Page wraper -->
            <div id="page-wrapper" class="gray-bg [[ $state.current.name ]]" >
                {{-- menu superior --}}
                @include("layouts.header-content")

                <!-- Main view  -->
                <div ui-view class="page-content">
                </div>

                @include("layouts.footer-content")

            </div>
        <!-- End page wrapper-->

        </div>
        <!-- End wrapper-->


@endsection