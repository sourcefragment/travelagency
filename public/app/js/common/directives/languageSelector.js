/**
 * Created by juan on 27/09/16.
 */
(function () {
    'use strict';

    angular.module('app')
        .directive('languageSelector',languageSelector);

    function languageSelector(){
        return {
            restrict:'AE',
            required:'ngModel',
            scope:{
                language:'=ngModel',
                languages:'='
            },
            templateUrl:'app/js/common/views/language-selector.html',
            controller:'LanguageController',
            controllerAs:'$ctrl'
        };
    }
})();