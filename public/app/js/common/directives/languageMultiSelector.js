/**
 * Created by juan on 19/09/16.
 */
(function () {
    'use strict';

    angular.module('app')
        .directive('languageMultiSelector',languageMultiSelector);

    function languageMultiSelector(){

        return {
            restrict:'AE',
            required:'ngModel',
            scope:{
                language:'=ngModel',
                languages:'='
            },
            templateUrl:'app/js/common/views/language-multi-selector.html',
            controller:'LanguageController',
            controllerAs:'$ctrl'
        };
    }
})();
