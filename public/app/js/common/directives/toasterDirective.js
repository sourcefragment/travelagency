/**
 * Created by mario on 9/22/16.
 */
(function () {

	'use strict';

	angular
		.module('app')
		.directive('toasterLabel',toasterLabel);

	function toasterLabel() {

		return {
			template:'<b ng-if="directiveData[0].title">[[ directiveData[0].title | translate ]]</b>' +
					'<div class="hr-line-dashed"></div>'+
					'<div ng-repeat="data in directiveData">' +
							'<b ng-if="data.subTitle">[[ data.subTitle | translate ]]</b>' +
							'<p ng-if="data.message">[[ data.message | translate ]]</p>' +
					'<div>'
		}
	}

})();