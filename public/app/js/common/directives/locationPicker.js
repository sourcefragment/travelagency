/**
 * Created by mario on 12/13/16.
 */
(function () {
	'use strict';

	angular.module('app')
		.directive('locationPicker',locationPicker);


	function locationPicker(){
		return {
			restrict : 'A',
			scope : {
				model:'=ngModel'
			},
			link:function( scope,element,attrs ){
				element.click( scope.openModal);
			},
			controller : function( $scope,$uibModal,NgMap ){

				/**Open location picker modal
				 * */
				$scope.openModal = function(){

					var modalInstance = $uibModal.open({
						templateUrl: 'app/js/common/views/map-modal.html',
						size:'lg',
						controller : function($scope,$timeout){

							$scope.point = {};
							$scope.mapLoaded = false;
							$scope.street = null;
							$scope.geocoder = null;
							$scope.map = null;
							$scope.locations = [];

							//On save modal
							$scope.search = searchAddress;
							$scope.saveModal = saveModal;
							$scope.setLocation = setLocation;


							//Init map
							NgMap.getMap()
								.then(function ( map ) {
									$scope.map = map;
									google.maps.event.trigger($scope.map,'resize');

									//Delay map initialization to avoid rendering problems
									$timeout(function () {

										$scope.geocoder = new google.maps.Geocoder();
										$scope.street = $scope.map.getStreetView();


										//On streetview POV changed
										$scope.street.addListener('pov_changed',function () {
											var pos = $scope.street.getPosition();
											var pov = $scope.street.getPov();
											$scope.point = {
												lat : pos.lat(),
												lng : pos.lng(),
												heading : pov.heading,
												pitch : pov.pitch,
												zoom : pov.zoom
											};

										});


									},1000);



								})
								.catch(function(map){
									console.log(map);
								});


							//On modal save
							function saveModal() {
								this.$close();
								setModel($scope.point);
							}

							/**Search location by address
							 *
							 * */
							function searchAddress(){

								$scope.geocoder.geocode({
									address : $scope.address
								},function ( results,status) {

									if( status == 'OK' ){
										$scope.locations = results;
										console.log(results);

										$scope.street.setPosition(results[0].geometry.location);

									}else if( status == 'ZERO_RESULT' ){

									}
									console.log( results,status);
								});
							}


							function setLocation( location ){
								$log.error(location);
							}

						}
					});

				};

				/**Set location is called from the uibModal on modal saved
				 * The location will be set only if there is a change on the map position
				 * */
				function setModel( location ){
					$scope.model = location;
				}


			}

		}
	}
})();