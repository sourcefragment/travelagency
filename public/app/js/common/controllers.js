(function () {

    angular.module('app')
        .controller('MainController',mainController);
        // .controller('CalendarCtrl',calendarCtrl);


    mainController.$inject = [
    	'$scope',
	    '$timeout',
	    'Idle',
        'SweetAlert',
	    '$location',
	    '$filter',
	    '$log',
	    '$uibModal',
	    '$interval'
    ];

    /**main controller to load the app
     * */
    function mainController(
    	$scope,
	    $timeout,
	    Idle,
        SweetAlert,
        $location,
        $filter,
	    $log,
	    $uibModal,
	    $interval
    ){

    	var vm = this;
    	vm.interval = false;

    	$scope.$on('IdleWarn',function( e,count ){
    		$log.error('idle warn '+count);

	    });

    	/**Show message on idle start
	     * */
    	$scope.$on('IdleStart',function () {
			$log.error('idle start');
		    showIdleMsg();
	    });

    	/**Logout if idle
	     * */
	    $scope.$on('IdleTimeout',function(){
	    	$log.error('idle timeout');
		    $location.path('/logout');

	    });

		/**Close modal on idle end
		 * */
	    $scope.$on('IdleEnd',function () {
		    $log.error('idle end');
		    $scope.warning.close();
	    });


	    /**Keepalive flag
	     * */
	    $scope.$on('Keepalive',function () {
		    $log.error('keepalive');
	    });


	    /**Show idle msg
	     * */
	    function showIdleMsg(){
		    $scope.warning = $uibModal.open({
			    templateUrl: 'app/js/common/views/idle-modal.html'
		    });
	    }



    }

})();
