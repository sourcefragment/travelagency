/**
 * INSPINIA - Responsive Admin Theme
 *
 * Inspinia theme use AngularUI Router to manage routing and views
 * Each view are defined as state.
 * Initial there are written state for all view in theme.
 *
 */
(function () {

	'use strict';

	angular
		.module('app')
		.service('apiInterceptor',apiInterceptor)
		.config(mainConfig)
		.run(function(
			$rootScope,
			$state,
			Idle,
		    ngTableDefaults,
		    toaster,
			$log,
			$filter

		){

			$rootScope.$state = $state;
			$rootScope.countries = [];
			$rootScope.roles = [];
			$rootScope.languages = [];
			$rootScope.offerTypes = [];
			$rootScope.isLoggedIn = true;
			$rootScope.companies = [];
			$rootScope.users = [];
			$rootScope.API_URL = 'api/v1/';
			$rootScope.user = window.user;
			$rootScope.settings = window.settings;

			$rootScope.days = ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'];
			$rootScope.calendarColors = ['#aac5ae','#72584d','#ff6666','#4c3935'];

			//Global status colors
			$rootScope.colors = {
				pending : {
					color: '#efdca9',
					highlight:'#deb853'
				},
				paid : {
					color:'#c3d5cb',
					highlight:'#789f8a'
				},
				cancelled : {
					color:'#f0b7a8',
					highlight:'#e06a4e'
				},
				confirmed:{
					color:'#a8ebf0',
					highlight:'#21b9c4'
				}
			};


			//Global toaster error
			$rootScope.showErrors = function( response,title){

				var errors = response.data.errors ? response.data.errors: [];

				//Show forbidden message
				if( response.status === 403 ){
					toaster.pop({
						type: 'error',
						body: 'toaster-label',
						directiveData:[{
							title : $filter('translate')('forbidden'),
							message : $filter('translate')('not_allowed_msg')
						}]
					});
					return false;

				}

				//Show single error
				if( response.data.error ){

					toaster.pop({
						type: 'error',
						body: 'toaster-label',
						directiveData:[{
							title : response.data.error,
							message : response.data.msg
						}]
					});
					return false;
				}

				if( !errors ){
					$log.error(response);
					return false;
				}
				//Do for each error
				var data = [];

				angular.forEach(errors,function (val,key) {

                    var msg = '';
					var subtitle = key;

                    if( typeof val === 'object' ){
						angular.forEach(val,function (v,k) {
							msg += v + '.';
                        });
					}else{
                    	subtitle = key;
                    	msg = val;
					}

                    data.push({
                        title : title,
                        subTitle : subtitle,
                        message : msg
                    });
                });

				toaster.pop({
					type: 'error',
					body: 'toaster-label',
					directiveData:data
				});
			};

			//Transform search params for api calls
			$rootScope.getParams = function ( params ) {

				return {
					page : params.page(),
					filter : params.filter(),
					sort : params.sorting(),
					limit : params.count()
				};
			};


			/**Show saved message
			 * */
			$rootScope.saved = function () {
				toaster.pop({
					body: 'toaster-label',
					directiveData:[{
						title:$filter('translate')('saved')
					}],
					type: 'success',

					timeout:1000
				});
			};

			/**Order languages by default
			 * */
			$rootScope.filterLanguages = function ( item ) {

				if( item.default === 1 )
					return  'Default';

				return 'Languages';
			};

			/**Sort language by filter
			 * */
			$rootScope.orderLanguage = ['Default','Languages'];

			$rootScope.range = function( start,end ){
				var result = [];
				for (var i = start; i <= end; i++) {
					result.push(i);
				}
				return result;
			};


			Idle.watch();


			//Config global table params
			ngTableDefaults.settings = angular.extend({},ngTableDefaults.settings,{

			});

			ngTableDefaults.params = angular.extend({},ngTableDefaults.params,{
				count : 10
			});



		})
		.constant('config',{
			'apiVersion' : '1.0',
			'apiURL' : 'api/v1/',
			'mapStyle' : [
				{
					stylers: [
						{ hue: "#00ffe6" },
						{ saturation: -20 }
					]
				},{
					featureType: "road",
					elementType: "geometry",
					stylers: [
						{ lightness: 100 },
						{ visibility: "simplified" }
					]
				},{
					featureType: "road",
					elementType: "labels",
					stylers: [
						{ visibility: "off" }
					]
				}
			],
			'mapCenter': {
				lat : 36.5579927,
				lng : -4.6846859
			},
			'mapZoom' : 10
		});



	mainConfig.$inject = [
		'$stateProvider',
		'$urlRouterProvider',
		'IdleProvider',
		'$authProvider',
		'KeepaliveProvider',
		'$httpProvider'
	];

    function mainConfig(

    	$stateProvider,
	    $urlRouterProvider,
	    IdleProvider,
        $authProvider,
        KeepaliveProvider,
        $httpProvider

	    ) {

        // Configure Idle settings
        IdleProvider.idle(3600);
        IdleProvider.timeout(3600);
		KeepaliveProvider.interval(60);

	    $authProvider.tokenName = 'token';
	    $authProvider.tokenPrefix = '';



	    //Default url
        $urlRouterProvider.otherwise('dashboard');

        $stateProvider.state( 'logout' , {
            url: '/logout',
            controller:function(){
            	// console.log($authProvider);
                window.location.href ='logout';
            }
        });

	    $httpProvider.interceptors.push('apiInterceptor');


    }



    /**Intercept 401 errors
     * */
    function apiInterceptor( $q ){

		return {
			request : function( response ){
				return response;
			},
			requestError : function( response ){

			},
			response : function( response ){
				return response;
			},
			responseError : function( response ){
				if (response.status === 401 ) {
					window.location.href ='logout';
				}

				if( response.status === 500 ){

				}
				return $q.reject(response);
			}
		};

    }




})();

