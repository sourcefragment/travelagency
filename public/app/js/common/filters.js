(function () {

	'use strict';

	angular
    .module('app')
    .filter('capitalize', function() {
        return function(input) {
            return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
        }
    })
    .filter('upperword', function() {
        return function(input) {
            var pieces = input.split(" ");
            for ( var i = 0; i < pieces.length; i++ )
            {
                var j = pieces[i].charAt(0).toUpperCase();
                pieces[i] = j + pieces[i].substr(1);
            }
            return pieces.join(" ");
        }
    })

    .filter('range',function(){
        return function(input,total){
            total = parseInt(total);

		    for( var i=0; i<total;i++){
		        input.push(i);
		    }
		    return input;
	    }
    });


})();
