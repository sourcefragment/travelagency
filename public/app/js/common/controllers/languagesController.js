(function () {

    'use strict';

    angular
        .module('app')
        .controller('LanguageController', languageController);


    languageController.$inject = [
        '$rootScope',
        'apiService',
    ];

    function languageController(
        $rootScope,
        apiService
    ) {
        var vm = this;
        vm.getLanguages = getLanguages;
        vm.tagHandler = function (tag){
            return null;
        }

        activate();

        /**Init controller
         * */
        function activate() {
            if( $rootScope.languages.length < 1)
                getLanguages();
        }

        /**Load roles
         * */
        function getLanguages() {
            apiService.get('languages')
                .then(function ( response ) {
                    $rootScope.languages = response.data;
                    return response;
                }).catch(function () {
                console.log(data);
            });
        }



    };
})();