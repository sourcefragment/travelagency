(function () {
    'use strict';

	/**
     * INSPINIA - Responsive Admin Theme
     *
     * Main directives.js file
     * Define directives for used plugin
     *
     *
     * Functions (directives)
     *  - sideNavigation
     *  - iboxTools
     *  - minimalizaSidebar
     *  - vectorMap
     *  - sparkline
     *  - icheck
     *  - ionRangeSlider
     *  - dropZone
     *  - responsiveVideo
     *  - chatSlimScroll
     *  - customValid
     *  - fullScroll
     *  - closeOffCanvas
     *  - clockPicker
     *  - landingScrollspy
     *  - fitHeight
     *  - iboxToolsFullScreen
     *  - slimScroll
     *  - truncate
     *  - touchSpin
     *  - markdownEditor
     *  - resizeable
     *
     */


    /**
     * pageTitle - Directive for set Page title - mata title
     */
    function pageTitle($rootScope, $timeout) {
        return {
            link: function(scope, element) {
                var listener = function(event, toState, toParams, fromState, fromParams) {
                    // Default title - load on Dashboard 1
                    var title = 'INSPINIA | Responsive Admin Theme';
                    // Create your own title pattern
                    if (toState.data && toState.data.pageTitle) title = 'INSPINIA | ' + toState.data.pageTitle;
                    $timeout(function() {
                        element.text(title);
                    });
                };
                $rootScope.$on('$stateChangeStart', listener);
            }
        }
    };

    /**
     * sideNavigation - Directive for run metsiMenu on sidebar navigation
     */
    function sideNavigation($timeout) {
        return {
            restrict: 'A',
            link: function(scope, element) {
                // Call the metsiMenu plugin and plug it to sidebar navigation
                $timeout(function(){
                    element.metisMenu();

                });
            }
        };
    }

    /**
     * responsibleVideo - Directive for responsive video
     */
    function responsiveVideo() {
        return {
            restrict: 'A',
            link:  function(scope, element) {
                var figure = element;
                var video = element.children();
                video
                    .attr('data-aspectRatio', video.height() / video.width())
                    .removeAttr('height')
                    .removeAttr('width');

                //We can use $watch on $window.innerWidth also.
                $(window).resize(function() {
                    var newWidth = figure.width();
                    video
                        .width(newWidth)
                        .height(newWidth * video.attr('data-aspectRatio'));
                }).resize();
            }
        };
    }

    /**
     * iboxTools - Directive for iBox tools elements in right corner of ibox
     */
    function iboxTools($timeout) {
        return {
            restrict: 'A',
            scope: true,
            templateUrl: 'app/js/common/views/ibox_tools.html',
            controller: function ($scope, $element) {
                // Function for collapse ibox
                $scope.showhide = function () {
                    var ibox = $element.closest('div.ibox');
                    var icon = $element.find('i:first');
                    var content = ibox.find('div.ibox-content');
                    content.slideToggle(200);
                    // Toggle icon from up to down
                    icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                    ibox.toggleClass('').toggleClass('border-bottom');
                    $timeout(function () {
                        ibox.resize();
                        ibox.find('[id^=map-]').resize();
                    }, 50);
                };
                // // Function for close ibox
                // $scope.closebox = function () {
                //     var ibox = $element.closest('div.ibox');
                //     ibox.remove();
                // }
            }
        };
    }

    /**
     * iboxTools with full screen - Directive for iBox tools elements in right corner of ibox with full screen option
     */
    function iboxToolsFullScreen($timeout) {
        return {
            restrict: 'A',
            scope: true,
            templateUrl: 'views/common/ibox_tools_full_screen.html',
            controller: function ($scope, $element) {
                // Function for collapse ibox
                $scope.showhide = function () {
                    var ibox = $element.closest('div.ibox');
                    var icon = $element.find('i:first');
                    var content = ibox.find('div.ibox-content');
                    content.slideToggle(200);
                    // Toggle icon from up to down
                    icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                    ibox.toggleClass('').toggleClass('border-bottom');
                    $timeout(function () {
                        ibox.resize();
                        ibox.find('[id^=map-]').resize();
                    }, 50);
                };
                // Function for close ibox
                $scope.closebox = function () {
                    var ibox = $element.closest('div.ibox');
                    ibox.remove();
                };
                // Function for full screen
                $scope.fullscreen = function () {
                    var ibox = $element.closest('div.ibox');
                    var button = $element.find('i.fa-expand');
                    $('body').toggleClass('fullscreen-ibox-mode');
                    button.toggleClass('fa-expand').toggleClass('fa-compress');
                    ibox.toggleClass('fullscreen');
                    setTimeout(function() {
                        $(window).trigger('resize');
                    }, 100);
                }
            }
        };
    }

    /**
     * minimalizaSidebar - Directive for minimalize sidebar
     */
    function minimalizaSidebar($timeout) {
        return {
            restrict: 'A',
            template: '<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="" ng-click="minimalize()"><i class="fa fa-bars"></i></a>',
            controller: function ($scope, $element) {
                $scope.minimalize = function () {
                    $('body').toggleClass('mini-navbar');
                    if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
                        // Hide menu in order to smoothly turn on when maximize menu
                        $('#side-menu').hide();
                        // For smoothly turn on menu
                        setTimeout(
                            function () {
                                $('#side-menu').fadeIn(400);
                            }, 200);
                    } else if ($('body').hasClass('fixed-sidebar')){
                        $('#side-menu').hide();
                        setTimeout(
                            function () {
                                $('#side-menu').fadeIn(400);
                            }, 100);
                    } else {
                        // Remove all inline style from jquery fadeIn function to reset menu state
                        $('#side-menu').removeAttr('style');
                    }
                }
            }
        };
    }


    function closeOffCanvas() {
        return {
            restrict: 'A',
            template: '<a class="close-canvas-menu" ng-click="closeOffCanvas()"><i class="fa fa-times"></i></a>',
            controller: function ($scope, $element) {
                $scope.closeOffCanvas = function () {
                    $("body").toggleClass("mini-navbar");
                }
            }
        };
    }

    /**
     * vectorMap - Directive for Vector map plugin
     */
    function vectorMap() {
        return {
            restrict: 'A',
            scope: {
                myMapData: '=',
            },
            link: function (scope, element, attrs) {
                var map = element.vectorMap({
                    map: 'world_mill_en',
                    backgroundColor: "transparent",
                    regionStyle: {
                        initial: {
                            fill: '#e4e4e4',
                            "fill-opacity": 0.9,
                            stroke: 'none',
                            "stroke-width": 0,
                            "stroke-opacity": 0
                        }
                    },
                    series: {
                        regions: [
                            {
                                values: scope.myMapData,
                                scale: ["#1ab394", "#22d6b1"],
                                normalizeFunction: 'polynomial'
                            }
                        ]
                    },
                });
                var destroyMap = function(){
                    element.remove();
                };
                scope.$on('$destroy', function() {
                    destroyMap();
                });
            }
        };
    }


    /**
     * sparkline - Directive for Sparkline chart
     */
    function sparkline() {
        return {
            restrict: 'A',
            scope: {
                sparkData: '=',
                sparkOptions: '=',
            },
            link: function (scope, element, attrs) {
                scope.$watch(scope.sparkData, function () {
                    render();
                });
                scope.$watch(scope.sparkOptions, function(){
                    render();
                });
                var render = function () {
                    $(element).sparkline(scope.sparkData, scope.sparkOptions);
                };
            }
        };
    }

    /**
     * icheck - Directive for custom checkbox icheck
     */
    function icheck($timeout) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function($scope, element, $attrs, ngModel) {
                return $timeout(function() {
                    var value;
                    value = $attrs['value'];

                    $scope.$watch($attrs['ngModel'], function(newValue){
                        $(element).iCheck('update');
                    })

                    return $(element).iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green'

                    }).on('ifChanged', function(event) {
                        if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
                            $scope.$apply(function() {
                                return ngModel.$setViewValue(event.target.checked);
                            });
                        }
                        if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
                            return $scope.$apply(function() {
                                return ngModel.$setViewValue(value);
                            });
                        }
                    });
                });
            }
        };
    }

    /**
     * ionRangeSlider - Directive for Ion Range Slider
     */
    function ionRangeSlider() {
        return {
            restrict: 'A',
            scope: {
                rangeOptions: '='
            },
            link: function (scope, elem, attrs) {
                elem.ionRangeSlider(scope.rangeOptions);
            }
        };
    }

    /**
     * dropZone - Directive for Drag and drop zone file upload plugin
     */
    function dropZone() {
        return {
            restrict: 'C',
            link: function(scope, element, attrs) {

                var config = {
                    url: 'http://localhost:8080/upload',
                    maxFilesize: 100,
                    paramName: "uploadfile",
                    maxThumbnailFilesize: 10,
                    parallelUploads: 1,
                    autoProcessQueue: false
                };

                var eventHandlers = {
                    'addedfile': function(file) {
                        scope.file = file;
                        if (this.files[1]!=null) {
                            this.removeFile(this.files[0]);
                        }
                        scope.$apply(function() {
                            scope.fileAdded = true;
                        });
                    },

                    'success': function (file, response) {
                    }

                };

                var dropzone = new Dropzone(element[0], config);

                angular.forEach(eventHandlers, function(handler, event) {
                    dropzone.on(event, handler);
                });

                scope.processDropzone = function() {
                    dropzone.processQueue();
                };

                scope.resetDropzone = function() {
                    dropzone.removeAllFiles();
                }
            }
        };
    }

    /**
     * chatSlimScroll - Directive for slim scroll for small chat
     */
    function chatSlimScroll($timeout) {
        return {
            restrict: 'A',
            link: function(scope, element) {
                $timeout(function(){
                    element.slimscroll({
                        height: '234px',
                        railOpacity: 0.4
                    });

                });
            }
        };
    }

    /**
     * customValid - Directive for custom validation example
     */
    function customValid(){
        return {
            require: 'ngModel',
            link: function(scope, ele, attrs, c) {
                scope.$watch(attrs.ngModel, function() {

                    // You can call a $http method here
                    // Or create custom validation

                    var validText = "Inspinia";

                    if(scope.extras == validText) {
                        c.$setValidity('cvalid', true);
                    } else {
                        c.$setValidity('cvalid', false);
                    }

                });
            }
        };
    }


    /**
     * fullScroll - Directive for slimScroll with 100%
     */
    function fullScroll($timeout){
        return {
            restrict: 'A',
            link: function(scope, element) {
                $timeout(function(){
                    element.slimscroll({
                        height: '100%',
                        railOpacity: 0.9
                    });

                });
            }
        };
    }

    /**
     * slimScroll - Directive for slimScroll with custom height
     */
    function slimScroll($timeout){
        return {
            restrict: 'A',
            scope: {
                boxHeight: '@'
            },
            link: function(scope, element) {
                $timeout(function(){
                    element.slimscroll({
                        height: scope.boxHeight,
                        railOpacity: 0.9
                    });

                });
            }
        };
    }

    /**
     * clockPicker - Directive for clock picker plugin
     */
    function clockPicker() {
        return {
            restrict: 'A',
            link: function(scope, element) {
                element.clockpicker();
            }
        };
    }


    /**
     * landingScrollspy - Directive for scrollspy in landing page
     */
    function landingScrollspy(){
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.scrollspy({
                    target: '.navbar-fixed-top',
                    offset: 80
                });
            }
        };
    }

    /**
     * fitHeight - Directive for set height fit to window height
     */
    function fitHeight(){
        return {
            restrict: 'A',
            link: function(scope, element) {
                element.css("height", $(window).height() + "px");
                element.css("min-height", $(window).height() + "px");
            }
        };
    }

    /**
     * truncate - Directive for truncate string
     */
    function truncate($timeout){
        return {
            restrict: 'A',
            scope: {
                truncateOptions: '='
            },
            link: function(scope, element) {
                $timeout(function(){
                    element.dotdotdot(scope.truncateOptions);

                });
            }
        };
    }


    /**
     * touchSpin - Directive for Bootstrap TouchSpin
     */
    function touchSpin() {
        return {
            restrict: 'A',
            scope: {
                spinOptions: '='
            },
            link: function (scope, element, attrs) {
                scope.$watch(scope.spinOptions, function(){
                    render();
                });
                var render = function () {
                    $(element).TouchSpin(scope.spinOptions);
                };
            }
        };
    }

    /**
     * markdownEditor - Directive for Bootstrap Markdown
     */
    function markdownEditor() {
        return {
            restrict: "A",
            require:  'ngModel',
            link:     function (scope, element, attrs, ngModel) {
                $(element).markdown({
                    savable:false,
                    onChange: function(e){
                        ngModel.$setViewValue(e.getContent());
                    }
                });
            }
        };
    }


    /**Show a lengend in a form if fields are required
     * */
    function fieldsRequiredLeyend( $translate ){
    	return {
    	    restrict:'EA',
		    template:'<p>'
		        +'<i class="fa fa-asterisk asterisk-info required"></i>'
		        +'<span translate>[[ "these_fields_are_required" | translate ]]</span>'
		        +'<div class="col-sm-12 hr-line-dashed"></div>'
	    };
    }




    /**Table search
     * */
    function tableSearch(){
	    return {
	        restrict : 'EA',
		    scope : {
	        	search : '=ngModel',
			    fields : '=fields'
		    },
		    templateUrl : 'app/js/common/views/table-search.html'
	    };
    }



    /**Table export tools
     * */
    function tableExportTools(){
		return {
			restrict : 'EA',
			scope : {
				ngModel:'=ngModel'
			},
			templateUrl : 'app/js/common/views/table-export-tools.html',
			controller:['$scope','$log',function($scope,$log){


				//Print
				$scope.print = function () {

					if( $scope.ngModel && typeof $scope.ngModel.print == 'function' ){
						$scope.ngModel.print();
					}else{
						$log.error('Print not implemented');
					}

				};

				//To CSV
				$scope.toCsv = function () {

					if( $scope.ngModel && typeof $scope.ngModel.toCsv == 'function' ){
						$scope.ngModel.toCsv();
					}else{
						$log.error('To CSV not implemented');
					}

				};

				$scope.toPdf = function(){
					if( $scope.ngModel && typeof $scope.ngModel.toPdf == 'function' ){
						$scope.ngModel.toPdf();
					}else{
						$log.error('To PDF not implemented');
					}
				};

				$scope.import = function(){
					if( $scope.ngModel && typeof $scope.ngModel.import == 'function' ){
						$scope.ngModel.import();
					}else{
						$log.error('Import not implemented');
					}
				};



			}]

		};
    }


    /**App Loader
     * */
    function appLoader(){
    	return {
    	    restrict : 'EA',
		    template : '<div class="spinner-container"><div class="sk-spinner sk-spinner-wave">'
		        +' <div class="sk-rect1"></div>'
		        +' <div class="sk-rect2"></div>'
		        +' <div class="sk-rect3"></div>'
		        +' <div class="sk-rect4"></div>'
		        +' <div class="sk-rect5"></div>'
		        +' </div></div>'
	    };
    }

    /**Toggle table columns dynamically
     * */
    function columnToggle(){
    	return {
    		restrict : 'A',
		    link :function( scope,element,attrs ){
    			element.addClass('toggable');
			    element.on('click',function ( e ) {

			    	var tr = angular.element(e.currentTarget).parent();
			    	var columns = tr.children();
				    var html = '<ul class="list-group">';
					var tbody = tr.parent().parent().find('tbody');

				    angular.forEach(columns,function (val,key) {

				    	var col = angular.element(val);
					    var visible = col.hasClass('hide');
						var icon = 'fa-check text-success';

					    if( visible ){
					    	icon = 'fa-close text-danger';
					    }

					    if( col.data('toggle') != false ){
					    	var li = '<a data-index="'
							    + key + '" class="list-group-item">'
								+ '<i class="fa '+icon+'"></i> '
							    + val.innerHTML+'</a>';

						    html += li;

					    }

				    });
				    html += '</ul>';

				    var th = angular.element( e.currentTarget ).append(html);

				    th.find('ul').on('mouseleave',function(e){
				    	angular.element(e.currentTarget).remove();
				    });

				    th.find('ul a').click(function(e){
						e.stopPropagation();
					    var index = angular.element(e.currentTarget).data('index');
						var cols = angular.element(tbody.children());

					    angular.element(tr.children()[index]).toggleClass('hide');

					    angular.forEach(cols,function (val,key) {
							var col = angular.element(val).children()[index];
						    angular.element(col).toggleClass('hide');
					    });

					    th.find('ul').remove();

				    });

			    });




		    }
	    }
    }



    /**Multi model selector for tables
     * */
    function tableFieldToggle(){
    	return {
    		restrict:'A',
		    scope:{
    			fields : '=ngModel'
		    },
		    templateUrl:'app/js/common/views/table-fields-selector.html',
		    controller:['$scope','$log',function($scope,$log){


			    //Print
			    $scope.openModal = function () {

				    if( $scope.ngModel && typeof $scope.ngModel.openModal == 'function' ){
					    $scope.ngModel.openModal();
				    }else{
					    $log.error('Fields not implemented');
				    }

			    };


		    }]
	    }
    }



	function passwordGenerator(){
        return {
            restrict: 'AE',
            replace: true,
            scope: {
                model: '=ngModel',
                confirmationField:'=confirmationField',
                confirmationRequired:'@confirmationRequired',
                passwordLength: '=?passwordLength',
                prefix: '=?prefix',
                uppercase: '=?uppercase',
                numbers: '=?numbers',
                specials: '=?specials',
                similarChars: '=?similarChars',
                buttonText: '@?buttonText'
            },
            templateUrl:'app/js/common/views/password-generator.html',
            link: function(scope,element,atts ) {

            },
            controller:function ($scope) {

                // console.log($scope);

	            // Initialize the default values
                $scope.confirmationRequired = ($scope.confirmationRequired) ? $scope.confirmationRequired:false;
	            $scope.passwordLength = ($scope.passwordLength) ? $scope.passwordLength : 12;
	            $scope.prefix = ($scope.prefix) ? $scope.prefix : '';
	            $scope.uppercase = ($scope.uppercase) ? $scope.uppercase : true;
	            $scope.numbers = ($scope.numbers) ? $scope.numbers : true;
	            $scope.specials = ($scope.specials) ? $scope.specials : false;
	            $scope.similarChars = ($scope.similarChars) ? $scope.similarChars : true;
	            $scope.value = 0;


	            // Enable password generation
	            $scope.generatePassword = function (){
                    // console.log('generate password');

		            // Create variables with characters, numbers and special
		            var lowerCharacters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
			            upperCharacters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],
			            numbers = ['2', '3', '4', '5', '6', '7', '8', '9'],
			            specials = ['!', '"', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '<', '=', '>', '?', '@', '[', '\\', ']', '^', '_', '`', '{', '}', '~'];

		            if ($scope.similarChars) {

			            lowerCharacters = lowerCharacters.concat(['i', 'l', 'o']);
			            upperCharacters = upperCharacters.concat(['I', 'O']);
			            numbers = numbers.concat(['0', '1']);
			            specials = specials.concat(['|']);
		            }

		            // Concatenate the differents variables according to true/false
		            var finalCharacters = lowerCharacters;

		            if ($scope.uppercase) {
			            finalCharacters = finalCharacters.concat(upperCharacters);
		            }

		            if ($scope.numbers) {
			            finalCharacters = finalCharacters.concat(numbers);
		            }

		            if ($scope.specials) {
			            finalCharacters = finalCharacters.concat(specials);
		            }

		            // Iterate while the number is less than passwordLength
		            var finalPassword = [];

		            for (var i = 0; i < $scope.passwordLength; i++) {
			            finalPassword.push(finalCharacters[Math.floor(Math.random() * finalCharacters.length)]);
		            }

		            // Save the result on model
                    var value = $scope.prefix + finalPassword.join('');
		            $scope.model = value;

		            if( $scope.confirmationRequired )
		                $scope.confirmationField = value;

		            $scope.calculateStrength();

	            };

	            /**Calculate password strength
                 * */
	            $scope.calculateStrength = function () {
                    // console.log('calculate strength');
	                var score = 0;

		            if (!$scope.model)
			            return score;

		            // award every unique letter until 5 repetitions
		            var letters = {};
		            for (var i=0; i<$scope.model.length; i++) {
			            letters[$scope.model[i]] = (letters[$scope.model[i]] || 0) + 1;
			            score += 5.0 / letters[$scope.model[i]];
		            }

		            // bonus points for mixing it up
		            var variations = {
			            digits: /\d/.test($scope.model),
			            lower: /[a-z]/.test($scope.model),
			            upper: /[A-Z]/.test($scope.model),
			            nonWords: /\W/.test($scope.model),
		            };

		            var variationCount = 0;
		            for (var check in variations) {
			            variationCount += ( variations[check] === true ) ? 1 : 0;
		            }
		            score += (variationCount - 1) * 10;

		            $scope.passwordStrength = score;

	            };


            }
        };
    }


    /**
     *
     * Pass all functions into module
     */
    angular
        .module('app')
        .directive('pageTitle', pageTitle)
        .directive('sideNavigation', sideNavigation)
        .directive('iboxTools', iboxTools)
        .directive('minimalizaSidebar', minimalizaSidebar)
        .directive('vectorMap', vectorMap)
        .directive('sparkline', sparkline)
        .directive('icheck', icheck)
        .directive('ionRangeSlider', ionRangeSlider)
        .directive('dropZone', dropZone)
        .directive('responsiveVideo', responsiveVideo)
        .directive('chatSlimScroll', chatSlimScroll)
        .directive('customValid', customValid)
        .directive('fullScroll', fullScroll)
        .directive('closeOffCanvas', closeOffCanvas)
        .directive('clockPicker', clockPicker)
        .directive('landingScrollspy', landingScrollspy)
        .directive('fitHeight', fitHeight)
        .directive('iboxToolsFullScreen', iboxToolsFullScreen)
        .directive('slimScroll', slimScroll)
        .directive('truncate', truncate)
        .directive('touchSpin', touchSpin)
        .directive('markdownEditor', markdownEditor)
	    .directive('fieldsRequiredLeyend',fieldsRequiredLeyend)
	    .directive('appLoader', appLoader )
	    .directive('tableExportTools',tableExportTools)
	    .directive('tableSearch', tableSearch )
	    .directive('columnToggle',columnToggle)
	    .directive('tableFieldToggle',tableFieldToggle)
        .directive('passwordGenerator',passwordGenerator);

})();

