/**
 * INSPINIA - Responsive Admin Theme
 *
 */
(function () {
    function config($translateProvider) {

        $translateProvider
            .translations('en', Lang);

        $translateProvider.preferredLanguage('en');

    }

    angular
        .module('app')
        .config(config)
})();
