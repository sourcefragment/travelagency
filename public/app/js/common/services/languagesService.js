/**
 * Created by juan on 24/08/16.
 */
(function () {
    angular.module('app')
        .factory('languagesService', rolesService);

    rolesService.$inject = ['$http', '$rootScope'];

    function rolesService($http, $rootScope) {

        var response = {
            languages: getLanguages,
        };

        return response;

        function getLanguages() {
            return $http.get('api/v1/languages')
                .then(function (response) {
                    $rootScope.languages = response.data;
                    console.log(response);
                    return response;
                })
                .catch(function (errors) {
                    console.log(errors);
                });
        }
    };
})();