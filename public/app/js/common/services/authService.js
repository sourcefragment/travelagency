/**
 * Created by mario on 9/13/16.
 */
(function () {
	'use strict';

	angular.module('auth',[
		'ngCookies'
	])
		.factory('authService',authService);

	authService.$inject = ['$cookieStore'];

	function authService( $cookieStore ){

		var current = {};
		return {
			user:current,

			set:function( current ){
				var existing = $cookieStore.get('current.user');
				current = current || existing;
				$cookieStore.put('current.user',current);

			},

			remove:function(){
				$cookieStore.remove('current.user',current);
			}
		}
	}
})();