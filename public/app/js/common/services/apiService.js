/**
 * Created by mario on 12/14/16.
 */
(function () {

	'use strict';


	angular
		.module('app')
		.service('apiService',service);


	service.$inject = ['$http','config'];

	function service( $http, config ){

		return {
			getAll : getAll,
			get : get,
			create : create,
			update : update,
			delete : deleteItem,
			getPermissions : getPermissions,
			singleDelete:singleDelete
		};

		/**get all agencies
		 * @param params object, search object
		 * */
		function getAll( name,params ){
			return $http({
				url : config.apiURL + name,
				method : 'GET',
				params : params
			})
				.then(function (response) {
					return response;
				})
				.catch(function (response) {
					throw response;
				})
		}


		/**get single agency
		 *@param id int, the agency id ref
		 * */
		function get( name,id ){
			return $http.get( config.apiURL + name +'/'+ id )
				.then(function (response) {
					return response;
				})
				.catch(function (response) {
					throw response;
				});
		}


		/**Create new agency
		 * @param data, object
		 * @return promise
		 * */
		function create( name,data ){
			return $http({
				url : config.apiURL + name,
				method : 'POST',
				data : data
			})
				.then(function (response) {
					return response;
				})
				.catch(function (response) {
					throw response;
				})
		}


		/**Update agency
		 * @param data, object
		 * */
		function update( name,data ){
			return $http({
				url : config.apiURL + name +'/'+ data.id,
				method : 'PUT',
				data : data
			})
				.then(function (response) {
					return response;
				})
				.catch(function (response) {
					throw response;
				})
		}


		/**Delete agency
		 * @param id int
		 * */
		function deleteItem( name,id ){
			return $http.delete( config.apiURL + name + '/' + id)
				.then(function (response) {
					return response;
				})
				.catch(function (response) {
					throw response;
				})
		}

		/**Simple route delete
		 * */
		function singleDelete(name){
			return $http.delete( config.apiURL + name )
				.then(function (response) {
					return response;
				})
				.catch(function (response) {
					throw response;
				})
		}


		/**Get all role's permissions
		 * */
		function getPermissions( id ){

			return $http.get( config.apiURL + 'role/' + id + '/permissions')
				.then(function (response) {
					// console.log( response, id );
					return response;
				})
				.catch(function (response) {
					throw response;
				});
		}

	}

})();