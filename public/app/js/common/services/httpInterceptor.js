/**
 * Created by mario on 9/21/16.
 */
(function () {

	'use strict';

	angular
		.module('app')
		.factory('httpInterceptor',httpInterceptor );

	httpInterceptor.$inject = ['$rootScope','$location','$q'];


	function httpInterceptor( $rootScope,$location,$q ) {

		return {
			request: function (config) {
				return config || $q.when(config)
			},
			response: function (response) {
				// console.error('RESPONSE ERROR',response);
				return response || $q.when(response);
			},
			responseError: function (response) {
				if (response.status === 401 ) {
					$location = "/login";
				}
				return $q.reject(response);
			}
		};


	}

})();