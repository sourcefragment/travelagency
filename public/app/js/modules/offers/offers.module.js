/**
 * Created by juan on 17/08/16.
 */

(function () {
    'use strict';

    angular.module('app.offers',[])
        .config(config);

    function config( $stateProvider ) {

        $stateProvider.state('offers', {
            url: '/offers',
            templateUrl: 'app/js/modules/offers/views/list.html',
            controller: 'MainOffersCtrl',
            controllerAs: 'moc'
        })
            .state('offer-create', {
                url: '/offers/create',
                templateUrl: 'app/js/modules/offers/views/edit.html',
                controller: 'OfferCtrl',
                controllerAs: 'oc'

            })
            .state('offer-edit', {
                url: '/offers/edit/:id',
                templateUrl: 'app/js/modules/offers/views/edit.html',
                controller: 'OfferCtrl',
                controllerAs: 'oc'

            });

    }


})();

