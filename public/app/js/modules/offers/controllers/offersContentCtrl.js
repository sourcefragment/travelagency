/**
 * Created by juan on 31/08/16.
 */
(function () {

    'use strict';

    angular
        .module('app.offers')
        .controller('OfferContentCtrl', offersContentController);


    offersContentController.$inject = [
    	'$scope',
	    '$stateParams',
	    '$uibModal',
	    '$filter',
	    'toaster',
	    'DTOptionsBuilder',
	    'DTColumnDefBuilder',
        'offersContentService'
    ];

    function offersContentController(
    	$scope,
	    $stateParams,
	    $uibModal,
	    $filter,
	    toaster,
	    DTOptionsBuilder,
	    DTColumnDefBuilder,
	    offersContentService) {


    	var vm = this;
        vm.offerContents = [];
        vm.offerContentNew = {};
        vm.editModal = false;
        vm.getOffersContent = getOffersContent;
        vm.getFormOfferContent = getFormOfferContent;
        vm.closeModal = closeModal;
        vm.save = save;

        activate();

        /**Init controller
         * */
        function activate() {
            initTable();
            if($stateParams.id)
                getOffersContent($stateParams.id);
        }

        /**Load form create offerType
         * */
        function getFormOfferContent(offerId) {
            vm.offerContents.offerId = offerId;
            vm.editModal = $uibModal.open({
                templateUrl: 'app/js/common/views/content-modal.html',
                scope: $scope,
            });
        }

        function closeModal() {
            vm.editModal.close();
        }

        /**Load offer content
         * */
        function getOffersContent( offerid ) {
            offersContentService.get( offerid )
                .then(function (response) {
                    if (response.data)
                        vm.offersContents = response.data;
                    return vm.offersContent;
                }).catch(function (data) {
                console.log(data);
            });
        }

        /**Save offer Content
         * */
        function save() {

        	if( vm.offerContentNew ){

	        }else{

	        }

        	vm.offerContents.data = vm.offerContentNew;



        }


        /**Create content for this offer
         * */
        function create(){

	        offersContentService.create( offerID,data )
		        .then(function ( response ) {

			        SweetAlert.swal({
				        title: $filter('translate')('content_created'),
				        text: $filter('translate')('content_created_successfully'),
				        type: 'success',
				        timer: 2000,
				        showConfirmButton: false
			        });


		        }).catch(function (response) {


	            });

        }


        /**Update content for the offer
         * */
        function update(){

	        offersContentService.update( offerID,contentID,data )
		        .then(function ( response ) {

			        SweetAlert.swal({
				        title: $filter('translate')('content_updated'),
				        text: $filter('translate')('content_updated_successfully'),
				        type: 'success',
				        timer: 2000,
				        showConfirmButton: false
			        });


		        }).catch(function (response) {


	        });

        }

        /**Init content table structure
         * */
        function initTable() {

            vm.dtOptions = $rootScope.dtOptions;
            vm.dtColumnDefs = [
                DTColumnDefBuilder.newColumnDef(3).notSortable()
            ];

        }


    }


})();
