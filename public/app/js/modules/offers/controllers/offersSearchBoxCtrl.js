/**
 * Created by mario on 1/23/17.
 */
(function () {
	'use strict';

	angular.module('app.offers')
		.controller('OffersSearchBoxCtrl',ctrl);

	ctrl.$inject = [
		'apiService',
		'$scope',
		'SweetAlert',
		'$uibModal',
		'$log'
	];

	function ctrl(
		apiService,
		$scope,
		SweetAlert,
		$uibModal,
		$log
	) {



		/**Open controller modal
		 * */
		$scope.openModal = function(){

			var modalInstance = $uibModal.open({
				templateUrl : 'app/js/modules/offers/views/search-box.html',
				size : 'lg',
				controller : [
					'$scope',
					'$rootScope',
					'NgTableParams',
					function ( $scope,$rootScope,NgTableParams ) {

						$scope.offers = [];
						$scope.tableParams = {};
						$scope.model = null;

						$scope.tableParams = new NgTableParams({},{
							getData : function (params ) {
								return get(params);
							}
						});

						$scope.saveModal = saveModal;

						/**Load vehicle models
						 * */
						function get( params ){
							var p = $rootScope.getParams(params);
							apiService.getAll('offers',p)
								.then(function (response) {
									$scope.offers = response.data.data;
									params.total( response.data.total);
									params.pages = params.generatePagesArray();
									params.current = response.data.current_page;
								});
						}


						/**On modal save
						 * */
						function saveModal(){

							for( var i in $scope.offers ){

								if( $scope.offers[i].checked != null ){
									setModel( $scope.offers[i] );
								}
							}
							this.$close();
						}


					}]

			});


		}

		/**Save selected model
		 * */
		function setModel( model ){
			$scope.model = model;
		}


	}


})();