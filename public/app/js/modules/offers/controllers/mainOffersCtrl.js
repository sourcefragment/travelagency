(function () {

    'use strict';

    angular
        .module('app.offers')
        .controller('MainOffersCtrl', ctrl);

    ctrl.$inject = [
    	'$filter',
	    'SweetAlert',
        '$rootScope',
	    'NgTableParams',
	    'apiService',
	    '$log'
    ];

    function ctrl(
    	$filter,
	    SweetAlert,
        $rootScope,
        NgTableParams,
        apiService,
        $log

    ){

        var vm = this;
        vm.offers = [];
	    vm.tableParams = {};

        vm.getOffers = getOffers;
        vm.delete = deleteOffer;

        activate();

        /**Init controller
         * */
        function activate() {
            initTable();
        }


        /**Init main table structure
         * */
        function initTable() {

        	vm.tableParams = new NgTableParams({},{
				getData : function( params ){
					return getOffers( params );
				}
			})
        }


        /**Load offers
         * */
        function getOffers( params ) {

        	var p = $rootScope.getParams( params );
			// console.log(p,params);
            apiService.getAll('offers', p )
                .then(function ( response ) {
                	vm.offers = response.data.data;
	                params.total( response.data.total);
	                params.pages = params.generatePagesArray();
	                params.current = response.data.current_page;
	                return response;

                }).catch(function (data) {
                    $log.error(data);
                });
        }


        /**Delete offer
         *@param id, the user id
         * @return promise
         */
        function deleteOffer(id) {
            SweetAlert.swal({
                    title: $filter('translate')('offer'),
                    text: $filter('translate')('offer_delete_text'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $filter('translate')('yes_delete'),
                    cancelButtonText: $filter('translate')('no_cancel'),
                    closeOnConfirm: false,
                },
                function (isConfirm) {
                    if (isConfirm) {

                        apiService.delete('offers',id)
                            .then(function (data) {

                                SweetAlert.swal({
                                    title: $filter('translate')('offer'),
                                    text: $filter('translate')('deleted_successfully'),
                                    type: "success",
                                    timer: 2000,
                                    showConfirmButton: false
                                });
                                initTable();

                            }).catch(function ( response ) {
                                $log.log(response);
                            });
                    }
                });
        }


    }

})();





