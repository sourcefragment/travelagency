(function () {

    'use strict';

    angular
        .module('app.offers')
        .controller('OfferCtrl', ctrl);


    ctrl.$inject = [
		'apiService',
        '$stateParams',
        '$location',
	    '$uibModal',
        '$rootScope',
	    'SweetAlert',
	    '$filter',
	    '$log',
		'$timeout'
    ];

    function ctrl(
    	apiService,
        $stateParams,
        $location,
		$uibModal,
        $rootScope,
        SweetAlert,
        $filter,
        $log,
		$timeout

    ) {

        var vm = this;
        vm.id = $stateParams.id;
	    vm.offer = {};
	    vm.offerTypes = [];
	    vm.tourOperators = [];
	    vm.selectedLanguages = [];

	    vm.prices = [];
	    vm.priceTypes = ['adult','children','senior','baby','adult-lunch','children-lunch','senior-lunch'];
	    vm.extraPriceTypes = ['daily','fixed'];
	    vm.spinOptions = {
		    min: 0,
		    max: 30,
		    step: 1
	    };
	    vm.saveAndExit = false;
	    vm.prices = [];

	    vm.loadOfferTypes = loadOfferTypes;
	    vm.setOfferType = setOfferType;

        vm.save = save;
		vm.cancel = cancel;

        activate();

        /**Init controller
         * */
        function activate() {

			loadOfferTypes();
			loadLanguages();

	        if ( vm.id ) {
	            get( vm.id );
            }

        }

        /**Load offer
         * */
        function get( id ) {

            apiService.get('offers',id )
                .then(function ( response ) {
					// $log.log(response);
                	vm.offer = response.data;
	                vm.tourOperator = response.data.tour_operator;
					vm.type = response.data.type;
                })
	            .catch(function (response) {
					$log.error( response );
                });

        }


        /**Save offer
         * */
        function save( mode ) {

			vm.saveAndExit = mode;

        	if( vm.tourOperator ){
        	    vm.offer.tour_operator_id = vm.tourOperator.id;
	        }

        	if( vm.offer.id ){
        		update();
	        }else{
	        	create();
	        }

        }

        /**Create new offer
         * */
        function create(){

	        apiService.create( 'offers',vm.offer )
		        .then( function( response ){
			        SweetAlert.swal({
				        title: $filter('translate')('offer'),
				        text: $filter('translate')('created_successfully'),
				        type: 'success',
				        timer: 1000,
				        showConfirmButton: false
			        });

			        vm.id = response.data.id;
			        vm.offer = response.data;
			        
			        if( vm.saveAndExit )
			            cancel();

		        }).catch(function (response) {
			        // $log.log(response);
			        $rootScope.showErrors(response,'create_error');

	            });

        }

        /**Update offer
         * */
        function update() {

	        apiService.update( 'offers',vm.offer )
		        .then( function( response ){

			        SweetAlert.swal({
				        title: $filter('translate')('offer'),
				        text: $filter('translate')('updated_successfully'),
				        type: 'success',
				        timer: 1000,
				        showConfirmButton: false
			        });

			        if( vm.saveAndExit )
			            cancel();

		        }).catch(function (response) {
		            $log.log(response);
		            $rootScope.showErrors(response,'create_error');

	            });


        }

	    /**Cancel form
	     * */
	    function cancel(){
		    $location.path('/offers');
	    }


	    /**Set offer as featured
	     * */
	    vm.setFeatured = setFeatured;
	    function setFeatured(){

	    	apiService.getAll('offer/'+vm.id+'/set-featured')
			    .then(function (response) {
					vm.offer.featured = response.data.featured;
					$rootScope.saved();
			    })
			    .catch(function (response) {
					$log.error(response);
			    });
	    }

	    /**====================
	     * PRICES
	     * ===================*/
	    vm.loadPrices = loadPrices;
	    function loadPrices(){

	    	apiService.getAll('offer/'+vm.id+'/prices')
			    .then(function (response) {
				   vm.prices = response.data;
			    })
			    .catch(function (response) {
				    $log.error(response);
			    });
	    }

	    /**Add a new price to the offer
	     * */
	    vm.addPrice = addPrice;
	    function addPrice(){

	    	vm.prices.push({
	    	    offer_id:vm.id,
			    net_price:vm.type.price,
			    pvp_price:vm.type.price,
			    discount_price:0,
			    discount:0,
			    changed:1
		    });

	    }


	    /**Update price
	     * */
	    vm.savePrice = savePrice;
	    function savePrice( item ){

	    	if( !item.id ){
	    		createPrice(item);
		    }else{
	    		updatePrice(item);
		    }
	    }


	    /**Create price
	     * */
	    function createPrice( item ){

	    	apiService.create('offer-prices',item)
			    .then(function (response) {
			    	item.id = response.data.id;
				    item.changed = false;
			    })
			    .catch(function (response) {
			    	$log.error(response);
				    $rootScope.showErrors(response);
			    });
	    }

		/**Update offer price
		 * */
	    function updatePrice( item ){

	    	apiService.update('offer-prices',item)
			    .then(function (response) {
				    item.changed = false;
			    })
			    .catch(function (response) {
				   $log.error(response);
				   $rootScope.showErrors(response);
			    });
	    }

	    /**Delete price
	     * */
	    vm.deletePrice = deletePrice;
	    function deletePrice( item,index ){

	    	$log.error(item,index);

	    	if( typeof item.id === 'undefined' ){
			    vm.prices.splice(index,1);
			    return false;
		    }

		    SweetAlert.swal({
				    title: $filter('translate')('price'),
				    text: $filter('translate')('price_delete_text'),
				    type: "warning",
				    showCancelButton: true,
				    confirmButtonColor: "#DD6B55",
				    confirmButtonText: $filter('translate')('yes_delete'),
				    cancelButtonText: $filter('translate')('no_cancel'),
				    closeOnConfirm: false,
			    },
			    function (isConfirm) {
				    if (isConfirm) {

					    apiService.delete('offer-prices',item.id)
						    .then(function (data) {

							    SweetAlert.swal({
								    title: $filter('translate')('price'),
								    text: $filter('translate')('deleted_successfully'),
								    type: "success",
								    timer: 2000,
								    showConfirmButton: false
							    });
							    loadPrices();

						    }).catch(function ( response ) {
						    $log.log(response);
					    });
				    }
			    });

	    }



	    /**====================
		 * ROUTES
		 * ==================*/
	    vm.routes = [];
	    vm.loadRoutes = loadRoutes;


	    /**Load routes
		 * */
	    function loadRoutes(){
	    	apiService.getAll('offer/'+vm.id+'/routes')
				.then(function (response) {
					vm.routes = response.data;
                })
				.catch(function (response) {
					$log.error(response);
                });
		}



	    /**======================
	     * MEDIA LOADER
	     * ====================*/

	    /**Get offer types
	     * */
	    function loadOfferTypes(){
		    apiService.getAll('offer-types',{limit:-1,sort:{name:'asc'}})
			    .then(function( response ){
				    vm.offerTypes = response.data;
			    })
			    .catch(function ( response ) {
				    $log.error( response );
			    });
	    }



	    /**Will load the offer type
	     * */
	    function setOfferType(){

	    	apiService.get('offer-types',vm.offer.offer_type_id)
			    .then(function (response) {

			    	vm.offer.adult_price = response.data.adult_price;
				    vm.offer.children_price = response.data.children_price;
				    vm.offer.senior_price = response.data.senior_price;
				    vm.offer.baby_discount = response.data.baby_discount;


			    })
			    .catch(function (response) {
				    $log.error(response);
			    });
	    }




	    /**==============
	     * EXTRAS
	     * ==============*/

        vm.extras = [];
        vm.availableExtras = [];
        vm.loadExtras = loadExtras;
        vm.addExtra = addExtra;
        vm.saveExtra = saveExtra;
        vm.deleteExtra = deleteExtra;

	    /**Load available items
	     * */
	    function loadAvailableExtras(){
	    	apiService.getAll('items',{limit:-1})
			    .then(function (response) {
				    $log.info(response);
			    	vm.availableExtras = response.data;
			    })
			    .catch(function (response) {
				    $log.error(response);
			    });
	    }

	    /**Load offer extras
	     * */
	    function loadExtras() {
	    	loadAvailableExtras();
			apiService.getAll('offer/'+vm.id+'/extras')
				.then(function (response) {
					vm.extras = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});
	    }

	    /**Add new extra
	     * */
	    function addExtra(){
			vm.extras.push({
				offer_id:vm.id,
				quantity:0,
				pvp_price:0,
				net_price:0,
				discount:0
			});
	    }

	    /**Save new extra
	     * */
	    function saveExtra( item ){

	    	if( item.id ){
	    		updateExtra(item);
		    }else{
		    	createExtra(item);
		    }
	    }

	    /**Create new extra
	     * */
	    function createExtra( item ){

			apiService.create('offer-extras',item)
				.then(function (response) {
					loadExtras();
				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'error');
				});
	    }


	    /**Update extra
	     * */
	    function updateExtra( item ){
			apiService.update('offer-extras',item)
				.then(function (response) {
					item.changed = false;
				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'error');
				});
	    }


	    /**Delete extra
	     * */
	    function deleteExtra( item,index ){

	    	if( item.id ){
	    		apiService.delete('offer-extras',item.id)
				    .then(function (response) {
					    loadExtras();
				    })
				    .catch(function (response) {
					    $log.error(response);
					    $rootScope.showErrors(response,'error');
				    });
		    }else{
		    	deleteTmpExtra(index);
		    }
	    }


	    /**Delte unsave object
	     * */
	    function deleteTmpExtra( index ){
		    vm.extras.splice(index,1);
	    }




	    /**==================
	     * VISAS
	     * =================*/

	    /**Load available countries
	     * */
		vm.requiredVisas = [];
		vm.notRequiredVisas = [];
		vm.countries = [];

		vm.loadVisas = loadVisas;
		function loadVisas(){
			loadCountries();
			loadRequiredVisas();
			loadNotRequiredVisas();
		}


		/**Load countries
		 * */
		vm.loadCountries = loadCountries;
		function loadCountries(){

			apiService.getAll('countries',{limit:-1})
				.then(function (response) {
					vm.countries = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				})
		}

		/**Load required visas
		 * */
		vm.loadRequiredVisas = loadRequiredVisas;
		function loadRequiredVisas(){

			apiService.getAll('offer/'+vm.id+'/required-visas')
				.then(function (response) {
					$log.error(response);
					vm.requiredVisas = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}


		/**Load not required visas
		 * */
		vm.loadNotRequiredVisas = loadNotRequiredVisas;
		function loadNotRequiredVisas() {

			apiService.getAll('offer/'+vm.id+'/not-required-visas')
				.then(function (response) {
					$log.error(response);
					vm.notRequiredVisas = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}



       /**======================
        * LANGUAGES
        * ======================*/
		vm.loadLanguages = loadLanguages;
		function loadLanguages(){

			apiService.getAll('languages',{limit:-1})
				.then(function (response) {
					vm.languages = response.data;
					loadOfferLanguages();
				})
				.catch(function (response) {
					$log.error(response);
				});
		}

		/**Load offer languages
		 * */
		vm.loadOfferLanguages = loadOfferLanguages;
		function loadOfferLanguages() {

			vm.selectedLanguages = [];
			apiService.getAll('offer/'+vm.id+'/languages',{limit:-1})
				.then(function (response) {

					angular.forEach(response.data,function (value,index) {
						vm.selectedLanguages.push(value.language_id);
					});
				})
				.catch(function (response) {
					$log.error(response);
				});
		}



		vm.onLanguageSelect = onLanguageSelect;
		function onLanguageSelect( item ) {

			apiService.getAll('offer/'+vm.id+'/add-language/'+item.id)
				.then(function (response) {
					$log.info(response);
				})
				.catch(function (response) {
					$rootScope.showErrors(response);
					$log.error(response);
				});
		}
		
		
		vm.onLanguageRemove = onLanguageRemove;
		function onLanguageRemove( item ) {

			apiService.getAll('offer/'+vm.id+'/remove-language/'+item.id)
				.then(function (response) {
					$log.info(response);
				})
				.catch(function (response) {
					$rootScope.showErrors(response);
					$log.error(response);
				});

		}


		/**Assign Tour operator
		 * */
		vm.assignTourOperator = assignTourOperator;
		function assignTourOperator( id ){

			apiService.get('tour-operators',id)
				.then(function (response) {
					vm.tourOperator = response.data;
                })
				.catch(function (response) {
					$log.error(response);
                });
		}

    }




})();





