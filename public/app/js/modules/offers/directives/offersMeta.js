(function () {
    'use strict';

    angular.module('app.offers')
        .directive('companySelector',companySelector);

    function companySelector(){

        return {
            restrict:'AE',
            required:'ngModel',
            scope:{
                company:'=ngModel'
            },
            templateUrl:'app/js/sections/companies/views/company-selector.html',
            controller:'CompaniesController',
            controllerAs:'$ctrl'
        };
    }
})();