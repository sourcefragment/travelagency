/**
 * Created by juan on 18/08/16.
 */
(function () {

    angular.module('app.offers')
        .directive('createOfferTypeBtn', createOfferTypeBtn)
        .directive('createOfferTypeBtnLong', createOfferTypeBtnLong)
        .directive('createOfferTypeBtnSmall', createOfferTypeBtnSmall);

    function createOfferTypeBtn() {
        return {
            template: '<button type="button" ng-click="$otctrl.openModal()" class="btn btn-primary btn-circle">'
                            +'<i class="fa fa-plus"></i>'
                     +'</button>',
            controller: 'OffersTypeController',
            controllerAs: '$otctrl',
        };
    }

    function createOfferTypeBtnLong() {
        return {
            template: '<button type="button" class="btn btn-primary btn-lg" ui-sref="offer-types-create">'
                            + '<i class="fa fa-plus-circle"></i> [[ "new_offers_type" | translate ]]'
                     +'</button>',
            controller: 'OffersTypeController',
            controllerAs: 'offersTypec',
        };
    }

    function createOfferTypeBtnSmall() {
        return {
            template: '<a ng-click="$ctrl.getFormOfferContent()" class="btn btn-primary btn-xs" style="margin: 0;">'
                            + '[[ "new_content_language" | translate ]]'
                     +'</a>',
            controller: 'OffersTypeContentController',
            controllerAs: '$ctrl',
        };
    }

})();