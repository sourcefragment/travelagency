(function () {

    angular.module('app.offers')
        .directive('createOfferBtn', createOfferBtn)
	    .directive('offerSearchBox',offerSearchBox);

	function offerSearchBox() {
		return {
			restrict:'A',
			scope:{
				model:'=ngModel'
			},
			link:function(scope,element,attrs){
				element.click( scope.openModal );
			},
			controller: 'OffersSearchBoxCtrl'
		}
	}


    function createOfferBtn() {
        return {
            template: '<button type="button" class="btn btn-primary btn-lg" ui-sref="offer-create">'
                + '<i class="fa fa-plus-circle"></i> [[ "new_offer" | translate ]]'
            + '</button>'
        };
    }
})();