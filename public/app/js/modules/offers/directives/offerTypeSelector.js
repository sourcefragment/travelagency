/**
 * Created by juan on 26/09/16.
 */
(function () {
    'use strict';

    angular.module('app.offers')
        .directive('offerTypeSelector',offerTypeSelector);

    function offerTypeSelector(){
        return {
            restrict:'AE',
            required:'ngModel',
            scope:{
                offerType:'=ngModel'
            },
            templateUrl:'app/js/sections/offers/views/offer-type-selector.html',
            controller:'OffersTypeController',
            controllerAs:'$ctrl'
        };
    }


})();