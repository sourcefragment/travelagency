/**
 * Created by juan on 31/08/16.
 */
(function () {

    angular.module('app.offers')
        .directive('createOfferContentBtn', createOfferContentBtn);

    function createOfferContentBtn() {
        return {
            template: '<button type="button" ng-click="offersContentc.getFormOfferContent()" class="btn btn-outline btn-primary btnadd">'
                             +'<i class="fa fa-plus"></i>'
                     +'</button>',
            controller: 'OffersContentController',
            controllerAs: 'offersContentc',
        };
    }
})();