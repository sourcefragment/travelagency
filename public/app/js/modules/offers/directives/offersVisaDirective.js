/**
 * Created by juan on 31/08/16.
 */
(function () {

    angular.module('app.offers')
        .directive('createOfferVisaBtn', createOfferVisaBtn);

    function createOfferVisaBtn() {
        return {
            template: '<button type="button" ng-click="offersVisac.getFormOfferVisa()" class="btn btn-outline btn-primary btnadd">'
                         +'<i class="fa fa-plus"></i>'
                     +'</button>',
            controller: 'OffersVisaController',
            controllerAs: 'offersVisac',
        };
    }
})();