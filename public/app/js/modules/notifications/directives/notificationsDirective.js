/**
 * Created by juan on 18/08/16.
 */
(function () {

    angular.module('app.notifications')
        .directive('newNotificationBtn', newNotificationBtn);



    function newNotificationBtn() {
        return {
            template: '<button type="button" class="btn btn-primary btn-lg" ng-click="$notificationBtn.openModal()">'
                          + '<i class="fa fa-plus-circle"></i> [[ "new_notification" | translate  ]]'
                    + '</button>',
            controller: 'NotificationsController',
            controllerAs: '$notificationBtn'
        };
    }


})();