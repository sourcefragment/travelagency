/**
 * Created by juan on 17/08/16.
 */

(function () {
    'use strict';

    angular.module('app.notifications',[])
        .config(config);

    function config($stateProvider) {

        $stateProvider.state('notifications', {
            url: '/notifications',
            templateUrl: 'app/js/modules/notifications/views/list.html',
            controller: 'MainNotificationsCtrl',
            controllerAs: 'mnc'
        })
            .state('notification-create', {
                url: '/notifications/create',
                templateUrl: 'app/js/modules/notifications/views/edit.html',
                controller: 'NotificationCtrl',
                controllerAs: 'nc'

            })
            .state('notification-edit', {
                url: '/notifications/edit/:id',
                templateUrl: 'app/js/modules/notifications/views/edit.html',
                controller: 'NotificationCtrl',
                controllerAs: 'nc'

            })

    };


})();

