(function () {
	'use strict';

	angular
		.module('app.notifications')
		.controller('NotificationCtrl',ctrl);


	ctrl.$inject = [
		'$stateParams',
		'$location',
		'$filter',
		'SweetAlert',
		'$uibModal',
		'$rootScope',
		'apiService',
		'$log'
	];

	function ctrl(
		$stateParams,
		$location,
		$filter,
		SweetAlert,
		$uibModal,
		$rootScope,
		apiService,
		$log

	){

		var vm = this;
		vm.id  = $stateParams.id;
		vm.notification = {};
		vm.locations = [];
		vm.saveAndExit = false;

		vm.save = save;
		vm.cancel = cancel;


		init();

		/**Init view
		 * */
		function init(){

			if( vm.id ){
				get( vm.id );
			}

		}


		/**Load notification
		 * */
		function get( id ){

			apiService.get( 'notifications',id )
				.then(function (response) {
					vm.notification = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}



		/**Save notification
		 * */
		function save( mode ){

			vm.saveAndExit = mode;

			if( vm.id ){
				update();
			}else{
				create();
			}
		}

		/**Create new notification
		 * */
		function create(){
			apiService.create( 'notifications',vm.notification )
				.then(function (response) {
					// console.log(response);
					SweetAlert.swal({
						title: $filter('translate')('notification'),
						text: $filter('translate')('created_successfully'),
						type: 'success',
						timer: 2000,
						showConfirmButton: false
					});

					vm.id = response.data.id;
					vm.notification = response.data;

					cancel();
				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'create_error');
				});

		}


		/**Update notification
		 * */
		function update(){
			apiService.update( 'notifications',vm.notification )
				.then(function (response) {
					// $log.error(response);
					SweetAlert.swal({
						title: $filter('translate')('notification'),
						text: $filter('translate')('updated_successfully'),
						type: 'success',
						timer: 2000,
						showConfirmButton: false
					});

					cancel();
				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'update_error')
				});

		}


		/**On cancel
		 * */
		function cancel(){
			$location.path('/notifications');
		}

	}

})();