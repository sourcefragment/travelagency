(function () {

	'use strict';

	angular
		.module('app.notifications')
		.controller('MainNotificationsCtrl',ctrl);

	ctrl.$inject = [
		'apiService',
		'$rootScope',
		'$filter',
		'SweetAlert',
		'NgTableParams',
		'$log'
	];


	function ctrl(
		apiService,
		$rootScope,
		$filter,
		SweetAlert,
		NgTableParams,
		$log

	){


		var vm = this;

		vm.notifications = [];
		vm.tableParams = {};
		vm.delete = deleteItem;


		init();

		function init(){
			initTable();
		}


		/**Init main view table
		 * */
		function initTable(){

			vm.tableParams = new NgTableParams({},{
				getData : function( params ){
					return getAll( params );
				}
			});
		}

		/**Get all notifications for listing
		 *@param params object
		 * */
		function getAll( params ){

			var p = $rootScope.getParams( params );
			apiService.getAll( 'notifications', p )
				.then(function (response) {
					// $log.error(response);
					vm.notifications = response.data.data;
					params.total( response.data.total);
					params.pages = params.generatePagesArray();
					params.current = response.data.current_page;
					return response;
				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'notification_error');
				})
		}

		/**Delete angecy
		 * */
		function deleteItem( id ){

			SweetAlert.swal({
					title: $filter('translate')('delete_notification'),
					text: $filter('translate')('delete_notification_text'),
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: $filter('translate')('yes_delete'),
					cancelButtonText: $filter('translate')('no_cancel'),
					closeOnConfirm: false,
				},
				function (isConfirm) {

					if (isConfirm) {

						apiService.delete( 'notifications',id )
							.then( function( response ){
								SweetAlert.swal({
									title: $filter('translate')('notification'),
									text: $filter('translate')('deleted_successfully'),
									type: 'success',
									timer: 2000,
									showConfirmButton: false
								});
								initTable();

							}).catch(function ( response ) {

							var msg = response.data.error;

							SweetAlert.swal({
								title : $filter('translate')('delete_error'),
								text : $filter('translate')( msg ),
								type : 'error',
								showConfirmButton : true,
								closeOnConfirm : true
							});

						});
					}

				});

		}
	}

})();