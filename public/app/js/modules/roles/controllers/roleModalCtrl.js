/**
 * Created by mario on 12/22/16.
 */
(function () {
	'use strict';

	angular.module('app.roles')
		.controller('RoleModalCtrl',ctrl );

	ctrl.$inject = [
		'$scope',
		'$uibModal',
		'$rootScope',
		'apiService',
		'$log'
	];


	/**Role modale ctrl
	 * */
	function ctrl(
		$scope,
		$uibModal,
		$rootScope,
		apiService,
		$log
	){


		/**Open directive modal
		 * */
		$scope.openModal = function () {

			var instance = $uibModal.open({
				templateUrl : 'app/js/sections/roles/views/new-role-modal.html',
				controller : ['$scope',function ($scope) {

					$scope.role = {};

					/**Save new role
					 * */
					$scope.saveModal = function () {

						apiService.create('roles',$scope.role)
							.then(function (response) {
								$scope.$close();
								onRoleCreated();

							})
							.catch(function (response) {
								console.log(response);
								$rootScope.showErrors(response,'role_create_error');
							});
					}

				}]
			});
		};

		/**On role created
		 * */
		function onRoleCreated(){

			if( typeof $scope.onSuccess == 'function' ){
				$scope.onSuccess();
			}

		}
	}

})();