(function () {

    'use strict';

    angular
        .module('app.roles')
        .controller('MainRolesCtrl', mainRolesController);

    mainRolesController.$inject = [
        'apiService',
        '$filter',
	    '$rootScope',
	    'SweetAlert',
	    'NgTableParams',
	    '$log'
    ];

    function mainRolesController(
        apiService,
        $filter,
        $rootScope,
        SweetAlert,
        NgTableParams,
        $log
    ) {


        var vm = this;
        vm.roles = [];
	    vm.tableParams = {};


        vm.delete = deleteRole;

        activate();

        /**Init controller
         * */
        function activate() {
            initTable();
        }


        /**Init main table structure
         * */
        function initTable() {
			vm.tableParams = new NgTableParams({},{
				getData : function( params ){
					return getAll( params );
				}
			})
        }


        /**Load roles
         * */
        function getAll( params ){

        	var p = $rootScope.getParams( params );
            apiService.getAll( 'roles',p )
                .then(function ( response ) {
					// $log.error( response );
                	vm.roles = response.data.data;
	                params.total( response.data.total);
	                params.pages = params.generatePagesArray();
	                params.current = response.data.current_page;
	                return response;
                }).catch(function () {
                    $log.error(data);
            });
        }


        /**Delete user
         *@param id, the user id
         * @return promise
         */
        function deleteRole( id ) {
			// $log.error('delete role');
	        SweetAlert.swal({
			        title: $filter('translate')('delete_role'),
			        text: $filter('translate')('delete_role_text'),
			        type: "warning",
			        showCancelButton: true,
			        confirmButtonColor: "#DD6B55",
			        confirmButtonText: $filter('translate')('yes_delete'),
			        cancelButtonText: $filter('translate')('no_cancel'),
			        closeOnConfirm: false,
		        },
		        function (isConfirm) {

			        if (isConfirm) {

				        apiService.delete( 'roles',id)
					        .then(function () {
						        SweetAlert.swal({
							        title: $filter('translate')('deleted'),
							        text: $filter('translate')('delete_role_text'),
							        type: "success",
							        timer: 3000,
							        showConfirmButton: false
						        });
						        initTable();

					        }).catch(function ( response ) {
					            // $log.error( response );
					            var msg = response.data.error;

						        SweetAlert.swal({
							        title : $filter('translate')('delete_role_error'),
							        text : $filter('translate')( msg ),
							        type : "error",
							        showConfirmButton : true,
							        closeOnConfirm : true
						        });
				            });
			        }

		        });

        }



    }


})();





