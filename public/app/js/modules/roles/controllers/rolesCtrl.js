(function () {

    'use strict';

    angular
        .module('app.roles')
        .controller('RolesCtrl', rolesController);


    rolesController.$inject = [
    	'$stateParams',
        '$rootScope',
	    'apiService',
	    'SweetAlert',
	    '$filter',
	    '$location',
	    '$log'
    ];

    function rolesController(
    	$stateParams,
        $rootScope,
	    apiService,
        SweetAlert,
        $filter,
        $location,
        $log
    ) {


        var vm = this;
        vm.role = false;
        vm.permissions = [];
	    vm.createBox = 0;
	    vm.readBox = 0;
	    vm.updateBox = 0;
	    vm.deleteBox = 0;
	    vm.selectedRole = false;
        vm.id = $stateParams.id;
        vm.saveAndExit = false;

        vm.getRoles = getRoles;
        vm.getRolePermissions = getRolePermissions;
	    vm.permissionChanged = permissionChanged;
        vm.save = save;
		vm.checkAll = checkAll;
	    vm.cancel = cancel;



        activate();

        /**Init controller
         * */
        function activate() {

            initTable();

            if( vm.id ){
	            get( vm.id );
            }

        }

        /**Init main table structure
         * */
        function initTable() {

        }

        /**Load role
         * */
        function get( id ) {

        	apiService.get( 'roles',id )
                .then(function (response) {
                	vm.role = response.data;
	                getRolePermissions( id );
                }).catch(function () {
            });
        }

        /**Load permissions for a specific role
         * @param id int
         * */
        function getRolePermissions( id ) {

            apiService.getAll( 'role/'+ id +'/permissions',{limit:-1})
                .then(function ( response ) {
                	// console.log(response,id);
                	vm.permissions = response.data;
                }).catch(function () {
                    console.error(data);
            });
        }

        /**Load roles
         * */
        function getRoles() {

            apiService.getAll('roles')
                .then(function ( response ) {
                	vm.roles = response.data.data;
                }).catch(function () {
                    console.error(data);
            });
        }


        /**Save
         * */
        function save( mode ) {

        	vm.saveAndExit = mode;

        	if( vm.role.id ){
        		update();
	        }else{
	        	create();
	        }

        }


		/**Create new role
		 * */
		function create(){

			apiService.create( 'roles',vm.role )
				.then(function ( response ) {
					// console.log(response);
					SweetAlert.swal({
						title: $filter('translate')('role'),
						text: $filter('translate')('created_successfully'),
						type: 'success',
						timer: 1000,
						showConfirmButton: false
					});

					vm.id = response.data.id;
					vm.role = response.data;

					if( vm.saveAndExit )
						cancel();

				})
				.catch(function ( response ) {
					console.log(response);
					$rootScope.showErrors(response,'create_error');

				});

		}

        /**Update role
         * */
        function update( ){

        	apiService.update( 'roles', vm.role )
		        .then(function ( response ) {

		        	for( var i in vm.permissions ){

		        		var data = vm.permissions[i];

						if( data.changed ){
							apiService.update( 'permissions',data )
								.then( function ( resp ) {
									$log.error( resp );
								})
								.catch(function ( resp ) {
									$log.error( resp );
								});
						}

			        }

			        SweetAlert.swal({
				        title: $filter('translate')('role'),
				        text: $filter('translate')('updated_successfully'),
				        type: 'success',
				        timer: 1000,
				        showConfirmButton: false
			        });

			        if( vm.saveAndExit )
			        	cancel();


		        })
		        .catch(function ( response ) {
			       console.error( response );
			        $rootScope.showErrors(response,'update_error');

		        });
        }




		/**Check all fields on permissions table
		 * */
        function checkAll( field ){


        	switch( field ){

		        case 'create':
			        angular.forEach(vm.permissions,function ( item ) {
				        item.c = vm.createBox;
				        item.changed = true;
			        });
		        	break;

		        case 'read':
			        angular.forEach(vm.permissions,function ( item ) {
				        item.r = vm.readBox;
				        item.changed = true;
			        });
		        	break;

		        case 'update':
			        angular.forEach(vm.permissions,function ( item ) {
				        item.u = vm.updateBox;
				        item.changed = true;
			        });
		        	break;

		        case 'delete':
			        angular.forEach(vm.permissions,function ( item ) {
				        item.d = vm.deleteBox;
				        item.changed = true;
			        });
		        	break;
	        }

        }


        /**On permission changed
         * */
        function permissionChanged( p ){
        	p.changed = true;
        }

        /**Cancel form
         * */
        function cancel(){
        	$location.path('/roles');
        }





    }


})();