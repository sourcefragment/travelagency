/**
 * Created by juan on 17/08/16.
 */

(function () {
    'use strict';

    angular.module('app.roles',[])
        .config(config);


    function config($stateProvider) {

        $stateProvider.state('roles', {
            url: '/roles',
            templateUrl: 'app/js/modules/roles/views/list.html',
            controller: 'MainRolesCtrl',
            controllerAs: 'mrc'
        })
            .state('role-create', {
                url: '/roles/create',
                templateUrl: 'app/js/modules/roles/views/edit.html',
                controller: 'RolesCtrl',
                controllerAs: 'rc'

            })
            .state('role-edit', {
                url: '/roles/edit/:id',
                templateUrl: 'app/js/modules/roles/views/edit.html',
                controller: 'RolesCtrl',
                controllerAs: 'rc'

            })


    }

})();

