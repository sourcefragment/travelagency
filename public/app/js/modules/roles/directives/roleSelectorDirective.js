/**
 * Created by mario on 9/12/16.
 */
(function () {
	'use strict';

	angular
		.module('app.roles')
		.directive('roleSelector',roleSelector);

	function roleSelector(){

		return {

			restrict:'AE',
			scope:{
				role : '=ngModel',
				roles : '=',
				class : '@ngClass',
			},
			template: '<select class="[[ class ]]" ng-model="$parent.role" ng-options="item.id as item.name for item in $parent.roles">'
			 +'<option value="">[[ "choose_role" | translate ]]</option>'
			+'</select>'



		};
	}
})();