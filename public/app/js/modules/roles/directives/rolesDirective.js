/**
 * Created by juan on 18/08/16.
 */
(function () {

    angular.module('app.roles')
        .directive('createRoleBtnLong', createRoleBtnLong)
        .directive('createRoleBtn', createRoleBtn);


    function createRoleBtn() {
        return {
            template: '<button type="button" ng-click="rolec.getFormRole()" class="btn btn-outline btn-primary">'
                             +'[[ "new_role" | translate]]'
                    +'</button>',
            controller: 'RolesController',
            controllerAs: 'rolec',
        };
    }

    function createRoleBtnLong() {
        return {
            template: '<button type="button"  class="btn btn-primary btn-lg" ui-sref="rolesCreate">'
                            +'<i class="fa fa-plus-circle"></i> [[ "new_role" | translate]]'
                    +'</button>',
            controller: 'MainRolesController',
            controllerAs: 'rolec',
        };
    }



})();