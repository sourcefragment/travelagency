/**
 * Created by mario on 9/15/16.
 */
(function () {
	'use strict';


	angular.module('app.roles')
		.directive('newRoleBtn',newRoleBtn);


	function newRoleBtn(){

		return {

			restrict:'EA',
			scope : {
				onSuccess : '&onSuccess'
			},
			template:'<button type="button" ng-click="openModal()" class="btn btn-primary btn-circle">'
						+'<i class="fa fa-plus"></i>'
					+'</button>',
			controller:'RoleModalCtrl'
		};
	}

})();