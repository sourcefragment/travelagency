/**
 * Created by mario on 3/8/17.
 */
(function () {

	'use strict';

	angular.module('app.cities')
		.controller('CityPickerCtrl',ctrl);


	ctrl.$inject = [
		'$scope',
		'$uibModal',
		'$rootScope',
		'apiService',
		'$log'
	];

	function ctrl(
		$scope,
		$uibModal,
		$rootScope,
		apiService,
		$log
	){

		/**Open modal
		 * */
		$scope.openModal = function(){

			$scope.modalInstance = $uibModal.open({
				templateUrl : 'app/js/modules/cities/views/city-picker-modal.html?v=2',
				controller : ['$scope',function ($scope) {

					$scope.countries = [];
					$scope.cities = [];
					$scope.states = [];
					$scope.city = {};
					$scope.country = {};
					$scope.state = {};
					$scope.foundCities = [];
					$scope.cityName = '';
					$scope.isSearching = false;


					$scope.loadCountries = loadCountries;
					$scope.loadStates = loadStates;
					$scope.loadCities = loadCities;
					$scope.searchCity = searchCity;
					$scope.selectFoundCity = selectFoundCity;

					loadCountries();
					/**Load countries
					 * */
					function loadCountries(){

						var p = {
							limit : -1,
							sort : {
								name : 'asc'
							}
						};

						apiService.getAll('countries',p)
							.then(function (response) {
								// $log.error(response);
								$scope.countries =  response.data;
							})
							.catch(function (response) {
								$log.error(response);
							})
					}


					/**Load states
					 * */
					function loadStates( country ){

						var p = {
							filter : {
								country_id : country
							},
							limit : -1,
							sort : {
								name : 'asc'
							}
						};
						// $log.error(p);
						apiService.getAll('states',p)
							.then(function (response) {
								// $log.error(response);
								$scope.states = response.data;
							})
							.catch(function (response) {
								$log.error(response);
							})
					}


					/**Load cities
					 * */
					function loadCities( state ) {

						var p = {
							filter : {
								state_id : state
							},
							limit : -1,
							sort : {
								name : 'asc'
							}
						};
						apiService.getAll('cities',p)
							.then(function (response) {
								// $log.error(response);
								$scope.cities = response.data;
							})
							.catch(function (response) {
								$log.error(response);
							})
					}



					/**Search city by its name
					 * */
					function searchCity( name ){

						if( name.length <= 2 ){
							return;
						}

						var p = {
							filter : {
								name : name
							},
							limit:-1,
							sort :{
								name : 'asc'
							}
						};

						$scope.isSearching = true;

						apiService.getAll('cities',p)
							.then(function (response) {
								$scope.isSearching = false;
								$scope.foundCities = response.data;
							})
							.catch(function (response) {
								$log.error(response);
							});

					}

					/**Select found city and auto complete
					 * */
					function selectFoundCity( city ){

						$scope.cities = [];
						$scope.cities.push(city);
						$scope.city = city.id;
						$scope.cityName = '';
						$scope.foundCities = [];

					}


					/**On modal save
					 * */
					$scope.saveModal = function(){

						var city = {};
						for(var i in $scope.cities ){

							if( $scope.cities[i].id == $scope.city){
								city = $scope.cities[i];
							}
						}
						setModel(city);
						this.$close();
					}


				}]
			})
		};


		/**Save values on modal save
		 * */
		function setModel( model ){
			$scope.model = model;

		}


	}
})();