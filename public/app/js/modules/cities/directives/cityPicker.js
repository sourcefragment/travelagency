/**
 * Created by mario on 12/17/16.
 */
(function () {
	'use strict';

	angular.module('app.cities')
		.directive('cityPicker',cityPicker);


	function cityPicker(){
		return {
			restrict : 'A',
			scope :{
				model:'=ngModel'
			},
			link : function( scope,element,attrs ){
				element.click( scope.openModal );
			},
			controller : 'CityPickerCtrl'
		}
	}
})();