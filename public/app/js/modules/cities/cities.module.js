/**
 * Created by mario on 11/30/16.
 */
(function () {

	'use strict';

	angular.module('app.cities',[])
		.config(config);

	function config( $stateProvider ){

		$stateProvider.state('cities', {
			url: '/cities',
			templateUrl: 'app/js/modules/cities/views/list.html',
			controller: 'MainAgenciesCtrl',
			controllerAs: 'mac'
		});

	}


})();