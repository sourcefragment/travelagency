/**
 * Created by mario on 2/17/17.
 */
(function () {
	'use strict';

	angular.module('app.clients')
		.directive('clientSearchBox',clientSearchBox)
		.directive('addressModal',addressModal)
		.directive('clientCreateModal',clientCreateModal);


	function clientSearchBox() {
		return {
			restrict:'A',
			scope:{
				model:'=ngModel'
			},
			link:function(scope,element,attrs){
				element.click( scope.openModal );
			},
			controller: 'ClientsSearchBoxCtrl'
		}
	}

	function clientCreateModal(){
		return {
			restrict:'A',
			scope:{
				onSuccess:'&onSuccess'
			},
			link:function(scope,element,attrs){
				element.click( scope.openModal );
			},
			controller: 'ClientCreateCtrl'
		}
	}

	/**Create new address
	 * */
	function addressModal(){
		return {
			restrict:'A',
			scope:{
				client:'@client',
				onSuccess:'&onSuccess',
				model:'=ngModel'
			},
			link:function(scope,element,attrs){
				element.click( scope.openModal );
			},
			controller: 'AddressModalCtrl'
		}
	}

})();