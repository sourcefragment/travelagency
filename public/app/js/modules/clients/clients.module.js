/**
 * Created by mario on 1/25/17.
 */
(function () {
	'use strict';

	angular.module('app.clients',[])
		.config(config);

	function config($stateProvider) {

		$stateProvider.state('clients', {
			url: '/clients',
			templateUrl: 'app/js/modules/clients/views/list.html',
			controller: 'MainClientsCtrl',
			controllerAs: 'mcc'
		})
			.state('client-create', {
				url: '/clients/create',
				templateUrl: 'app/js/modules/clients/views/edit.html',
				controller: 'ClientCtrl',
				controllerAs: 'cc'

			})
			.state('client-edit', {
				url: '/clients/edit/:id',
				templateUrl: 'app/js/modules/clients/views/edit.html',
				controller: 'ClientCtrl',
				controllerAs: 'cc'

			});



	};


})();