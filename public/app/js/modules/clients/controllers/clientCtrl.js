/**
 * Created by mario on 2/17/17.
 */
(function () {

	'use strict';

	angular
		.module('app.clients')
		.controller('ClientCtrl', ctrl);

	ctrl.$inject = [
		'$stateParams',
		'$location',
		'apiService',
		'$filter',
		'SweetAlert',
		'NgTableParams',
		'$rootScope',
		'$log'
	];

	function ctrl(
		$stateParams,
		$location,
		apiService,
		$filter,
		SweetAlert,
		NgTableParams,
		$rootScope,
		$log
	) {


		var vm = this;
		vm.id = $stateParams.id;
		vm.client = false;
		vm.tableParams = {};
		vm.languages = [];
		vm.treatments = [
			'sr',
			'mr',
			'mrs',
			'dr',
			'ms'
		];
		vm.saveAndExit = false;


		vm.save = save;
		vm.cancel = cancel;


		activate();

		/**Init controller
		 * */
		function activate() {

			loadLanguages();
			if ( vm.id ){
				get(vm.id);
			}

		}


		/**Load languages
		 * */
		function loadLanguages(){
			apiService.getAll('languages',{limit:-1})
				.then(function(response){
					vm.languages = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});

		}


		/**Load client<
		 * */
		function get( id ) {

			apiService.get('clients',id)
				.then(function ( response ) {
					// $log.error(response);
					vm.client = response.data;
					vm.city = response.data.city;
					vm.billingCity = response.data.billing_city;
					vm.company = response.data.company;

				}).catch(function () {
					$log.error(data);
				});
		}


		/**Save clients
		 * */
		function save( mode ) {

			vm.saveAndExit = mode;

			//Update city
			if( vm.city ){
				vm.client.city_id = vm.city.id;
			}

			//Update billing city
			if( vm.billingCity ){
				vm.client.billing_city_id = vm.billingCity.id;
			}

			//Save client dob
			if( vm.client.dob && typeof  vm.client.dob !== 'string'){
				vm.client.dob = vm.client.dob.format('YYYY-MM-DD');
			}

			if( vm.client.passport_date && typeof  vm.client.passport_date !== 'string'){
				vm.client.passport_date = vm.client.passport_date.format('YYYY-MM-DD');
			}



			if( vm.client.id ){
				update();
			}else{
				create();
			}
		}


		/**Create new client
		 * */
		function create(){

			apiService.create( 'clients',vm.client )
				.then(function ( response ) {

					SweetAlert.swal({
						title: $filter('translate')('client'),
						text: $filter('translate')('created_successfully'),
						type: 'success',
						timer: 2000,
						showConfirmButton: false
					});


					vm.id = response.data.id;
					vm.client = response.data;

					if( vm.saveAndExit )
						cancel();

				}).catch( function( response ) {
				$log.error(response);
				$rootScope.showErrors(response,'create_error');

			});

		}


		/**Update  client
		 * */
		function update(){

			apiService.update( 'clients',vm.client )
				.then(function ( response ) {

					// $log.error(response);
					SweetAlert.swal({
						title: $filter('translate')('client'),
						text: $filter('translate')('updated_successfully'),
						type: 'success',
						timer: 2000,
						showConfirmButton: false
					});

					if( vm.saveAndExit )
						cancel();

				}).catch( function( response ) {
				$log.error(response);
				$rootScope.showErrors(response,'update_error');

			});
		}


		/**Cancel method
		 * */
		function cancel(){
			$location.path('/clients');
		}



		/**Load addresses
		 * */
		vm.addresses = [];
		vm.loadAddresses = loadAddresses;
		function loadAddresses(){
			apiService.getAll('client/'+vm.id+'/addresses')
				.then(function (response) {

					vm.addresses = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}





	}

})();