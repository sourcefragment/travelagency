/**
 * Created by mario on 2/7/17.
 */
(function () {

	'use strict';

	angular.module('app.clients')
		.controller('AddressModalCtrl',ctrl);


	ctrl.$inject = [
		'$rootScope',
		'$scope',
		'apiService',
		'$log',
		'$uibModal'
	];

	function ctrl(
		$rootScope,
		$scope,
		apiService,
		$log,
		$uibModal
	){

		$scope.openModal = function () {

			var client = typeof  $scope.client !== 'undefined' ? $scope.client:{};
			var model = typeof $scope.model !== 'undefined' ? $scope.model:{};

			$scope.modalInstance = $uibModal.open({
				templateUrl : 'app/js/modules/clients/views/address-modal.html',
				controller :['$scope',function ($scope) {

					$scope.types = ['local','home'];
					$scope.address = model;
					$scope.address.client_id = client;


					/**On modal save
					 * */
					$scope.saveModal = function () {

						apiService.create('addresses',$scope.address)
							.then(function (response) {
								$scope.$close();
							})
							.catch(function (response) {
								$log.error(response);
								$rootScope.showErrors(response);
							});

					}

				}]
			});
		};


		/**Trigger on success
		 * */
		function onSuccess( id ) {

			if( typeof $scope.onSuccess === 'function' ){
				$scope.onSuccess()( id );
			}
		}

	}


})();