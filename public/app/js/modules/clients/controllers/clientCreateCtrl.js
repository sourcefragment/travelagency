/**
 * Created by mario on 2/7/17.
 */
(function () {

	'use strict';

	angular.module('app.clients')
		.controller('ClientCreateCtrl',ctrl);


	ctrl.$inject = [
		'$rootScope',
		'$scope',
		'apiService',
		'$log',
		'$uibModal'
	];

	function ctrl(
		$rootScope,
	    $scope,
	    apiService,
	    $log,
	    $uibModal
	){

		$scope.openModal = function () {

			var instance = $uibModal.open({
				templateUrl : 'app/js/modules/clients/views/create-modal.html',
				controller :['$scope',function ($scope) {

					$scope.client = {};
					$scope.types = [];
					$scope.countries = [];


					loadTypes();
					loadCountries();

					/**Load client tyes
					 * */
					function loadTypes(){
						apiService.getAll('option/client-types')
							.then(function (response) {
								$scope.types = response.data;
							})
							.catch(function (response) {
								$log.error(response);
							});
					}



					/**Load available countries
					 * */
					function loadCountries(){

						apiService.getAll('countries',{limit:-1})
							.then(function (response) {
								$scope.countries = response.data;
							})
							.catch(function (response) {
								$log.error(response);
							});
					}

					/**On modal save
					 * */
					$scope.saveModal = function () {

						apiService.create('clients',$scope.client)
							.then(function (response) {
								$scope.$close();
								onSuccess( response.data.id );
							})
							.catch(function (response) {
								$log.error(response);
								$rootScope.showErrors(response,'create_error');
							});
					}

				}]
			});
		};


		/**Trigger on success
		 * */
		function onSuccess( id ) {

			if( typeof $scope.onSuccess === 'function' ){
                $scope.onSuccess()( id );
            }
        }

	}


})();