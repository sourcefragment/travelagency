/**
 * Created by mario on 2/17/17.
 */

(function () {

	'use strict';

	angular
		.module('app.clients')
		.controller('MainClientsCtrl', ctrl);

	ctrl.$inject = [
		'$filter',
		'SweetAlert',
		'apiService',
		'NgTableParams',
		'$rootScope',
		'$log'
	];

	function ctrl(
		$filter,
		SweetAlert,
		apiService,
		NgTableParams,
		$rootScope,
	    $log
	){

		var vm = this;
		vm.clients = false;
		vm.tableParams = {};

		vm.delete = deleteItem;

		activate();

		/**Init controller
		 * */
		function activate() {
			initTable();
		}



		/**Init main table structure
		 * */
		function initTable() {

			vm.tableParams = new NgTableParams({},{
				getData : function( params ){
					return  get( params );
				}
			});

		}


		/**Change
		 * */
		/**Load clients
		 * */
		function get( params ) {

			var p = $rootScope.getParams( params );
			apiService.getAll( 'clients',p )
				.then(function ( response ) {
					// $log.error(response);
					vm.clients = response.data.data;
					params.total( response.data.total);
					params.pages = params.generatePagesArray();
					params.current = response.data.current_page;
					params.roles = vm.roles;
					return response;

				}).catch(function ( response ) {
				$log.error( response );
			});
		}


		/**Delete client
		 *@param id, the client id
		 * @return promise
		 */
		function deleteItem( id ) {

			SweetAlert.swal({
					title: $filter('translate')('delete_client'),
					text: $filter('translate')('delete_client_text'),
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: $filter('translate')('yes_delete'),
					cancelButtonText: $filter('translate')('no_cancel'),
					closeOnConfirm: false,
				},
				function (isConfirm) {

					if (isConfirm) {

						apiService.delete( 'clients',id)
							.then( function( response ){
								SweetAlert.swal({
									title: $filter('translate')('deleted'),
									text: $filter('translate')('deleted_successfully'),
									type: 'success',
									timer: 2000,
									showConfirmButton: false
								});
								initTable();

							}).catch(function ( response ) {
							var msg = response.data.error;

							SweetAlert.swal({
								title : $filter('translate')('delete_error'),
								text : $filter('translate')( msg ),
								type : 'error',
								showConfirmButton : true,
								closeOnConfirm : true
							});
						});
					}

				});
		}




	}

})();