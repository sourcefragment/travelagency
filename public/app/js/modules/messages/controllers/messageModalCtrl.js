/**
 * Created by mario on 2/7/17.
 */
(function () {

	'use strict';

	angular.module('app.messages')
		.controller('MessageModalCtrl',ctrl);


	ctrl.$inject = [
		'$rootScope',
		'$scope',
		'apiService',
		'$log',
		'$uibModal'
	];

	function ctrl(
		$rootScope,
		$scope,
		apiService,
		$log,
		$uibModal
	){

		$scope.openModal = function () {

			var client = $scope.client;

			$scope.modalInstance = $uibModal.open({
				templateUrl : 'app/js/modules/clients/views/message-modal.html',
				controller :['$scope',function ($scope) {

					$scope.types = ['local','home'];


					/**On modal save
					 * */
					$scope.saveModal = function () {


					}

				}]
			});
		};


		/**Trigger on success
		 * */
		function onSuccess( id ) {

			if( typeof $scope.onSuccess === 'function' ){
				$scope.onSuccess()( id );
			}
		}

	}


})();