/**
 * Created by mario on 2/17/17.
 */
(function () {
	'use strict';

	angular.module('app.messages')
		.directive('messageModal',messageModal);


	function messageModal() {
		return {
			restrict:'A',
			scope:{
				client:'@client',
				subject:'@subject',
				message:'@message'
			},
			link:function(scope,element,attrs){
				element.click( scope.openModal );
			},
			controller: 'MessageModalCtrl'
		}
	}



})();