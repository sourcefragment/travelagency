/**
 * Created by mario on 11/30/16.
 */
(function () {

	'use strict';

	angular.module('app.invoices',[])
		.config(config);

	function config( $stateProvider ){

		$stateProvider.state('invoices', {
			url: '/invoices',
			templateUrl: 'app/js/modules/invoices/views/list.html',
			controller: 'MainInvoicesCtrl',
			controllerAs: 'mic'
		})
			.state('invoice-create', {
				url: '/invoices/create',
				templateUrl: 'app/js/modules/invoices/views/edit.html',
				controller: 'InvoiceCtrl',
				controllerAs: 'ic'

			})
			.state('invoice-edit', {
				url: '/invoices/edit/:id',
				templateUrl: 'app/js/modules/invoices/views/edit.html',
				controller: 'InvoiceCtrl',
				controllerAs: 'ic'

			});

	}


})();