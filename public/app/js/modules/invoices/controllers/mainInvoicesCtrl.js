/**
 * Created by mario on 12/11/16.
 */
(function () {

	'use strict';

	angular
		.module('app.invoices')
		.controller('MainInvoicesCtrl',ctrl);

	ctrl.$inject = [
		'apiService',
		'$rootScope',
		'$filter',
		'SweetAlert',
		'toaster',
		'NgTableParams',
		'$log'
	];


	function ctrl(
		apiService,
		$rootScope,
		$filter,
		SweetAlert,
		toaster,
		NgTableParams,
	    $log

	){


		var vm = this;

		vm.invoices = [];
		vm.tableParams = {};

		vm.delete = deleteInvoice;


		init();

		function init(){
			initTable();
		}


		/**Init main view table
		 * */
		function initTable(){

			vm.tableParams = new NgTableParams({},{
				getData : function( params ){
					return getInvoices( params );
				}
			});
		}

		/**Get all invoices for listing
		 *@param params object
		 * */
		function getInvoices( params ){

			var p = $rootScope.getParams( params );
			// $log.error(p);
			apiService.getAll( 'invoices', p )
				.then(function (response) {
					// $log.error(response);
					vm.invoices = response.data.data;
					params.total( response.data.total);
					params.pages = params.generatePagesArray();
					params.current = response.data.current_page;
					return response;
				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'invoice_error');
				})
		}

		/**Delete angecy
		 * */
		function deleteInvoice( id ){

			SweetAlert.swal({
					title: $filter('translate')('delete_invoice'),
					text: $filter('translate')('delete_invoice_text'),
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: $filter('translate')('yes_delete'),
					cancelButtonText: $filter('translate')('no_cancel'),
					closeOnConfirm: false,
				},
				function (isConfirm) {

					if (isConfirm) {

						apiService.delete( 'invoices',id )
							.then( function( response ){
								SweetAlert.swal({
									title: $filter('translate')('invoice'),
									text: $filter('translate')('deleted_successfully'),
									type: 'success',
									timer: 2000,
									showConfirmButton: false
								});
								initTable();

							}).catch(function ( response ) {

							var msg = response.data.error;

							SweetAlert.swal({
								title : $filter('translate')('invoice_delete'),
								text : $filter('translate')( msg ),
								type : 'error',
								showConfirmButton : true,
								closeOnConfirm : true
							});

						});
					}

				});

		}
	}

})();