/**
 * Created by mario on 12/11/16.
 */
(function () {
	'use strict';

	angular
		.module('app.invoices')
		.controller('InvoiceCtrl',ctrl);


	ctrl.$inject = [
		'$stateParams',
		'$location',
		'$filter',
		'SweetAlert',
		'$uibModal',
		'$rootScope',
		'apiService',
		'$log'
	];

	function ctrl(
		$stateParams,
		$location,
		$filter,
		SweetAlert,
		$uibModal,
		$rootScope,
		apiService,
	    $log

	){

		var vm = this;
		vm.id  = $stateParams.id;
		vm.invoice = {};
		vm.invoiceNumber = '';
		vm.types = [];
		vm.paymentMethods = [];
		vm.statuses = [];
		vm.taxes = [];
		vm.items = [];
		vm.validItems = [];
		vm.saveAndExit = false;

		vm.save = save;
		vm.cancel = cancel;
		vm.validate = validate;

		vm.loadTypes = loadTypes;
		vm.loadPaymentMethods = loadPaymentMethods;

		vm.calcSubtotal = calcSubtotal;
		vm.calcTaxes = calcTaxes;
		vm.calcTotal = calcTotal;


		//Items
		vm.addItem = addItem;
		vm.saveItem = saveItem;
		vm.deleteItem = deleteItem;
		vm.calcItemPrice = calcItemPrice;



		init();

		/**Init view
		 * */
		function init(){

			loadTypes();
			loadPaymentMethods();
			loadStatus();
			loadTaxes();
			loadValidItems();

			if( vm.id ){
				loadItems();
				get( vm.id );
			}

		}


		/**Load invoice
		 * */
		function get( id ){

			apiService.get( 'invoices',id )
				.then(function (response) {
					// $log.error(response);
					vm.invoice = response.data;
					vm.booking = response.data.booking;
					vm.client = response.data.client;

				})
				.catch(function (response) {
					console.log(response);
				})
		}


		/**Save invoice
		 * */
		function save( mode ){


			vm.saveAndExit = mode;

			if( vm.booking ){
				vm.invoice.booking_id = vm.booking.id;
			}

			if( vm.client ){
				vm.invoice.client_id = vm.client.id;
			}

			if( vm.invoice.date && typeof vm.invoice.date !== 'string' ){
				vm.invoice.date = vm.invoice.date.format('YYYY-MM-DD');
			}


			if( vm.id ){
				update();
			}else{
				create();
			}
		}

		/**Create new invoice
		 * */
		function create(){
			apiService.create( 'invoices',vm.invoice )
				.then(function (response) {
					SweetAlert.swal({
						title: $filter('translate')('invoice'),
						text: $filter('translate')('created_successfully'),
						type: 'success',
						timer: 1000,
						showConfirmButton: false
					});

					vm.id = response.data.id;
					vm.invoice = response.data;

					if( vm.saveAndExit )
						cancel();
				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'create_error');
				});

		}


		/**Update invoice
		 * */
		function update(){
			apiService.update( 'invoices',vm.invoice )
				.then(function (response) {
					// console.log(response);
					SweetAlert.swal({
						title: $filter('translate')('invoice'),
						text: $filter('translate')('updated_successfully'),
						type: 'success',
						timer: 1000,
						showConfirmButton: false
					});

					if( vm.saveAndExit )
						cancel();
				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'update_error')
				});

		}


		/**On cancel
		 * */
		function cancel(){
			$location.path('/invoices');
		}


		/**====================
		 * ===================
		 * */

		/**Load valid items
		 * */
		function loadValidItems(){
			apiService.getAll('option/item')
				.then(function (response) {
					vm.validItems = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}


		/**Load items
		 * */
		function loadItems( id ){
			apiService.getAll('invoice/'+id+'/items',{limit:-1})
		}

		/**Load taxes
		 * */
		function loadTaxes(){
			apiService.getAll('taxes',{limit:-1})
				.then(function (response) {
					vm.taxes = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}

		/**Load types
		 * */
		function loadTypes() {
			apiService.getAll('option/invoice-type')
				.then(function (response) {
					vm.types = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}

		/**Load payment methods
		 * */
		function loadPaymentMethods(){
			apiService.getAll('payment-methods',{limit:-1})
				.then(function (response) {
					vm.paymentMethods = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}

		/**Load invoice status
		 * */
		function loadPaymentStatus(){
			apiService.getAll('payment-status',{limit:-1})
				.then(function (response) {
					vm.paymentStatuses = response.data;
				})
				.catch(function(response){
					$log.error(response);
				});
		}


		/**Load invoice status
		 * */
		function loadStatus(){
			apiService.getAll('option/invoice-status')
				.then(function (response) {
					vm.statuses = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}

		/**Validate invoice
		 * */
		function validate(){

			apiService.get('invoice/validate',vm.invoice.type_id)
				.then(function (response) {
					// $log.error(response);
					vm.invoice.number = response.data.number;
				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'error');
				});
		}


		/**Calculate expense total
		 * */
		function calcTotal(){
			var total = calcSubtotal() + calcTaxes();
			vm.invoice.total = total;

			return total.toFixed(2);
		}


		/**Get tax by id
		 */
		function getTax( id ){

			for( var i in vm.taxes ){
				if( vm.taxes[i].id == id ){
					return parseFloat(vm.taxes[i].value);
				}
			}
			return 0;

		}

		/**Calculate taxes
		 * */
		function calcTaxes(){
			var tax = getTax( vm.invoice.tax_id );
			var subtotal = calcSubtotal();
			return parseFloat( subtotal * tax / 100 );
		}



		/**Subtotal is amount - discount
		 * */
		function calcSubtotal(){
			var total = vm.invoice.total ? parseFloat(vm.invoice.total) : 0;
			var discount = vm.invoice.discount ? parseFloat(vm.invoice.discount) : 0;
			var subtotal = parseFloat( total - ( total * discount / 100 ));
			vm.invoice.subtotal = subtotal;
			return subtotal;
		}


		/**Add new item to invoice
		 * */
		function addItem(){

			vm.items.push({
				quantity:'',
				price:'',
				discount:''
			});
		}


		/**Calc product price
		 * */
		function calcItemPrice( item ){
			var quantity = item.quantity ? parseInt(item.quantity):0;
			var price = item.price ? parseFloat(item.price):0;
			var discount = item.discount ? parseInt(item.discount):0;
			var subtotal = ((quantity * price) - discount);
			return subtotal;
		}


		/**Save modified item
		 * */
		function saveItem( item ){

			if(item.id){
				updateItem(item);
			}else{
				createItem(item);
			}
		}

		/**Update booking item
		 * */
		function updateItem(item){
			apiService.update('invoice-items',item)
				.then(function (response) {
					item.changed = false;
				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'error');
				});
		}

		/**Create invoice item
		 * */
		function createItem(item){
			apiService.create('invoice-items',item)
				.then(function (response) {
					item.changed = false;
					item.id = response.data.id;
				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'error');
				});
		}


		/**Delete item
		 * */
		function deleteItem( item,index){

			if(item.id){
				apiService.delete('invoice-items',item.id)
					.then(function (response) {
						loadItems();
					})
					.catch(function (response) {
						$log.error(response);
						$rootScope.showErrors(response,'error');
					});
			}else{
				deleteTmpItem(index);
			}

		}


		/**Delete tmp item
		 * */
		function deleteTmpItem( index ){
			vm.items.splice(index,1);
		}


		/**Assign client
		 * */
		vm.assignClient = assignClient;
		function assignClient( id  ){

			apiService.get('clients',id)
				.then(function (response) {
					vm.client = response.data;
                })
				.catch(function (response) {
					$log.error(response);
                });
		}


	}

})();