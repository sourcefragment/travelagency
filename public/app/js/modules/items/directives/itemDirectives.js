/**
 * Created by mario on 3/19/17.
 */
(function () {
	'use strict';

	angular.module('app.items')
		.directive('itemSearchBox',itemSearchBox)
		.directive('itemCreateModal',itemCreateModal);


	function itemSearchBox() {
		return {
			restrict:'A',
			scope:{
				model:'=ngModel'
			},
			link:function(scope,element,attrs){
				element.click( scope.openModal );
			},
			controller: 'AgentsSearchBoxCtrl'
		}
	}

	function itemCreateModal(){
		return {
			restrict:'A',
			link:function(scope,element,attrs){
				element.click( scope.openModal );
			},
			controller: 'AgentCreateCtrl'
		}
	}

})();