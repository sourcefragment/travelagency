/**
 * Created by mario on 3/19/17.
 */
(function () {
	'use strict';

	angular.module('app.items',[])
		.config(config);

	function config($stateProvider) {

		$stateProvider.state('items', {
			url: '/items',
			templateUrl: 'app/js/modules/items/views/list.html',
			controller: 'MainItemsCtrl',
			controllerAs: 'mic'
		})
			.state('item-create', {
				url: '/items/create',
				templateUrl: 'app/js/modules/items/views/edit.html',
				controller: 'ItemCtrl',
				controllerAs: 'ic'

			})
			.state('item-edit', {
				url: '/items/edit/:id',
				templateUrl: 'app/js/modules/items/views/edit.html',
				controller: 'ItemCtrl',
				controllerAs: 'ic'

			});



	};


})();