/**
 * Created by mario on 3/19/17.
 */
(function () {

	'use strict';

	angular
		.module('app.items')
		.controller('ItemCtrl', ctrl);

	ctrl.$inject = [
		'$stateParams',
		'$location',
		'apiService',
		'$filter',
		'SweetAlert',
		'NgTableParams',
		'$rootScope',
		'$log'
	];

	function ctrl(
		$stateParams,
		$location,
		apiService,
		$filter,
		SweetAlert,
		NgTableParams,
		$rootScope,
		$log
	) {


		var vm = this;
		vm.id = $stateParams.id;
		vm.item = false;
		vm.tableParams = {};
		vm.languages = [];
		vm.treatments = [
			'sr',
			'mr',
			'mrs',
			'dr',
			'ms'
		];
		vm.saveAndExit = false;


		vm.save = save;
		vm.cancel = cancel;


		activate();

		/**Init controller
		 * */
		function activate() {

			if ( vm.id ){
				get(vm.id);
			}

		}



		/**Load item<
		 * */
		function get( id ) {

			apiService.get('items',id)
				.then(function ( response ) {
					// $log.error(response);
					vm.item = response.data;

				}).catch(function () {
				$log.error(data);
			});
		}


		/**Save items
		 * */
		function save( mode ) {

			vm.saveAndExit = mode;

			if( vm.item.id ){
				update();
			}else{
				create();
			}
		}


		/**Create new item
		 * */
		function create(){

			apiService.create( 'items',vm.item )
				.then(function ( response ) {

					SweetAlert.swal({
						title: $filter('translate')('item'),
						text: $filter('translate')('created_successfully'),
						type: 'success',
						timer: 2000,
						showConfirmButton: false
					});

					vm.id = response.data.id;
					vm.item = response.data;

					if( vm.saveAndExit )
						cancel();

				}).catch( function( response ) {
				$log.error(response);
				$rootScope.showErrors(response,'create_error');

			});

		}


		/**Update  item
		 * */
		function update(){

			apiService.update( 'items',vm.item )
				.then(function ( response ) {

					// $log.error(response);
					SweetAlert.swal({
						title: $filter('translate')('item'),
						text: $filter('translate')('updated_successfully'),
						type: 'success',
						timer: 2000,
						showConfirmButton: false
					});

					if( vm.saveAndExit )
						cancel();

				}).catch( function( response ) {
				$log.error(response);
				$rootScope.showErrors(response,'update_error');

			});
		}



		/**==========================
		 * USER META DATA
		 * ==========================*/


		/**Cancel method
		 * */
		function cancel(){
			$location.path('/items');
		}



	}

})();