/**
 * Created by mario on 3/19/17.
 */
(function () {

	'use strict';

	angular.module('app.items')
		.controller('ItemCreateCtrl',ctrl);


	ctrl.$inject = [
		'$rootScope',
		'$scope',
		'apiService',
		'$log',
		'$uibModal'
	];

	function ctrl(
		$rootScope,
		$scope,
		apiService,
		$log,
		$uibModal
	){

		$scope.openModal = function () {

			var instance = $uibModal.open({
				templateUrl : 'app/js/modules/items/views/create-modal.html',
				controller :['$scope',function ($scope) {

					$scope.item = {};

				}]
			});
		}

	}


})();