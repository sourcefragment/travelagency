/**
 * Created by mario on 3/19/17.
 */
(function () {
	'use strict';

	angular.module('app.items')
		.controller('ItemsSearchBoxCtrl',ctrl);

	ctrl.$inject = [
		'apiService',
		'$scope',
		'SweetAlert',
		'$uibModal',
		'$log'
	];

	function ctrl(
		apiService,
		$scope,
		SweetAlert,
		$uibModal,
		$log
	) {



		/**Open controller modal
		 * */
		$scope.openModal = function(){

			var modalInstance = $uibModal.open({
				templateUrl : 'app/js/modules/items/views/search-box.html',
				size : 'lg',
				controller : [
					'$scope',
					'$rootScope',
					'NgTableParams',
					function ( $scope,$rootScope,NgTableParams ) {

						$scope.items = [];
						$scope.tableParams = {};
						$scope.model = null;

						$scope.tableParams = new NgTableParams({},{
							getData : function (params ) {
								return loadClients(params);
							}
						});

						$scope.saveModal = saveModal;

						/**Load vehicle models
						 * */
						function loadClients( params ){
							var p = $rootScope.getParams(params);
							apiService.getAll('items',p)
								.then(function (response) {
									$scope.items = response.data.data;
									params.total( response.data.total);
									params.pages = params.generatePagesArray();
									params.current = response.data.current_page;
								});
						}


						/**On modal save
						 * */
						function saveModal(){

							for( var i in $scope.items ){

								if( $scope.items[i].checked != null ){
									setModel( $scope.items[i] );
								}
							}
							this.$close();
						}


					}]

			});


		}

		/**Save selected model
		 * */
		function setModel( model ){
			$scope.model = model;
		}


	}


})();