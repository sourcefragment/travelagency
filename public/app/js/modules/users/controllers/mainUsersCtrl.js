(function () {

    'use strict';

    angular
        .module('app.users')
        .controller('MainUsersCtrl', ctrl);

    ctrl.$inject = [
    	'$filter',
	    'SweetAlert',
        'apiService',
	    'NgTableParams',
	    '$rootScope',
	    '$log'
    ];

    function ctrl(
    	$filter,
	    SweetAlert,
	    apiService,
		NgTableParams,
        $rootScope,
        $log
	){

        var vm = this;
        vm.users = false;
		vm.tableParams = {};
		vm.roles = [];
	    vm.tableBtns = {

	    };

        vm.delete = deleteItem;

        activate();

        /**Init controller
         * */
        function activate() {

        	loadRoles();
        	initTable();
        }


        /**Load roles
         * */
        function loadRoles(){
	        apiService.getAll( 'roles',{limit:-1} )
		        .then(function (response) {
			        vm.roles = response.data;
		        });
        }
        /**Init main table structure
         * */
        function initTable() {

        	vm.tableParams = new NgTableParams({},{
	        	getData : function( params ){
	        		return  get( params );
		        }
	        });
        }


        /**Change
         * */
	    /**Load users
	     * */
	    function get( params ) {

			var p = $rootScope.getParams( params );
		    apiService.getAll( 'users',p )
			    .then(function ( response ) {
			    	// $log.error(response);
				    vm.users = response.data.data;
				    params.total( response.data.total);
				    params.pages = params.generatePagesArray();
				    params.current = response.data.current_page;
				    params.data = vm.roles;
					return response;

			    }).catch(function ( response ) {
			        $log.error( response );
		    });
	    }


        /**Delete user
         *@param id, the user id
         * @return promise
         */
        function deleteItem( id ) {

        	SweetAlert.swal({
                    title: $filter('translate')('delete_user'),
                    text: $filter('translate')('delete_user_text'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $filter('translate')('yes_delete'),
                    cancelButtonText: $filter('translate')('no_cancel'),
                    closeOnConfirm: false,
                },
                function (isConfirm) {

                	if (isConfirm) {

                        apiService.delete( 'users',id)
                            .then( function( response ){
                                SweetAlert.swal({
                                    title: $filter('translate')('deleted'),
                                    text: $filter('translate')('deleted_successfully'),
                                    type: 'success',
                                    timer: 2000,
                                    showConfirmButton: false
                                });
                                initTable();

                            }).catch(function ( response ) {
                            	var msg = response.data.error;

		                        SweetAlert.swal({
			                        title : $filter('translate')('delete_error'),
			                        text : $filter('translate')( msg ),
			                        type : 'error',
			                        showConfirmButton : true,
			                        closeOnConfirm : true
		                        });
                        });
                    }

                });
        }




    }

})();