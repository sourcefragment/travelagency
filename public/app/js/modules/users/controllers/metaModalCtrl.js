/**
 * Created by mario on 12/22/16.
 */
(function () {
	'use strict';

	angular.module('app.users')
		.controller('MetaModalCtrl',ctrl);


	ctrl.$inject = [
		'$scope',
		'$uibModal',
		'$rootScope',
		'apiService'
	];

	function ctrl(
		$scope,
		$uibModal,
		$rootScope,
		apiService
	){


		$scope.openModal = function(){

			var uid = parseInt( $scope.uid );

			var instance = $uibModal.open({
				templateUrl : 'app/js/modules/users/views/meta-field-modal.html',
				size:'lg',
				controller : ['$scope',function ($scope) {

					$scope.fields = [{
						meta_name: '',
						meta_value : ''
					}];

					/**Save meta modal form
					 * */
					$scope.saveModal = function(){


						for( var i in $scope.fields ){

							var data = $scope.fields[i];
							data.user_id = uid;

							apiService.create('user-metas',data)
								.then(function (response) {

								})
								.catch(function (response) {
									console.log(response);
									$rootScope.showErrors(response,'meta_create_error');
								});
						}


						onSuccess();
						$scope.$close();

					};


					/**Add new field
					 * */
					$scope.addField = function(){
						$scope.fields.push({
							meta_name : '',
							meta_value : ''
						});
					};

					/**Delete field
					 * */
					$scope.deleteField = function( index ){
						for( var i in $scope.fields ){
							if( i == index ){
								$scope.fields.splice(i,1);
							}
						}
					};

				}]
			});

		};

		/**On meta created successfully
		 * */
		function onSuccess(){

			if( $scope.onSuccess && typeof $scope.onSuccess == 'function' ){
				$scope.onSuccess();
			}
		}
	}

})();