(function () {

    'use strict';

    angular
        .module('app.users')
        .controller('UsersCtrl', usersController);

    usersController.$inject = [
	    '$stateParams',
	    '$location',
        'apiService',
        '$filter',
	    'SweetAlert',
	    'NgTableParams',
	    '$rootScope',
	    '$log'
    ];

    function usersController(
	    $stateParams,
	    $location,
        apiService,
        $filter,
        SweetAlert,
        NgTableParams,
        $rootScope,
        $log
    ) {


    	var vm = this;
        vm.user = false;
	    vm.metadata = [];
	    vm.roles = [];
	    vm.companies = [];
        vm.uid = $stateParams.id;
	    vm.tableParams = {};
	    vm.changePass = false;
	    vm.languages = [];
	    vm.saveAndExit = false;

	    vm.timezones = $rootScope.settings.timezones;
	    vm.currencies = $rootScope.settings.currencies;
	    vm.dateFormats = $rootScope.settings.dateFormats;
	    vm.timeFormats = $rootScope.settings.timeFormats;
	    vm.days = ['monday','tuesday','wednesday','thursday','friday','saturday','sunday'];

	    vm.save = save;
	    vm.cancel = cancel;

	    vm.deleteMeta = deleteMeta;
	    vm.getMetaFields = getMetaFields;
	    vm.loadRoles = loadRoles;
	    vm.loadCompanies = loadCompanies;

        activate();

        /**Init controller
         * */
        function activate() {

	        //Get one user
        	if ( $stateParams.id ){
        		vm.changingPassword = false;
                get($stateParams.id);
            }

            loadRoles();
	        loadCompanies();
	        loadLanguages();

        }


        /**Load languages
         * */
        function loadLanguages() {

        	apiService.getAll('languages',{limit:-1})
		        .then(function (response) {
			        vm.languages = response.data;
		        })
		        .catch(function (response) {
			       $log.error(response);
		        });
        }


        /**Load roles
         * */
        function loadRoles(){

        	apiService.getAll('roles',{limit:-1})
		        .then(function (response) {
			        vm.roles = response.data;
		        })
		        .catch(function (response) {
					$log.errors(response);
		        });
        }

        /**Load companies
         * */
        function loadCompanies(){

        	apiService.getAll('companies',{limit:-1})
		        .then(function (response) {
			        vm.companies = response.data;
		        })
		        .catch(function (response) {
					$log.error(response);
		        });

        }


        /**Load user<
         * */
        function get( id ) {

        	apiService.get('users',id)
                .then(function ( response ) {
                	$log.error(response);
                	vm.user = response.data;
	                vm.city = response.data.city;
	                vm.billingCity = response.data.billing_city;
	                vm.company = response.data.company;
	                vm.language = response.data.language;


                }).catch(function () {
                    $log.error(data);
                });
        }


        /**Save users
         * */
        function save( mode ) {

        	vm.saveAndExit = mode;

        	//Update city
			if( vm.city ){
				vm.user.city_id = vm.city.id;
			}

			//Update billing city
	        if( vm.billingCity ){
	        	vm.user.billing_city_id = vm.billingCity.id;
	        }

			if( vm.user.id ){
				update();
			}else{
				create();
			}
        }


        /**Create new user
         * */
        function create(){

	        apiService.create( 'users',vm.user )
		        .then(function ( response ) {

		        	SweetAlert.swal({
				        title: $filter('translate')('user'),
				        text: $filter('translate')('created_successfully'),
				        type: 'success',
				        timer: 1000,
				        showConfirmButton: false
			        });

		        	vm.id = response.data.id;
		        	vm.user = response.data;

		        	if( vm.saveAndExit )
			            cancel();

		        }).catch( function( response ) {
					$log.error(response);
		            $rootScope.showErrors(response,'create_error');

	        });

        }


        /**Update  user
         * */
        function update(){

	        apiService.update( 'users',vm.user )
		        .then(function ( response ) {

			        SweetAlert.swal({
				        title: $filter('translate')('user'),
				        text: $filter('translate')('updated_successfully'),
				        type: 'success',
				        timer: 1000,
				        showConfirmButton: false
			        });

			        if( vm.saveAndExit )
			            cancel();

		        }).catch( function( response ) {
					$log.error(response);
		            $rootScope.showErrors(response,'update_error');

	        });
        }



        /**==========================
         * USER META DATA
         * ==========================*/
	    /**Open modal to add a new meta field
	     * */

	    /**Delete metadata field
	     * */
	    function deleteMeta( id ){

		    SweetAlert.swal({
				    title: $filter('translate')('delete_meta'),
				    text: $filter('translate')('delete_meta_text'),
				    type: "warning",
				    showCancelButton: true,
				    confirmButtonColor: "#DD6B55",
				    confirmButtonText: $filter('translate')('yes_delete'),
				    cancelButtonText: $filter('translate')('no_cancel'),
				    closeOnConfirm: false,
			    },
			    function (isConfirm) {

				    if (isConfirm) {

					    apiService.delete('user-metas', id )
						    .then( function( response ){

						    	// $log.erro( response );
							    SweetAlert.swal({
								    title: $filter('translate')('user_meta'),
								    text: $filter('translate')('deleted_successfully'),
								    type: 'success',
								    timer: 2000,
								    showConfirmButton: false
							    });
							    getMetaFields();

						    }).catch(function ( response ) {
							    $log.error(response);
						        $rootScope.showErrors(response,'delete_error');

					    });
				    }

			    });

	    }


	    /**Load user extra fields*/
	    function getMetaFields(){

	    	vm.tableParams = new NgTableParams({},{
	    	    getData : function( params ){
	    	    	return loadMetaFields(params);
		        }
		    });

	    }

	    /**Load meta fields
	     * */
	    function loadMetaFields( params ){

	    	var p = $rootScope.getParams(params);

		    apiService.getAll( 'user-metas',p )
			    .then(function ( response ) {
				    vm.metadata = response.data.data;
				    params.total( response.data.total);
				    params.pages = params.generatePagesArray();
				    params.current = response.data.current_page;
				    return response;
			    })
			    .catch(function ( response ) {
				    $log.error( response );
			    });
	    }



		/**Cancel method
		 * */
		function cancel(){
			$location.path('/users');
		}




    }

})();




