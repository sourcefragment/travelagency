(function () {

    angular.module('app.users')
        .directive('createUserBtnLong', createUserBtnLong)
	    .directive('addUserMeta',addUserMeta);


    function createUserBtnLong() {
        return {
            template: '<button type="button" class="btn btn-primary btn-lg" ui-sref="usersCreate">'
                        + '<i class="fa fa-plus-circle"></i> [[ "new_user" | translate ]]'
                    + '</button>'
        };
    }

    /**Add meta fields btn directive
     * */
    function addUserMeta(){
    	return{
    		restrict : 'A',
		    scope : {
    			onSuccess : '&onSuccess',
			    user : '=user'
		    },
			link:function(scope,element,attrs){
		    	element.click(scope.openModal);
			},
		    controller : 'MetaModalCtrl'
	    }
    }

})();