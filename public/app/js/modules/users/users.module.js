/**
 * Created by juan on 17/08/16.
 */

(function () {
    'use strict';

    angular.module('app.users',[])
        .config(config);

    function config($stateProvider) {

        $stateProvider.state('users', {
            url: '/users',
            templateUrl: 'app/js/modules/users/views/list.html',
            controller: 'MainUsersCtrl',
            controllerAs: 'muc'
        })
        .state('user-create', {
            url: '/users/create',
            templateUrl: 'app/js/modules/users/views/edit.html',
            controller: 'UsersCtrl',
            controllerAs: 'uc'

        })
        .state('user-edit', {
            url: '/users/edit/:id',
            templateUrl: 'app/js/modules/users/views/edit.html',
            controller: 'UsersCtrl',
            controllerAs: 'uc'

        });



    }


})();

