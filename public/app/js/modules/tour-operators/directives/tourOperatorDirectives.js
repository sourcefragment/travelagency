/**
 * Created by mario on 3/19/17.
 */
(function () {
	'use strict';

	angular.module('app.tour-operators')
		.directive('tourOperatorSearchBox',tourOperatorSearchBox)
		.directive('tourOperatorCreateModal',tourOperatorCreateModal);


	function tourOperatorSearchBox() {
		return {
			restrict:'A',
			scope:{
				model:'=ngModel'
			},
			link:function(scope,element,attrs){
				element.click( scope.openModal );
			},
			controller: 'TourOperatorSearchBoxCtrl'
		}
	}

	function tourOperatorCreateModal(){
		return {
			restrict:'A',
			scope:{
				onSuccess:'&onSuccess'
			},
			link:function(scope,element,attrs){
				element.click( scope.openModal );
			},
			controller: 'TourOperatorCreateCtrl'
		}
	}

})();