/**
 * Created by mario on 3/19/17.
 */
(function () {
	'use strict';

	angular.module('app.tour-operators')
		.controller('TourOperatorSearchBoxCtrl',ctrl);

	ctrl.$inject = [
		'apiService',
		'$scope',
		'SweetAlert',
		'$uibModal',
		'$log'
	];

	function ctrl(
		apiService,
		$scope,
		SweetAlert,
		$uibModal,
		$log
	) {



		/**Open controller modal
		 * */
		$scope.openModal = function(){

			var modalInstance = $uibModal.open({
				templateUrl : 'app/js/modules/tour-operators/views/search-box.html',
				size : 'lg',
				controller : [
					'$scope',
					'$rootScope',
					'NgTableParams',
					function ( $scope,$rootScope,NgTableParams ) {

						$scope.tourOperators = [];
						$scope.tableParams = {};
						$scope.model = null;

						$scope.tableParams = new NgTableParams({},{
							getData : function (params ) {
								return getAll(params);
							}
						});

						$scope.saveModal = saveModal;

						/**Load vehicle models
						 * */
						function getAll( params ){
							var p = $rootScope.getParams(params);
							apiService.getAll('tour-operators',p)
								.then(function (response) {
									$scope.tourOperators = response.data.data;
									params.total( response.data.total);
									params.pages = params.generatePagesArray();
									params.current = response.data.current_page;
								});
						}


						/**On modal save
						 * */
						function saveModal(){

							for( var i in $scope.tourOperators ){

								if( $scope.tourOperators[i].checked != null ){
									setModel( $scope.tourOperators[i] );
								}
							}
							this.$close();
						}


					}]

			});


		}

		/**Save selected model
		 * */
		function setModel( model ){
			$scope.model = model;
		}


	}


})();