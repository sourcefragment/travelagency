/**
 * Created by mario on 3/19/17.
 */
(function () {

	'use strict';

	angular.module('app.tour-operators')
		.controller('TourOperatorCreateCtrl',ctrl);


	ctrl.$inject = [
		'$rootScope',
		'$scope',
		'apiService',
		'$log',
		'$uibModal'
	];

	function ctrl(
		$rootScope,
		$scope,
		apiService,
		$log,
		$uibModal
	){

		$scope.openModal = function () {

			var instance = $uibModal.open({
				templateUrl : 'app/js/modules/tour-operators/views/create-modal.html',
				controller :['$scope',function ($scope) {

					$scope.tourOperator = {};
					$scope.types = [];

					/**On modal save
					 * */
					$scope.saveModal = function () {

						apiService.create('tour-operators',$scope.tourOperator)
							.then(function (response) {
								$scope.$close();
								onSuccess(response.data.id);
							})
							.catch(function (response) {
								$log.error(response);
								$rootScope.showErrors(response,'create_error');
							});
					}

				}]
			});
		};


		function onSuccess( id ) {

			if( typeof $scope.onSuccess === 'function' ){
				$scope.onSuccess()(id);
			}
        }




	}


})();