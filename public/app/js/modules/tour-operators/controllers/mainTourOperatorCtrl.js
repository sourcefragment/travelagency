/**
 * Created by mario on 3/19/17.
 */
(function () {

	'use strict';

	angular
		.module('app.tour-operators')
		.controller('MainTourOperatorsCtrl', ctrl);

	ctrl.$inject = [
		'$filter',
		'SweetAlert',
		'apiService',
		'NgTableParams',
		'$rootScope',
		'$log'
	];

	function ctrl(
		$filter,
		SweetAlert,
		apiService,
		NgTableParams,
		$rootScope,
		$log
	){

		var vm = this;
		vm.tourOperators = false;
		vm.tableParams = {};

		vm.delete = deleteItem;

		activate();

		/**Init controller
		 * */
		function activate() {
			initTable();
		}



		/**Init main table structure
		 * */
		function initTable() {

			vm.tableParams = new NgTableParams({},{
				getData : function( params ){
					return  get( params );
				}
			});

		}


		/**Change
		 * */
		/**Load tourOperators
		 * */
		function get( params ) {

			var p = $rootScope.getParams( params );
			apiService.getAll( 'tour-operators',p )
				.then(function ( response ) {
					// $log.error(response);
					vm.tourOperators = response.data.data;
					params.total( response.data.total);
					params.pages = params.generatePagesArray();
					params.current = response.data.current_page;
					params.roles = vm.roles;
					return response;

				}).catch(function ( response ) {
				$log.error( response );
			});
		}


		/**Delete agent
		 *@param id, the agent id
		 * @return promise
		 */
		function deleteItem( id ) {

			SweetAlert.swal({
					title: $filter('translate')('delete_agent'),
					text: $filter('translate')('delete_agent_text'),
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: $filter('translate')('yes_delete'),
					cancelButtonText: $filter('translate')('no_cancel'),
					closeOnConfirm: false,
				},
				function (isConfirm) {

					if (isConfirm) {

						apiService.delete( 'tour-operators',id)
							.then( function( response ){
								SweetAlert.swal({
									title: $filter('translate')('deleted'),
									text: $filter('translate')('deleted_successfully'),
									type: 'success',
									timer: 2000,
									showConfirmButton: false
								});
								initTable();

							}).catch(function ( response ) {
							var msg = response.data.error;

							SweetAlert.swal({
								title : $filter('translate')('delete_error'),
								text : $filter('translate')( msg ),
								type : 'error',
								showConfirmButton : true,
								closeOnConfirm : true
							});
						});
					}

				});
		}




	}

})();