/**
 * Created by mario on 3/19/17.
 */
(function () {

	'use strict';

	angular
		.module('app.tour-operators')
		.controller('TourOperatorCtrl', ctrl);

	ctrl.$inject = [
		'$stateParams',
		'$location',
		'apiService',
		'$filter',
		'SweetAlert',
		'$rootScope',
		'$log'
	];

	function ctrl(
		$stateParams,
		$location,
		apiService,
		$filter,
		SweetAlert,
		$rootScope,
		$log
	) {


		var vm = this;
		vm.id = $stateParams.id;
		vm.tourOperator = false;
		vm.tableParams = {};
		vm.languages = [];
		vm.treatments = [
			'sr',
			'mr',
			'mrs',
			'dr',
			'ms'
		];
		vm.saveAndExit = false;



		vm.save = save;
		vm.cancel = cancel;


		activate();

		/**Init controller
		 * */
		function activate() {

			loadLanguages();
			if ( vm.id ){
				get(vm.id);
			}

		}


		/**Load tourOperator<
		 * */
		function get( id ) {

			apiService.get('tour-operators',id)
				.then(function ( response ) {
					// $log.error(response);
					vm.tourOperator = response.data;
					vm.city = response.data.city;
					vm.billingCity = response.data.billing_city;
					vm.company = response.data.company;

				}).catch(function () {
				$log.error(data);
			});
		}


		/**Save tour-operators
		 * */
		function save( mode ) {

			vm.saveAndExit = mode;

			//Update city
			if( vm.city ){
				vm.tourOperator.city_id = vm.city.id;
			}

			//Update billing city
			if( vm.billingCity ){
				vm.tourOperator.billing_city_id = vm.billingCity.id;
			}

			//Save tourOperator dob
			if( vm.tourOperator.dob && typeof  vm.tourOperator.dob !== 'string'){
				vm.tourOperator.dob = vm.tourOperator.dob.format('YYYY-MM-DD');
			}

			if( vm.tourOperator.id ){
				update();
			}else{
				create();
			}
		}


		/**Create new tourOperator
		 * */
		function create(){

			apiService.create( 'tour-operators',vm.tourOperator )
				.then(function ( response ) {

					SweetAlert.swal({
						title: $filter('translate')('tour_operator'),
						text: $filter('translate')('created_successfully'),
						type: 'success',
						timer: 2000,
						showConfirmButton: false
					});

					vm.id = response.data.id;
					vm.tourOperator = response.data;

					if( vm.saveAndExit )
						cancel();

				}).catch( function( response ) {
					$log.error(response);
					$rootScope.showErrors(response,'create_error');

				});

		}


		/**Update  tourOperator
		 * */
		function update(){

			apiService.update( 'tour-operators',vm.tourOperator )
				.then(function ( response ) {

					// $log.erro(response);
					SweetAlert.swal({
						title: $filter('translate')('tour_operator'),
						text: $filter('translate')('updated_successfully'),
						type: 'success',
						timer: 2000,
						showConfirmButton: false
					});

					if( vm.saveAndExit )
						cancel();


				}).catch( function( response ) {
				$log.error(response);
				$rootScope.showErrors(response,'update_error');

			});
		}



		/**==========================
		 * USER META DATA
		 * ==========================*/


		/**Cancel method
		 * */
		function cancel(){
			$location.path('/tour-operators');
		}


		/**Load languages
		 * */
		function loadLanguages(){

			apiService.getAll('languages',{limit:-1})
				.then(function(response){
					vm.languages = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});

		}





	}

})();