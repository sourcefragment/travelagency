/**
 * Created by mario on 3/19/17.
 */
(function () {
	'use strict';

	angular.module('app.tour-operators',[])
		.config(config);

	function config($stateProvider) {

		$stateProvider.state('tour-operators', {
			url: '/tour-operators',
			templateUrl: 'app/js/modules/tour-operators/views/list.html',
			controller: 'MainTourOperatorsCtrl',
			controllerAs: 'mtoc'
		})
			.state('tour-operator-create', {
				url: '/tour-operators/create',
				templateUrl: 'app/js/modules/tour-operators/views/edit.html',
				controller: 'TourOperatorCtrl',
				controllerAs: 'toc'

			})
			.state('tour-operator-edit', {
				url: '/tour-operators/edit/:id',
				templateUrl: 'app/js/modules/tour-operators/views/edit.html',
				controller: 'TourOperatorCtrl',
				controllerAs: 'toc'

			});



	};


})();