/**
 * Created by mario on 12/3/16.
 */
(function () {
	'use strict';

	angular.module('app.offer-types',[])
		.config(config);

	function config( $stateProvider ) {

		$stateProvider
			.state('offer-types', {
				url: '/offer-types',
				templateUrl: 'app/js/modules/offer-types/views/list.html',
				controller: 'MainOffersTypeCtrl',
				controllerAs: 'motc'

			})
			.state('offer-type-create', {
				url: '/offer-type/create',
				templateUrl: 'app/js/modules/offer-types/views/edit.html',
				controller: 'OffersTypeCtrl',
				controllerAs: 'otc'

			})
			.state('offer-type-edit', {
				url: '/offer-type/edit/:id',
				templateUrl: 'app/js/modules/offer-types/views/edit.html',
				controller: 'OffersTypeCtrl',
				controllerAs: 'otc'

			})


	};


})();
