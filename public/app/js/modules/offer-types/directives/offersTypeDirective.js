/**
 * Created by juan on 18/08/16.
 */
(function () {

    angular.module('app.offer-types')
        .directive('createOfferTypeBtn', createOfferTypeBtn)
        .directive('createOfferTypeBtnLong', createOfferTypeBtnLong)
        .directive('createOfferTypeBtnSmall', createOfferTypeBtnSmall)
	    .directive('otContentModal',otContentModal);

    function createOfferTypeBtn() {
        return {
        	restrict : 'EA',
	        scope : {
        		onSuccess : '&onSuccess'
	        },
            template: '<button type="button" ng-click="openModal()" class="btn btn-primary btn-circle">'
                            +'<i class="fa fa-plus"></i>'
                     +'</button>',
            controller: 'OfferTypeModalCtrl'

        };
    }

    function createOfferTypeBtnLong() {
        return {
            template: '<button type="button" class="btn btn-primary btn-lg" ui-sref="offer-types-create">'
                            + '<i class="fa fa-plus-circle"></i> [[ "new_offers_type" | translate ]]'
                     +'</button>',
            controller: 'OffersTypeController',
            controllerAs: 'ofc',
        };
    }

    function createOfferTypeBtnSmall() {
        return {
            template: '<a ng-click="otcc.getFormOfferContent()" class="btn btn-primary btn-xs" style="margin: 0;">'
                            + '[[ "new_content_language" | translate ]]'
                     +'</a>',
            controller: 'OffersTypeContentController',
            controllerAs: 'otcc'
        };
    }

    function otContentModal(){
    	return {
    		restrict:'A',
		    scope:{
    			ngModel:'=ngModel',
			    onSuccess:'&onSuccess',
			    id:'@id'

		    },
		    link:function(scope,element,attrs){
			    element.click( scope.openModal );
		    },
		    controller: 'OfferTypeContentModalCtrl',
		    controllerAs:'$ctrl'
	    }
    }

})();