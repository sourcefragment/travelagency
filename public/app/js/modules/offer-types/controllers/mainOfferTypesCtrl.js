(function () {

    'use strict';

    angular
        .module('app.offer-types')
        .controller('MainOffersTypeCtrl', ctrl);


    ctrl.$inject = [
        '$filter',
        'SweetAlert',
        'apiService',
	    '$rootScope',
	    'NgTableParams',
        '$log'
    ];

    function ctrl(
        $filter,
        SweetAlert,
        apiService,
        $rootScope,
        NgTableParams,
        $log
    ){


        var vm = this;
        vm.types = {};
        vm.tableParams = {};

        vm.getOffersType = getOffersType;
        vm.delete = deleteOffersType;


        activate();

        /**Init controller
         * */
        function activate() {
            initTable();
        }

        /**Init main table structure
         * */
        function initTable() {

	        vm.tableParams = new NgTableParams({},{
	        	getData : function( params ){
	        		return getOffersType( params );
		        }
	        })
        }

        /**Load offersType
         * */
        function getOffersType( params ){

        	var p = $rootScope.getParams( params );

            apiService.getAll( 'offer-types',p )
                .then(function (response) {
                    $log.error(response);
                	vm.types = response.data.data;
	                params.total( response.data.total);
	                params.pages = params.generatePagesArray();
	                params.current = response.data.current_page;
	                return response;

                }).catch(function (data) {
                console.error(data);
            });
        }


        /**Delete offer type
         *@param id, the offer type id
         * @return promise
         */
        function deleteOffersType(id) {

            SweetAlert.swal({
                    title: $filter('translate')('delete_offer_type'),
                    text: $filter('translate')('delete_offer_type_text'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: $filter('translate')('yes_delete'),
                    cancelButtonText: $filter('translate')('no_cancel'),
                    closeOnConfirm: false,
                },
                function (isConfirm) {
                    if (isConfirm) {

                        apiService.delete('offer-types',id)
                            .then(function () {
                                SweetAlert.swal({
                                    title: $filter('translate')('offer_type'),
                                    text: $filter('translate')('deleted_successfully'),
                                    type: 'success',
                                    timer: 2000,
                                    showConfirmButton: false
                                });
                                initTable();
                            }).catch(function ( response ) {
                            console.error( response );
                        });
                    }

                });
        }





    }


})();

