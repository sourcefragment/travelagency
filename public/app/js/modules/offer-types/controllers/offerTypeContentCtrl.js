/**
 * Created by mario on 3/22/17.
 */
(function () {

	'use strict';

	angular.module('app.offer-types')
		.controller('OfferTypeContentModalCtrl',ctrl);


	ctrl.$inject = [
		'$rootScope',
		'$scope',
		'apiService',
		'$log',
		'$uibModal'
	];

	function ctrl(
		$rootScope,
		$scope,
		apiService,
		$log,
		$uibModal
	){

		$scope.openModal = function () {

			var model = $scope.ngModel;
			var id = parseInt($scope.id);

			$log.error($scope);

			var instance = $uibModal.open({
				templateUrl : 'app/js/modules/offer-types/views/content-modal.html',
				size:'lg',
				controller :['$scope',function ($scope) {

					$log.error(model,id);

					$scope.content = model ? model : {};
					$scope.languages = [];

					if( !$scope.content.offer_type_id ){
						$scope.content.offer_type_id = id;
					}

					loadLanguages();

					/**Load client tyes
					 * */
					function loadLanguages(){
						apiService.getAll('languages',{limit:-1})
							.then(function (response) {
								$scope.languages = response.data;
							})
							.catch(function (response) {
								$log.error(response);
							});
					}

					/**On modal save
					 * */
					$scope.saveModal = function () {

						if( $scope.content.id ){

							apiService.update('offer-type-content',$scope.content)
								.then(function (response) {
									onClose();
									$scope.$close();
								})
								.catch(function (response) {
									$log.error(response);
									$rootScope.showErrors(response,'create_error');
								});

						}else{

							apiService.create('offer-type-content',$scope.content)
								.then(function (response) {
									onClose();
									$scope.$close();
								})
								.catch(function (response) {
									$log.error(response);
									$rootScope.showErrors(response,'create_error');
								});

						}





					}

				}]
			});
		}

		function onClose(){
			if( typeof  $scope.onSuccess == 'function'){
				$scope.onSuccess();
			}
		}

	}


})();