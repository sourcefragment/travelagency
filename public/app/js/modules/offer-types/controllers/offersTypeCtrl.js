(function () {

    'use strict';

    angular
        .module('app.offer-types')
        .controller('OffersTypeCtrl', ctrl);


    ctrl.$inject = [
        '$location',
        '$stateParams',
        '$rootScope',
	    'SweetAlert',
	    '$filter',
	    '$auth',
	    'apiService',
	    '$log',
	    '$window',
		'uiCalendarConfig',
		'$timeout'
    ];




	function ctrl(
		$location,
		$stateParams,
		$rootScope,
		SweetAlert,
		$filter,
		$auth,
		apiService,
	    $log,
	    $window,
		uiCalendarConfig,
		$timeout

	){

		var vm = this;
		vm.type = false;
		vm.id = $stateParams.id;
		vm.contents = [];
		vm.attachments = [];
		vm.languages = [];
		vm.spinOptions = {
			min: 0,
			max: 100,
			step: 1
		};

        vm.dtOptions = {
			// minDate: new Date(),
			// customClass:getDays,
			showWeeks:true
		};
        vm.dt = new Date();
        vm.saveAndExit = false;

        vm.newDate = {
        	item_id:vm.id,
	        module:'offer-types'
        };
        vm.categories = [];
        vm.locations = [];

		vm.save = save;
		vm.cancel = cancel;

		//Content
		vm.loadContent = loadContent;
		vm.deleteContent = deleteContent;

		//Media
		vm.loadMedia = loadMedia;
		vm.downloadAttachment = downloadAttachment;
		vm.deleteAttachment = deleteAttachment;


		activate();

		/**Init controller
		 * */
		function activate() {

			loadLanguages();
			if ( $stateParams.id ){
				get( $stateParams.id );
			}
		}


		/**Load type
		 * */
		function get( id ) {

			apiService.get( 'offer-types',id )
				.then(function ( response ) {
					// $log.error(response);
					vm.type = response.data;

				}).catch(function ( response ) {
					$log.error( response );
				});
		}


		/**Save type
		 * */
		function save( mode ) {

			vm.saveAndExit = mode;

			if( vm.type.id ){
				update();
			}else{
				create();
			}

		}


		/**Create new offer type
		 *
		 * */
		function create(){

			apiService.create( 'offer-types',vm.type )
				.then(function ( response) {
					// $log.error(response);
					SweetAlert.swal({
						title: $filter('translate')('offer_type'),
						text: $filter('translate')('created_successfully'),
						type: 'success',
						timer: 2000,
						showConfirmButton: false
					});

					vm.id = response.data.id;
					vm.type = response.data;

					if( vm.saveAndExit )
						cancel();

				}).catch(function (response) {
				$log.error(response);
				$rootScope.showErrors(response,'create_error');

			});

		}


		/**Update offer type
		 * */
		function update(){

			apiService.update( 'offer-types',vm.type )
				.then( function( response ){

					SweetAlert.swal({
						title: $filter('translate')('offer_type'),
						text: $filter('translate')('updated_successfully'),
						type: 'success',
						timer: 2000,
						showConfirmButton: false
					});

					if( vm.saveAndExit )
						cancel();


				}).catch( function( response ){
				$log.error(response);
				$rootScope.showErrors(response,'update_error');
			});

		}




		/**Cancel method
		 * */
		function cancel(){
			$location.path('/offer-types');
		}


		/**Load all languages
		 * */
		function loadLanguages(){
			apiService.getAll('languages',{limit:-1})
				.then(function (response) {
					vm.languages = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}

		/**===================
		 * CONTENT
		 * ==================*/

		/**Get offer type content
		 * @param id int, the offer type id to retrieve the content from
		 * @retun null
		 */
		function loadContent(){

			apiService.getAll( 'offer-type/'+vm.id+'/content')
				.then(function ( response ) {
					// $log.error( response );
					vm.contents = response.data;
				})
				.catch( function ( response ) {
					$log.error( response );
				});
		}

		/**Delete offer type content
		 *
		 * @param id
		 */

		function deleteContent( id ){

			SweetAlert.swal({
					title: $filter('translate')('content'),
					text: $filter('translate')('offer_type_content_delete_text'),
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: $filter('translate')('yes_delete'),
					cancelButtonText: $filter('translate')('no_cancel'),
					closeOnConfirm: false,
				},
				function (isConfirm) {
					if (isConfirm) {

						apiService.delete( 'offer-type-content',id )
							.then(function ( response ) {
								SweetAlert.swal({
									title: $filter('translate')('content'),
									text: $filter('translate')('deleted_successfully'),
									type: "success",
									timer: 2000,
									showConfirmButton: false
								});
								loadContent();

							}).catch(function ( response ) {
							$log.error( response );
							$rootScope.showErrors(response,'delete_error');
						});
					}
				});
		}

		/**======================
		 * MEDIA
		 * ======================
		 * */

		/**Load offer type attachments files
		 * */
		function loadMedia(){
			apiService.getAll( 'offer-type/'+vm.id+'/attachments' )
				.then(function ( response ) {
					$log.error(response);
					vm.attachments = response.data;
				})
				.catch(function ( response ) {
					$log.error(response);
				});

		}

		/**Delete attached attachments
		 * */
		function deleteAttachment( id ){

			SweetAlert.swal({
					title: $filter('translate')('delete_file'),
					text: $filter('translate')('delete_file_text'),
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: $filter('translate')('yes_delete'),
					cancelButtonText: $filter('translate')('no_cancel'),
					closeOnConfirm: false,
				},
				function (isConfirm) {
					if (isConfirm) {

						apiService.delete( 'attachments',id )
							.then(function (response) {
								$log.error(response);
								SweetAlert.swal({
									title: $filter('translate')('file'),
									text: $filter('translate')('deleted_successfully'),
									type: "success",
									timer: 1000,
									showConfirmButton: false
								});
								vm.loadMedia();
							})
							.catch(function (response) {
								$log.error(response);
							});

					}
				});


		}


		/**Download vehicle attachment
		 * */
		function downloadAttachment( id ){
			var token = $auth.getToken();
			$window.open( $rootScope.API_URL+'download/attachment/'+id+'?token='+token);
		}


        /**=================
		 * DATES
		 * =================
         */

        vm.dates = [];
        vm.loadDates = loadDates;

        function loadDates(){

			apiService.getAll('offer-type/'+vm.id+'/dates')
				.then(function (response) {
					// $log.info('date updated',response);
					vm.dates = response.data;
                })
				.catch(function (response) {
					$log.error(response);
                });
		}


		/**Delete date
		 * */
		vm.deleteDate = deleteDate;
		function deleteDate( id ) {

            SweetAlert.swal({
                    title: $filter('translate')('delete_date'),
                    text: $filter('translate')('delete_date_text'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $filter('translate')('yes_delete'),
                    cancelButtonText: $filter('translate')('no_cancel'),
                    closeOnConfirm: false
                },
                function (isConfirm) {
                    if (isConfirm) {

                        apiService.delete( 'events',id )
                            .then(function (response) {
                                // $log.error(response);
                                SweetAlert.swal({
                                    title: $filter('translate')('date'),
                                    text: $filter('translate')('deleted_successfully'),
                                    type: "success",
                                    timer: 1000,
                                    showConfirmButton: false
                                });
                                vm.loadDates();
                            })
                            .catch(function (response) {
                                $log.error(response);
                            });

                    }
                });

        }


        /**==================
         * CATEGORIES AND LOCATIONS
         * ==================
         * */
        vm.loadCategoriesLocations = loadCategoriesLocations;
        function loadCategoriesLocations(){

        	loadCategories();
        	loadLocations();
        }

        //CATEGORIES
        vm.categories = [];
        vm.loadCategories = loadCategories;
        function loadCategories() {


        	apiService.getAll('categories',{limit:-1})
		        .then(function (response) {
			        vm.categories = response.data;
			        loadOfferCategories();
		        })
		        .catch(function (response) {
			       $log.error(response);
		        });
        }

        vm.selectedCategories = [];
        function loadOfferCategories(){

        	vm.selectedCategories = [];
        	apiService.getAll('offer-type/'+vm.id+'/categories')
		        .then(function (response) {
			        angular.forEach(response.data,function(value,index){
			        	vm.selectedCategories.push(value.category_id);
			        });
		        })
        }


        vm.onCategorySelect = onCategorySelect;
        function onCategorySelect( item,model ){

        	apiService.create('offer-type/'+vm.id+'/category',{category_id:item.id})
	            .then(function (response) {
	            })
		        .catch(function (response) {
					$log.error(response);
		        });
        }

        vm.onCategoryRemove = onCategoryRemove;
        function onCategoryRemove( item ) {

        	apiService.getAll('offer-type/'+vm.id+'/remove-category',{category_id:item.id})
		        .then(function (response) {
		        })
		        .catch(function (response) {
			        $log.error(response);
		        });
        }


        //LOCATIONS
        vm.locations = [];
        vm.loadLocations = loadLocations;
        function loadLocations(){

        	apiService.getAll('locations',{limit:-1})
		        .then(function (response) {
					vm.locations = response.data;
					loadOfferLocations();
		        })
		        .catch(function (response) {
					$log.error(response);
		        });
        }

        vm.selectedLocations = [];
        function loadOfferLocations(){

        	vm.selectedLocations = [];
        	apiService.getAll('offer-type/'+vm.id+'/locations')
		        .then(function (response) {
			        angular.forEach(response.data,function (value,index) {
				        vm.selectedLocations.push(value.location_id);
			        });
		        })
		        .catch(function (response) {
			        $log.error(response);
		        });
        }

        vm.onLocationSelect = onLocationSelect;
        function onLocationSelect( item,model ){

			apiService.create('offer-type/'+vm.id+'/location',{location_id:item.id})
				.then(function (response) {
				})
				.catch(function (response) {
					$log.error(response);
				});

        }

        vm.onLocationRemove = onLocationRemove;
        function onLocationRemove( item ){
        	apiService.getAll('offer-type/'+vm.id+'/remove-location',{location_id:item.id})
		        .then(function (response) {
		        })
		        .catch(function (response) {
			       $log.error(response);
		        });
        }



		vm.setFeatured = setFeatured;
        function setFeatured(){

        	apiService.getAll('offer-types/'+vm.id+'/toggle-featured')
		        .then(function (response) {
			        vm.type.featured = response.data.featured;
			        $rootScope.saved();
		        })
        }
	}


})();
