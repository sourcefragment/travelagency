/**
 * Created by mario on 12/23/16.
 */
(function () {
	'use strict';

	angular.module('app.offer-types')
		.controller('OfferTypeModalCtrl',ctrl);


	ctrl.$inject = ['$scope','$uibModal','$rootScope','apiService'];

	function ctrl( $scope,$uibModal,$rootScope,apiService ){


		/**Open directives modal
		 * */
		$scope.openModal = function () {

			var instance = $uibModal.open({
				templateUrl : 'app/js/sections/offer-types/views/create-modal.html',
				controller : ['$scope',function ($scope) {

					$scope.type = {};



					/**Called on modal save
					 * */
					$scope.onSave = function () {

					};


				}]
			})
		};
	}
})();