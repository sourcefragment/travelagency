/**
 * Created by juan on 17/08/16.
 */

(function () {
    'use strict';

    angular.module('app.companies',[])
        .config(config);

    function config($stateProvider) {

        $stateProvider.state('companies', {
            url: '/companies',
            templateUrl: 'app/js/modules/companies/views/list.html',
            controller: 'MainCompaniesController',
            controllerAs: 'mcc'
        })
            .state('company-create', {
                url: '/companies/create',
                templateUrl: 'app/js/modules/companies/views/edit.html',
                controller: 'CompaniesController',
                controllerAs: 'cc'

            })
            .state('company-edit', {
                url: '/companies/edit/:id',
                templateUrl: 'app/js/modules/companies/views/edit.html',
                controller: 'CompaniesController',
                controllerAs: 'cc'
            })
    }

})();

