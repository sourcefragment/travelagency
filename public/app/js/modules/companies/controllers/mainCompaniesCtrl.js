(function () {

    'use strict';

    angular
        .module('app.companies')
        .controller('MainCompaniesController', mainCompaniesController);

    mainCompaniesController.$inject = [
        '$filter',
        'SweetAlert',
	    '$rootScope',
	    'NgTableParams',
	    'apiService',
	    '$log'

    ];

    function mainCompaniesController(
        $filter,
        SweetAlert,
        $rootScope,
        NgTableParams,
        apiService,
        $log
        ){

        var vm = this;
	    vm.companies = {};
	    vm.tableParams = {};

        vm.delete = deleteCompany;
	    vm.checkAll = false;



	    activate();

        /**Init controller
         * */
        function activate() {
            initTable();
        }

	    /**Init main table structure
	     * */
	    function initTable() {
			vm.tableParams = new NgTableParams({},{
				getData : function ( params ) {
					return getCompanies( params );
				}
			});
	    }

        /**Load companies
         * */
        function getCompanies( params ) {

        	var p = $rootScope.getParams( params );
            apiService.getAll('companies',p)
                .then( function( response ) {
                	vm.companies = response.data;
	                params.total( response.data.total);
	                params.pages = params.generatePagesArray();
	                params.current = response.data.current_page;
	                return response;

                }).catch( function( response ) {
                    $log.error( response );
            });
        }

        /**Delete user
         *@param id, the user id
         * @return promise
         */
        function deleteCompany(id) {
            SweetAlert.swal({
                    title: $filter('translate')('delete_company'),
                    text: $filter('translate')('delete_company_text'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: $filter('translate')('yes_delete'),
                    cancelButtonText: $filter('translate')('no_cancel'),
                    closeOnConfirm: false,
                },
                function (isConfirm) {
                    if (isConfirm) {
                        apiService.delete('companies',id)
                            .then(function () {
                                SweetAlert.swal({
                                    title: $filter('translate')('company'),
                                    text: $filter('translate')('deleted_successfully'),
                                    type: 'success',
                                    timer: 2000,
                                    showConfirmButton: false
                                });
                                initTable();
                            }).catch(function (response) {
                                $log.log(response);
                        });
                    }
                });
        }



    }

})();