(function () {

    'use strict';

    angular
        .module('app.companies')
        .controller('CompaniesController', companiesController);


    companiesController.$inject = [
	    '$rootScope',
        '$stateParams',
        '$location',
	    'SweetAlert',
	    '$filter',
	    'apiService',
	    '$log'
    ];

    function companiesController(
	    $rootScope,
        $stateParams,
        $location,
        SweetAlert,
        $filter,
        apiService,
        $log

    ){

    	var vm = this;
        vm.company = false;
        vm.modal = false;
        vm.id = $stateParams.id;
	    vm.spinOptions = {
		    min: 0,
		    max: 10,
		    step: 1
	    };
	    vm.saveAndExit = false;


        vm.getCompany = getCompany;
        vm.save = save;
	    vm.cancel = cancel;

        activate();

        function activate() {

            if( vm.id ){
	            getCompany( vm.id );
            }

        }


        /**Load a company
         * */
        function getCompany(id) {

        	apiService.get('companies',id)
                .then(function (response) {
                    vm.company = response.data;
	                vm.city = response.data.city;

                }).catch(function (response) {
                    $log.error(response);
                });
        }



        /**Save company
         * */
        function save( mode ) {

        	vm.saveAndExit = mode;

			//update city
        	if( vm.city ){
        	    vm.company.city_id = vm.city.id;
	        }

        	if( vm.company.id ){
        		update();
	        }else{
	        	create();
	        }

        }


        /**Create new company
         * */
        function create(){

	        apiService.create( 'companies',vm.company )
		        .then(function (response) {

			        SweetAlert.swal({
				        title: $filter('translate')('company'),
				        text: $filter('translate')('created_successfully'),
				        type: 'success',
				        timer: 1000,
				        showConfirmButton: false
			        });

			        vm.id = response.data.id;
			        vm.company = response.data;

			        if( vm.saveAndExit )
			            cancel();

		        }).catch( function( response ) {
		        	$log.error(response);
					$rootScope.showErrors( response,'create_error');
	            });

        }


        /**Update company
         *
         * */
        function update(){

	        apiService.update( 'companies',vm.company )
		        .then(function (response) {

			        SweetAlert.swal({
				        title: $filter('translate')('company'),
				        text: $filter('translate')('updated_successfully'),
				        type: 'success',
				        timer: 1000,
				        showConfirmButton: false
			        });

			        $log.error(response);
			        vm.id = response.data.id;
			        vm.company = response.data;

			        if( vm.saveAndExit )
			            cancel();

		        }).catch( function( response ) {
		        	$log.error(response);
					$rootScope.showErrors(response,'update_error');

		        });

        }


	    /**Cancel form edit
	     * */
	    function cancel(){
	    	$location.path('/companies');
	    }

    }

})();
