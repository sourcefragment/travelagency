/**
 * Created by mario on 9/13/16.
 */
(function () {
	'use strict';

	angular.module('app.companies')
		.directive('companySelector',companySelector);

	function companySelector(){
		return {
			restrict:'AE',
			required:'ngModel',
			scope:{
				company:'=ngModel',
				companies:'='
			},
			templateUrl:'app/js/sections/companies/views/company-selector.html',
			controller:'CompaniesController',
			controllerAs:'$ctrl'
		};
	}
})();