/**
 * Created by juan on 18/08/16.
 */
(function () {

    angular.module('app.companies')
        .directive('newCompanyBtn', newCompanyBtn)
        .directive('createCompanyBtnLong', newCompanyBtnLong);

    function newCompanyBtn() {
        return {
            templateUrl:'app/js/sections/companies/views/new-company-btn.html',
            controller: 'CompaniesController',
            controllerAs: '$ctrl',
        };
    }

    function newCompanyBtnLong() {
        return {
            template: '<button type="button" class="btn btn-primary btn-lg" ui-sref="company-create">'
            + '<i class="fa fa-plus-circle"></i> [[ "new_company" | translate ]]'
            + '</button>'
        };
    }

})();