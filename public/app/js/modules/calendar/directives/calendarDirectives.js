/**
 * Created by mario on 11/04/17.
 */
(function () {
    'use strict';

    angular.module('app.calendar')
        .directive('calendarPicker',calendarPicker);


    function calendarPicker() {
        return {
            restrict:'A',
            scope:{
                model:'=ngModel',
                onSuccess:'&onSuccess',
                title:'@title'
            },
            link:function(scope,element,attrs){
                element.click( scope.openModal );
            },
            controller: 'CalendarCtrl',
            controllerAs:'$ctrl'
        }
    }



})();