/**
 * Created by mario on 11/04/17.
 */
(function () {

    'use strict';

    angular.module('app.calendar')
        .controller('CalendarCtrl',ctrl);



    ctrl.$inject = [
        'apiService',
        '$scope',
        '$uibModal',
        '$log'
    ];

    function ctrl(
        apiService,
        $scope,
        $uibModal,
        $log
    ) {



        /**Open controller modal
         * */
        $scope.openModal = function(){

            var model = $scope.model;
            var title = $scope.title;

            var defaultDate = {
            	id:null,
                item_id:null,
                module:null,
                name:null,
                slug:null,
                start_date:null,
                end_date:null,
                endless:false,
                repeat_each:false,
	            repeat_mode:'',
                monday:false,
                tuesday:false,
                wednesday:false,
                thursday:false,
                friday:false,
                saturday:false,
                sunday:false
            };

            var event = angular.extend(defaultDate,model);


            var modalInstance = $uibModal.open({
                templateUrl : 'app/js/modules/calendar/views/modal.html',
                controller : [
                    '$scope',
                    '$rootScope',
                    'NgTableParams',
                    function ( $scope,$rootScope,NgTableParams ) {

                        $scope.event = event;
                        $scope.event.endless = 1;
                        $scope.modes = ['daily','weekly','monthly','yearly'];
                        $scope.title = title;


                        $scope.saveModal = saveModal;

                        /**On modal save
                         * */
                        function saveModal(){


                            //Parse start date
                            if( $scope.event.start_date && typeof $scope.event.start_date !== 'string' ){
                                $scope.event.start_date = $scope.event.start_date.format('YYYY-MM-DD');
                            }

                            //Parse end date
                            if( $scope.event.end_date && typeof $scope.event.end_date !== 'string' ){
                                $scope.event.end_date = $scope.event.end_date.format('YYYY-MM-DD');
                            }

                            // $log.error($scope.event);

                            if( $scope.event.id ){
                                update();
                            }else{
                                create();
                            }


                        }


                        /**Update event
                         * */
                        function update(){

	                        apiService.update('events',$scope.event)
		                        .then(function ( response ) {
		                        	// $log.info('updated',response);
			                        $scope.$close();
			                        onSave();

		                        })
		                        .catch(function (response) {
			                        $log.error(response);
			                        $rootScope.showErrors(response,'create_error');
		                        });

                        }


                        /**Create event
                         * */
                        function create(){

	                        apiService.create('events',$scope.event)
		                        .then(function ( response ) {
			                        // $log.info('created',response);
			                        $scope.$close();
			                        onSave();

		                        })
		                        .catch(function (response) {
			                        $log.error(response);
			                        $rootScope.showErrors(response,'create_error');
		                        });

                        }


                    }]

            });


        };

        /**Save selected model
         * */
        function onSave(){

            if( typeof $scope.onSuccess === 'function' ){
                $scope.onSuccess();
            }
        }


    }

})();