/**
 * Created by mario on 3/19/17.
 */
(function () {
	'use strict';

	angular.module('app.agents',[])
		.config(config);

	function config($stateProvider) {

		$stateProvider.state('agents', {
			url: '/agents',
			templateUrl: 'app/js/modules/agents/views/list.html',
			controller: 'MainAgentsCtrl',
			controllerAs: 'mac'
		})
			.state('agent-create', {
				url: '/agents/create',
				templateUrl: 'app/js/modules/agents/views/edit.html',
				controller: 'AgentCtrl',
				controllerAs: 'ac'

			})
			.state('agent-edit', {
				url: '/agents/edit/:id',
				templateUrl: 'app/js/modules/agents/views/edit.html',
				controller: 'AgentCtrl',
				controllerAs: 'ac'

			});



	}


})();