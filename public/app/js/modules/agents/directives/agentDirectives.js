/**
 * Created by mario on 3/19/17.
 */
(function () {
	'use strict';

	angular.module('app.agents')
		.directive('agentSearchBox',agentSearchBox)
		.directive('agentCreateModal',agentCreateModal);


	function agentSearchBox() {
		return {
			restrict:'A',
			scope:{
				model:'=ngModel'
			},
			link:function(scope,element,attrs){
				element.click( scope.openModal );
			},
			controller: 'AgentsSearchBoxCtrl'
		}
	}

	function agentCreateModal(){
		return {
			restrict:'A',
			scope:{
				onSuccess:'&onSuccess'
			},
			link:function(scope,element,attrs){
				element.click( scope.openModal );
			},
			controller: 'AgentCreateCtrl'
		}
	}

})();