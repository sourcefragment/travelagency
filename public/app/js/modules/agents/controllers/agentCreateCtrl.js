/**
 * Created by mario on 3/19/17.
 */
(function () {

	'use strict';

	angular.module('app.agents')
		.controller('AgentCreateCtrl',ctrl);


	ctrl.$inject = [
		'$rootScope',
		'$scope',
		'apiService',
		'$log',
		'$uibModal'
	];

	function ctrl(
		$rootScope,
		$scope,
		apiService,
		$log,
		$uibModal
	){

		$scope.openModal = function () {

			var instance = $uibModal.open({
				templateUrl : 'app/js/modules/agents/views/create-modal.html',
				controller :['$scope',function ($scope) {

					$scope.agent = {};
					$scope.types = [];


					loadTypes();

					/**Load agent tyes
					 * */
					function loadTypes(){
						apiService.getAll('option/agent-types')
							.then(function (response) {
								$scope.types = response.data;
							})
							.catch(function (response) {
								$log.error(response);
							});
					}

					/**On modal save
					 * */
					$scope.saveModal = function () {

						apiService.create('agents',$scope.agent)
							.then(function (response) {
								$scope.$close();
								onSuccess( response.data.id );
							})
							.catch(function (response) {
								$log.error(response);
								$rootScope.showErrors(response,'create_error');
							});
					}

				}]
			});
		};


		/**Triggered on success
		 * */
		function onSuccess( id  ){

			if( typeof $scope.onSuccess === 'function' ){
				$scope.onSuccess()( id );
			}
		}

	}


})();