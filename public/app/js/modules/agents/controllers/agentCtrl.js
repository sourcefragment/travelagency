/**
 * Created by mario on 3/19/17.
 */
(function () {

	'use strict';

	angular
		.module('app.agents')
		.controller('AgentCtrl', ctrl);

	ctrl.$inject = [
		'$stateParams',
		'$location',
		'apiService',
		'$filter',
		'SweetAlert',
		'NgTableParams',
		'$rootScope',
		'$log'
	];

	function ctrl(
		$stateParams,
		$location,
		apiService,
		$filter,
		SweetAlert,
		NgTableParams,
		$rootScope,
		$log
	) {


		var vm = this;
		vm.id = $stateParams.id;
		vm.agent = false;
		vm.tableParams = {};
		vm.languages = [];
		vm.defaultLanguages = [];

		vm.treatments = [
			'sr',
			'mr',
			'mrs',
			'dr',
			'ms'
		];
		vm.saveAndExit = true;


		vm.save = save;
		vm.cancel = cancel;


		activate();

		/**Init controller
		 * */
		function activate() {

			loadLanguages();

			if ( vm.id ){
				get(vm.id);
			}

		}





		/**Load agent<
		 * */
		function get( id ) {

			apiService.get('agents',id)
				.then(function ( response ) {
					// $log.error(response);
					vm.agent = response.data;
					vm.city = response.data.city;
					vm.billingCity = response.data.billing_city;
					vm.company = response.data.company;

				}).catch(function () {
				$log.error(data);
			});
		}


		/**Save agents
		 * */
		function save( mode ) {

			vm.saveAndExit = mode;

			//Update city
			if( vm.city ){
				vm.agent.city_id = vm.city.id;
			}

			//Update billing city
			if( vm.billingCity ){
				vm.agent.billing_city_id = vm.billingCity.id;
			}

			//Save agent dob
			if( vm.agent.dob && typeof  vm.agent.dob !== 'string'){
				vm.agent.dob = vm.agent.dob.format('YYYY-MM-DD');
			}

			if( vm.agent.id ){
				update();
			}else{
				create();
			}
		}


		/**Create new agent
		 * */
		function create(){

			apiService.create( 'agents',vm.agent )
				.then(function ( response ) {

					SweetAlert.swal({
						title: $filter('translate')('agent'),
						text: $filter('translate')('created_successfully'),
						type: 'success',
						timer: 1000,
						showConfirmButton: false
					});

					vm.id = response.data.id;
					vm.agent = response.data;

					if( vm.saveAndExit )
						cancel();

				}).catch( function( response ) {
				$log.error(response);
				$rootScope.showErrors(response,'create_error');

			});

		}


		/**Update  agent
		 * */
		function update(){

			apiService.update( 'agents',vm.agent )
				.then(function ( response ) {

					// $log.error(response);
					SweetAlert.swal({
						title: $filter('translate')('agent'),
						text: $filter('translate')('updated_successfully'),
						type: 'success',
						timer: 1000,
						showConfirmButton: false
					});

					if( vm.saveAndExit )
						cancel();

				}).catch( function( response ) {
				$log.error(response);
				$rootScope.showErrors(response,'update_error');

			});
		}



		/**==========================
		 * USER META DATA
		 * ==========================*/


		/**Cancel method
		 * */
		function cancel(){
			$location.path('/agents');
		}


		/**Load languages
		 * */
		function loadLanguages(){

			apiService.getAll('languages',{limit:-1})
				.then(function(response){
					vm.languages = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});


		}





	}

})();