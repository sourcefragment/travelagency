/**
 * Created by mario on 11/30/16.
 */
(function () {

	'use strict';

	angular.module('app.options',[])
		.config(config);

	function config( $stateProvider ){

		$stateProvider.state('options', {
			url: '/options',
			templateUrl: 'app/js/modules/options/views/list.html',
			controller: 'MainOptionsCtrl',
			controllerAs: 'moc'
		})
			.state('option-create', {
				url: '/options/create',
				templateUrl: 'app/js/modules/options/views/edit.html',
				controller: 'OptionCtrl',
				controllerAs: 'oc'

			})
			.state('option-edit', {
				url: '/options/edit/:id',
				templateUrl: 'app/js/modules/options/views/edit.html',
				controller: 'OptionCtrl',
				controllerAs: 'oc'

			});

	}


})();