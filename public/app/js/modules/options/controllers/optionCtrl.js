/**
 * Created by mario on 12/11/16.
 */
(function () {
	'use strict';

	angular
		.module('app.options')
		.controller('OptionCtrl',ctrl);


	ctrl.$inject = [
		'$stateParams',
		'$location',
		'$filter',
		'SweetAlert',
		'$uibModal',
		'$rootScope',
		'apiService'
	];

	function ctrl(
		$stateParams,
		$location,
		$filter,
		SweetAlert,
		$uibModal,
		$rootScope,
		apiService

	){

		var vm = this;
		vm.id  = $stateParams.id;
		vm.option = {};
		vm.types = [
			'booking-status',
			'invoice-status',
			'invoice-type',
			'item'
		];
		vm.saveAndExit = false;

		vm.save = save;
		vm.cancel = cancel;


		init();

		/**Init view
		 * */
		function init(){

			if( vm.id ){
				get( vm.id );
			}

		}


		/**Load option
		 * */
		function get( id ){

			apiService.get( 'options',id )
				.then(function (response) {
					vm.option = response.data;
				})
				.catch(function (response) {
					console.log(response);
				})
		}


		/**Save option
		 * */
		function save( mode ){

			vm.saveAndExit = mode;

			if( vm.id ){
				update();
			}else{
				create();
			}
		}

		/**Create new option
		 * */
		function create(){
			apiService.create( 'options',vm.option )
				.then(function (response) {
					// console.log(response);
					SweetAlert.swal({
						title: $filter('translate')('option'),
						text: $filter('translate')('created_successfully'),
						type: 'success',
						timer: 1000,
						showConfirmButton: false
					});

					vm.id = response.data.id;
					vm.option = response.data;

					if( vm.saveAndExit )
						cancel();
				})
				.catch(function (response) {
					console.log(response);
					$rootScope.showErrors(response,'create_error');
				});

		}


		/**Update option
		 * */
		function update(){
			apiService.update( 'options',vm.option )
				.then(function (response) {
					// console.log(response);
					SweetAlert.swal({
						title: $filter('translate')('option'),
						text: $filter('translate')('updated_successfully'),
						type: 'success',
						timer: 1000,
						showConfirmButton: false
					});

					if( vm.saveAndExit )
						cancel();

				})
				.catch(function (response) {
					console.log(response);
					$rootScope.showErrors(response,'update_error')
				});

		}


		/**On cancel
		 * */
		function cancel(){
			$location.path('/options');
		}

	}

})();