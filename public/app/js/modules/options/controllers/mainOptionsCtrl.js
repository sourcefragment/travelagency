/**
 * Created by mario on 12/11/16.
 */
(function () {

	'use strict';

	angular
		.module('app.options')
		.controller('MainOptionsCtrl',mainAgenciesCtrl);

	mainAgenciesCtrl.$inject = [
		'apiService',
		'$rootScope',
		'$filter',
		'SweetAlert',
		'toaster',
		'NgTableParams'
	];


	function mainAgenciesCtrl(
		apiService,
		$rootScope,
		$filter,
		SweetAlert,
		toaster,
		NgTableParams

	){


		var vm = this;

		vm.options = [];
		vm.tableParams = {};
		vm.delete = deleteOption;


		init();

		function init(){
			initTable();
		}


		/**Init main view table
		 * */
		function initTable(){

			vm.tableParams = new NgTableParams({},{
				getData : function( params ){
					return getOptions( params );
				}
			});
		}

		/**Get all options for listing
		 *@param params object
		 * */
		function getOptions( params ){

			var p = $rootScope.getParams( params );
			// console.log(p);
			apiService.getAll( 'options', p )
				.then(function (response) {
					// console.log(response);
					vm.options = response.data.data;
					params.total( response.data.total);
					params.pages = params.generatePagesArray();
					params.current = response.data.current_page;
					return response;
				})
				.catch(function (response) {
					console.log(response);
					$rootScope.showErrors(response,'option_error');
				})
		}

		/**Delete angecy
		 * */
		function deleteOption( id ){

			SweetAlert.swal({
					title: $filter('translate')('delete_option'),
					text: $filter('translate')('delete_option_text'),
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: $filter('translate')('yes_delete'),
					cancelButtonText: $filter('translate')('no_cancel'),
					closeOnConfirm: false,
				},
				function (isConfirm) {

					if (isConfirm) {

						apiService.delete( 'options',id )
							.then( function( response ){
								SweetAlert.swal({
									title: $filter('translate')('option'),
									text: $filter('translate')('deleted_successfully'),
									type: 'success',
									timer: 2000,
									showConfirmButton: false
								});
								initTable();

							}).catch(function ( response ) {

							var msg = response.data.error;

							SweetAlert.swal({
								title : $filter('translate')('option_delete'),
								text : $filter('translate')( msg ),
								type : 'error',
								showConfirmButton : true,
								closeOnConfirm : true
							});

						});
					}

				});

		}
	}

})();