(function () {

    angular.module('app.bookings')
        .directive('createBookingBtn', createBookingBtn)
        .directive('bookingSearchBox', bookingSearchBox)
        .directive('eventPicker',eventPicker);


    function createBookingBtn() {
        return {
            template: '<button type="button" class="btn btn-primary btn-lg btnadd" ui-sref="booking-create">'
                        + '<i class="fa fa-plus-circle"></i> [[ "new_booking" | translate ]]'
                    + '</button>'
        };
    }


    /**
     * */
    function bookingSearchBox(){
	    return {
		    restrict:'A',
		    scope:{
			    model:'=ngModel'
		    },
		    link:function(scope,element,attrs){
			    element.click( scope.openModal );
		    },
		    controller: 'BookingsSearchBoxCtrl'
	    }
    }


    /**Event picker
     * */
    function eventPicker(){
        return{
            restrict:'A',
            scope:{
                ngModel:'=ngModel',
                onSuccess:'&onSuccess'
            },
            link:function (scope,element,attrs) {
                element.click(scope.openModal);
            },
            controller:'EventCtrl'
        }
    }

})();