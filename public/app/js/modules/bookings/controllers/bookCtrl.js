/**
 * Created by mario on 7/06/17.
 */
(function () {

    'use strict';

    angular
        .module('app.bookings')
        .controller('BookCtrl',ctrl);


    ctrl.$inject = [
        '$stateParams',
        '$location',
        '$filter',
        'SweetAlert',
        '$uibModal',
        '$rootScope',
        'apiService',
        '$log'
    ];

    function ctrl(
        $stateParams,
        $location,
        $filter,
        SweetAlert,
        $uibModal,
        $rootScope,
        apiService,
        $log

    ){

        var vm = this;
        vm.availableOffers = [];

        /**Search for available offers
         * */
        vm.searchOffers = searchOffers;
        function searchOffers(){

        }

    }

})();