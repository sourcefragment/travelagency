(function () {

	'use strict';

	angular
		.module('app.bookings')
		.controller('MainBookingsCtrl',ctrl);

	ctrl.$inject = [
		'apiService',
		'$rootScope',
		'$filter',
		'SweetAlert',
		'NgTableParams',
		'$log'
	];


	function ctrl(
		apiService,
		$rootScope,
		$filter,
		SweetAlert,
		NgTableParams,
		$log

	){


		var vm = this;

		vm.bookings = [];
		vm.tableParams = {};
		vm.delete = deleteItem;


		init();

		function init(){
			initTable();
		}


		/**Init main view table
		 * */
		function initTable(){

			vm.tableParams = new NgTableParams({},{
				getData : function( params ){
					return getAll( params );
				}
			});
		}

		/**Get all bookings for listing
		 *@param params object
		 * */
		function getAll( params ){

			var p = $rootScope.getParams( params );
			// $log.error(p);
			apiService.getAll( 'bookings', p )
				.then(function (response) {
					// $log.error(response);
					vm.bookings = response.data.data;
					params.total( response.data.total);
					params.pages = params.generatePagesArray();
					params.current = response.data.current_page;
					return response;
				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'booking_error');
				})
		}

		/**Delete angecy
		 * */
		function deleteItem( id ){

			SweetAlert.swal({
					title: $filter('translate')('delete_booking'),
					text: $filter('translate')('delete_booking_text'),
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: $filter('translate')('yes_delete'),
					cancelButtonText: $filter('translate')('no_cancel'),
					closeOnConfirm: false,
				},
				function (isConfirm) {

					if (isConfirm) {

						apiService.delete( 'bookings',id )
							.then( function( response ){
								SweetAlert.swal({
									title: $filter('translate')('booking'),
									text: $filter('translate')('deleted_successfully'),
									type: 'success',
									timer: 2000,
									showConfirmButton: false
								});
								initTable();

							}).catch(function ( response ) {

							var msg = response.data.error;

							SweetAlert.swal({
								title : $filter('translate')('delete_error'),
								text : $filter('translate')( msg ),
								type : 'error',
								showConfirmButton : true,
								closeOnConfirm : true
							});

						});
					}

				});

		}
	}

})();