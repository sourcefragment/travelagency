/**
 * Created by mario on 1/23/17.
 */
(function () {
    'use strict';

    angular.module('app.bookings')
        .controller('EventCtrl',ctrl);

    ctrl.$inject = [
        'apiService',
        '$scope',
        'SweetAlert',
        '$uibModal',
        '$log'
    ];

    function ctrl(
        apiService,
        $scope,
        SweetAlert,
        $uibModal,
        $log
    ) {



        /**Open controller modal
         * */
        $scope.openModal = function(){

            var modalInstance = $uibModal.open({
                templateUrl : 'app/js/modules/bookings/views/event-pciker.html',
                size : 'lg',
                controller : [
                    '$scope',
                    '$rootScope',
                    'NgTableParams',
                    function ( $scope,$rootScope,NgTableParams ) {


                    }]

            });


        };

        /**Save model save
         * */
        function onSave(){

        }


    }


})();