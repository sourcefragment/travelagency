/**
 * Created by mario on 4/05/17.
 */
(function () {
    'use strict';

    angular
        .module('app.bookings')
        .controller('BookingSingleCtrl',ctrl);


    ctrl.$inject = [
        '$stateParams',
        '$location',
        '$filter',
        'SweetAlert',
        '$uibModal',
        '$rootScope',
        'apiService',
        '$log'
    ];

    function ctrl(
        $stateParams,
        $location,
        $filter,
        SweetAlert,
        $uibModal,
        $rootScope,
        apiService,
        $log

    ){

        var vm = this;
        vm.id  = $stateParams.id;
        vm.booking = {};
        vm.locations = [];
        vm.statuses = [];
        vm.offerTypes = [];
        vm.offers = [];

        vm.save = save;
        vm.cancel = cancel;


        init();

        /**Init view
         * */
        function init(){

            loadStatuses();
            loadOfferTypes();
            if( vm.id ){
                get( vm.id );
            }

        }


        /**Load booking
         * */
        function get( id ){

            apiService.get( 'bookings',id )
                .then(function (response) {
                    $log.error(response);
                    vm.booking = response.data;
                    vm.agent = response.data.agent;
                    vm.client = response.data.client;
                    vm.stop = response.data.stop;
                })
                .catch(function (response) {
                    $log.error(response);
                });
        }



        /**Save booking
         * */
        function save(){


            if( vm.agent ){
                vm.booking.agent_id = vm.agent.id;
            }

            if( vm.client ){
                vm.booking.client_id = vm.client.id;
            }

            if( vm.booking.from_date && typeof vm.booking.from_date !== 'string' ){
                vm.booking.from_date = vm.booking.from_date.format('YYYY-MM-DD');
            }

            if( vm.id ){
                update();
            }else{
                create();
            }
        }

        /**Create new booking
         * */
        function create(){
            apiService.create( 'bookings',vm.booking )
                .then(function (response) {
                    // console.log(response);
                    SweetAlert.swal({
                        title: $filter('translate')('booking'),
                        text: $filter('translate')('created_successfully'),
                        type: 'success',
                        timer: 2000,
                        showConfirmButton: false
                    });

                    cancel();
                })
                .catch(function (response) {
                    $log.error(response);
                    $rootScope.showErrors(response,'create_error');
                });

        }


        /**Update booking
         * */
        function update(){
            apiService.update( 'bookings',vm.booking )
                .then(function (response) {
                    // $log.error(response);
                    SweetAlert.swal({
                        title: $filter('translate')('booking'),
                        text: $filter('translate')('updated_successfully'),
                        type: 'success',
                        timer: 2000,
                        showConfirmButton: false
                    });

                    cancel();
                })
                .catch(function (response) {
                    $log.error(response);
                    $rootScope.showErrors(response,'update_error')
                });

        }


        /**On cancel
         * */
        function cancel(){
            $location.path('/bookings');
        }


        /**Load booking statuses
         * */
        function loadStatuses(){
            apiService.getAll('option/booking-status')
                .then(function (response) {
                    vm.statuses = response.data;
                })
                .catch(function (response) {
                    $log.error(response);
                });
        }

        /**Load offer types
         * */
        function loadOfferTypes(){
            apiService.getAll('offer-types',{limit:-1})
                .then(function (response) {
                    vm.offerTypes = response.data;
                })
                .catch(function (response) {
                    $log.error(response);
                });
        }


        /**Load offers when offer type is selected
         * */
        vm.loadOffers = loadOffers;
        function loadOffers(){

            var id = vm.booking.offer_type_id;
            apiService.getAll('offer-type/'+id+'/offers')
                .then(function (response) {
                    vm.offers = response.data;
                })
                .catch(function (response) {
                    $log.error(response);
                });
        }


        /**Get available stops
         * */
        vm.availableStops = [];
        vm.getStops = getStops;

        function getStops(){

            var id = vm.booking.offer_id;
            var date = vm.booking.from_date ? vm.booking.from_date.format('YYYY-MM-DD'):'';
            apiService.getAll('offer/'+id+'/available-stops',{
                date:date
            })
                .then(function (response) {
                    vm.availableStops = response.data;
                })
                .catch(function (response) {
                    $log.error(response);
                });
        }
    }

})();