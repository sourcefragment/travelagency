(function () {
	'use strict';

	angular
		.module('app.bookings')
		.controller('BookingCtrl',ctrl);


	ctrl.$inject = [
		'$stateParams',
		'$location',
		'$filter',
		'SweetAlert',
		'$uibModal',
		'$rootScope',
		'apiService',
		'$log'
	];

	function ctrl(
		$stateParams,
		$location,
		$filter,
		SweetAlert,
		$uibModal,
		$rootScope,
		apiService,
		$log

	){

		var vm = this;
		vm.id  = $stateParams.id;
		vm.booking = {};
		vm.locations = [];
		vm.statuses = [];
		vm.offerTypes = [];
		vm.offers = [];
		vm.offer = false;
		vm.stop = false;
		vm.saveAndExit = false;
		vm.paymentMethods = [];
		vm.calendar = {
			events:[],
			eventClick:onEventClick
			// dayClick:onEventClick
		};

		vm.save = save;
		vm.cancel = cancel;


		init();

		/**Init view
		 * */
		function init(){

			loadStatuses();
			loadOfferTypes();
			loadPaymentMethods();

			if( vm.id ){
				get( vm.id );

			}

		}


		/**Load booking
		 * */
		function get( id ){

			apiService.get( 'bookings',id )
				.then(function (response) {
					// $log.error(response);
					vm.booking = response.data;
					vm.agent = response.data.agent;
					vm.client = response.data.client;
					vm.stop = response.data.stop;
					vm.offer = response.data.offer;

					if( vm.booking.offer_type_id )
						loadOffers();


				})
				.catch(function (response) {
					$log.error(response);
				});
		}



		/**Save booking
		 * */
		function save( mode ){

			vm.saveAndExit = mode;

			if( vm.agent ){
				vm.booking.agent_id = vm.agent.id;
			}

			if( vm.client ){
				vm.booking.client_id = vm.client.id;
			}

			if( vm.booking.date && typeof vm.booking.date !== 'string' ){
				vm.booking.date = vm.booking.date.format('YYYY-MM-DD');
			}

			if( vm.id ){
				update();
			}else{
				create();
			}
		}

		/**Create new booking
		 * */
		function create(){
			apiService.create( 'bookings',vm.booking )
				.then(function (response) {
					// console.log(response);
					SweetAlert.swal({
						title: $filter('translate')('booking'),
						text: $filter('translate')('created_successfully'),
						type: 'success',
						timer: 2000,
						showConfirmButton: false
					});

					vm.id = response.data.id;
					vm.booking = response.data;


					if( vm.saveAndExit )
						cancel();

				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'create_error');
				});

		}


		/**Update booking
		 * */
		function update(){
			apiService.update( 'bookings',vm.booking )
				.then(function (response) {
					// $log.error(response);
					SweetAlert.swal({
						title: $filter('translate')('booking'),
						text: $filter('translate')('updated_successfully'),
						type: 'success',
						timer: 2000,
						showConfirmButton: false
					});

					if( vm.saveAndExit )
						cancel();
				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'update_error')
				});

		}


		/**On cancel
		 * */
		function cancel(){
			$location.path('/bookings');
		}


		/**Load payment methods
		 * */
		function loadPaymentMethods(){
			apiService.getAll('payment-methods',{limit:-1})
				.then(function (response) {
					vm.paymentMethods = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}

		/**Load booking statuses
		 * */
		function loadStatuses(){
			apiService.getAll('option/booking-status')
				.then(function (response) {
					vm.statuses = response.data;
                })
				.catch(function (response) {
					$log.error(response);
                });
		}

		/**Load offer types
		 * */
		function loadOfferTypes(){
			apiService.getAll('offer-types',{limit:-1,sort:{name:'asc'}})
				.then(function (response) {
					vm.offerTypes = response.data;
                })
				.catch(function (response) {
					$log.error(response);
                });
		}


		/**Load offers when offer type is selected
		 * */
		vm.loadOffers = loadOffers;
		function loadOffers(){

			var id = vm.booking.offer_type_id;

			apiService.getAll('offer-type/'+id+'/offers')
				.then(function (response) {

					vm.offers = response.data;
					loadAvailableDates();

				})
				.catch(function (response) {
					$log.error(response);
                });
		}



		/**Get available stops
		 * */
		vm.availableStops = [];
		vm.loadStops = loadStops;

		function loadStops(){

			loadOfferDetails();

			var id = vm.booking.offer_id;
			var date = vm.booking.from_date ? vm.booking.from_date.format('YYYY-MM-DD'):'';
			apiService.getAll('offer/'+id+'/available-stops',{
				date:date
			})
				.then(function (response) {
					$log.info(response);
					vm.availableStops = response.data;
                })
				.catch(function (response) {
					$log.error(response);
                });
		}

		/**Load prices when offer is selected
		 * */
		vm.loadOfferDetails = loadOfferDetails;

		function loadOfferDetails() {

			var id = vm.booking.offer_id;
			apiService.get('offers',id)
				.then(function (response) {

					vm.adultPrice = response.data.adult_price;
					vm.childrenPrice = response.data.children_price;
					vm.babyDiscount = response.data.baby_discount;
					vm.seniorPrice = response.data.senior_price;

					vm.adultLunchPrice = response.data.adult_lunch_price;
					vm.childrenLunchPrice = response.data.children_lunch_price;

				})
				.catch(function (response) {
					$log.error(response);
				});
        }


        /**Auto assign client on creation
		 * */
        vm.assignClient = assignClient;
        function assignClient( id ) {

        	apiService.get('clients',id)
				.then(function (response) {
					vm.client = response.data;
                })
				.catch(function (response) {
					$log.error(response);
                });
        }


        /**Auto assign agent
		 * */
        vm.assignAgent = assignAgent;
        function assignAgent( id  ) {

        	apiService.get('agents',id)
				.then(function (response) {
					vm.agent = response.data;
                })
				.catch(function (response) {

                });
        }


        /**Load available dates
         * */
        vm.loadAvailableDates = loadAvailableDates;
        vm.availableDates = [];
        function loadAvailableDates(){

        	vm.calendar.events = [];

        	if( !vm.booking.offer_id )
        		return false;

        	apiService.getAll('offer/'+vm.booking.offer_id+'/available-dates')
		        .then(function (response) {
		        	// $log.error(response);
		        	parseEvents(response.data);
		        })
		        .catch(function (response) {
			       $log.error(response);
		        });
        }



        /**On event click
         * */
        function onEventClick( date,event,view ){

        	vm.booking.date = date.currentDate;

        	$log.error(date.routeId);

        	apiService.getAll('route/'+date.routeId+'/pickup-points',{date:date.currentDate})
		        .then(function (response) {
			        $log.error(response);
			        vm.availableStops = response.data;
		        })
		        .catch(function (response) {
			        $log.error(response);
		        });
        }




        /**Parse events
         * */
        function parseEvents( data ) {

        	$log.error(data);
        	var events = [];

	        angular.forEach(data,function (value,index) {

	        	var color = $rootScope.calendarColors[index];


	        	//Range date
	        	if( value.start_date && value.end_date ){

			        var start = new Date(value.start_date);
			        var end = new Date(value.end_date);

			        for(start; start <= end; start.setDate( start.getDate() + 1 ) ){

				        var day = $rootScope.days[start.getDay()];

				        //Repeat weekly
				        if( value.repeat_mode === 'weekly' ){
					        if( !value[day] )
						        continue;
				        }

				        events.push({
					        // id:value.id,
					        title:value.route_name,
					        start:new Date(start),
					        color:color,
					        currentDate:$filter('date')(start,'yyyy-MM-dd'),
					        routeId:value.item_id
				        });

			        }


		        }else{

	        		var start = value.start_date;
			        events.push({
				        title:value.route_name,
				        start:new Date(start),
				        color: color,
				        currentDate:$filter('date')(start,'yyyy-MM-DD'),
				        routeId:value.item_id
			        });

		        }


	        });

	        vm.calendar.events = events;

        }



        /**===========================
         * CALCULATE PRICES
         * ===========================*/
        vm.calcAdultPrice = calcAdultPrice;
		function calcAdultPrice(){
			var total =  parseInt(vm.booking.total_adults);
			var price = parseFloat(vm.offer.adult_price);
			return total * price;
        }

        vm.calcChildrenPrice = calcChildrenPrice;
        function calcChildrenPrice() {
        	var total = parseInt(vm.booking.total_children);
        	var price = parseFloat(vm.offer.children_price);
			return total * price;
        }

        vm.calcBabyDiscount = calcBabyDiscount;
        function calcBabyDiscount(){
        	var total = parseInt(vm.booking.total_babies);
        	var price = parseFloat(vm.booking.adult_price);
			return total * price;
        }

        vm.calcSeniorPrice = calcSeniorPrice;
        function calcSeniorPrice(){
        	var total = parseInt(vm.booking.total_senior);
        	var price = parseFloat(vm.offer.adultLunchPrice);
			return total * price;
        }

        vm.calcAdultLunchPrice = calcAdultLunchPrice;
        function calcAdultLunchPrice(){
        	var total = parseInt(vm.booking.total_adult_lunch);
        	var price = parseFloat(vm.offer.adultLunchPrice);
			return total * price;
        }

        vm.calcChildrenLunchPrice = calcChildrenLunchPrice;
        function calcChildrenLunchPrice(){
        	var total = parseInt(vm.booking.total_children_lunch);
        	var price = parseFloat(vm.offer.childrenLunchPrice);
			return total * price;
        }

        vm.calcTotal = calcTotal;
        function calcTotal(){

        	var adultPrice = calcAdultPrice();
        }

	}

})();