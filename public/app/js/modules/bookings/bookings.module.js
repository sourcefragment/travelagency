/**
 * Created by juan on 17/08/16.
 */

(function () {
    'use strict';

    angular.module('app.bookings',[])
        .config(config);

    function config($stateProvider) {

        $stateProvider.state('bookings', {
            url: '/bookings',
            templateUrl: 'app/js/modules/bookings/views/list.html',
            controller: 'MainBookingsCtrl',
            controllerAs: 'mbc'
        })
            .state('booking-create', {
                url: '/bookings/create',
                templateUrl: 'app/js/modules/bookings/views/edit.html',
                controller: 'BookingCtrl',
                controllerAs: 'bc'

            })
	        .state('bookings-filter', {
		        url: '/bookings/filter/:id',
		        templateUrl: 'app/js/modules/bookings/list/edit.html',
		        controller: 'MainBookingsCtrl',
		        controllerAs: 'mbc'

	        })
            .state('booking-edit', {
                url: '/bookings/edit/:id',
                templateUrl: 'app/js/modules/bookings/views/edit.html',
                controller: 'BookingCtrl',
                controllerAs: 'bc'

            })
            .state('booking-single', {
                url: '/bookings/single/:id',
                templateUrl: 'app/js/modules/bookings/views/single-details.html',
                controller: 'BookingSingleCtrl',
                controllerAs: 'bc'

            })
            .state('book', {
                url: '/bookings/book',
                templateUrl: 'app/js/modules/bookings/views/book/index.html',
                controller: 'BookCtrl',
                controllerAs: 'bc'

            })

    }


})();

