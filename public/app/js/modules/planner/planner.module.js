/**
 * Created by mario on 10/27/16.
 */

(function () {
	'use strict';

	angular.module('app.planner',[])
		.config(config);

	function config($stateProvider) {

		$stateProvider.state('planner', {
			url: '/planner',
			templateUrl: 'app/js/modules/planner/views/table.html',
			controller: 'PlannerController',
			controllerAs: 'plc'
		})

	}


})();

