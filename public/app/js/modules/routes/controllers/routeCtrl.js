/**
 * Created by mario on 11/17/16.
 */
(function () {
	'use strict';

	angular.module('app.routes')
		.controller('RoutesCtrl',routesCtrl);

	routesCtrl.$inject = [
		'$stateParams',
		'$location',
		'$uibModal',
		'toaster',
		'$rootScope',
		'SweetAlert',
		'$filter',
		'apiService',
		'NgMap',
		'config',
		'$log',
		'$timeout'
	];

	function routesCtrl(
		$stateParams,
		$location,
		$uibModal,
		toaster,
		$rootScope,
		SweetAlert,
		$filter,
	    apiService,
	    NgMap,
	    config,
	    $log,
		$timeout
	) {


		var vm = this;

		vm.id = $stateParams.id;
		vm.offerId = $stateParams.offer;
		vm.stops = [];
		vm.route = {};
		vm.path = [];
		vm.dates = [];
		vm.availableStops = [];
		vm.mapOptions = {
			center : {
				lat: 36.743196,
				lng: -4.432251
			},
			zoom : 10
		};
		vm.saveAndExit = false;
		vm.newDate = {
			item_id:vm.id,
			module:'routes'
		};

		vm.markers = [];
		vm.path = null;
		vm.points = [];

		vm.save = save;
		vm.cancel = cancel;

		init();

		/**Init controller
		 * */
		function init(){

			loadLocations();

			if( vm.id ){

                get( vm.id );
			}

			if( vm.offerId ){
				loadOffer();
			}

		}


		/**Load route
		 * */
		function get( id ){

			apiService.get( 'routes',id )
				.then(function ( response ) {
					vm.route = response.data;
					vm.offer = response.data.offer;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}

		/**Create or update a route
		 * */
		function save( mode ){

			vm.saveAndExit = mode;

			if( vm.offer ){
				vm.route.offer_id = vm.offer.id;
			}

			if( vm.city ){
				vm.route.city_id = vm.city.id;
			}


			if( vm.id ){
				update();
			}else{
				create();
			}
		}

		/**Create new route
		 * */
		function create(){

			apiService.create( 'routes',vm.route )
				.then(function (response) {
					// $log.error(response);
					SweetAlert.swal({
						title: $filter('translate')('route'),
						text: $filter('translate')('created_successfully'),
						type: 'success',
						timer: 1000,
						showConfirmButton: false
					});

					vm.id = response.data.id;
					vm.route = response.data;

					if( vm.saveAndExit )
						cancel();

				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'create_error');

				});
		}


		/**Update an existing route
		 * */
		function update(){

			apiService.update( 'routes',vm.route )
				.then(function (response) {
					//$log.error(response);
					SweetAlert.swal({
						title: $filter('translate')('route'),
						text: $filter('translate')('updated_successfully'),
						type: 'success',
						timer: 1000,
						showConfirmButton: false
					});

					if( vm.saveAndExit )
						cancel();
				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'update_error');

				});

		}


		/**Cancel form edit
		 * */
		function cancel(){
			$location.path('/routes');
		}


		/**=====================
		 * ====================*/

		/**Load available locations
		 * */
		function loadLocations(){
			apiService.getAll('locations',{limit:-1})
				.then(function (response) {
					vm.locations = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}





		/**Set offer
		 * */
		function loadOffer(){

			apiService.get('offers',vm.offerId)
				.then(function (response) {
					vm.offer = response.data;
                })
				.catch(function (response) {
					$log.error(response);
                });
		}


		/**=====================
		 * ZONES
		 * =====================
		 * */
		vm.zones = [];
		vm.new = {};
		vm.availableZones = [];
		vm.getAvailableZones = getAvailableZones;
		vm.loadZones = loadZones;

		/**Load route zones
		 * */
		function loadZones(){

			getAvailableZones();
			getZones();

		}

		/**Get route zones
		 * */
		vm.getZones = getZones;
		function getZones() {
			apiService.getAll('route/'+vm.id+'/zones')
				.then(function (response) {
					vm.zones = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}

		/**Load available zones
		 * */
		function getAvailableZones(){
			apiService.getAll('route/'+vm.id+'/available-zones')
				.then(function (response) {
					vm.availableZones = response.data;
                })
				.catch(function (response) {
					$log.error(response);
                });
		}


		/**Add zone to route
		 * */
		vm.addZone = addZone;

		function addZone(){

			var zone = {
				route_id:vm.id,
				zone_id:vm.new.zone,
				supplement:vm.new.supplement,
				time_offset:vm.new.time_offset,
				order:vm.new.order
			};


			apiService.create('route/'+vm.id+'/zone',zone)
				.then(function(response){
					$log.info(response);
					loadZones();
					vm.new.supplement = '';
					vm.new.time_offset = '';
				})
				.catch(function (response) {
					$rootScope.showErrors(response,'error');
                });
		}


		/**Edit zone
		 * */
		vm.editZone = editZone;
		function editZone( item ){
			item.editing = true;
		}


		/**Save zone
		 * */
		vm.saveZone = saveZone;
		function saveZone( item ){

			apiService.update('route-zones',item)
				.then(function (response) {
					getZones();
				})
				.catch(function (response) {
					$rootScope.showErrors(response);
				})
		}

		/**Delete route zone
		 * */
		vm.deleteZone = deleteZone;
		function deleteZone( id ){

			SweetAlert.swal({
					title: $filter('translate')('delete_zone'),
					text: $filter('translate')('delete_zone_text'),
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: $filter('translate')('yes_delete'),
					cancelButtonText: $filter('translate')('no_cancel'),
					closeOnConfirm: false
				},
				function (isConfirm) {
					if (isConfirm) {

						apiService.delete('route-zones',id)
							.then(function (response) {


								SweetAlert.swal({
									title: $filter('translate')('deleted'),
									text: $filter('translate')('deleted_successfully'),
									type: "success",
									timer: 1000,
									showConfirmButton: false
								});

								loadZones();
								vm.stops = [];

							})
							.catch(function (response) {
								$log.error(response);
							});


					}
				});



		}


		/**Sort zones
		 * */
		vm.updateZonesOrder = updateZonesOrder;
		function updateZonesOrder(e,ui) {

			var model = ui.item.sortable.model;
			var index = ui.item.sortable.dropindex;

			apiService.getAll('route/zone/'+model.id+'/reorder',{
				index:index+1
			})
				.then(function (response) {
					loadZones();
					// $log.error(response);
				})
				.catch(function (response) {

				});
		}


		/**Toggle zone status
		 * */
		vm.toggleZone = toggleZone;
		function toggleZone( item ){

			apiService.getAll('route-zone/'+item.id+'/toggle')
				.then(function (response) {
					item.status = response.data.status;
					$rootScope.saved();
				})
				.catch(function (response) {
					$log.error(response);
				});
		}

		/**=========================
		 * STOPS
		 * ========================*/

		/**Load route assigned stops
		 * */
		vm.loadRouteStops = loadRouteStops;
		function loadRouteStops( item ){

			// $log.error(item);

			apiService.getAll('route/'+vm.id+'/zone-stops',{
				zone_id:item.zone_id
			})
				.then(function ( response ) {
					// $log.error(response);
					vm.stops = response.data;
				})
				.catch(function ( response ) {
					$log.error( response );
				});

		}


		/**Toggle route stops
		 * @param id int, route stop id reference
		 * */
		vm.toggleRouteStop = toggleRouteStop;
		function toggleRouteStop( item ) {

			if( item.status ){

				apiService.update('route-stops',item)
					.then(function (response) {
						loadRouteStops(item);

					})
					.catch(function (response) {
						$log.error(response);
					});
			}else{
				apiService.update('route-stops',item)
					.then(function (response) {
						loadRouteStops(item);
					})
					.catch(function (response) {
						$log.error(response);
					});
			}

		}


		/**Update stops order
		 * */
		vm.updateStopsOrder = updateStopsOrder;
		function updateStopsOrder(e,ui) {


			var model = ui.item.sortable.model;
			var index = ui.item.sortable.dropindex;
			var id = vm.stops[index].id;

			apiService.getAll('route/stop/'+model.id+'/reorder',{
				index:id
			})
				.then(function (response) {
					loadRouteStops(model);
					// $log.error(response);
				})
				.catch(function (response) {
					$log.error(response);
				});

		}


		/**====================
		 * DATES
		 * ====================*/

		/**Load dates
		 * */
		vm.loadDates = loadDates;
		function loadDates() {

			apiService.getAll('route/'+vm.id+'/available-dates')
				.then(function (response) {
					// $log.info(response);
					vm.dates = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}

		/**Delete route availability date
		 * */
		vm.deleteDate = deleteDate;
		function deleteDate( id ){

			SweetAlert.swal({
					title: $filter('translate')('delete_date'),
					text: $filter('translate')('delete_date_text'),
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: $filter('translate')('yes_delete'),
					cancelButtonText: $filter('translate')('no_cancel'),
					closeOnConfirm: false,
				},
				function (isConfirm) {
					if (isConfirm) {

						apiService.delete( 'events',id )
							.then(function (data) {
								// $log.error(data);
								SweetAlert.swal({
									title: $filter('translate')('date'),
									text: $filter('translate')('deleted_successfully'),
									type: "success",
									timer: 1000,
									showConfirmButton: false
								});
								loadDates();
							}).catch(function () {
							$log.error(data);
						});
					}
				});

		}


		/**Render map
		 * */
		function renderMap(){

			NgMap.getMap()
				.then(function (map) {

					var bound = new google.maps.LatLngBounds();

					//Delete any previous markers and redraw
					angular.forEach(vm.markers,function (item) {
						item.setMap(null);
					});

					//Delete path
					if( vm.path ){
						vm.path.setMap(null);
						vm.points = [];
					}


					$timeout(function () {

						angular.forEach(vm.stops,function (item,key) {

							var latLng = new google.maps.LatLng(parseFloat(item.stop.lat),parseFloat(item.stop.lng));
							var marker = new google.maps.Marker({
								position:latLng,
							});
							marker.setMap(map);
							bound.extend(latLng);
							vm.markers.push(marker);
							vm.points.push(latLng);
						});

						//Get bounds center

						map.fitBounds(bound);
						map.setCenter(bound.getCenter());
						// map.setZoom(16);

						//Create path
						vm.path = new google.maps.Polyline({
							path: vm.points,
							strokeColor: '#16987e',
							strokeOpacity: 0.8,
							strokeWeight: 3
						});
						vm.path.setMap(map);


						//Trigger resize to make map visible
						google.maps.event.trigger(map,'resize');



					},1000);


				})
				.catch(function (map) {
					$log.error(map);
				});
		}


	}

})();