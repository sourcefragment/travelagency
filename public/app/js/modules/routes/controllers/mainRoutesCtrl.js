/**
 * Created by mario on 11/17/16.
 */
(function () {
	'use strict';

	angular.module('app.routes')
		.controller('MainRoutesCtrl',mainRoutesCtrl);

	mainRoutesCtrl.$inject = [
		'apiService',
		'$filter',
		'SweetAlert',
		'NgTableParams',
		'$rootScope',
		'$log'
	];

	function mainRoutesCtrl(
		apiService,
	    $filter,
	    SweetAlert,
	    NgTableParams,
	    $rootScope,
	    $log
	){

		var vm = this;

		vm.routes = [];
		vm.tableParams = {};

		vm.delete = deleteRoute;

		init();

		function init(){

			initTable();

		}

		/**Init table
		 * */
		function initTable(){

			vm.tableParams = new NgTableParams({},{
				getData : function( params ){
					return getAll( params );
				}
			});
		}

		/**Get all routes
		 * */
		function getAll( params ){

			var p = $rootScope.getParams( params );
			apiService.getAll( 'routes',p )
				.then(function (response) {
					// $log.error(response);
					vm.routes = response.data.data;
					params.total( response.data.total);
					params.pages = params.generatePagesArray();
					params.current = response.data.current_page;
					return response;
				})
				.catch(function (response) {
					$log.error(response);
				})
		}


		/**Delete selected route
		 *@param id int */
		function deleteRoute( id ){

			SweetAlert.swal({
					title: $filter('translate')('delete_route'),
					text: $filter('translate')('route_delete_text'),
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: $filter('translate')('yes_delete'),
					cancelButtonText: $filter('translate')('no_cancel'),
					closeOnConfirm: false,
				},
				function (isConfirm) {
					if (isConfirm) {

						apiService.delete( 'routes',id )
							.then(function (data) {
								$log.error(data);
								SweetAlert.swal({
									title: $filter('translate')('deleted'),
									text: $filter('translate')('deleted_successfully'),
									type: "success",
									timer: 1000,
									showConfirmButton: false
								});
								initTable();
							}).catch(function () {
							$log.error(data);
						});
					}
				});

		}



		/**Clone route
		 * @param id int, route id ref
		 * */
		vm.cloneRoute = cloneRoute;
		function cloneRoute( id ){

            SweetAlert.swal({
                    title: $filter('translate')('clone_route'),
                    text: $filter('translate')('clone_route_text'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $filter('translate')('yes'),
                    cancelButtonText: $filter('translate')('cancel'),
                    closeOnConfirm: false,
                },
                function (isConfirm) {
                    if (isConfirm) {

                        apiService.getAll( 'route/'+id+'/clone')
                            .then(function (response) {

	                            $log.error(response);

                                SweetAlert.swal({
                                    title: $filter('translate')('route'),
                                    text: $filter('translate')('cloned_successfully'),
                                    type: "success",
                                    timer: 1000,
                                    showConfirmButton: false
                                });

                                initTable();

                            }).catch(function () {
                                $log.error(data);
                            });
                    }
                });

		}

	}

})();