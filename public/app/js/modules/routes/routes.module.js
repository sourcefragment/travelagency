/**
 * Created by juan on 17/08/16.
 */

(function () {
    'use strict';

	angular.module('app.routes',[])
		.config(config);

	function config($stateProvider) {


		$stateProvider.state('routes', {
			url: '/routes',
			templateUrl: 'app/js/modules/routes/views/list.html',
			controller: 'MainRoutesCtrl',
			controllerAs: 'mrc'

		})
			.state('route-create', {
				url: '/routes/create',
				params:{
					offer:''
				},
				templateUrl: 'app/js/modules/routes/views/edit.html',
				controller: 'RoutesCtrl',
				controllerAs: 'rc'

			})
			.state('route-edit', {
				url: '/routes/edit/:id',
				templateUrl: 'app/js/modules/routes/views/edit.html',
				controller: 'RoutesCtrl',
				controllerAs: 'rc'

			});



	}

})();

