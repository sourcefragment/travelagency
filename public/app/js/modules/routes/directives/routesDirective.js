/**
 * Created by juan on 18/08/16.
 */
(function () {

    angular.module('app.routes')
        .directive('createRouteBtn', createRouteBtn);

    function createRouteBtn() {
        return {
            template: '<button type="button" ng-click="routesc.getFormRoute()" class="btn btn-outline btn-primary">'
                            +'[[ "new_route" | translate]]'
                     +'</button>',
            controller: 'RoutesController',
            controllerAs: 'routesc',
        };
    }
})();