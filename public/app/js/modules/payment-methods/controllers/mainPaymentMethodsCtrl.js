(function () {

    'use strict';

    angular
        .module('app.payment-methods')
        .controller('MainPaymentMethodsCtrl', mainPaymentMethodsController);

    mainPaymentMethodsController.$inject = [
        '$filter',
        'SweetAlert',
        'apiService',
        '$stateParams',
	    '$location',
	    'NgTableParams',
	    '$rootScope'
    ];

    function mainPaymentMethodsController(
        $filter,
        SweetAlert,
        apiService,
        $stateParams,
        $location,
        NgTableParams,
        $rootScope

        ){

        var vm = this;
	    vm.id = $stateParams.id;
        vm.methods = {};
        vm.tableParams = {};

        vm.getAll = getAll;
        vm.deletePaymentMethod = deletePaymentMethod;


        activate();


        /**Init controller
         * */
        function activate() {
            initTable();
        }


        /**Init main table structure
         * */
        function initTable() {
			vm.tableParams = new NgTableParams({},{
				getData : function( params ){
					return getAll( params );
				}
			})
        }


        /**Load paymentMethod
         * */
        function getAll( params ) {

        	var p = $rootScope.getParams(params);

            apiService.getAll( 'payment-methods',p )
                .then(function ( response ) {
                	// console.log(response);
                	vm.methods = response.data.data;
	                params.total( response.data.total);
	                params.pages = params.generatePagesArray();
	                params.current = response.data.current_page;
	                return response;

                }).catch(function ( response ) {
                    console.error( response );
            });
        }


        /**Delete paymentMethod
         *@param id, the paymentMethod id
         * @return promise
         */
        function deletePaymentMethod(id) {

            SweetAlert.swal({
                    title: $filter('translate')('delete_payment_method'),
                    text: $filter('translate')('delete_payment_method_text'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $filter('translate')('yes_delete'),
                    cancelButtonText: $filter('translate')('no_cancel'),
                    closeOnConfirm: false,
                },
                function (isConfirm) {

                    if (isConfirm) {

                        apiService.delete( 'payment-methods',id )
                            .then(function () {
                                SweetAlert.swal({
                                    title: $filter('translate')('payment_method'),
                                    text: $filter('translate')('deleted_successfully'),
                                    type: "success",
                                    timer: 2000,
                                    showConfirmButton: false
                                });
								initTable();
                            }).catch(function ( response ) {
                            console.error( response );
                        });
                    }

                });
        }

    }




})();











