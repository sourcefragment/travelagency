(function () {

    'use strict';

    angular
        .module('app.payment-methods')
        .controller('PaymentMethodsCtrl', paymentMethodsController);


    paymentMethodsController.$inject = [
    	'$rootScope',
        '$stateParams',
        '$location',
        'apiService',
	    '$filter',
	    'SweetAlert',
	    '$log'
    ];

    function paymentMethodsController(
    	$rootScope,
        $stateParams,
        $location,
	    apiService,
        $filter,
        SweetAlert,
        $log
    ){

    	var vm = this;
        vm.method = false;
        vm.id = $stateParams.id;
        vm.saveAndExit = false;

        vm.get = get;
        vm.save = save;
	    vm.cancel = cancel;



        activate();

        function activate() {

            if( vm.id ){
	            get( vm.id );
            }

        }

        /**Load user
         * */
        function get(id) {

            apiService.get( 'payment-methods',id )
                .then(function (response) {
                    vm.method = response.data;
                }).catch(function (response) {
                    $log.error(response);
                });
        }

        /**Save paymentMethod modal
         * */
        function save( mode ) {

        	vm.saveAndExit = mode;

        	if( vm.method.id ){
        		update();
	        }else{
	        	create();
	        }

        }

        /**Save paymentMethod
         * */
        function create() {

            apiService.create( 'payment-methods',vm.method )
                .then(function ( response ) {

	                SweetAlert.swal({
		                title: $filter('translate')('payment_method'),
		                text: $filter('translate')('created_successfully'),
		                type: 'success',
		                timer: 1000,
		                showConfirmButton: false
	                });

	                vm.id = response.data.id;
	                vm.method = response.data;

	                if( vm.saveAndExit )
                	    cancel();

                }).catch( function( response ) {
                    $log.error(response);
	                $rootScope.showErrors(response,'create_error');
                });

        }


        /**Upodate payment method
         * */
        function update(){

	        apiService.update('payment-methods',vm.method )
		        .then(function ( response ) {

			        SweetAlert.swal({
				        title: $filter('translate')('payment_method'),
				        text: $filter('translate')('updated_successfully'),
				        type: 'success',
				        timer: 1000,
				        showConfirmButton: false
			        });

			        if( vm.saveAndExit )
			            cancel();

		        }).catch( function( response ) {
			        $log.error(response);
		            $rootScope.showErrors(response,'update_error');
	        });
        }


        /**Cancel form
         * */
        function cancel(){
        	$location.path('/payment-methods');
        }

    }

})();
