/**
 * Created by juan on 17/08/16.
 */

(function () {
    'use strict';

    angular.module('app.payment-methods',[])
        .config(config);

    function config($stateProvider) {

        $stateProvider.state('payment-methods', {
            url: '/payment-methods',
            templateUrl: 'app/js/modules/payment-methods/views/list.html',
            controller: 'MainPaymentMethodsCtrl',
            controllerAs: 'mpmc'
        })
            .state('payment-method-create', {
                url: '/payment-methods/create',
                templateUrl: 'app/js/modules/payment-methods/views/edit.html',
                controller: 'PaymentMethodsCtrl',
                controllerAs: 'pmc'

            })
            .state('payment-method-edit', {
                url: '/payment-methods/edit/:id',
                templateUrl: 'app/js/modules/payment-methods/views/edit.html',
                controller: 'PaymentMethodsCtrl',
                controllerAs: 'pmc'
            })
    }


})();

