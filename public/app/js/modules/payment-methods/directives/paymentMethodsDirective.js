/**
 * Created by juan on 18/08/16.
 */
(function () {

    angular.module('app.payment-methods')
        .directive('newPaymentMethodBtn', newPaymentMethodBtn)
        .directive('createPaymentMethodBtnLong', createPaymentMethodBtnLong);

    function newPaymentMethodBtn() {
        return {
            templateUrl:'app/js/sections/paymentMethods/views/new-paymentMethod-btn.html',
            controller: 'PaymentMethodsController',
            controllerAs: '$ctrl',
        };
    }

    function createPaymentMethodBtnLong() {
        return {
            template: '<button type="button" class="btn btn-primary btn-lg" ui-sref="payment-method-create">'
                        + '<i class="fa fa-plus-circle"></i> [[ "new_payment_method" | translate ]]'
                    + '</button>'
        };
    }

})();