/**
 * Created by mario on 3/22/17.
 */
(function () {

    'use strict';

    angular.module('app.stops')
        .controller('StopContentCtrl',ctrl);


    ctrl.$inject = [
        '$rootScope',
        '$scope',
        'apiService',
        '$log',
        '$uibModal'
    ];

    function ctrl(
        $rootScope,
        $scope,
        apiService,
        $log,
        $uibModal
    ){

        $scope.openModal = function () {

            var model = $scope.ngModel;
            var id = $scope.id;
            var module = $scope.module;

            var instance = $uibModal.open({
                templateUrl : 'app/js/modules/stops/views/content-modal.html',
                size:'lg',
                controller :['$scope',function ($scope) {

                    $scope.content = model ? model : {};
                    $scope.languages = [];


                    loadLanguages();

                    /**Load client tyes
                     * */
                    function loadLanguages(){
                        apiService.getAll('languages',{limit:-1})
                            .then(function (response) {
                                $scope.languages = response.data;
                            })
                            .catch(function (response) {
                                $log.error(response);
                            });
                    }

                    /**On modal save
                     * */
                    $scope.saveModal = function () {

                        $scope.content.module = module;
                        $scope.content.object_id = id;

                        if( $scope.content.id ){

                            apiService.update('contents',$scope.content)
                                .then(function (response) {
                                    onClose();
                                    $scope.$close();
                                })
                                .catch(function (response) {
                                    $log.error(response);
                                    $rootScope.showErrors(response,'create_error');
                                });

                        }else{

                            apiService.create('contents',$scope.content)
                                .then(function (response) {
                                    onClose();
                                    $scope.$close();
                                })
                                .catch(function (response) {
                                    $log.error(response);
                                    $rootScope.showErrors(response,'create_error');
                                });

                        }





                    }

                }]
            });
        };

        function onClose(){

            if( typeof  $scope.onSuccess === 'function'){
                $scope.onSuccess();
            }
        }

    }


})();