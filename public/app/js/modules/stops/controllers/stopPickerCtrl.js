/**
 * Created by mario on 2/21/17.
 */
(function () {

	'use strict';

	angular.module('app.stops')
		.controller('StopPickerCtrl',ctrl);


	ctrl.$inject = [
		'$scope',
		'$uibModal',
		'$rootScope',
		'apiService',
		'NgTableParams',
		'$log'
	];

	function ctrl(
		$scope,
		$uibModal,
		$rootScope,
		apiService,
		NgTableParams,
	    $log
	) {


		/**Open modal
		 * */
		$scope.openModal = function () {

			var modalInstance = $uibModal.open({
				templateUrl : 'app/js/modules/stops/views/search-modal.html',
				size : 'lg',
				controller : ['$scope',
					function ( $scope ) {

						$scope.saveModal = saveModal;
						$scope.stops = [];
						$scope.stop = {};

						$scope.tableParams = new NgTableParams({},{
							getData : function ( params ) {
								return loadStops( params );
							}
						});

						/**Load stops
						 * */
						function loadStops( params ){
							var p = $rootScope.getParams(params);
							apiService.getAll('stops',p)
								.then(function (response) {
									$scope.stops = response.data.data;
									params.total( response.data.total);
									params.pages = params.generatePagesArray();
									params.current = response.data.current_page;
									return response;
								})
								.catch(function (response) {
									$log.error(response);
								});
						}


						/**On modal save
						 * */
						function saveModal(){

							angular.forEach( $scope.stops,function(val){

								if( val.checked ){
									$scope.stop = val;
								}
							});
							onSave( $scope.stop );
							$scope.$close();
						}
					}]
			});

		};


		/**Assign the selected stop id
		 * */
		function onSave( stop ) {

			if( $scope.ngModel && typeof $scope.ngModel != 'undefined'){
				$scope.ngModel = stop;
			}

			if( $scope.onSave && typeof $scope.onSave == 'function' ){
				$scope.onSave()(stop);
			}
		}
	}

})();