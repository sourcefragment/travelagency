/**
 * Created by mario on 12/7/16.
 */
(function () {

	'use strict';

	angular
		.module('app.stops')
		.controller('MainStopsCtrl', stopsCtrl);

	stopsCtrl.$inject = [
		'$filter',
		'SweetAlert',
		'NgTableParams',
		'$rootScope',
		'apiService',
		'$log'
	];

	function stopsCtrl(
		$filter,
		SweetAlert,
		NgTableParams,
		$rootScope,
	    apiService,
	    $log
	) {

		var vm = this;

		vm.stops = {};
		vm.tableParams = {};

		vm.delete = deleteItem;

		activate();


		/**Init controller
		 * */
		function activate() {

			initTable();

		}


		/**Init main table structure
		 * */
		function initTable() {

			vm.tableParams = new NgTableParams({},{
				getData : function( params ){
					return getAll(params);
				}
			})
		}


		/**Load stop
		 * */
		function getAll( params ) {

			var p = $rootScope.getParams(params);

			apiService.getAll('stops', p )
				.then(function ( response ) {
					// $log.log(response);
					vm.stops = response.data.data;
					params.total( response.data.total);
					params.pages = params.generatePagesArray();
					params.current = response.data.current_page;
					return response;
				}).catch(function ( response ) {
				$log.error( response );
			});
		}


		/**Delete stop by id
		 * */
		function deleteItem( id ){


			SweetAlert.swal({
					title: $filter('translate')('delete_stop'),
					text: $filter('translate')('delete_stop_text'),
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: $filter('translate')('yes_delete'),
					cancelButtonText: $filter('translate')('no_cancel'),
					closeOnConfirm: false
				},
				function (isConfirm) {

					if (isConfirm) {

						apiService.delete( 'stops',id )
							.then(function ( response ) {

								SweetAlert.swal({
									title: $filter('translate')('stop'),
									text: $filter('translate')('deleted_successfully'),
									type: "success",
									timer: 1000,
									showConfirmButton: false
								});
								initTable();

							})
							.catch( function ( response ) {
								$log.log(response);
								$rootScope.showErrors(response,'delete_error');
							});

					}

				});


		}


		/**Duplicate stop
		 * */
		vm.duplicateStop = duplicateStop;
		function duplicateStop( id ){

			SweetAlert.swal({
					title: $filter('translate')('duplicate_stop'),
					text: $filter('translate')('duplicate_stop_text'),
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: $filter('translate')('yes'),
					cancelButtonText: $filter('translate')('cancel'),
					closeOnConfirm: false,
				},
				function (isConfirm) {

					if (isConfirm) {

						apiService.create( 'stop/clone/'+id )
							.then(function ( response ) {

								$log.error(response);

								SweetAlert.swal({
									title: $filter('translate')('stop'),
									text: $filter('translate')('duplicated'),
									type: "success",
									timer: 1000,
									showConfirmButton: false
								});
								initTable();

							})
							.catch( function ( response ) {
								$log.log(response);
								$rootScope.showErrors(response,'duplicate_error');
							});

					}

				});

		}



	}

})();