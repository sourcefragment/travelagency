/**
 * Created by mario on 12/7/16. sss
 */
(function () {

	'use strict';

	angular.module('app.stops')
		.controller('StopCtrl', ctrl);


	ctrl.$inject = [
		'$rootScope',
		'$stateParams',
		'$location',
		'apiService',
		'SweetAlert',
		'$filter',
		'$log'

	];

	function ctrl(
		$rootScope,
		$stateParams,
		$location,
		apiService,
		SweetAlert,
		$filter,
	    $log
	){

		var vm = this;
		vm.stop = {};
		vm.stops = [];
		vm.id = $stateParams.id;
		vm.saveAndExit = false;
		vm.zones = [];

		vm.get = get;
		vm.save = save;
		vm.cancel = cancel;

		activate();

		/**Init function
		 * */
		function activate() {


			loadZones();

			if($stateParams.id){
				get($stateParams.id);
			}

		}



		/**Load available zones
		 * */
		function loadZones(){

			apiService.getAll('zones',{limit:-1})
				.then(function (response) {
					vm.zones = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}

		/**Get stop by id ref
		 * @param id int
		 * */
		function get( id ) {

			apiService.get( 'stops',id )
				.then(function (response) {
					vm.stop = response.data;
					vm.location = {
						lat : response.data.lat,
						lng : response.data.lng,
						heading : response.data.heading,
						pitch : response.data.pitch,
						zoom : response.data.zoom
					};
				}).catch(function (response) {
				$log.error(response);
			});

		}



		/**Save stop
		 * */
		function save( mode ) {

			vm.saveAndExit = mode;

			// $log.error(vm.stop);
			//Set location
			if( vm.location ){
				vm.stop.lat = vm.location.lat;
				vm.stop.lng = vm.location.lng;
				vm.stop.heading = vm.location.heading;
				vm.stop.pitch = vm.location.pitch;
				vm.stop.zoom = vm.location.zoom;
			}

			if( vm.id ){
				update();
			}else{
				create();
			}

		}


		/**Create stop
		 * */
		function create(){

			apiService.create( 'stops',vm.stop )
				.then(function ( response ) {

					SweetAlert.swal({
						title: $filter('translate')('stop'),
						text: $filter('translate')('created_successfully'),
						type: 'success',
						timer: 1000,
						showConfirmButton: false
					});

					vm.id = response.data.id;
					vm.stop = response.data;
					if( vm.saveAndExit )
						cancel();

				})
				.catch(function ( response ) {
					$log.error(response);
					$rootScope.showErrors(response,'create_error');
				});

		}

		/**Update stop
		 * */
		function update( ){

			apiService.update( 'stops',vm.stop )
				.then(function( response ){

					SweetAlert.swal({
						title: $filter('translate')('stop'),
						text: $filter('translate')('updated_successfully'),
						type: 'success',
						timer: 1000,
						showConfirmButton: false
					});

					if( vm.saveAndExit )
						cancel();

				})
				.catch( function ( response ) {
					$rootScope.showErrors(response,'update_error');
				});

		}

		/**Cancel form
		 * */
		function cancel(){
			$location.path('/stops');
		}


		/**==============
		 * CONTENT
		 * ====================
		 * */
		vm.content = [];
		vm.loadContent = loadContent;
		vm.deleteContent = deleteContent;


		/**Load stop content
		 * */
		function loadContent(){

			apiService.getAll('stop/'+vm.id+'/content')
				.then(function (response) {
					vm.content = response.data;
                })
				.catch(function (response) {
					$log.error(response);
                });

		}

		/**Delete stop content
		 * */
		function deleteContent(id){

			SweetAlert.swal({
					title: $filter('translate')('delete_content'),
					text: $filter('translate')('delete_content_text'),
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: $filter('translate')('yes_delete'),
					cancelButtonText: $filter('translate')('no_cancel'),
					closeOnConfirm: false
				},
				function (isConfirm) {

					if (isConfirm) {

						apiService.delete( 'contents',id )
							.then(function ( response ) {

								SweetAlert.swal({
									title: $filter('translate')('content'),
									text: $filter('translate')('deleted_successfully'),
									type: "success",
									timer: 1000,
									showConfirmButton: false
								});
								loadContent();

							})
							.catch( function ( response ) {
								$log.log(response);
								$rootScope.showErrors(response,'delete_error');
							});

					}

				});


		}





	}

})();