/**
 * Created by mario on 12/14/16.
 */
(function () {
	'use strict';

	angular.module('app.stops')
		.directive('stopPicker',stopPicker)
		.directive('stopContentModal',stopContentModal);


	function stopPicker(){
		return {
			restrict : 'A',
			scope : {
				ngModel : '=ngModel',
				onSave : '&onSave',
				ref : '@ref'
			},
			link : function( scope,element,attrs){
				element.click( scope.openModal );
			},
			controller : 'StopPickerCtrl'
		};
	}

	function stopContentModal(){
        return {
            restrict:'A',
            scope:{
                ngModel:'=ngModel',
                onSuccess:'&onSuccess',
				id:'@id',
				module:'@module'

            },
            link:function(scope,element,attrs){
                element.click( scope.openModal );
            },
            controller: 'StopContentCtrl',
            controllerAs:'$ctrl'
        }
	}


})();