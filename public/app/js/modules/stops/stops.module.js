/**
 * Created by mario on 12/7/16.
 */
(function () {
	'use strict';

	angular.module('app.stops',[])
		.config(config);

	function config($stateProvider) {

		$stateProvider.state('stops', {
			url: '/stops',
			templateUrl: 'app/js/modules/stops/views/list.html',
			controller: 'MainStopsCtrl',
			controllerAs: 'msc'
		})
			.state('stop-create', {
				url: '/stops/create',
				templateUrl: 'app/js/modules/stops/views/edit.html',
				controller: 'StopCtrl',
				controllerAs: 'sc'

			})
			.state('stop-edit', {
				url: '/stops/edit/:id',
				templateUrl: 'app/js/modules/stops/views/edit.html',
				controller: 'StopCtrl',
				controllerAs: 'sc'

			});

	}


})();