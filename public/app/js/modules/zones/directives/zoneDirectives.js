/**
 * Created by juan on 18/08/16.
 */
(function () {

    angular.module('app.zones')
        .directive('newZoneBtn', newZoneBtn)
        .directive('createZoneBtnLong', createZoneBtnLong);

    function newZoneBtn() {
        return {
            templateUrl:'app/js/sections/zones/views/new-zone-btn.html',
            controller: 'ZonesController',
            controllerAs: '$ctrl',
        };
    }

    function createZoneBtnLong() {
        return {
            template: '<button type="button" class="btn btn-primary btn-lg" ui-sref="zone-create">'
                         + '<i class="fa fa-plus-circle"></i> [[ "new_zone" | translate ]]'
                    + '</button>'
        };
    }

})();