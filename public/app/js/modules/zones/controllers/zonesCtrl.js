(function () {

    'use strict';

    angular
        .module('app.zones')
        .controller('ZoneCtrl', ctrl);


    ctrl.$inject = [
        '$stateParams',
        '$location',
        'apiService',
	    'SweetAlert',
	    '$filter',
	    '$log',
	    'NgMap',
	    '$rootScope'

    ];

    function ctrl(
        $stateParams,
        $location,
	    apiService,
        SweetAlert,
        $filter,
        $log,
        NgMap,
        $rootScope
    ){

    	var vm = this;
	    vm.id = $stateParams.id;
	    vm.zone = false;
	    vm.stops = [];
	    vm.location = {};
	    vm.zoneStops = [];
		vm.selectedStop = {};

	    vm.spinOptions = {
		    min: 10,
		    max:10000,
		    step: 10
	    };

	    //Zone circle area
	    vm.circle = false;
	    vm.map = false;
	    vm.saveAndExit = false;



	    vm.save = save;
		vm.cancel = cancel;
	    vm.setRadius = setRadius;

	    //Stops
	    vm.loadStops = loadStops;
	    vm.deleteStop = deleteStop;
	    vm.saveStop = saveStop;


        activate();

	    /**Init function
	     * */
        function activate() {

        	loadStops();

		    if( vm.id ){
	            get( vm.id );
            }

        }


        /**Get zone by id ref
         * @param id int
         * */
        function get( id ) {

            apiService.get( 'zones',id )
                .then(function (response) {
                    vm.zone = response.data;
	                vm.location.lat = response.data.lat;
	                vm.location.lng = response.data.lng;
	                vm.city = response.data.city;

                }).catch(function (response) {
                    $log.error(response);
                });
        }


        /**Set zone radius
         * */
        function setRadius( radius ){

        	if( vm.circle ){
		        vm.circle.setRadius( parseInt(radius) );
	        }
        }



        /**Save zone
         * */
        function save( mode ) {

			vm.saveAndExit = mode;

        	//Set location if present
	        if( vm.location ){
	        	vm.zone.lat = vm.location.lat;
		        vm.zone.lng = vm.location.lng;
	        }

	        //Set city
	        if( vm.city ){
	        	vm.zone.city_id = vm.city.id;
	        }


        	if( vm.id ){
        		update();
	        }else{
	        	create();
	        }

        }


        /**Create zone
         * */
        function create(){

	        apiService.create( 'zones',vm.zone )
		        .then(function ( response ) {

			        SweetAlert.swal({
				        title: $filter('translate')('zone'),
				        text: $filter('translate')('created_successfully'),
				        type: 'success',
				        timer: 1000,
				        showConfirmButton: false
			        });

			        vm.id = response.data.id;
			        vm.zone = response.data;
			        
			        if( vm.saveAndExit )
			            cancel();

		        })
		        .catch(function ( response ) {
					// $log.error(response );
			        $rootScope.showErrors(response,'create_error');
		        });

        }

        /**Update zone
         * */
        function update( ){

	        apiService.update( 'zones',vm.zone )
		        .then(function( response ){

			        SweetAlert.swal({
				        title: $filter('translate')('zone'),
				        text: $filter('translate')('updated_successfully'),
				        type: 'success',
				        timer: 1000,
				        showConfirmButton: false
			        });

			        if( vm.saveAndExit )
			            cancel();

		        })
		        .catch( function ( response ) {
			        // $log.error( response );
			        $rootScope.showErrors(response,'update_error');
		        });

        }

        /**Cancel form
         * */
        function cancel(){
        	$location.path('/zones');
        }




        /**==============
         * STOPS
         * ===============
         * */


        /**Load stops
         * */
        function loadStops(){
			apiService.getAll('zone/'+vm.id+'/stops')
				.then(function (response) {
					$log.error(response);
					vm.zoneStops = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});
        }


        /**Save seletecd stop
         * */
        function saveStop( stop ){

        	var newStop = {
        		zone_id : vm.id,
		        stop_id : stop.id
	        };

        	apiService.create('zone-stops',newStop)
		        .then(function (response) {
					$log.error(response);
			        loadStops();
		        })
		        .catch(function (response) {
			        // $log.error(response);
			        $rootScope.showErrors(response,'error');
		        });
        }


        /**Delete stop related to zone
         * @param id int
         * */
        function deleteStop( id ){

	        SweetAlert.swal({
			        title: $filter('translate')('delete_stop'),
			        text: $filter('translate')('delete_zone_stop_text'),
			        type: "warning",
			        showCancelButton: true,
			        confirmButtonColor: "#DD6B55",
			        confirmButtonText: $filter('translate')('yes_delete'),
			        cancelButtonText: $filter('translate')('no_cancel'),
			        closeOnConfirm: false,
		        },
		        function (isConfirm) {

			        if (isConfirm) {

				        apiService.delete('zone-stops',id)
					        .then(function (response) {
						        SweetAlert.swal({
							        title: $filter('translate')('stop'),
							        text: $filter('translate')('deleted_successfully'),
							        type: 'success',
							        timer: 2000,
							        showConfirmButton: false
						        });
						        loadStops();
					        })
					        .catch(function (response) {
						        $log.error(response);
					        });

			        }

		        });


        }



    }

})();
