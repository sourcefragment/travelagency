(function () {

    'use strict';

    angular
        .module('app.zones')
        .controller('MainZonesCtrl', ctrl);

    ctrl.$inject = [
        '$filter',
        'SweetAlert',
        'apiService',
	    'NgTableParams',
	    '$rootScope',
	    '$log'
    ];

    function ctrl(
        $filter,
        SweetAlert,
        apiService,
        NgTableParams,
        $rootScope,
        $log
    ) {

        var vm = this;

        vm.zones = {};
        vm.tableParams = {};

        vm.delete = deleteZone;


        activate();


        /**Init controller
         * */
        function activate() {

            initTable();

        }


        /**Init main table structure
         * */
        function initTable() {

        	vm.tableParams = new NgTableParams({},{
        		getData : function( params ){
        			return getAll(params);
		        }
	        })
        }


        /**Load zone
         * */
        function getAll( params ) {

        	var p = $rootScope.getParams(params);
            apiService.getAll( 'zones',p )
                .then(function ( response ) {
					// console.log(response);
                    vm.zones = response.data.data;
	                params.total( response.data.total);
	                params.pages = params.generatePagesArray();
	                params.current = response.data.current_page;
	                return response;
                }).catch(function ( response ) {
                    console.error( response );
                });
        }


	    /**Delete zone by id
	     * */
	    function deleteZone( id ){


		    SweetAlert.swal({
				    title: $filter('translate')('delete_zone'),
				    text: $filter('translate')('delete_zone_text'),
				    type: "warning",
				    showCancelButton: true,
				    confirmButtonColor: "#DD6B55",
				    confirmButtonText: $filter('translate')('yes_delete'),
				    cancelButtonText: $filter('translate')('no_cancel'),
				    closeOnConfirm: false,
			    },
			    function (isConfirm) {

				    if (isConfirm) {

					    apiService.delete( 'zones',id )
						    .then(function ( response ) {

							    SweetAlert.swal({
								    title: $filter('translate')('zone'),
								    text: $filter('translate')('deleted_successfully'),
								    type: 'success',
								    timer: 1000,
								    showConfirmButton: false
							    });
							    initTable();

						    })
						    .catch( function ( response ) {
							    console.log(response);
							    $rootScope.showErrors(response,'delete_error');
						    });

				    }

			    });


	    }

	    /**Duplicate zone
	     * */
	    vm.cloneZone = cloneZone;
	    function cloneZone( id ) {

		    SweetAlert.swal({
				    title: $filter('translate')('duplicate_zone'),
				    text: $filter('translate')('duplicate_zone_text'),
				    type: "info",
				    showCancelButton: true,
				    confirmButtonColor: "#16987e",
				    confirmButtonText: $filter('translate')('yes'),
				    cancelButtonText: $filter('translate')('no'),
				    closeOnConfirm: false,
			    },
			    function (isConfirm) {

				    if (isConfirm) {

					    apiService.create('zone/clone/'+id)
						    .then(function (response) {
							    SweetAlert.swal({
								    title: $filter('translate')('zone'),
								    text: $filter('translate')('duplicated_successfully'),
								    type: 'success',
								    timer: 1000,
								    showConfirmButton: false
							    });
							    initTable();
						    })
						    .catch(function (response) {
							    $log.error(response);
						    });

				    }

			    });

	    }



    }

})();