/**
 * Created by juan on 17/08/16.
 */

(function () {
    'use strict';

    angular.module('app.zones',[])
        .config(config);

    function config($stateProvider) {

        $stateProvider.state('zones', {
            url: '/zones',
            templateUrl: 'app/js/modules/zones/views/list.html',
            controller: 'MainZonesCtrl',
            controllerAs: 'mzc'
        })
            .state('zone-create', {
                url: '/zones/create',
                templateUrl: 'app/js/modules/zones/views/edit.html',
                controller: 'ZoneCtrl',
                controllerAs: 'zc'

            })
            .state('zone-edit', {
                url: '/zones/edit/:id',
                templateUrl: 'app/js/modules/zones/views/edit.html',
                controller: 'ZoneCtrl',
                controllerAs: 'zc'

            })

    }


})();

