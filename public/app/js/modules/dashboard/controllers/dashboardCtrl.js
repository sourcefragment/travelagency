/**
 * Created by mario on 10/24/16.
 */
(function () {

	'use strict';

	angular.module('app.dashboard')
		.controller('DashboardController',ctrl);

	ctrl.$inject = [
		'$rootScope',
		'apiService',
		'$log',
		'$filter',
		'$timeout',
		'$location',
		'$interval',
		'$scope'

	];

	function ctrl(
		$rootScope,
	    apiService,
	    $log,
		$filter,
	    $timeout,
	    $location,
		$interval,
		$scope
	){

		var vm = this;
		vm.pendingOfConfirmation = [];
		vm.totalPendingOfConfirmation = 0;
		vm.pendingOfPayment = [];
		vm.totalPendingOfPayment = 0;
		vm.rejectedByTo = [];
		vm.totalRejected = 0;
		vm.agents = [];
		vm.totalAgents = 0;
		vm.latestBookings = [];

		vm.totalActiveAgents = 0;
		vm.totalOffers = 0;
		vm.totalClients = 0;
		vm.totalTourOperators = 0;
		vm.interval = false;

		init();

		function init(){

			load();

			//Load dashboard and refresh each 20 sec
			vm.interval = $interval(function () {
				load();
			},5000,0,false);

		}

		//Destroy interval on destroy
		$scope.$on('$destroy',function(){
			$interval.cancel(vm.interval);
		});

		/**Load
		 * */
		function load(){

			$log.error('loading dashboard');

			loadPendingOfConfimation();
			loadPendingOfPayment();
			loadRejectedByTo();
			loadAgentsBalance();
			loadStats();
			loadLatestBookings();
		}

		/**Load bookings pending of confirmation
		 * */
		vm.loadPendingOfConfimation = loadPendingOfConfimation;
		function loadPendingOfConfimation(){

			apiService.getAll('dashboard/bookings/pending-of-confirmation')
				.then(function (response) {

					vm.totalPendingOfConfirmation = response.data.total;
					vm.pendingOfConfirmation = response.data.bookings;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}


		/**Load bookings pending of payment
		 * */
		vm.loadPendingOfPayment = loadPendingOfPayment;
		function loadPendingOfPayment(){

			apiService.getAll('dashboard/bookings/pending-of-payment')
				.then(function (response) {
					vm.totalPendingOfPayment = response.data.total;
					vm.pendingOfPayment = response.data.bookings;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}


		/**Load rejected by TO
		 * */
		vm.loadRejectedByTo = loadRejectedByTo;
		function loadRejectedByTo(){

			apiService.getAll('dashboard/bookings/rejected-by-to')
				.then(function (response) {

					vm.totalRejected = response.data.total;
					vm.rejectedByTo = response.data.bookings;
				})
				.catch(function (response) {
					$log.error(response);
				});

		}


		/**Load agent's balance
		 * */
		vm.loadAgentsBalance = loadAgentsBalance;
		function loadAgentsBalance() {

			apiService.getAll('dashboard/agents/negative-balance')
				.then(function (response) {
					vm.totalAgents = response.data.total;
					vm.agents = response.data.agents;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}


		/**Load stats
		 * */
		function loadStats(){

			apiService.getAll('dashboard/stats')
				.then(function (response) {

					vm.totalOffers = response.data.total_offers;
					vm.totalClients = response.data.total_clients;
					vm.totalActiveAgents = response.data.total_agents;
					vm.totalTourOperators = response.data.total_tour_operators;

				})
				.catch(function (response) {
					$log.error(response);
				});
		}

		/**Latest bookings
		 * */
		function loadLatestBookings(){

			apiService.getAll('dashboard/bookings/latest')
				.then(function (response) {
					vm.latestBookings = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}





	}

})();