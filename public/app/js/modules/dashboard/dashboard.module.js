/**
 * Created by mario on 10/24/16.
 */
(function () {

	'use strict';

	angular.module('app.dashboard',[])
		.config( config );

	config.$inject = [
		'$stateProvider',
		'$ocLazyLoadProvider'
	];

	function config(
		$stateProvider,
		$ocLazyLoad
	){

		$stateProvider.state('dashboard', {
			url: '/dashboard',
			templateUrl: 'app/js/modules/dashboard/views/dashboard.html',
			controller: 'DashboardController',
			controllerAs: 'mdc',
			resolve: {
				loadPlugin: function ($ocLazyLoad) {
					return $ocLazyLoad.load([
						{

							serie: true,
							name: 'angular-flot',
							files: [
								'app/lib/plugins/flot/jquery.flot.js',
								'app/lib/plugins/flot/jquery.flot.time.js',
								'app/lib/plugins/flot/jquery.flot.tooltip.min.js',
								'app/lib/plugins/flot/jquery.flot.spline.js',
								'app/lib/plugins/flot/jquery.flot.resize.js',
								'app/lib/plugins/flot/jquery.flot.pie.js',
								'app/lib/plugins/flot/curvedLines.js',
								'app/lib/plugins/flot/angular-flot.js'
							]
						},
						{
							name: 'angles',
							files: [
								'app/lib/plugins/chartJs/angles.js',
								'app/lib/plugins/chartJs/Chart.min.js'
							]
						},
						{
							name: 'angular-peity',
							files: [
								'app/lib/plugins/peity/jquery.peity.min.js',
								'app/lib/plugins/peity/angular-peity.js'
							]
						}
					]);
				}
			}
		})

	}


})();