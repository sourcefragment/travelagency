/**
 * Created by mario on 1/26/17.
 */
(function () {

	'use strict';

	angular.module('app.attachments')
		.directive('fileUploader',fileUploader);


	function fileUploader() {
		return{
			restrict : 'A',
			scope : {
				onSuccess : '&onSuccess',
				id : '@id',
				module : '@module'
			},
			link : function(scope,element,attrs ){
				element.click( scope.openModal );
			},
			controller:'AttachmentCtrl'
		}
	}


})();