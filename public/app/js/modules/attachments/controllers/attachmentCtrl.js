/**
 * Created by mario on 1/26/17.
 */
(function () {
	'use strict';

	angular.module('app.attachments')
		.controller('AttachmentCtrl',ctrl);

	ctrl.$inject = [
		'$scope',
		'$uibModal',
		'$rootScope',
		'config',
		'$auth',
		'$log'
	];

	function ctrl(
		$scope,
		$uibModal,
		$rootScope,
		config,
		$auth,
		$log
	) {


		/**Open modal
		 * */
		$scope.openModal = function(){

			var id = $scope.id;
			var module = $scope.module;

			var instance = $uibModal.open({
				templateUrl : 'app/js/modules/attachments/views/file-upload-modal.html',
				controller : ['$scope',function ($scope) {

					var token = $auth.getToken();

					$scope.file = {};
					$scope.dropzoneConfig = {
						parallelUploads : 3,
						maxFileSize : 30,
						url : config.apiURL + 'attachments?item_id='+id+'&module='+module+'&token='+token
					};

					/**Triggered on file success
					 * */
					$scope.onAddedFile = function( file ) {
						$log.error( file );

					};

					/**Triggered on error
					 * */
					$scope.onError = function( file,error,xhr){
						$log.error(file,error,xhr);
					};

					/**Triggered on close
					 * */
					$scope.close = function(){
						onClose();
						this.$close();
					};


				}]
			});
		};

		/**On save or close
		 * */
		function onClose( model ){
			if( typeof  $scope.onSuccess === 'function'){
				$scope.onSuccess();
			}
		}



	}


})();