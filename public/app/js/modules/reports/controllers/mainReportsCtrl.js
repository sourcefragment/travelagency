(function () {

    'use strict';

    angular
        .module('app.reports')
        .controller('MainReportsController', mainReportsController);

    mainReportsController.$inject = ['DTColumnDefBuilder',
        ];

    function mainReportsController(DTColumnDefBuilder
                                 ) {

        var vm = this;

        activate();

        /**Init controller
         * */
        function activate() {
           initTable();
        }


        /**Init main table structure
         * */
        function initTable() {
            vm.dtColumnDefs = [
                DTColumnDefBuilder.newColumnDef(0).notSortable(),
                DTColumnDefBuilder.newColumnDef(3).notSortable()
            ];
        }

    };
})();





