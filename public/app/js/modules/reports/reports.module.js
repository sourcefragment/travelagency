/**
 * Created by juan on 17/08/16.
 */

(function () {
    'use strict';

    angular.module('app.reports',[])
        .config(config);

    function config($stateProvider) {


	    $stateProvider.state('transactions-report', {
                url: '/transactions-report',
                templateUrl: 'app/js/modules/reports/views/transactions/index.html',
                // controller: 'ReportsController',
                // controllerAs: 'oc'

            })
	        .state('bookings-report', {
		        url: '/bookings-report',
		        templateUrl: 'app/js/modules/reports/views/bookings/index.html',
		        // controller: 'ReportsController',
		        // controllerAs: 'oc'

	        })
	        .state('commissions-report', {
		        url: '/commissions-report',
		        templateUrl: 'app/js/modules/reports/views/commissions/index.html',
		        // controller: 'ReportsController',
		        // controllerAs: 'oc'

	        })
	        .state('activity-report', {
		        url: '/activity-report',
		        templateUrl: 'app/js/modules/reports/views/activity/index.html',
		        // controller: 'ReportsController',
		        // controllerAs: 'oc'

	        })
	        .state('payments-report', {
		        url: '/payments-report',
		        templateUrl: 'app/js/modules/reports/views/payments/index.html',
		        // controller: 'ReportsController',
		        // controllerAs: 'oc'

	        })
	        .state('users-report', {
		        url: 'users-report',
		        templateUrl: 'app/js/modules/reports/views/users/index.html',
		        // controller: 'ReportsController',
		        // controllerAs: 'oc'

	        })
	        .state('traffic-report', {
		        url: 'traffic-report',
		        templateUrl: 'app/js/modules/reports/views/traffic/index.html',
		        // controller: 'ReportsController',
		        // controllerAs: 'oc'

	        })



    };


})();

