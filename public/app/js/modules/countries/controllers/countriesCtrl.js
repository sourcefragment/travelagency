(function () {

    'use strict';

    angular
        .module('app.countries')
        .controller('CountriesController', countriesController);


    countriesController.$inject = ['$scope', '$rootScope',
        'countriesService'
    ];

    function countriesController( $scope, $rootScope,
        countriesService) {

    	var vm = this;
        vm.country = false;
        vm.getCountries = getCountries;

        activate();

        /**Init controller
         * */
        function activate(){
            if( !$rootScope.countries.length ){
	            getCountries();
            }


        }

        /**Load countries
         * */
        function getCountries() {
            countriesService.countries()
                .then( function( response ) {
                    $rootScope.countries = response.data;
                    return response;

                }).catch( function( response ) {
                    console.error( response );
	                return response;
            });
        }

        $scope.tagHandler = function (tag){
            return null;
        }


    }

})();