/**
 * Created by mario on 9/7/16.
 */
(function () {
	'use strict';

	angular.module('app.countries')
		.directive('countrySelector', countriesSelector);

	function countriesSelector(){

		return {
			restrict : 'AE',
			required : 'ngModel',
			scope : {
				country : '=ngModel',
				countries : '='
			},
			templateUrl : 'app/js/sections/countries/views/countries-selector.html',
			controller : 'CountriesController',
			controllerAs : '$ctrl'
		}
	}


})();