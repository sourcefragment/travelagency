/**
 * Created by mario on 1/26/17.
 */
(function () {

	'use strict';

	angular.module('app.templates',[])
		.config(config);

	function config( $stateProvider ){

		$stateProvider.state('templates', {
			url: '/templates',
			templateUrl: 'app/js/modules/templates/views/list.html',
			controller: 'MainTemplatesCtrl',
			controllerAs: 'mtc'
		})
			.state('template-create', {
				url: '/template/create',
				templateUrl: 'app/js/modules/templates/views/edit.html',
				controller: 'TemplateCtrl',
				controllerAs: 'tc'

			})
			.state('template-edit', {
				url: '/templates/edit/:id',
				templateUrl: 'app/js/modules/templates/views/edit.html',
				controller: 'TemplateCtrl',
				controllerAs: 'tc'

			});

	}


})();