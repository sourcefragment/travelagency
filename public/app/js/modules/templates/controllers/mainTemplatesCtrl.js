/**
 * Created by mario on 1/26/17.
 */
(function () {

	'use strict';

	angular
		.module('app.templates')
		.controller('MainTemplatesCtrl',ctrl);

	ctrl.$inject = [
		'apiService',
		'$rootScope',
		'$filter',
		'SweetAlert',
		'NgTableParams',
		'$log'
	];


	function ctrl(
		apiService,
		$rootScope,
		$filter,
		SweetAlert,
		NgTableParams,
		$log

	){


		var vm = this;

		vm.templates = [];
		vm.tableParams = {};
		vm.delete = deleteItem;


		init();

		function init(){
			initTable();
		}


		/**Init main view table
		 * */
		function initTable(){

			vm.tableParams = new NgTableParams({},{
				getData : function( params ){
					return getAll( params );
				}
			});
		}

		/**Get all templates for listing
		 *@param params object
		 * */
		function getAll( params ){

			var p = $rootScope.getParams( params );
			// $log.error(p);
			apiService.getAll( 'templates', p )
				.then(function (response) {
					$log.error(response);
					vm.templates = response.data.data;
					params.total( response.data.total);
					params.pages = params.generatePagesArray();
					params.current = response.data.current_page;
					return response;
				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'template_error');
				})
		}

		/**Delete angecy
		 * */
		function deleteItem( id ){

			SweetAlert.swal({
					title: $filter('translate')('delete_template'),
					text: $filter('translate')('delete_template_text'),
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: $filter('translate')('yes_delete'),
					cancelButtonText: $filter('translate')('no_cancel'),
					closeOnConfirm: false,
				},
				function (isConfirm) {

					if (isConfirm) {

						apiService.delete( 'templates',id )
							.then( function( response ){
								SweetAlert.swal({
									title: $filter('translate')('template'),
									text: $filter('translate')('deleted_successfully'),
									type: 'success',
									timer: 2000,
									showConfirmButton: false
								});
								initTable();

							}).catch(function ( response ) {

							var msg = response.data.error;

							SweetAlert.swal({
								title : $filter('translate')('delete_error'),
								text : $filter('translate')( msg ),
								type : 'error',
								showConfirmButton : true,
								closeOnConfirm : true
							});

						});
					}

				});

		}
	}

})();