/**
 * Created by mario on 1/26/17.
 */
(function () {
	'use strict';

	angular
		.module('app.templates')
		.controller('TemplateCtrl',ctrl);


	ctrl.$inject = [
		'$stateParams',
		'$location',
		'$filter',
		'SweetAlert',
		'$uibModal',
		'$rootScope',
		'apiService',
		'$log'
	];

	function ctrl(
		$stateParams,
		$location,
		$filter,
		SweetAlert,
		$uibModal,
		$rootScope,
		apiService,
		$log

	){

		var vm = this;
		vm.id  = $stateParams.id;
		vm.template = {};
		vm.locations = [];
		vm.saveAndExit = false;
		vm.modules = [];
		vm.types = [];


		vm.save = save;
		vm.cancel = cancel;


		init();

		/**Init view
		 * */
		function init(){

			if( vm.id ){
				get( vm.id );
			}

			loadLocations();
			loadAvailableModules();
			// loadTemplateTypes();

		}


		/**Load template
		 * */
		function get( id ){

			apiService.get( 'templates',id )
				.then(function (response) {
					vm.template = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				})
		}

		/**Load locations
		 * */
		function loadLocations(){

			apiService.getAll('locations')
				.then(function (response) {
					vm.locations = response.data.data;
				})
				.catch(function (response) {
					$log.error(response);
				})
		}

		/**Load modules
		 * */
		function loadAvailableModules() {

			apiService.getAll('modules')
				.then(function (response) {
					// $log.error(response);
					vm.modules = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}

		/**Load template types
		 * */
		function loadTemplateTypes(){

			apiService.getAll('template/types')
				.then(function (response) {
					// $log.error(response);
					vm.types = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}


		/**Save template
		 * */
		function save( mode ){

			vm.saveAndExit = mode;

			if( vm.id ){
				update();
			}else{
				create();
			}
		}

		/**Create new template
		 * */
		function create(){
			apiService.create( 'templates',vm.template )
				.then(function (response) {
					$log.error(response);
					SweetAlert.swal({
						title: $filter('translate')('template'),
						text: $filter('translate')('created_successfully'),
						type: 'success',
						timer: 1000,
						showConfirmButton: false
					});

					vm.id = response.data.id;
					vm.template = response.data;

					if( vm.saveAndExit )
						cancel();
				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'create_error');
				});

		}


		/**Update template
		 * */
		function update(){
			apiService.update( 'templates',vm.template )
				.then(function (response) {
					// $log.error(response);
					SweetAlert.swal({
						title: $filter('translate')('template'),
						text: $filter('translate')('updated_successfully'),
						type: 'success',
						timer: 1000,
						showConfirmButton: false
					});

					if( vm.saveAndExit )
						cancel();

				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'update_error')
				});

		}


		/**On cancel
		 * */
		function cancel(){
			$location.path('/templates');
		}


		/**Load module fields
		 * */
		vm.loadFields = loadFields;
		function loadFields( ){

			apiService.getAll('module/'+vm.module_field.name+'/fields')
				.then(function (response) {
					$log.error(response);
					vm.fields = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}

		/**Get selected field
		 * */
		vm.getSelectedField = getSelectedField;
		function getSelectedField(){

		}

	}

})();