/**
 * Created by mario on 1/30/17.
 */
(function () {
	'use strict';

	angular.module('app.locations',[])
		.config(config);

	function config($stateProvider) {

		$stateProvider.state('locations', {
			url: '/locations',
			templateUrl: 'app/js/modules/locations/views/list.html',
			controller: 'MainLocationsCtrl',
			controllerAs: 'mlc'
		})
			.state('location-create', {
				url: '/locations/create',
				templateUrl: 'app/js/modules/locations/views/edit.html',
				controller: 'LocationCtrl',
				controllerAs: 'lc'

			})
			.state('location-edit', {
				url: '/locations/edit/:id',
				templateUrl: 'app/js/modules/locations/views/edit.html',
				controller: 'LocationCtrl',
				controllerAs: 'lc'

			});



	}


})();