/**
 * Created by mario on 3/22/17.
 */
(function () {
	'use strict';

	angular.module('app')
		.directive('locationPicker',locationPicker)
		.directive('locationPopup',locationPopup);


	function locationPicker(){
		return {
			restrict : 'A',
			scope : {
				model:'=ngModel',
				radius:'@radius',
				name:'@name'
			},
			link:function( scope,element,attrs ){
				element.click( scope.openModal);
			},
			controller : 'LocationPickerCtrl',
			controllerAs : '$ctrl'

		}
	}


	/**
	 * */
	function locationPopup(){
		return {
			restrict : 'A',
			scope : {
				lat:'@lat',
				lng:'@lng',
				name:'@name'
			},
			link:function( scope,element,attrs ){
				element.click( scope.openModal);
			},
			controller : 'LocationPopupCtrl',
			controllerAs : '$ctrl'

		}
	}

})();