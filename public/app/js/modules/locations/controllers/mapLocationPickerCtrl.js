/**
 * Created by mario on 3/22/17.
 */
(function () {

	'use strict';

	angular.module('app.locations')
		.controller('LocationPickerCtrl',ctrl);


	ctrl.$inject = [
		'$scope',
		'$uibModal',
		'NgMap',
		'$log',
		'$rootScope'
	];

	function ctrl(
		$scope,
		$uibModal,
		NgMap,
	    $log,
		$rootScope
	){

		/**Open location picker modal
		 * */
		$scope.openModal = function(){

			var model = $scope.model;
			var radius = parseInt($scope.radius);
			var circle = null;
			var name = $scope.name;

			var modalInstance = $uibModal.open({
				templateUrl: 'app/js/modules/locations/views/location-picker-modal.html',
				size:'lg',
				controller : function($scope,$timeout){

					var location;

					if( typeof model === 'undefined' ){
						location = {
							lat : 36.721826,
							lng : -4.420916,
							heading : 0,
							pitch : 0,
							zoom : 16
						}
					}else{
						location = {
							lat: model.lat !== null ? model.lat:36.721826,
							lng:  model.lng !== null ? model.lng:-4.420916,
							heading: model.heading !== null ? model.heading:0,
							pitch: model.pitch !== null ? model.pitch:0,
							zoom: 16
						};

					}



					$scope.mapLoaded = false;
					$scope.street = null;
					$scope.geocoder = null;
					$scope.map = null;
					$scope.locations = [];
					$scope.isSearching = false;
					$scope.name = name;
					$log.error($scope.name);


					//On save modal
					$scope.search = searchAddress;
					$scope.saveModal = saveModal;
					$scope.closeModal = closeModal;
					$scope.setLocation = setLocation;


					$rootScope.$on('mapInitialized',function( event,map ){


					});

					//Init map
					NgMap.getMap()
						.then(function ( map ) {

							$scope.map = map;

							$scope.map.setZoom( location.zoom );
							$scope.map.setCenter({
								lat : parseFloat(location.lat),
								lng : parseFloat(location.lng)
							});

							//Draw circle if radius
							if( radius > 0 ){

								circle = new google.maps.Circle({
									strokeColor: '#1ab394',
									strokeOpacity: 0.8,
									strokeWeight: 2,
									fillColor: '#1ab394',
									fillOpacity: 0.35,
									map: $scope.map,
									center: $scope.map.getCenter(),
									radius: radius
								});

							}

							google.maps.event.trigger($scope.map,'resize');

							//Delay map initialization to avoid rendering problems
							$timeout(function () {

								$scope.geocoder = new google.maps.Geocoder();
								$scope.street = $scope.map.getStreetView();

								$scope.street.setPov({
									heading:location.heading,
									pitch:location.pitch
								});


								//Listener on map changed
								$scope.map.addListener('zoom_changed',function (event) {
										location.zoom = $scope.map.getZoom();
								});

								//On streetview POV changed
								$scope.street.addListener('pov_changed',function () {
									var pos = $scope.street.getPosition();
									var pov = $scope.street.getPov();

									location.lat = pos.lat();
									location.lng = pos.lng();
									location.heading = pov.heading;
									location.pitch = pov.pitch;

								});


							},2000);



						})
						.catch(function(map){
							console.log(map);
						});


					//On modal save
					function saveModal() {
						this.$close();
						setModel(location);
					}


					function closeModal(){
						this.$close();
					}

					/**Search location by address
					 *
					 * */
					function searchAddress(){

						$scope.isSearching = true;
						$scope.geocoder.geocode({
							address : $scope.address
						},function ( results,status) {

							$scope.isSearching = false;

							if( status === 'OK' ){
								$scope.locations = results;
								// $scope.street.setPosition(results[0].geometry.location);

							}else if( status === 'ZERO_RESULT' ){
								$scope.locatins = [];
							}
							$log.error(results,status);
						});
					}


					/**Set location on click
					 * */
					function setLocation( location ){
						$scope.street.setPosition( location.geometry.location );
						$scope.map.setZoom(18);
						// $log.error(location);
					}

				}
			})
				.closed.then(function () {

					if( circle ){
						$log.info('Removing circle');
						circle.setMap(null);
					}

				});

		};

		/**Set location is called from the uibModal on modal saved
		 * The location will be set only if there is a change on the map position
		 * */
		function setModel( location ){
			$log.info(location);
			$scope.model = location;
		}


	}

})();