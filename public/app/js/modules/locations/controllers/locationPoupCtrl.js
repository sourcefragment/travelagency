/**
 * Created by mario on 3/22/17.
 */
(function () {

	'use strict';

	angular.module('app.locations')
		.controller('LocationPopupCtrl',ctrl);


	ctrl.$inject = [
		'$scope',
		'$uibModal',
		'NgMap',
		'$log',
		'$rootScope'
	];

	function ctrl(
		$scope,
		$uibModal,
		NgMap,
		$log,
		$rootScope
	){

		/**Open location picker modal
		 * */
		$scope.openModal = function(){

			var lat = $scope.lat;
			var lng = $scope.lng;
			var name = $scope.name;

			var modalInstance = $uibModal.open({
				templateUrl: 'app/js/modules/locations/views/location-popup-modal.html',
				size:'lg',
				controller : function($scope,$timeout){

					$scope.name = name;

					//Init map
					NgMap.getMap()
						.then(function ( map ) {

							$scope.map = map;
							$scope.map.setZoom( 16 );
							$scope.map.setCenter({
								lat : parseFloat(lat),
								lng : parseFloat(lng)
							});

							google.maps.event.trigger($scope.map,'resize');


						})
						.catch(function(map){
							console.log(map);
						});



					/**Download map as image
					 * */
					$scope.downloadMapImage = function(){

					};


					/**Download streetview image
					 * */
					$scope.downloadStreetImage = function () {

					}



				}
			});


		};




	}

})();