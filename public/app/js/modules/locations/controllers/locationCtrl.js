/**
 * Created by mario on 1/30/17.
 */
(function () {
	'use strict';

	angular
		.module('app.locations')
		.controller('LocationCtrl',ctrl);


	ctrl.$inject = [
		'$stateParams',
		'$location',
		'$filter',
		'SweetAlert',
		'$uibModal',
		'$rootScope',
		'apiService',
		'$log'
	];

	function ctrl(
		$stateParams,
		$location,
		$filter,
		SweetAlert,
		$uibModal,
		$rootScope,
		apiService,
		$log

	){

		var vm = this;
		vm.id  = $stateParams.id;
		vm.location = {};
		vm.locations = [];
		vm.spinOptions = {
			min: 0,
			max: 10,
			step: 1
		};
		vm.saveAndExit = false;

		vm.save = save;
		vm.cancel = cancel;


		init();

		/**Init view
		 * */
		function init(){

			if( vm.id ){
				get( vm.id );
			}

		}


		/**Load location
		 * */
		function get( id ){

			apiService.get( 'locations',id )
				.then(function (response) {
					vm.location = response.data;
					vm.city = response.data.city;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}



		/**Save location
		 * */
		function save( mode ){

			vm.saveAndExit = mode;

			//Assign city
			if( vm.city ){
				vm.location.city_id = vm.city.id;
			}

			if( vm.id ){
				update();
			}else{
				create();
			}
		}

		/**Create new location
		 * */
		function create(){
			apiService.create( 'locations',vm.location )
				.then(function (response) {
					// console.log(response);
					SweetAlert.swal({
						title: $filter('translate')('location'),
						text: $filter('translate')('created_successfully'),
						type: 'success',
						timer: 1000,
						showConfirmButton: false
					});

					vm.id = response.data.id;
					vm.location = response.data;

					if( vm.saveAndExit )
						cancel();
				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'create_error');
				});

		}


		/**Update location
		 * */
		function update(){
			apiService.update( 'locations',vm.location )
				.then(function (response) {
					// $log.error(response);
					SweetAlert.swal({
						title: $filter('translate')('location'),
						text: $filter('translate')('updated_successfully'),
						type: 'success',
						timer: 1000,
						showConfirmButton: false
					});

					if( vm.saveAndExit )
						cancel();
				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'update_error')
				});

		}


		/**On cancel
		 * */
		function cancel(){
			$location.path('/locations');
		}


		/**Toggle featured
		 * */
		vm.toggleFeatured = toggleFeatured;
		function toggleFeatured(){

			apiService.getAll('locations/'+vm.id+'/toggle-featured')
				.then(function (response) {
					vm.location.featured = response.data.featured;
					$rootScope.saved();
				});
		}

	}

})();