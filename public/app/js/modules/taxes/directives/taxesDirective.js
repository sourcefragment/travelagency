/**
 * Created by juan on 18/08/16.
 */
(function () {

    angular.module('app.taxes')
        .directive('newTaxeBtn', newTaxeBtn)
        .directive('createTaxeBtnLong', createTaxeBtnLong);

    function newTaxeBtn() {
        return {
            templateUrl:'app/js/sections/taxes/views/new-taxe-btn.html',
            controller: 'TaxesController',
            controllerAs: '$ctrl',
        };
    }

    function createTaxeBtnLong() {
        return {
            template: '<button type="button" class="btn btn-primary btn-lg" ui-sref="taxe-create">'
            + '<i class="fa fa-plus-circle"></i> [[ "new_taxe" | translate ]]'
            + '</button>'
        };
    }

})();