(function () {

    'use strict';

    angular
        .module('app.taxes')
        .controller('MainTaxesCtrl', ctrl);

    ctrl.$inject = [
        '$filter',
        'SweetAlert',
        'apiService',
	    'NgTableParams',
	    '$rootScope',
	    '$log'
    ];

    function ctrl(
        $filter,
        SweetAlert,
        apiService,
        NgTableParams,
        $rootScope,
        $log

    ) {

        var vm = this;
        vm.taxes = {};
        vm.tableParams = {};


        vm.delete = deleteItem;

        activate();


        /**Init controller
         * */
        function activate() {
            initTable();
        }


        /**Init main table structure
         * */
        function initTable() {

        	vm.tableParams = new NgTableParams({},{
        		getData : function( params ){
        			return getAll( params );
		        }
	        });
        }


        /**Load tax
         * */
        function getAll( params ){

        	var p = $rootScope.getParams( params );

        	apiService.getAll( 'taxes',p )
                .then(function ( response ) {
                	vm.taxes = response.data.data;
	                params.total( response.data.total);
	                params.pages = params.generatePagesArray();
	                params.current = response.data.current_page;
	                return response;

                }).catch(function ( response ) {
	                $log.error( response );
	            });
        }


        /**Delete taxe
         *@param id, the taxe id
         * @return promise
         */
        function deleteItem( id ) {

            SweetAlert.swal({
                    title: $filter('translate')('delete_tax'),
                    text: $filter('translate')('tax_delete_text'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $filter('translate')('yes_delete'),
                    cancelButtonText: $filter('translate')('no_cancel'),
                    closeOnConfirm: false,
                },
                function (isConfirm) {

                    if (isConfirm) {

                        apiService.delete( 'taxes',id )
                            .then(function () {
                                SweetAlert.swal({
                                    title: $filter('translate')('deleted'),
                                    text: $filter('translate')('deleted_successfully'),
                                    type: "success",
                                    timer: 2000,
                                    showConfirmButton: false
                                });
                                getAll();

                            }).catch(function ( response ) {
                                $log.error( response );
                            });
                    }

                });
        }




    }



})();











