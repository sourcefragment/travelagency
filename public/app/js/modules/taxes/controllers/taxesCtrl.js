(function () {

    'use strict';

    angular
        .module('app.taxes')
        .controller('TaxesCtrl', ctrl);


    ctrl.$inject = [
    	'$rootScope',
        '$stateParams',
        '$location',
        'apiService',
	    'SweetAlert',
	    '$filter',
	    '$log'
    ];

    function ctrl(
    	$rootScope,
        $stateParams,
        $location,
	    apiService,
        SweetAlert,
        $filter,
        $log
    ){

    	var vm = this;
        vm.tax = false;
        vm.id = $stateParams.id;
        vm.saveAndExit = false;

        vm.save = save;
	    vm.cancel = cancel;


        activate();

        function activate() {

            if(vm.id){
	            get(vm.id)
            }
        }


        /**Get tax by id
         * @param id int
         * */
        function get( id ) {

            apiService.get( 'taxes',id )
                .then(function (response) {
                    vm.tax = response.data;
                }).catch(function (response) {
                $log.error(response);
            });
        }


        /**Save tax
         * */
        function save( mode ) {

        	vm.saveAndExit = mode;

        	if( vm.tax.id ){
        		update();
	        }else{
	        	create();
	        }
        }


        /**Create a new tax
         * */
        function create(){

	        apiService.create( 'taxes',vm.tax )
		        .then(function ( response ) {

			        SweetAlert.swal({
				        title: $filter('translate')('tax'),
				        text: $filter('translate')('created_successfully'),
				        type: 'success',
				        timer: 2000,
				        showConfirmButton: false
			        });

			        vm.id = response.data.id;
			        vm.tax = response.data;

			        if( vm.saveAndExit )
		        	    cancel();

		        }).catch( function( response ) {
		        	$log.error(response);
		            $rootScope.showErrors(response,'create_error');

	            });
        }


        /**Update an existing tax
         * */
        function update(){

	        apiService.update( 'taxes',vm.tax )
		        .then(function () {

		        	SweetAlert.swal({
				        title: $filter('translate')('tax'),
				        text: $filter('translate')('updated_successfully'),
				        type: 'success',
				        timer: 2000,
				        showConfirmButton: false
			        });

		        	if( vm.saveAndExit )
			            cancel();

		        }).catch( function( response ){
		        	$log.error(response);
		            $rootScope.showErrors(response,'update_error');

	            });
        }

        /**Cancel method
         * */
        function cancel(){
        	$location.path('/taxes');
        }

    }

})();
