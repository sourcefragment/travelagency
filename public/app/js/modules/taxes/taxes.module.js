/**
 * Created by juan on 17/08/16.
 */

(function () {
    'use strict';

    angular.module('app.taxes',[])
        .config(config);

    function config($stateProvider) {

        $stateProvider.state('taxes', {
            url: '/taxes',
            templateUrl: 'app/js/modules/taxes/views/list.html',
            controller: 'MainTaxesCtrl',
            controllerAs: 'mtc'
        })
            .state('tax-create', {
                url: '/taxes/create',
                templateUrl: 'app/js/modules/taxes/views/edit.html',
                controller: 'TaxesCtrl',
                controllerAs: 'tc'

            })
            .state('tax-edit', {
                url: '/taxes/edit/:id',
                templateUrl: 'app/js/modules/taxes/views/edit.html',
                controller: 'TaxesCtrl',
                controllerAs: 'tc'

            })

    };


})();

