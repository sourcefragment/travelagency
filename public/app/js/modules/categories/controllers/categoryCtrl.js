/**
 * Created by mario on 1/27/17.
 */
(function () {
	'use strict';

	angular
		.module('app.categories')
		.controller('CategoryCtrl',ctrl);


	ctrl.$inject = [
		'$stateParams',
		'$location',
		'$filter',
		'SweetAlert',
		'$rootScope',
		'apiService',
		'$log'
	];

	function ctrl(
		$stateParams,
		$location,
		$filter,
		SweetAlert,
		$rootScope,
		apiService,
		$log

	){

		var vm = this;
		vm.id  = $stateParams.id;
		vm.category = {};
		vm.locations = [];
		vm.saveAndExit = false;

		vm.save = save;
		vm.cancel = cancel;


		init();

		/**Init view
		 * */
		function init(){

			if( vm.id ){
				get( vm.id );
			}

		}


		/**Load category
		 * */
		function get( id ){

			apiService.get( 'categories',id )
				.then(function (response) {
					vm.category = response.data;
				})
				.catch(function (response) {
					$log.error(response);
				});
		}



		/**Save category
		 * */
		function save( mode ){

			vm.saveAndExit = mode;

			if( vm.id ){
				update();
			}else{
				create();
			}
		}

		/**Create new category
		 * */
		function create(){
			apiService.create( 'categories',vm.category )
				.then(function (response) {
					// console.log(response);
					SweetAlert.swal({
						title: $filter('translate')('category'),
						text: $filter('translate')('created_successfully'),
						type: 'success',
						timer: 2000,
						showConfirmButton: false
					});

					vm.id = response.data.id;
					vm.category = response.data;

					if( vm.saveAndExit )
						cancel();
				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'create_error');
				});

		}


		/**Update category
		 * */
		function update(){
			apiService.update( 'categories',vm.category )
				.then(function (response) {
					// $log.error(response);
					SweetAlert.swal({
						title: $filter('translate')('category'),
						text: $filter('translate')('updated_successfully'),
						type: 'success',
						timer: 2000,
						showConfirmButton: false
					});

					if( vm.saveAndExit )
						cancel();
				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'update_error')
				});

		}


		/**On cancel
		 * */
		function cancel(){
			$location.path('/categories');
		}



	}

})();