/**
 * Created by mario on 12/3/16.
 */
(function () {
	'use strict';

	angular.module('app.categories',[])
		.config(config);

	function config($stateProvider) {

		$stateProvider.state('categories', {
			url: '/categories',
			templateUrl: 'app/js/modules/categories/views/list.html',
			controller: 'MainCategoriesCtrl',
			controllerAs: 'mcc'
		})
			.state('category-create', {
				url: '/categories/create',
				templateUrl: 'app/js/modules/categories/views/edit.html',
				controller: 'CategoryCtrl',
				controllerAs: 'cc'

			})
			.state('category-edit', {
				url: '/categories/edit/:id',
				templateUrl: 'app/js/modules/categories/views/edit.html',
				controller: 'CategoryCtrl',
				controllerAs: 'cc'

			});


	};


})();