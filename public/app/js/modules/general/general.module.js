/**
 * Created by mario on 3/9/17.
 */
(function () {

	'use strict';

	angular.module('app.general',[])
		.config(config);

		function config($stateProvider) {

			$stateProvider.state('general', {
				url: '/general',
				templateUrl: 'app/js/modules/general/views/list.html',
				controller: 'MainGeneralCtrl',
				controllerAs: 'mgc'
			})


		}



})();
