/**
 * Created by mario on 11/30/16.
 */
(function () {

	'use strict';

	angular.module('app.payment-status',[])
		.config(config);

	function config( $stateProvider ){

		$stateProvider.state('payment-status', {
			url: '/payment-status',
			templateUrl: 'app/js/modules/payment-status/views/list.html',
			controller: 'MainPaymentStatusCtrl',
			controllerAs: 'mpsc'
		})
			.state('payment-status-create', {
				url: '/payment-status/create',
				templateUrl: 'app/js/modules/payment-status/views/edit.html',
				controller: 'PaymentStatusCtrl',
				controllerAs: 'psc'

			})
			.state('payment-status-edit', {
				url: '/payment-status/edit/:id',
				templateUrl: 'app/js/modules/payment-status/views/edit.html',
				controller: 'PaymentStatusCtrl',
				controllerAs: 'psc'

			});

	}


})();