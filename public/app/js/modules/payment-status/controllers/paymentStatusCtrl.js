/**
 * Created by mario on 12/11/16.
 */
(function () {
	'use strict';

	angular
		.module('app.payment-status')
		.controller('PaymentStatusCtrl',ctrl);


	ctrl.$inject = [
		'$stateParams',
		'$location',
		'$filter',
		'SweetAlert',
		'$rootScope',
		'apiService'
	];

	function ctrl(
		$stateParams,
		$location,
		$filter,
		SweetAlert,
		$rootScope,
		apiService

	){

		var vm = this;
		vm.id  = $stateParams.id;
		vm.paymentStatus = {};
		vm.saveAndExit = false;

		vm.save = save;
		vm.cancel = cancel;


		init();

		/**Init view
		 * */
		function init(){

			if( vm.id ){
				getPaymentStatus( vm.id );
			}

		}


		/**Load paymentStatus
		 * */
		function getPaymentStatus( id ){

			apiService.get( 'payment-status',id )
				.then(function (response) {
					vm.paymentStatus = response.data;
				})
				.catch(function (response) {
					console.log(response);
				})
		}


		/**Save paymentStatus
		 * */
		function save( mode ){

			vm.saveAndExit = mode;

			if( vm.id ){
				update();
			}else{
				create();
			}
		}

		/**Create new paymentStatus
		 * */
		function create(){
			apiService.create( 'payment-status',vm.paymentStatus )
				.then(function (response) {
					// console.log(response);
					SweetAlert.swal({
						title: $filter('translate')('payment_status'),
						text: $filter('translate')('created_successfully'),
						type: 'success',
						timer: 1000,
						showConfirmButton: false
					});

					vm.id = response.data.id;
					vm.paymentStatus = response.data;

					if( vm.saveAndExit )
						cancel();

				})
				.catch(function (response) {
					console.log(response);
					$rootScope.showErrors(response,'create_error');
				});

		}


		/**Update paymentStatus
		 * */
		function update(){
			apiService.update( 'payment-status',vm.paymentStatus )
				.then(function (response) {
					// console.log(response);
					SweetAlert.swal({
						title: $filter('translate')('payment_status'),
						text: $filter('translate')('updated_successfully'),
						type: 'success',
						timer: 1000,
						showConfirmButton: false
					});

					if( vm.saveAndExit )
						cancel();

				})
				.catch(function (response) {
					console.log(response);
					$rootScope.showErrors(response,'update_error')
				});

		}


		/**On cancel
		 * */
		function cancel(){
			$location.path('/payment-status');
		}

	}

})();