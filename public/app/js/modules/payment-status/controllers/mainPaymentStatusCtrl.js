/**
 * Created by mario on 12/11/16.
 */
(function () {

	'use strict';

	angular
		.module('app.payment-status')
		.controller('MainPaymentStatusCtrl',mainAgenciesCtrl);

	mainAgenciesCtrl.$inject = [
		'apiService',
		'$rootScope',
		'$filter',
		'SweetAlert',
		'toaster',
		'NgTableParams',
		'$log'
	];


	function mainAgenciesCtrl(
		apiService,
		$rootScope,
		$filter,
		SweetAlert,
		toaster,
		NgTableParams,
	    $log

	){


		var vm = this;

		vm.paymentStatus = [];
		vm.tableParams = {};
		vm.delete = deletePaymentStatus;


		init();

		function init(){
			initTable();
		}


		/**Init main view table
		 * */
		function initTable(){

			vm.tableParams = new NgTableParams({},{
				getData : function( params ){
					return getPaymentStatus( params );
				}
			});
		}

		/**Get all agencies for listing
		 *@param params object
		 * */
		function getPaymentStatus( params ){

			var p = $rootScope.getParams( params );
			// $log.error(p);
			apiService.getAll( 'payment-status', p )
				.then(function (response) {
					$log.error(response);
					vm.paymentStatus = response.data.data;
					params.total( response.data.total);
					params.pages = params.generatePagesArray();
					params.current = response.data.current_page;
					return response;
				})
				.catch(function (response) {
					$log.error(response);
					$rootScope.showErrors(response,'payment_status_error');
				})
		}

		/**Delete angecy
		 * */
		function deletePaymentStatus( id ){

			SweetAlert.swal({
					title: $filter('translate')('delete_payment_status'),
					text: $filter('translate')('delete_payment_status_text'),
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: $filter('translate')('yes_delete'),
					cancelButtonText: $filter('translate')('no_cancel'),
					closeOnConfirm: false,
				},
				function (isConfirm) {

					if (isConfirm) {

						apiService.delete( 'payment-status',id )
							.then( function( response ){
								SweetAlert.swal({
									title: $filter('translate')('payment_status'),
									text: $filter('translate')('deleted_successfully'),
									type: 'success',
									timer: 2000,
									showConfirmButton: false
								});
								initTable();

							}).catch(function ( response ) {

							var msg = response.data.error;

							SweetAlert.swal({
								title : $filter('translate')('payment_status_delete'),
								text : $filter('translate')( msg ),
								type : 'error',
								showConfirmButton : true,
								closeOnConfirm : true
							});

						});
					}

				});

		}
	}

})();