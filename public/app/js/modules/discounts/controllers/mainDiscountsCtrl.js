(function () {

    'use strict';

    angular
        .module('app.discounts')
        .controller('MainDiscountsController', mainDiscountsController);

    mainDiscountsController.$inject = [
        '$filter',
        'DTColumnDefBuilder',
        'SweetAlert',
        'discountsService'];

    function mainDiscountsController(
        $filter,
        DTColumnDefBuilder,
        SweetAlert,
        discountsService) {

        var vm = this;
        vm.discounts = {};
        vm.getDiscounts = getDiscounts;
        vm.deleteDiscount = deleteDiscount;
        vm.deleteDiscounts = deleteDiscounts;
        vm.discountToDelete = [];


        activate();


        /**Init controller
         * */
        function activate() {
            initTable();
            getDiscounts();
        }


        /**Init main table structure
         * */
        function initTable() {
            vm.dtColumnDefs = [
                DTColumnDefBuilder.newColumnDef(0).notSortable(),
            ];
        }


        /**Load discount
         * */
        function getDiscounts() {
            discountsService.discounts()
                .then(function ( response ) {
                    vm.discounts = response.data;
                }).catch(function ( response ) {
                console.error( response );
            });
        }


        /**Delete discount
         *@param id, the discount id
         * @return promise
         */
        function deleteDiscount(id) {

            SweetAlert.swal({
                    title: $filter('translate')('are_you_sure'),
                    text: $filter('translate')('the_discount_will_be_deleted'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $filter('translate')('yes_delete_it'),
                    cancelButtonText: $filter('translate')('no_cancel'),
                    closeOnConfirm: false,
                },
                function (isConfirm) {

                    if (isConfirm) {

                        discountsService.del(id)
                            .then(function () {
                                SweetAlert.swal({
                                    title: $filter('translate')('discount_deleted'),
                                    text: $filter('translate')('discount_deleted_successfully'),
                                    type: "success",
                                    timer: 2000,
                                    showConfirmButton: false
                                });
                                getDiscounts();
                            }).catch(function ( response ) {
                            console.error( response );
                        });
                    }

                });
        }

        /**Batch delete discount
         * @return promise
         * */
        function deleteDiscounts() {

        }


    };
})();











