(function () {

    'use strict';

    angular
        .module('app.discounts')
        .controller('DiscountsController', discountsController);


    discountsController.$inject = [
    	'$scope',
	    '$uibModal',
        '$stateParams',
        '$location',
	    'toaster',
        'discountsService'
    ];

    function discountsController(
    	$scope,
	    $uibModal,
        $stateParams,
        $location,
	    toaster,
	    discountsService ) {

    	var vm = this;
        vm.discount = false;
        vm.editModal = false;
        vm.discounts = [];
        vm.discountId = $stateParams.id;
        vm.errorMessages = [];
	    vm.closeModal = closeModal;
        vm.openModal = openModal;
        vm.getDiscount = getDiscount;
        vm.saveModal = saveModal;
        vm.save = save;


        activate();

        function activate() {

            if($stateParams.id)
                getDiscount($stateParams.id)

        }

        function openModal() {

            vm.editModal = $uibModal.open({
                templateUrl: 'app/js/sections/discounts/views/new-discount-modal.html',
	            scope: $scope
            });

	        //Save modal on discount created
	        $scope.$on( 'discountSaveSuccess',function () {
		        vm.discount = false;
	        	closeModal();
		        getDiscounts();

	        } );

	        //Show errors
	        $scope.$on('discountSaveError',function ( error,response ) {

	        	var errors = response.data.errors;
		        var msgs = [];
	        	for( var i in errors ){

	        		msgs.push({
	        			type:'error',
				        text: errors[i][0],
				        field:i
			        });

		        }
		        vm.errorMessages = msgs;
	        });

        }

        function closeModal() {
            vm.editModal.close();
	        vm.errorMessages = [];
        }

        /**Load user
         * */
        function getDiscount(id) {
            discountsService.discount(id)
                .then(function (response) {
                    vm.discount = response.data;
                }).catch(function (response) {
                console.log(response);
            });
        }

        /**Save discount modal
         * */
        function saveModal() {

        	discountsService.save( vm.discount )
                .then(function () {

                    var data = [
                        {
                            title : $filter('translate')('discount_saved'),
                            message : $filter('translate')('discount_saved_successfully')
                        }
                    ];

                    toaster.pop({
                        type: 'success',
                        body:'toaster-label',
                        directiveData:data
                    });

                    getDiscounts();
	                $scope.$emit('discountSaveSuccess');
                }).catch( function( response ) {
		            console.error( response );
		            $scope.$emit('discountSaveError',response);
                });

        }

        /**Save discount
         * */
        function save() {
            discountsService.save( vm.discount )
                .then(function () {

                    var data = [
                        {
                            title  :    'discount_saved',
                            message:    'the_discount_was_saved_successfully'
                        }
                    ];

                    toaster.pop({
                        type: 'success',
                        body:'toaster-label',
                        directiveData:data
                    });
                    $location.path("/discounts");
                }).catch( function( response ) {
                    var errors = response.data.errors;
                    var data = [];

                    for( var i in errors ){
                        data.push({
                            title:'upss_there_ are_errors_to_create',
                            subTitle:i,
                            message:errors[i][0]
                        });
                    }
                    toaster.pop({
                        type: 'error',
                        body: 'toaster-label',
                        directiveData:data
                    });
            });

        }

    }

})();
