/**
 * Created by juan on 17/08/16.
 */

(function () {
    'use strict';

    angular.module('app.discounts',[])
        .config(config);

    function config($stateProvider) {

        $stateProvider.state('discounts', {
            url: '/discounts',
            templateUrl: 'app/js/modules/discounts/views/list.html',
            controller: 'MainDiscountsController',
            controllerAs: 'mdc'
        })
            .state('discount-create', {
                url: '/discount/create',
                templateUrl: 'app/js/modules/discounts/views/edit.html',
                controller: 'DiscountsController',
                controllerAs: 'dc'

            })
            .state('discount-edit', {
                url: '/discount/edit/:id',
                templateUrl: 'app/js/modules/discounts/views/edit.html',
                controller: 'DiscountsController',
                controllerAs: 'dc'

            })

    };


})();

