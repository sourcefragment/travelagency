/**
 * Created by juan on 18/08/16.
 */
(function () {

    angular.module('app.discounts')
        .directive('newDiscountBtn', newDiscountBtn)
        .directive('createDiscountBtnLong', createDiscountBtnLong);

    function newDiscountBtn() {
        return {
            templateUrl:'app/js/sections/discounts/views/new-discount-btn.html',
            controller: 'DiscountsController',
            controllerAs: '$ctrl',
        };
    }

    function createDiscountBtnLong() {
        return {
            template: '<button type="button" class="btn btn-primary btn-lg" ui-sref="discount-create">'
            + '<i class="fa fa-plus-circle"></i> [[ "new_discount" | translate ]]'
            + '</button>'
        };
    }

})();