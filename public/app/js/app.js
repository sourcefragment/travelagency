(function () {
    angular.module('app', [
        'ui.router',
        'ui.bootstrap',
        'ui.calendar',
        'oc.lazyLoad',
        'pascalprecht.translate',
        'ngIdle',
        'ngSanitize',
        'summernote',
        'ngAnimate',
        'toaster',
        'oitozero.ngSweetAlert',
        'datePicker',
        'daterangepicker',
        'ui.switchery',
        'ui.event',
	    'satellizer',
	    'ui.select',
	    'colorpicker.module',
	    'ngImgCrop',
	    'ngMap',
	    'ui.sortable',
	    'ngDropzone',
	    'ngTable',
	    'app.modules'
    ], function ($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    })
        .run(
            function ($rootScope, $filter, apiService ) {

                // get cities
                $rootScope.getCities = function (country) {
                    apiService.get('country/cities/'+country)
                        .then(function (response) {
                            $rootScope.cities = response.data;
                            return $rootScope.cities;
                        })
                        .catch(function (error) {
                            console.log(error);
                            return error;
                        })
                }
            }
        );
})();