/**
 * INSPINIA - Responsive Admin Theme
 *
 */
(function () {
    angular.module('login', [
        'ui.router',
        'oc.lazyLoad',
        'pascalprecht.translate',
        'ngIdle',
        'ngAnimate',
        'toaster',
        'satellizer'
    ])
        .config( appConfig)
	    .constant('config',{
		    'apiVersion' : '1.0',
		    'apiURL' : 'api/v1/',
		    'mapStyle' : [
			    {
				    stylers: [
					    { hue: "#00ffe6" },
					    { saturation: -20 }
				    ]
			    },{
				    featureType: "road",
				    elementType: "geometry",
				    stylers: [
					    { lightness: 100 },
					    { visibility: "simplified" }
				    ]
			    },{
				    featureType: "road",
				    elementType: "labels",
				    stylers: [
					    { visibility: "off" }
				    ]
			    }
		    ],
		    'mapCenter': {
			    lat : 36.5579927,
			    lng : -4.6846859
		    },
		    'mapZoom' : 10
	    })
	    .service('apiService',service);



	/**Main loging config
	 * */
	function appConfig(
		$interpolateProvider,
		$stateProvider,
		$urlRouterProvider,
		$authProvider){


		$interpolateProvider.startSymbol('[[');
		$interpolateProvider.endSymbol(']]');

		$authProvider.loginUrl = BASE_URL+'/login';
		$authProvider.tokenName = 'token';
		$authProvider.tokenPrefix = '';


		$urlRouterProvider.otherwise('form');

		$stateProvider.state('login', {
			url: '/form',
			templateUrl: 'app/js/login/views/login.html',
			controller: 'LoginCtrl',
			controllerAs: '$ctrl'
		})
			.state('register', {
				url: '/register',
				templateUrl: 'app/js/login/views/register.html',
				controller: 'RegisterCtrl',
				controllerAs: '$ctrl'

			})
			.state('restore', {
				url: '/restore',
				templateUrl: 'app/js/login/views/restore.html',
				controller: 'RestoreCtrl',
				controllerAs: '$ctrl'

			})
			.state('reset-password', {
				url: '/reset-password',
				templateUrl: 'app/js/login/views/reset-password.html',
				controller: 'ResetPasswordCtrl',
				controllerAs: '$ctrl'

			})
			.state('terms-and-conditions', {
				url: '/terms-and-condition',
				templateUrl: 'app/js/login/views/terms-and-conditions.html',
			})


	}




	service.$inject = ['$http','config'];

	function service( $http, config ){

		return {
			getAll : getAll,
			get : get,
			create : create,
			update : update,
			delete : deleteItem,
			getPermissions : getPermissions
		};

		/**get all agencies
		 * @param params object, search object
		 * */
		function getAll( name,params ){
			return $http({
				url : config.apiURL + name,
				method : 'GET',
				params : params
			})
				.then(function (response) {
					return response;
				})
				.catch(function (response) {
					throw response;
				})
		}


		/**get single agency
		 *@param id int, the agency id ref
		 * */
		function get( name,id ){
			return $http.get( config.apiURL + name +'/'+ id )
				.then(function (response) {
					return response;
				})
				.catch(function (response) {
					throw response;
				});
		}


		/**Create new agency
		 * @param data, object
		 * @return promise
		 * */
		function create( name,data ){
			return $http({
				url : config.apiURL + name,
				method : 'POST',
				data : data
			})
				.then(function (response) {
					return response;
				})
				.catch(function (response) {
					throw response;
				})
		}


		/**Update agency
		 * @param data, object
		 * */
		function update( name,data ){
			return $http({
				url : config.apiURL + name +'/'+ data.id,
				method : 'PUT',
				data : data
			})
				.then(function (response) {
					return response;
				})
				.catch(function (response) {
					throw response;
				})
		}


		/**Delete agency
		 * @param id int
		 * */
		function deleteItem( name,id ){
			return $http.delete( config.apiURL + name + '/' + id)
				.then(function (response) {
					return response;
				})
				.catch(function (response) {
					throw response;
				})
		}


		/**Get all role's permissions
		 * */
		function getPermissions( id ){

			return $http.get( config.apiURL + 'role/' + id + '/permissions')
				.then(function (response) {
					// console.log( response, id );
					return response;
				})
				.catch(function (response) {
					throw response;
				});
		}

	}

})();