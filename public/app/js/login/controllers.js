(function(){

    'use strict';

    angular
        .module('login')
	    .controller('MainCtrl',mainCtrl)
	    .controller('RegisterCtrl',registerCtrl)
	    .controller('RestoreCtrl',restoreCtrl)
	    .controller('ResetPasswordCtrl',resetCtrl)
        .controller('LoginCtrl', loginCtrl);



	/**Main controller
	 * */
	function mainCtrl(){

	}

	/**Login controller
	 * */
	loginCtrl.$inject = [
		'toaster',
		'$filter',
		'$auth',
		'$log',
		'$window'
	];

	function loginCtrl(
    	toaster,
	    $filter,
	    $auth,
	    $log,
	    $window
	){

        var vm = this;
        vm.login = login;

		return vm;

	    function login(){

            var credentials = {
                email: vm.email,
                password: vm.password
            };

            // Use Satellizer's $auth service to login
            $auth.login(credentials)
                .then(function(data) {
	                $log.info(data);
	                $window.location.href = BASE_URL;
					return false;
                })
                .catch(function(data){
                    $log.error(data);
                    toaster.pop({
                        type: 'error',
                        title: $filter('translate')('login_error'),
                        body: $filter('translate')('user_password_mismatch')
                    });
	                return false;
                });
        }

    }


    /**Register Controller
     * Register a new user or request a new user to be created
     * */
    function registerCtrl() {

    	var vm = this;

	    vm.register = register;

	    return vm;

	    function register(){

	    }
    }


    function restoreCtrl(
    	$log,
	    $http

    ){

    	var vm = this;
	    vm.submit = submit;

	    /**Send restore form
	     * */
	    function submit(){

	    	$log.info(vm);
	    	$http({
			    url:'restore',
			    data:{
			    	email:vm.email
			    },
			    method:'POST'
			})
			    .then(function (response) {
				    $log.info(response);
			    })
			    .catch(function (response) {
				    $log.error(response);
				    toaster.pop({
					    type: 'error',
					    title: $filter('translate')('restore_error'),
					    body: $filter('translate')(response.msg)
				    });
				    return false;
			    });

	    }
    }


    /**Reset password ctrl
     * */
    resetCtrl.$inject = [
    	'$log',
	    '$http'
    ];
    function resetCtrl(
    	$log,
	    $http
    ){

    	var vm = this;
    }




})();

