/**
 * INSPINIA - Responsive Admin Theme
 *
 */
function config($translateProvider) {

    $translateProvider.preferredLanguage('en');
    $translateProvider
        .translations('en', Lang);


}

angular
    .module('login')
    .config(config)